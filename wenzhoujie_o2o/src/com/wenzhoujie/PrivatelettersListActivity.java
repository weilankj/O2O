package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.PrivateLettersAdapter;
import com.wenzhoujie.adapter.PrivateLettersSysAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.PrivateLettersActivityMsg_listModel;
import com.wenzhoujie.model.PrivateLettersActivitySys_MsgsModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.PrivateLettersActivityModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class PrivatelettersListActivity extends BaseActivity
{

	public static final int REQUEST_CODE_SEND_LETTER = 1;

	@ViewInject(id = R.id.act_privateletters_list_ptrlv_all)
	private PullToRefreshScrollView mPtrlvAll = null;

	@ViewInject(id = R.id.act_privateletters_list_ll_MessageLinearAll)
	private LinearLayout mLlMessagelinearall = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();
	private List<PrivateLettersActivityMsg_listModel> mMsg_listModel = new ArrayList<PrivateLettersActivityMsg_listModel>();
	private List<PrivateLettersActivitySys_MsgsModel> mSys_MsgsModel = new ArrayList<PrivateLettersActivitySys_MsgsModel>();
	private PrivateLettersAdapter mAdapter = null;
	private PrivateLettersSysAdapter mSysAdapter = null;

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_privateletters_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullToRefreshScrollView();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				Intent intent = new Intent(PrivatelettersListActivity.this, SendlettersActivity.class);
				startActivityForResult(intent, REQUEST_CODE_SEND_LETTER);
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("我的信件");
		mTitleSimple.setRightImage(R.drawable.ic_write);
	}

	private void bindDefaultData()
	{
		mAdapter = new PrivateLettersAdapter(mMsg_listModel, this);
		mSysAdapter = new PrivateLettersSysAdapter(mSys_MsgsModel, this);
	}

	private void initPullToRefreshScrollView()
	{
		mPtrlvAll.setMode(Mode.BOTH);
		mPtrlvAll.setOnRefreshListener(new OnRefreshListener2<ScrollView>()
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView)
			{
				mCurPage = 1;
				requestPrivateLetters(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView)
			{

				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					mPtrlvAll.onRefreshComplete();
					SDToast.showToast("没有更多数据了");
				} else
				{
					requestPrivateLetters(true);
				}

			}
		});
		mPtrlvAll.setRefreshing();
	}

	private void requestPrivateLetters(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "message");
		model.put("email", mSearcher.getEmail());
		model.put("pwd", mSearcher.getPwd());
		model.put("page", mCurPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				PrivateLettersActivityModel model = JsonUtil.json2Object(responseInfo.result,
						PrivateLettersActivityModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mCurPage = model.getPage().getPage();
							mTotalPage = model.getPage().getPage_total();
						} else
						{
							SDToast.showToast("未收到任何私信");
						}

						bindContentData(model.getMsg_list(), model.getSys_msgs(), isLoadMore);

						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mPtrlvAll.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	private void bindContentData(List<PrivateLettersActivityMsg_listModel> listModel,
			List<PrivateLettersActivitySys_MsgsModel> listSysModel, boolean isLoadMore)
	{
		if (!isLoadMore)
		{
			mLlMessagelinearall.removeAllViews();
		}

		if (listModel != null && listModel.size() > 0)
		{
			mAdapter.updateListViewData(listModel);
			for (int i = 0; i < listModel.size(); i++)
			{
				mLlMessagelinearall.addView(mAdapter.getView(i, null, null));
			}
		}
		if (listSysModel != null && listSysModel.size() > 0)
		{
			mSysAdapter.updateListViewData(listSysModel);
			for (int i = 0; i < listSysModel.size(); i++)
			{
				mLlMessagelinearall.addView(mSysAdapter.getView(i, null, null));
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case REQUEST_CODE_SEND_LETTER:
			switch (resultCode)
			{
			case SendlettersActivity.RESULT_CODE_SEND_SUCCESS:
				mPtrlvAll.setRefreshing();
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}