package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.MyFollowsAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.MyFollowsActivityItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.MyFollowsActivityModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class UserFansAndMyFollowsActivity extends BaseActivity
{
	/** act */
	public static final String EXTRA_ACT = "EXTRA_ACT";
	/** type */
	public static final String EXTRA_TYPE = "EXTRA_TYPE";
	/** uid */
	public static final String EXTRA_UID = "EXTRA_UID";

	@ViewInject(id = R.id.act_my_follows_lv_list)
	private PullToRefreshListView mLvList = null;

	@ViewInject(id = R.id.act_my_follows_iv_empty)
	private ImageView mIvEmpty = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();
	private List<MyFollowsActivityItemModel> mListMyFollowsItemModel = new ArrayList<MyFollowsActivityItemModel>();
	private MyFollowsAdapter mAdapter = null;

	private int mCurPage = 1;
	private int mTotalPage = -1;
	private String act = null;
	private boolean type = false;
	private String uid = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_follows);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		initTitle();
		bindDefaultLvData();
		initPullRefreshLv();
	}

	private void initIntentData()
	{
		Intent intent = getIntent();
		act = intent.getStringExtra(EXTRA_ACT);

		if (intent.hasExtra(EXTRA_TYPE))
		{
			type = intent.getBooleanExtra(EXTRA_TYPE, false);
		}
		if (intent.hasExtra(EXTRA_UID))
		{
			uid = intent.getStringExtra(EXTRA_UID);
		}
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

		if (act.equals("followlist"))
		{
			mTitleSimple.setTitleTop("我关注的");
		} else if (act.equals("fanslist"))
		{
			mTitleSimple.setTitleTop("fans");
		}

	}

	private void bindDefaultLvData()
	{
		mAdapter = new MyFollowsAdapter(mListMyFollowsItemModel, UserFansAndMyFollowsActivity.this, mSearcher, type);
		mLvList.setAdapter(mAdapter);
	}

	private void initPullRefreshLv()
	{
		mLvList.setMode(Mode.BOTH);
		mLvList.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mSearcher.getPageModel().setPage(1);
				requestData(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{

				mCurPage++;
				if (mCurPage > mTotalPage)
				{
					SDToast.showToast("没有更多数据了");
					mLvList.onRefreshComplete();
				} else
				{
					mSearcher.getPageModel().setPage(mCurPage);
					requestData(true);
				}
			}
		});
		mLvList.setRefreshing();
	}

	private void requestData(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", act);
		model.put("uid", uid);
		model.put("email", mSearcher.getEmail());
		model.put("pwd", mSearcher.getPwd());
		model.put("page", mSearcher.getPageModel().getPage());
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				MyFollowsActivityModel actModel = JsonUtil.json2Object(responseInfo.result, MyFollowsActivityModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{

					if (actModel.getPage() != null)
					{
						mCurPage = actModel.getPage().getPage();
						mTotalPage = actModel.getPage().getPage_total();
					}
					if (actModel.getItem() != null)
					{
						if (!isLoadMore)
						{
							mListMyFollowsItemModel.clear();
						}
						mListMyFollowsItemModel.addAll(actModel.getItem());
						mAdapter.updateListViewData(mListMyFollowsItemModel);
					} else
					{
						if (!isLoadMore)
						{
							SDToast.showToast("未找到数据");
							mAdapter.updateListViewData(null);

						} else
						{
							SDToast.showToast("未找到更多数据");
						}

						Toast.makeText(UserFansAndMyFollowsActivity.this, "用户暂未分享任何商品", Toast.LENGTH_SHORT).show();

					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mLvList.onRefreshComplete();
				SDViewUtil.toggleEmptyMsgByList(mListMyFollowsItemModel, mIvEmpty);
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}