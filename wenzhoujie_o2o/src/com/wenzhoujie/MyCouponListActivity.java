package com.wenzhoujie;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.wenzhoujie.library.customview.SDTabItemCorner;
import com.wenzhoujie.library.customview.SDViewAttr;
import com.wenzhoujie.library.customview.SDViewBase.EnumTabPosition;
import com.wenzhoujie.library.customview.SDViewNavigatorManager;
import com.wenzhoujie.library.customview.SDViewNavigatorManager.SDViewNavigatorManagerListener;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.MyCouponListFragment;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 我的团购券
 * 
 * @author js02
 * 
 */
public class MyCouponListActivity extends BaseActivity implements OnClickListener
{
	@ViewInject(id = R.id.act_my_coupon_list_fl_container_unused)
	private FrameLayout mFlUnused = null;

	@ViewInject(id = R.id.act_my_coupon_list_fl_container_will_overdue)
	private FrameLayout mFlWillOverdue = null;

	@ViewInject(id = R.id.act_my_coupon_list_fl_container_overdue)
	private FrameLayout mFlOverdue = null;

	@ViewInject(id = R.id.act_my_coupon_list_fl_container_all)
	private FrameLayout mFlAll = null;

	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();

	/** 1: 快过期 2:可以使用 3:失败 0:所有 */
	private String mStrStatus = null;

	private MyCouponListFragment mFragUnused = null;
	private MyCouponListFragment mFragWillOverdue = null;
	private MyCouponListFragment mFragOverdue = null;
	private MyCouponListFragment mFragAll = null;

	private MyCouponListFragment mFragLastVisible = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_coupon_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		registeClick();
		initTitle();
		initTabs();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{

			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

	}

	private void initTabs()
	{
		SDTabItemCorner tabUnused = new SDTabItemCorner(this);
		tabUnused.getmAttr().setmBackgroundColorNormalResId(R.color.bg_title_bar);
		tabUnused.getmAttr().setmBackgroundColorSelectedResId(R.color.white);
		tabUnused.getmAttr().setmTextColorNormalResId(R.color.white);
		tabUnused.getmAttr().setmTextColorSelectedResId(R.color.bg_title_bar);
		tabUnused.getmAttr().setmStrokeColorResId(R.color.white);
		tabUnused.getmAttr().setmStrokeWidth(SDViewUtil.dp2px(1));

		tabUnused.setTabName("未使用");
		tabUnused.setTabTextSizeSp(14);
		tabUnused.setmPosition(EnumTabPosition.FIRST);

		SDTabItemCorner tabWillOverdue = new SDTabItemCorner(this);
		tabWillOverdue.setmAttr((SDViewAttr) tabUnused.getmAttr().clone());
		tabWillOverdue.setTabName("即将过期");
		tabWillOverdue.setTabTextSizeSp(14);
		tabWillOverdue.setmPosition(EnumTabPosition.MIDDLE);

		SDTabItemCorner tabOverdue = new SDTabItemCorner(this);
		tabOverdue.setmAttr((SDViewAttr) tabUnused.getmAttr().clone());
		tabOverdue.setTabName("已失效");
		tabOverdue.setTabTextSizeSp(14);
		tabOverdue.setmPosition(EnumTabPosition.MIDDLE);

		SDTabItemCorner tabAll = new SDTabItemCorner(this);
		tabAll.setmAttr((SDViewAttr) tabUnused.getmAttr().clone());
		tabAll.setTabName("全部");
		tabAll.setTabTextSizeSp(14);
		tabAll.setmPosition(EnumTabPosition.LAST);

		mTitleSimple.mTlMiddle.removeAllViews();
		mTitleSimple.mTlMiddle.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);

		mTitleSimple.mTlMiddle.addView(tabUnused, params);
		mTitleSimple.mTlMiddle.addView(tabWillOverdue, new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.2f));
		mTitleSimple.mTlMiddle.addView(tabOverdue, params);
		mTitleSimple.mTlMiddle.addView(tabAll, params);

		SDViewUtil.setViewHeight(mTitleSimple.mTlMiddle, SDViewUtil.dp2px(35));
		SDViewUtil.setViewWidth(mTitleSimple.mTlMiddle, SDViewUtil.dp2px(250));

		mViewManager.setItems(tabUnused, tabWillOverdue, tabOverdue, tabAll);
		mViewManager.setmListener(new SDViewNavigatorManagerListener()
		{
			@Override
			public void onItemClick(View v, int index)
			{
				switch (index)
				{
				case 0: // 未使用
					mStrStatus = "2";
					selectUnused();
					break;
				case 1: // 即将过期
					mStrStatus = "1";
					selectWillOverdue();
					break;
				case 2: // 已失效
					mStrStatus = "3";
					selectOverdue();
					break;
				case 3: // 全部
					mStrStatus = "0";
					selectAll();
					break;
				default:
					break;
				}
			}
		});
		mViewManager.setSelectIndex(0, tabUnused, true);
	}

	protected void selectUnused()
	{
		if (mFragUnused == null)
		{
			mFragUnused = new MyCouponListFragment();
			mFragUnused.setmStrStatus(mStrStatus);
			replaceFragment(mFragUnused, R.id.act_my_coupon_list_fl_container_unused);
		}
		toggleFragment(mFragUnused);
	}

	protected void selectWillOverdue()
	{
		if (mFragWillOverdue == null)
		{
			mFragWillOverdue = new MyCouponListFragment();
			mFragWillOverdue.setmStrStatus(mStrStatus);
			replaceFragment(mFragWillOverdue, R.id.act_my_coupon_list_fl_container_will_overdue);
		}
		toggleFragment(mFragWillOverdue);
	}

	protected void selectOverdue()
	{
		if (mFragOverdue == null)
		{
			mFragOverdue = new MyCouponListFragment();
			mFragOverdue.setmStrStatus(mStrStatus);
			replaceFragment(mFragOverdue, R.id.act_my_coupon_list_fl_container_overdue);
		}
		toggleFragment(mFragOverdue);
	}

	protected void selectAll()
	{
		if (mFragAll == null)
		{
			mFragAll = new MyCouponListFragment();
			mFragAll.setmStrStatus(mStrStatus);
			replaceFragment(mFragAll, R.id.act_my_coupon_list_fl_container_all);
		}
		toggleFragment(mFragAll);
	}

	private void toggleFragment(MyCouponListFragment fragment)
	{
		if (mFragLastVisible != null)
		{
			hideFragment(mFragLastVisible);
		}
		showFragment(fragment);
		mFragLastVisible = fragment;
	}

	private void registeClick()
	{

	}

	@Override
	public void onClick(View v)
	{
	}

}