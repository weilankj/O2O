package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.NearbyVipAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.NearbyuserActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.NearbyuserActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.work.AppRuntimeWorker;

public class NearbyVipActivity extends BaseActivity
{
	public static final int REQUEST_CODE_LOGIN_FOR_FOLLOW_FANS = 1;

	@ViewInject(id = R.id.act_nearby_vip_lv_vip)
	private PullToRefreshListView mLvVip = null;

	private List<NearbyuserActItemModel> mlistModel = new ArrayList<NearbyuserActItemModel>();
	private NearbyVipAdapter mAdapter = null;

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_nearby_vip);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullListView();
		startLocation();
	}

	private void bindDefaultData()
	{
		mAdapter = new NearbyVipAdapter(mlistModel, this);
		mLvVip.setAdapter(mAdapter);
		mLvVip.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				NearbyuserActItemModel model = mAdapter.getItem((int) id);
				if (model != null && !TextUtils.isEmpty(model.getUid()))
				{
					Intent intent = new Intent(getApplicationContext(), UserCenterActivity.class);
					intent.putExtra(UserCenterActivity.EXTRA_UID_ID, model.getUid());
					startActivity(intent);
				}
			}

		});
	}

	/**
	 * 开始定位
	 */
	private void startLocation()
	{
		AppHelper.showLoadingDialog("定位中");
		BaiduMapManager.getInstance().startLocation(new BDLocationListener()
		{
			@Override
			public void onReceivePoi(BDLocation arg0)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onReceiveLocation(BDLocation location)
			{
				if (location != null) // 定位成功
				{
					requestNearByVip(false);
					BaiduMapManager.getInstance().destoryLocation();
				} else
				{
					SDToast.showToast("定位失败无法获取附近的会员...");
					AppHelper.hideLoadingDialog();
				}
			}
		});
	}

	protected void requestNearByVip(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "nearbyuser");
		model.put("city_id", AppRuntimeWorker.getCity_id());
		model.putUser();
		model.putLocation();
		model.put("page", mCurPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请求数据中");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				NearbyuserActModel model = JsonUtil.json2Object(responseInfo.result, NearbyuserActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mCurPage = model.getPage().getPage();
							mTotalPage = model.getPage().getPage_total();
						}
						if (!isLoadMore)
						{
							mlistModel.clear();
						}
						if (model.getItem() != null && !model.getItem().isEmpty())
						{
							mlistModel.addAll(model.getItem());
						} else
						{
							SDToast.showToast("未找到数据");
						}
						mAdapter.updateListViewData(mlistModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mLvVip.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void initPullListView()
	{
		mLvVip.setMode(Mode.BOTH);
		mLvVip.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestNearByVip(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					SDToast.showToast("没有更多数据了");
					mLvVip.onRefreshComplete();
				} else
				{
					requestNearByVip(true);
				}
			}
		});
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("附近的会员");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case REQUEST_CODE_LOGIN_FOR_FOLLOW_FANS:
			if (resultCode == LoginNewActivity.RESULT_CODE_LOGIN_SUCCESS)
			{
				mLvVip.setRefreshing();
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}