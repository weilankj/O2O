package com.wenzhoujie;

import android.os.Bundle;

import com.lingou.www.R;
import com.wenzhoujie.fragment.MerchantListFragment;

/**
 * 商家列表
 * 
 * @author js02
 * 
 */
public class MerchantListActivity extends BaseActivity
{

	private MerchantListFragment mFragMerchant;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_ecshop_list);
		init();

	}

	private void init()
	{
		mFragMerchant = new MerchantListFragment();
		replaceFragment(mFragMerchant, R.id.act_echsop_fl_fragment_content);
	}

}