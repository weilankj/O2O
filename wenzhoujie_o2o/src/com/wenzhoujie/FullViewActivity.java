package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class FullViewActivity extends BaseActivity implements OnClickListener
{

	/** url */
	public static final String EXTRA_URL = "extra_url";

	@ViewInject(id = R.id.act_full_view_ll_detailsLinear)
	private LinearLayout mLlDetailslinear = null;

	@ViewInject(id = R.id.act_full_view_iv_detailsImage)
	private ImageView mIvDetailsimage = null;

	private String url;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_full_view);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		full();
		registeClick();

	}

	private void full()
	{
		SDViewBinder.setImageView(mIvDetailsimage, url);
	}

	/**
	 * 从intent获取数据
	 */
	private void initIntentData()
	{
		Intent intent = getIntent();
		if (intent.getStringExtra(EXTRA_URL) != null) // 获取小分类id
		{
			url = intent.getStringExtra(EXTRA_URL);
		}
	}

	private void registeClick()
	{
		mLlDetailslinear.setOnClickListener(this);
		mIvDetailsimage.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_full_view_ll_detailsLinear:
		case R.id.act_full_view_iv_detailsImage:

			finish();

			break;

		default:
			break;
		}
	}

}