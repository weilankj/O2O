package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.NewsListAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.Article_listActModel;
import com.wenzhoujie.model.Article_listListModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 公告列表
 * 
 * @author js02
 * 
 */
public class NewsListActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_news_list_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	private List<Article_listListModel> mListModel = new ArrayList<Article_listListModel>();
	private NewsListAdapter mAdapter = null;

	private int pageTotal;
	private int page;
	private String cate_id;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_news_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void initPullToRefreshListView()
	{
		mPtrlvContent.setMode(Mode.BOTH);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page = 1;
				requestData(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page++;
				if (page > pageTotal && pageTotal > 0)
				{
					SDToast.showToast("没有更多数据了");
					mPtrlvContent.onRefreshComplete();
				} else
				{
					requestData(true);
				}
			}
		});

		mPtrlvContent.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				Article_listListModel model = mAdapter.getItem((int) id);
				if (model != null && model.getId() != null)
				{
					Intent intent = new Intent(getApplicationContext(), NewsDetailActivity.class);
					intent.putExtra(NewsDetailActivity.EXTRA_NEWS_ID, model.getId());
					startActivity(intent);
				}

			}
		});

		mPtrlvContent.setRefreshing();

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("公告列表");
	}

	private void bindDefaultData()
	{
		mAdapter = new NewsListAdapter(mListModel, this);
		mPtrlvContent.setAdapter(mAdapter);
	}

	private void requestData(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "article_list");
		model.put("page", page);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				Article_listActModel model = JsonUtil.json2Object(responseInfo.result, Article_listActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					if (model.getResponse_code() == 1)
					{
						if (model.getPage() != null)
						{
							page = model.getPage().getPage();
							pageTotal = model.getPage().getPage_total();
						}
						SDViewUtil.updateAdapterByList(mListModel, model.getList(), mAdapter, isLoadMore);
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mPtrlvContent.onRefreshComplete();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}