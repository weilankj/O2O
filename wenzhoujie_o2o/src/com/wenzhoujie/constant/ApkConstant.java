package com.wenzhoujie.constant;

public class ApkConstant {

	public static final boolean DEBUG = true;

	public static final String SERVER_API_URL_PRE = "http://";
	// public static final String SERVER_API_URL_MID = "o2o.fanwe.net";
	// public static final String SERVER_API_URL_MID = "test.hzsozo.com";
	// public static final String SERVER_API_URL_MID = "www.wenzhoujie.com";
	public static final String SERVER_API_URL_MID = "www.52lingou.com";
	// public static final String SERVER_API_URL_MID = "devo2o.fanwe.net";
	public static final String SERVER_API_URL_END = "/sjmapi/index.php";
	// public static final String SERVER_API_URL_END = "/mapi/index.php";

	public static final String SERVER_API_URL = SERVER_API_URL_PRE
			+ SERVER_API_URL_MID + SERVER_API_URL_END;

	public static final String SERVER_URL = SERVER_API_URL_PRE
			+ SERVER_API_URL_MID;

	public static final class SoftType {
		public static final String O2O = "o2o";
		public static final String P2P = "p2p";
	}

	public static final class DeviceType {
		public static final String DEVICE_ANDROID = "android";
	}

	// 微信支付
	public static final String WX_APP_ID = "";
	public static final String WX_APP_SECRET = "";
	public static final String WX_PARTNER_ID = "";
	public static final String WX_NOTIFY_URL = "";
	public static final String WX_APP_KEY = "";
}
