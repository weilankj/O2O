package com.wenzhoujie.constant;

import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.lingou.www.R;

public class Constant
{

	public static final String DOWN_LOAD_DIR_NAME = "wenzhoujie";

	public static final String EARN_SUB_CHAR = "市";

	public static final class PaymentType
	{
		public static final String MALIPAY = "malipay";
		public static final String MCOD = "mcod";
		public static final String WALIPAY = "walipay";
		public static final String WTENPAY = "wtenpay";
		//增加微信支付
		public static final String WXPAY = "wxapp";
	}

	public static final class PaymentTypeString
	{
		public static final String MALIPAY = "支付宝/各银行 支付";
		public static final String MCOD = "货到付款";
		public static final String WALIPAY = "支付宝支付";
		public static final String WTENPAY = "财付通支付";
		//增加微信支付
		public static final String WXPAY = "微信支付";
	}

	public static final class WaterFall
	{
		public final static int COLUMN_COUNT = 3; // 显示列数
		public final static int PICTURE_COUNT_PER_LOAD = 10; // 每次加载10张图片
		public final static int PICTURE_TOTAL_COUNT = 10000; // 允许加载的最多图片数
		public final static int HANDLER_WHAT = 1;
		public final static int MESSAGE_DELAY = 200;
	}

	public static final class LoadImageType
	{
		/** 任何网络下都加载图片 */
		public static final int ALL = 1;
		/** wifi网络下加载图片,移动网络下不允许加载图片 */
		public static final int ONLY_WIFI = 0;
	}
	
	public static final class canJPush
	{
		/** 允许推送 */
		public static final int YES = 1;
		/** 不能推送 */
		public static final int NO = 0;
	}

	public static final class PushType
	{
		public static final int NORMAL = 1;
		public static final int PROJECT_ID = 2;
		public static final int ARTICLE_ID = 3;
		public static final int URL = 4;
		public static final int TUANDETAIL_ID = 5;
		public static final int ECSHOPDETAIL_ID = 6;
		public static final int EVENTSDETAIL_ID = 7;
		public static final int FAVORABLEDETAIL_ID = 8;
		public static final int MERCHANTDETAIL_ID = 9;
		public static final int VOUCHERDETAIL_ID = 10;
	}

	public enum TitleType
	{
		TITLE_NONE, TITLE_SIMPLE, TITLE_TWO_RIGHT_BUTTON;
	}

	public static final class SearchTypeMap
	{
		/** 全部 */
		public static final int ALL = -1;
		/** 优惠券 */
		public static final int YOU_HUI = 0;
		/** 活动 */
		public static final int EVENT = 1;
		/** 团购 */
		public static final int TUAN = 2;
		/** 商家 */
		public static final int MERCHANT = 4;
	}

	public static final class SearchTypeNormal
	{
		/** 优惠券 */
		public static final int YOU_HUI = 0;
		/** 团购 */
		public static final int TUAN = 2;
		/** 商家 */
		public static final int MERCHANT = 3;
		/** 活动 */
		public static final int EVENT = 4;
		/** 商城 */
		public static final int SHOP = 5;
	}

	public static final class SearchTypeNormalString
	{
		public static final String YOU_HUI = SDResourcesUtil.getString(R.string.youhui_coupon);
		public static final String TUAN = SDResourcesUtil.getString(R.string.tuan_gou);
		public static final String MERCHANT = SDResourcesUtil.getString(R.string.store);
		public static final String EVENT = SDResourcesUtil.getString(R.string.event);
		public static final String SHOP = SDResourcesUtil.getString(R.string.goods);
	}

	public static final class CategoryOrderTypeValue
	{
		public static final String DEFAULT = "default";
		public static final String AVG_POINT = "avg_point";
		public static final String NEARBY = "nearby";
		public static final String NEWEST = "newest";
		public static final String BUY_COUNT = "buy_count";
		public static final String PRICE_ASC = "price_asc";
		public static final String PRICE_DESC = "price_desc";
	}

	public static final class CategoryOrderTypeName
	{
		public static final String DEFAULT = "智能排序";
		public static final String AVG_POINT = "评价最高";
		public static final String NEARBY = "离我最近";
		public static final String NEWEST = "最新发布";
		public static final String BUY_COUNT = "人气最高";
		public static final String PRICE_ASC = "价格最低";
		public static final String PRICE_DESC = "价格最高";
	}

}
