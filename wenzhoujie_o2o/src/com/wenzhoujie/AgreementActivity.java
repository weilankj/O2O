package com.wenzhoujie;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.BaseFragment;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.utils.IocUtil;

public class AgreementActivity extends BaseActivity{
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.agreement_activity);
		IocUtil.initInjectedView(this);
		initTitle();
	}
	
	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				//startRegisterActivity();
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("用户协议");
		//mTitleSimple.setRightText("注册");
		mTitleSimple.setRightText("");
	}
}
