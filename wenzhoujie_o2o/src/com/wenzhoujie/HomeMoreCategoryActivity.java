package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.lingou.www.R;
import com.wenzhoujie.adapter.HomeIndexAdapter;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.model.IndexActIndexsModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 更多首页分类activity
 * 
 * @author song
 * 
 */
public class HomeMoreCategoryActivity extends BaseActivity {

	public static final String EXTRA_INITACTCITYLISTMODEL = "extra_initactcitylistmodel";

	@ViewInject(id = R.id.act_home_more_category_gv_cates)
	private GridView mGvCates = null;

	private HomeIndexAdapter mAdapter = null;
	private List<IndexActIndexsModel> mListModel = new ArrayList<IndexActIndexsModel>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_home_more_category);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init() {
		initIntentData();
		initTitle();
		bindData();
	}

	private void bindData() {
		mAdapter = new HomeIndexAdapter(mListModel, this);
		mGvCates.setAdapter(mAdapter);
		mGvCates.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				IndexActIndexsModel model = mAdapter.getItem((int) id);
				clickIndex(model);
			}
		});
	}

	protected void clickIndex(IndexActIndexsModel model) {
		if (model != null) {
			int type = SDTypeParseUtil.getIntFromString(model.getType(), -1);
			Intent intent = IndexActNewModel.createIntentByType(type,
					model.getData(), true);
			if (intent != null) {
				startActivity(intent);
			}
		}
	}

	private void initIntentData() {
		List<IndexActIndexsModel> listModel = (List<IndexActIndexsModel>) getIntent()
				.getSerializableExtra(EXTRA_INITACTCITYLISTMODEL);
		if (listModel != null) {
			mListModel = listModel;
		}
	}

	private void initTitle() {
		mTitleSimple.setmListener(new SDTitleSimpleListener() {

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v) {
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v) {
				finish();
			}
		});
		mTitleSimple.setTitleTop("更多分类");
	}

}
