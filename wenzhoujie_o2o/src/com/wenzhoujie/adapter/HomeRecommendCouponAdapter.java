package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.YouHuiDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.IndexActYouhui_listModel;
import com.wenzhoujie.utils.ViewHolder;

public class HomeRecommendCouponAdapter extends SDBaseAdapter<IndexActYouhui_listModel>
{
	public HomeRecommendCouponAdapter(List<IndexActYouhui_listModel> listModel, Activity activity)
	{

		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_coupon, null);
		}
		View viewDiv = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_view_div);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_iv_image);
		ImageView ivDoNotNeedOrder = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_iv_do_not_need_order);
		ImageView ivIsNew = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_iv_is_new);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_name);
		TextView tvTip = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_tip);
		TextView tvAddress = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_address);
		TextView tvPublishTime = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_publish_time);
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_distance);
		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		final IndexActYouhui_listModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getMerchant_logo());
			SDViewBinder.setTextView(tvName, model.getTitle());
			SDViewBinder.setTextView(tvTip, model.getContent());
			SDViewBinder.setTextView(tvAddress, model.getApi_address());
			SDViewBinder.setTextView(tvPublishTime, model.getBegin_time());
			SDViewBinder.setTextView(tvDistance, model.getDistance_format());

			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != null)
					{
						Intent intent = new Intent(App.getApplication(), YouHuiDetailActivity.class);
						intent.putExtra(YouHuiDetailActivity.EXTRA_YOUHUI_ID, model.getId());
						mActivity.startActivity(intent);
					}
				}
			});
		}
		return convertView;
	}

}