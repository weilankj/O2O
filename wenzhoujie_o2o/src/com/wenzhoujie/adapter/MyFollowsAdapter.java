package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.SendlettersActivity;
import com.wenzhoujie.UserCenterActivity;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.MyFollowsActivityItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.utils.ViewHolder;

public class MyFollowsAdapter extends SDBaseAdapter<MyFollowsActivityItemModel>
{

	private SearchConditionModel mSearcher;
	private Activity activity;
	private boolean type;
	private boolean a;

	public MyFollowsAdapter(List<MyFollowsActivityItemModel> listModel, Activity activity, SearchConditionModel mSearcher, boolean type)
	{
		super(listModel, activity);
		this.mSearcher = mSearcher;
		this.activity = activity;
		this.type = type;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_my_follows, null);
		}

		ImageView ivAvatar = ViewHolder.get(convertView, R.id.item_my_follows_iv_avatar);
		TextView tvName = ViewHolder.get(convertView, R.id.item_my_follows_tv_name);
		TextView tvCount = ViewHolder.get(convertView, R.id.item_my_follows_tv_count);
		// TextView tvIntroduce = ViewHolder.get(convertView,
		// R.id.item_my_follows_tv_introduce);
		final Button btnFollow = ViewHolder.get(convertView, R.id.item_my_follows_btn_follow);
		final CheckBox cbCb = ViewHolder.get(convertView, R.id.item_my_follows_cb_cb);

		final MyFollowsActivityItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivAvatar, model.getUser_avatar());

			ivAvatar.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent();
					intent.setClass(activity, UserCenterActivity.class);
					intent.putExtra(UserCenterActivity.EXTRA_UID_ID, model.getUid());
					activity.startActivity(intent);
				}
			});

			SDViewBinder.setTextView(tvName, model.getUser_name());
			SDViewBinder.setTextView(tvCount, model.getFans());

			btnFollow.setTag(position);
			// 设置关注button的状态
			if (model.getIs_follow() == 0)
			{
				btnFollow.setBackgroundResource(R.drawable.guanzhu_uncollect);
				cbCb.setChecked(false);
			} else if (model.getIs_follow() == 1)
			{
				btnFollow.setBackgroundResource(R.drawable.guanzhu_collect);
				cbCb.setChecked(true);
			} else if (model.getIs_follow() == -1)
			{
				btnFollow.setBackgroundResource(R.drawable.guanzhu_me);
			}

			if (type)
			{
				if (btnFollow.getVisibility() != View.GONE)
				{
					btnFollow.setVisibility(View.GONE);
				}

				btnFollow.setOnClickListener(null);

				convertView.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						Intent intent = new Intent();
						intent.setClass(activity, SendlettersActivity.class);
						intent.putExtra(SendlettersActivity.EXTRA_FANS_NAME, model.getUser_name());
						activity.setResult(1, intent);
						activity.finish();
					}
				});

			} else
			{

				if (btnFollow.getVisibility() != View.VISIBLE)
				{
					btnFollow.setVisibility(View.VISIBLE);
				}

				convertView.setOnClickListener(null);

				btnFollow.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						if (model.getIs_follow() == -1)
						{
							Toast.makeText(activity, "不能关注你自己。", Toast.LENGTH_SHORT).show();
						} else
						{
							requestAddFollows(model.getUid(), btnFollow, cbCb);
						}

					}
				});
			}

		}

		return convertView;
	}

	/**
	 * 关注
	 */
	private void requestAddFollows(String uid, final Button btnFollow, final CheckBox cbCb)
	{

		RequestModel model = new RequestModel();
		model.put("act", "followuser");
		model.put("uid", uid);
		model.put("email", mSearcher.getEmail());
		model.put("pwd", mSearcher.getPwd());
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				JSONObject json = JSON.parseObject(responseInfo.result);
				a = (json.getIntValue("status") == 1) ? true : false;
				if (a)
				{
					if (cbCb.isChecked())
					{
						btnFollow.setBackgroundResource(R.drawable.guanzhu_uncollect);
						cbCb.setChecked(false);
					} else
					{
						btnFollow.setBackgroundResource(R.drawable.guanzhu_collect);
						cbCb.setChecked(true);
					}
					Toast.makeText(activity, "操作成功", Toast.LENGTH_SHORT).show();
				} else
				{
					Toast.makeText(activity, "操作失败", Toast.LENGTH_SHORT).show();
				}

			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{

			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}
