package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.UserCenterActivity;
import com.wenzhoujie.model.CommentlistItemModel;
import com.wenzhoujie.utils.SDMyImageGetterUtil;
import com.wenzhoujie.utils.ViewHolder;

public class ShareDetailCommentAdapter extends SDBaseAdapter<CommentlistItemModel>
{

	private Activity activity;

	public ShareDetailCommentAdapter(List<CommentlistItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_share_list_details_comments, null);
		}

		ImageView ivUserAvatar = ViewHolder.get(convertView, R.id.item_share_list_details_comments_iv_user_avatar);
		TextView tvName = ViewHolder.get(convertView, R.id.item_share_list_details_comments_tv_user_name);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_share_list_details_comments_tv_user_time);
		TextView tvContent = ViewHolder.get(convertView, R.id.item_share_list_details_comments_tv_user_content);

		final CommentlistItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivUserAvatar, model.getUser_avatar());
			ivUserAvatar.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Intent intent = new Intent();
					intent.setClass(activity, UserCenterActivity.class);
					intent.putExtra(UserCenterActivity.EXTRA_UID_ID, model.getUid());
					activity.startActivity(intent);
				}
			});

			SDViewBinder.setTextView(tvName, model.getUser_name());
			SDViewBinder.setTextView(tvTime, model.getTime());

			// 设置评论内容(表情替换)
			if (model.getParse_expres().size() != 0)
			{
				String b = model.getContent();
				for (int i1 = 0; i1 < model.getParse_expres().size(); i1++)
				{
					String c = "\\[\\w*" + model.getParse_expres().get(i1).getKey() + "\\]";// expresList.get(i).getKey().toString();
					// \[[衰]\]
					String g = "<img src='" + model.getParse_expres().get(i1).getValue() + "'/>";
					String d = b.replaceAll(c, g);
					b = d;

					Spanned text2 = Html.fromHtml(b, new SDMyImageGetterUtil(activity, tvContent), null);
					tvContent.setText(text2);
				}
			} else if (model.getParse_expres().size() == 0)
			{
				String text1 = model.getContent();
				tvContent.setText(text1);
			}
		}
		return convertView;
	}
}
