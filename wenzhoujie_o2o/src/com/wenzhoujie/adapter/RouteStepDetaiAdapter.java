package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.utils.ViewHolder;

public class RouteStepDetaiAdapter extends SDBaseAdapter<String>
{

	private int mSelectPos = 0;

	public RouteStepDetaiAdapter(List<String> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_route_step, null);
		}

		TextView tvIndex = ViewHolder.get(convertView, R.id.item_route_step_tv_index);
		TextView tvContent = ViewHolder.get(convertView, R.id.item_route_step_tv_content);

		tvIndex.setText(String.valueOf(position + 1));
		SDViewBinder.setTextView(tvContent, getItem(position));
		if (position == mSelectPos)
		{
			convertView.setBackgroundResource(R.drawable.layer_gray);
		} else
		{
			convertView.setBackgroundResource(R.drawable.layer_white);
		}

		return convertView;
	}

	public void setSelectPos(int pos)
	{
		this.mSelectPos = pos;
		notifyDataSetChanged();
	}

}
