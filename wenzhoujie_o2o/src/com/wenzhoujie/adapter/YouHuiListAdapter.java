package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.YouHuiDetailActivity;
import com.wenzhoujie.model.YouhuilistItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class YouHuiListAdapter extends SDBaseAdapter<YouhuilistItemModel>
{

	private Activity activity;

	public YouHuiListAdapter(List<YouhuilistItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_coupon, null);
		}
		View viewDiv = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_view_div);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_iv_image);
		ImageView ivDoNotNeedOrder = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_iv_do_not_need_order);
		ImageView ivIsNew = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_iv_is_new);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_name);
		TextView tvTip = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_tip);
		TextView tvAddress = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_address);
		TextView tvPublishTime = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_publish_time);
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_home_recommend_coupon_tv_distance);
		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		final YouhuilistItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getMerchant_logo());
			SDViewBinder.setTextView(tvName, model.getTitle());
			SDViewBinder.setTextView(tvDistance, model.getDistance_format());
			String content = model.getContent();
			if (!TextUtils.isEmpty(content))
			{
				tvTip.setText(Html.fromHtml(content));
			}
			SDViewBinder.setTextView(tvAddress, model.getAddress());
			SDViewBinder.setTextView(tvPublishTime, model.getBegin_time_format());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Intent intent = new Intent(activity, YouHuiDetailActivity.class);
					intent.putExtra(YouHuiDetailActivity.EXTRA_YOUHUI_ID, model.getId());
					activity.startActivity(intent);
				}
			});
		}
		return convertView;
	}

}
