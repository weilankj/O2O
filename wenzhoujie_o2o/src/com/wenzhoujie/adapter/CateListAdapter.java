package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.CateListActivity;
import com.wenzhoujie.ECshopListActivity;
import com.wenzhoujie.VoucherActivity;
import com.wenzhoujie.model.CateListActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class CateListAdapter extends SDBaseAdapter<CateListActItemModel>
{

	private List<CateListActItemModel> listModel;
	private int cate_type;
	private int type;
	private Activity activity;

	public CateListAdapter(List<CateListActItemModel> listModel, Activity activity, int cate_type, int type)
	{
		super(listModel, activity);
		this.listModel = listModel;
		this.cate_type = cate_type;
		this.type = type;
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.act_cate_list_item, null);
		}

		TextView tvName = ViewHolder.get(convertView, R.id.act_cate_list_item_tv_name);
		LinearLayout llbg = ViewHolder.get(convertView, R.id.act_cate_list_item);

		final CateListActItemModel model = getItem(position);
		if (model != null)
		{

			if (position == 0)
			{
				llbg.setBackgroundResource(R.drawable.layer_white_stroke_item_top);
			} else if (position == listModel.size() - 1)
			{
				llbg.setBackgroundResource(R.drawable.layer_white_stroke_item_single);
			} else
				llbg.setBackgroundResource(R.drawable.layer_white_stroke_item_single);
			SDViewBinder.setTextView(tvName, model.getName());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					if (model.getHas_child().equals("0"))
					{
						Intent intent = new Intent();

						switch (cate_type)
						// 0:代金券,团购分类,优惠,商家; 1:商品分类;2:活动分类
						{
						case 0:
							if (type == 1)// 1: 团购 2:优惠 3:代金劵 4:商家
							{
								activity.startActivity(new Intent(activity, com.wenzhoujie.TuanListActivity.class));
							} else if (type == 2)
							{
								// intent.setClass(activity,
								// FavorableListActivity.class);
								// intent.putExtra(YouHuiListActivity.EXTRA_CATE_NAME,
								// model.getName());
								// intent.putExtra(YouHuiListActivity.EXTRA_CATE_ID,
								// model.getId());
								// intent.putExtra(YouHuiListActivity.EXTRA_CATE_PID,
								// model.getPid());
								activity.setResult(0, intent);
							} else if (type == 3)
							{
								intent.putExtra(VoucherActivity.EXTRA_TITLE, model.getName());
								intent.putExtra(VoucherActivity.EXTRA_CATALOG_ID, model.getId());
								activity.startActivity(intent);
							} else if (type == 4)
							{
								activity.startActivity(new Intent(activity, com.wenzhoujie.MerchantListActivity.class));
							}
							activity.finish();
							break;
						case 1:
							activity.startActivity(new Intent(activity, ECshopListActivity.class));
							activity.finish();
							break;
						case 2:
							// intent.putExtra(EventsListActivity.EXTRA_TITLE,
							// model.getName());
							// intent.putExtra(EventsListActivity.EXTRA_CATALOG_ID,
							// model.getId());
							// intent.setClass(activity,
							// EventsListActivity.class);
							// activity.startActivity(intent);
							// activity.finish();
							break;

						default:
							break;
						}

					} else
					{
						Intent intent = new Intent();
						intent.setClass(activity, CateListActivity.class);
						intent.putExtra(CateListActivity.EXTRA_PID, model.getId());
						intent.putExtra(CateListActivity.EXTRA_CATE_TYPE, cate_type);
						intent.putExtra(CateListActivity.EXTRA_TYPE, type);
						activity.startActivity(intent);
					}

				}
			});
		}
		return convertView;
	}
}
