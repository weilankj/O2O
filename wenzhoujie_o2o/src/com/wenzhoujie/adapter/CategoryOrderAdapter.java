package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.customview.SDLvCategoryView.SDLvCategoryViewAdapterInterface;
import com.wenzhoujie.model.CategoryOrderModel;
import com.wenzhoujie.utils.ViewHolder;

public class CategoryOrderAdapter extends SDBaseAdapter<CategoryOrderModel> implements SDLvCategoryViewAdapterInterface
{

	public CategoryOrderAdapter(List<CategoryOrderModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_category_single, null);
		}
		TextView tvTitle = ViewHolder.get(convertView, R.id.item_category_single_tv_title);

		CategoryOrderModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvTitle, model.getName());
			if (model.isSelect())
			{
				convertView.setBackgroundColor(SDResourcesUtil.getColor(R.color.bg_gray_categoryview_item_select));
			} else
			{
				convertView.setBackgroundColor(SDResourcesUtil.getColor(R.color.white));
			}
		}
		return convertView;
	}

	@Override
	public void setPositionSelectState(int position, boolean select, boolean notify)
	{
		getItem(position).setSelect(select);
		if (notify)
		{
			notifyDataSetChanged();
		}
	}

	@Override
	public String getTitleNameFromPosition(int position)
	{
		return getItem(position).getName();
	}

	@Override
	public BaseAdapter getAdapter()
	{
		return this;
	}

	@Override
	public Object getSelectModelFromPosition(int position)
	{
		return getItem(position);
	}

	@Override
	public int getTitleIndex()
	{
		return -1;
	}

	@Override
	public int getItemCount()
	{
		return getCount();
	}

}
