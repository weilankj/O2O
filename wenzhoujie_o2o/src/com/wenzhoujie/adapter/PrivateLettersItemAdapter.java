package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.PrivateLettersItemActivityMsg_listModel;
import com.wenzhoujie.utils.SDMyImageGetterUtil;
import com.wenzhoujie.utils.ViewHolder;

public class PrivateLettersItemAdapter extends SDBaseAdapter<PrivateLettersItemActivityMsg_listModel>
{

	private Activity activity;

	public PrivateLettersItemAdapter(List<PrivateLettersItemActivityMsg_listModel> listModel, Activity activity)
	{
		super(listModel, activity);
		this.activity = activity;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_private_letters, null);

		}

		ImageView ivUser_avatar = ViewHolder.get(convertView, R.id.item_private_letters_iv_useravatar);
		TextView tvTitle = ViewHolder.get(convertView, R.id.item_private_letters_tv_msg_titles);
		// TextView tvCounts = ViewHolder.get(convertView,
		// R.id.item_private_letters_tv_msg_counts);
		TextView tvContent = ViewHolder.get(convertView, R.id.item_private_letters_tv_msg_content);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_private_letters_tv_msg_time);

		final PrivateLettersItemActivityMsg_listModel model = getItem(position);

		if (model != null)
		{
			SDViewBinder.setImageView(ivUser_avatar, model.getUser_avatar());
			SDViewBinder.setTextView(tvTitle, model.getUser_name());
			SDViewBinder.setTextView(tvTime, model.getTime());

			// 设置内容(表情)
			if (model.getParse_expres() != null && model.getParse_expres().size() != 0)
			{
				String b = model.getContent();
				for (int i1 = 0; i1 < model.getParse_expres().size(); i1++)
				{
					String c = model.getParse_expres().get(i1).getKey();
					String g = "<img src='" + model.getParse_expres().get(i1).getValue() + "'/>";
					String d = b.replace(c, g);
					b = d;
					Spanned text = Html.fromHtml(b, new SDMyImageGetterUtil(activity, tvContent), null);
					SDViewBinder.setTextView(tvContent, text);

				}
			} else
			{

				String text1 = model.getContent();
				SDViewBinder.setTextView(tvContent, text1);
			}

		}
		return convertView;
	}

}
