package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.InitActCitylistModel;
import com.wenzhoujie.utils.ViewHolder;

public class PopHomeEarnsAdapter extends SDBaseAdapter<InitActCitylistModel>
{

	public PopHomeEarnsAdapter(List<InitActCitylistModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_gv_earns, null);
		}
		TextView tvEarn = ViewHolder.get(convertView, R.id.item_gv_earns_tv_earn);
		InitActCitylistModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvEarn, model.getName());
		}

		return convertView;
	}

}
