package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.MerchantDetailNewActivity;
import com.wenzhoujie.SuperMarketDetailActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.act.GuessGoodsActModel;
import com.lingou.www.R;
import com.wenzhoujie.utils.ViewHolder;

public class HomeRecommendGuessAdapter extends SDBaseAdapter<GuessGoodsActModel> {

	public HomeRecommendGuessAdapter(List<GuessGoodsActModel> listModel, Activity activity) {
		super(listModel, activity);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_guess, null);
		}
		View viewDiv = ViewHolder.get(convertView, R.id.item_home_recommend_guess_view_div);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_guess_iv_image);
		//ImageView ivDoNotNeedOrder = ViewHolder.get(convertView, R.id.item_home_recommend_guess_iv_do_not_need_order);
		//ImageView ivIsNew = ViewHolder.get(convertView, R.id.item_home_recommend_guess_iv_is_new);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_recommend_guess_tv_name);
		TextView tvAddress = ViewHolder.get(convertView, R.id.item_home_recommend_guess_tv_address);
		TextView tvTel = ViewHolder.get(convertView, R.id.item_home_recommend_guess_tv_current_tel);
		TextView tvDPCount = ViewHolder.get(convertView, R.id.item_home_recommend_guess_tv_dp_count);
		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		final GuessGoodsActModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getPreview());
			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setTextView(tvAddress, model.getAddress());
			SDViewBinder.setTextView(tvTel, model.getTel());
			SDViewBinder.setTextView(tvDPCount, model.getGood_dp_count());
			//System.out.println(model.getName()+model.getAddress()+model.getTel()+model.getGood_dp_count());
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != null)
					{
						if(model.getShop_type() == 5)
						{
							
						Intent itemintent = new Intent();
							itemintent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_ID, model.getId());
							itemintent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_TITLE, model.getName());
							itemintent.setClass(App.getApplication(), SuperMarketDetailActivity.class);
							mActivity.startActivity(itemintent);
							
							
						}
						else
						{
						Intent intent = new Intent(App.getApplication(), MerchantDetailNewActivity.class);
						intent.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, model.getId());
						//intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 1);
						mActivity.startActivity(intent);
						}
					}
				}
			});
		}

		return convertView;
	}
}
