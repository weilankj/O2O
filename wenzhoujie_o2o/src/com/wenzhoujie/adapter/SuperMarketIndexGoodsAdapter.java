package com.wenzhoujie.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.MerchantShopItem;
import com.wenzhoujie.utils.ViewHolder;

public class SuperMarketIndexGoodsAdapter extends SDBaseAdapter<MerchantShopItem> {
	
	public SuperMarketIndexGoodsAdapter(List<MerchantShopItem> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_supermarket_today, null);
		}
		
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_supermarket_today_img);
		TextView tvName = ViewHolder.get(convertView, R.id.item_supermarket_today_name);
		TextView tvPrice = ViewHolder.get(convertView, R.id.item_supermarket_today_price);

		final MerchantShopItem model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getImg());
			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setTextView(tvPrice, "￥"+stringTo2double(String.valueOf(model.getCurrent_price())));
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent(App.getApplication(), TuanDetailActivity.class);
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getId());
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 0);
					mActivity.startActivity(intent);
				}
			});
		}

		return convertView;
	}
	
	public String stringTo2double(String num) {
		Double total_price_double = Double.parseDouble(num);
		if(total_price_double>0){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format(total_price_double);
		}else
		{
			return "0.00";
		}
	}
}
