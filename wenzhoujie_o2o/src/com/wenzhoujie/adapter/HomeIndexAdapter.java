package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.IndexActIndexsModel;
import com.wenzhoujie.utils.ViewHolder;

public class HomeIndexAdapter extends SDBaseAdapter<IndexActIndexsModel>
{

	public HomeIndexAdapter(List<IndexActIndexsModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_index, null);
		}
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_index_iv_image);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_index_tv_name);

		IndexActIndexsModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setImageView(ivImage, model.getImg());
		}
		return convertView;
	}

}