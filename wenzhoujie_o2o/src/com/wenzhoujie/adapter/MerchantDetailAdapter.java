package com.wenzhoujie.adapter;

import java.util.List;

import com.lingou.www.R;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.model.CateTypeModel;
import com.wenzhoujie.model.MerchantdetailClassifyModel;
import com.wenzhoujie.model.Quan_listQuan_subModel;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MerchantDetailAdapter extends SDBaseAdapter<MerchantdetailClassifyModel>{

	public MerchantDetailAdapter(List<MerchantdetailClassifyModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewClass view = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.merchantdetail_item, null);
			view = new ViewClass();
			view.tv_merchantdetail=(TextView)convertView.findViewById(R.id.tv_merchantdetail);
			convertView.setTag(view);

		} else {
			view = (ViewClass) convertView.getTag();
		}
		MerchantdetailClassifyModel model = getItem(position);
		view.tv_merchantdetail.setText(model.getName());
		return convertView;
	}
	public static class ViewClass {
		TextView tv_merchantdetail;
	}
}
