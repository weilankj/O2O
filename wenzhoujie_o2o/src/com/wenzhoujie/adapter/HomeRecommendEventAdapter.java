package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.EventsDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.IndexActEvent_listModel;
import com.wenzhoujie.utils.ViewHolder;

public class HomeRecommendEventAdapter extends SDBaseAdapter<IndexActEvent_listModel>
{
	public HomeRecommendEventAdapter(List<IndexActEvent_listModel> listModel, Activity activity)
	{

		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_event, null);
		}
		RelativeLayout rlWrapper = ViewHolder.get(convertView, R.id.item_home_recommend_event_rl_wrapper);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_event_iv_image);
		TextView tvLeftTime = ViewHolder.get(convertView, R.id.item_home_recommend_event_tv_left_time);

		final IndexActEvent_listModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageFillScreenWidthByScale(ivImage, rlWrapper, model.getIcon());
			SDViewBinder.setTextView(tvLeftTime, model.getSheng_time_format());
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != null)
					{
						Intent intent = new Intent(App.getApplication(), EventsDetailActivity.class);
						intent.putExtra(EventsDetailActivity.EXTRA_EVENTS_ID, model.getId());
						mActivity.startActivity(intent);
					}
				}
			});

		}
		return convertView;
	}

}