package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.CommentListActivityItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class CommentListAdapter extends SDBaseAdapter<CommentListActivityItemModel>
{

	public CommentListAdapter(List<CommentListActivityItemModel> listModel, Activity activity)
	{
		super(listModel, activity);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.act_comment_list_item, null);
		}

		TextView tvName = ViewHolder.get(convertView, R.id.act_comment_list_item_name);
		TextView tvTime = ViewHolder.get(convertView, R.id.act_comment_list_item_time);
		TextView tvContent = ViewHolder.get(convertView, R.id.act_comment_list_item_content);

		final CommentListActivityItemModel model = getItem(position);
		if (model != null)
		{

			SDViewBinder.setTextView(tvName, model.getUser_name());
			SDViewBinder.setTextView(tvTime, model.getCreate_time_format());
			SDViewBinder.setTextView(tvContent, model.getContent());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{

				}
			});
		}
		return convertView;
	}
}
