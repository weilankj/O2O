package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.MerchantDetailNewActivity;
import com.wenzhoujie.SuperMarketDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.IndexActSupplier_listModel;
import com.wenzhoujie.utils.ViewHolder;

public class HomeRecommendSupplierAdapter extends SDBaseAdapter<IndexActSupplier_listModel>
{

	public HomeRecommendSupplierAdapter(List<IndexActSupplier_listModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_supplier, null);
		}
		
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_supplier_iv_image);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_recommend_supplier_tv_name);
		
		
		final IndexActSupplier_listModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getPreview());
			SDViewBinder.setTextView(tvName, model.getName());
			//ivImage.setScaleType(ImageView.ScaleType.FIT_XY);
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != null)
					{
						if(model.getShop_type() == 5)
						{
							
							Intent itemintent = new Intent();
							itemintent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_ID, model.getId());
							itemintent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_TITLE, model.getName());
							itemintent.setClass(App.getApplication(), SuperMarketDetailActivity.class);
							mActivity.startActivity(itemintent);
							
							
						}
						else
						{
						Intent intent = new Intent(App.getApplication(), MerchantDetailNewActivity.class);
						intent.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, model.getId());
						mActivity.startActivity(intent);
						}
					}
				}
			});
		}
		return convertView;
	}

}
