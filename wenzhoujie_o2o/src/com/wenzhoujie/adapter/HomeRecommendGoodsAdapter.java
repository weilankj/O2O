package com.wenzhoujie.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.utils.ViewHolder;

public class HomeRecommendGoodsAdapter extends SDBaseAdapter<IndexActDeal_listModel>
{
	public HomeRecommendGoodsAdapter(List<IndexActDeal_listModel> listModel, Activity activity)
	{

		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_goods, null);
		}
		View viewDiv = ViewHolder.get(convertView, R.id.item_home_recommend_goods_view_div);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_goods_iv_image);
		ImageView ivDoNotNeedOrder = ViewHolder.get(convertView, R.id.item_home_recommend_goods_iv_do_not_need_order);
		ImageView ivIsNew = ViewHolder.get(convertView, R.id.item_home_recommend_goods_iv_is_new);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_name);
		TextView tvBrief = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_brief);
		TextView tvCurrentPrice = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_current_price);
		TextView tvOriginalPrice = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_original_price);
		TextView tvBuyCount = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_buy_count);
		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		final IndexActDeal_listModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getImg());
			SDViewBinder.setTextView(tvName, model.getSub_name());
			SDViewBinder.setTextView(tvBrief, model.getBrief());
			SDViewBinder.setTextView(tvCurrentPrice, "￥"+stringTo2double(model.getCurrent_price()));
			SDViewBinder.setTextView(tvOriginalPrice, "￥"+stringTo2double(model.getOrigin_price()));
			tvOriginalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); // 设置中划线效果
			SDViewBinder.setTextView(tvBuyCount, model.getBuy_count());
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != null)
					{
						Intent intent = new Intent(App.getApplication(), TuanDetailActivity.class);
						intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getId());
						intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 0);
						mActivity.startActivity(intent);
					}
				}
			});
		}

		return convertView;
	}

	public String stringTo2double(String num) {
		Double total_price_double = Double.parseDouble(num);
		if(total_price_double>0){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format(total_price_double);
		}else
		{
			return "0.00";
		}
	}
}