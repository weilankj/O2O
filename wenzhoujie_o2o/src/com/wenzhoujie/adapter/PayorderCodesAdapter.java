package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.Pay_orderActCouponlistModel;
import com.wenzhoujie.utils.ViewHolder;

public class PayorderCodesAdapter extends SDBaseAdapter<Pay_orderActCouponlistModel>
{

	public PayorderCodesAdapter(List<Pay_orderActCouponlistModel> listModel, Activity activity)
	{
		super(listModel, activity);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_pay_order_code, null);
		}
		ImageView ivCode = ViewHolder.get(convertView, R.id.item_pay_order_code_iv_code);
		TextView tvCode = ViewHolder.get(convertView, R.id.item_pay_order_code_tv_code);

		Pay_orderActCouponlistModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivCode, model.getQrcode());
			if (!TextUtils.isEmpty(model.getCouponPw()))
			{
				tvCode.setText("密码:" + model.getCouponPw());
			}
		}

		return convertView;
	}

}
