package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.MerchantDetailNewActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.YouhuiitemActMerchantModel;
import com.wenzhoujie.utils.SDIntentUtil;
import com.wenzhoujie.utils.SDNumberUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewHolder;

public class YouHuiDetailMerchantAdapter extends SDBaseAdapter<YouhuiitemActMerchantModel>
{
	public YouHuiDetailMerchantAdapter(List<YouhuiitemActMerchantModel> listModel, Activity activity)
	{

		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_goods_detail_merchant, null);
		}
		TextView tvBusinessesName = ViewHolder.get(convertView, R.id.item_goods_detail_merchant_tv_businesses_name);
		TextView tvBusinessesAddress = ViewHolder.get(convertView, R.id.item_goods_detail_merchant_tv_businesses_address);
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_goods_detail_merchant_tv_distance);
		LinearLayout llBusinessPhone = ViewHolder.get(convertView, R.id.item_goods_detail_merchant_ll_business_phone);
		View viewDiv = ViewHolder.get(convertView, R.id.item_goods_detail_merchant_view_div);

		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		final YouhuiitemActMerchantModel model = getItem(position);
		if (model != null)
		{
			// 商家名称
			SDViewBinder.setTextView(tvBusinessesName, model.getName());
			// 商家地址
			SDViewBinder.setTextView(tvBusinessesAddress, model.getAddress());
			// 距离
			double distance = SDTypeParseUtil.getDoubleFromString(model.getDistance(), 0);
			if (distance > 1000)
			{
				tvDistance.setText(SDNumberUtil.round(distance / 1000, 2) + "千米");
			} else
			{
				tvDistance.setText(SDNumberUtil.round(distance, 2) + "米");
			}

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					if (model != null && !TextUtils.isEmpty(model.getId()) && mActivity != null)
					{
						Intent intent = new Intent();
						intent.setClass(App.getApplication(), MerchantDetailNewActivity.class);
						intent.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, model.getId());
						mActivity.startActivity(intent);
					} else
					{
						SDToast.showToast("抱歉，商家id错误", Toast.LENGTH_SHORT);
					}
				}
			});

			llBusinessPhone.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model != null && !TextUtils.isEmpty(model.getTel()) && mActivity != null)
					{
						Intent intent = SDIntentUtil.getCallPhoneIntent(model.getTel());
						mActivity.startActivity(intent);
					} else
					{
						SDToast.showToast("抱歉，没有商家电话", Toast.LENGTH_SHORT);
					}
				}
			});
		}

		return convertView;
	}

}