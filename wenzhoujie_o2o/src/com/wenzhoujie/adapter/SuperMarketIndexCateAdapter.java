package com.wenzhoujie.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.SuperMarketDetailActivity;
import com.wenzhoujie.SuperMarketSubCateActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.fragment.SuperMarketSubCateFragment;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.MerchantShopItem;
import com.wenzhoujie.model.MerchantdetailClassifyModel;
import com.wenzhoujie.model.SmCatesModel;
import com.wenzhoujie.utils.ViewHolder;

public class SuperMarketIndexCateAdapter extends SDBaseAdapter<SmCatesModel> {
	
	SuperMarketDetailActivity pActivty = null;
	public SuperMarketIndexCateAdapter(List<SmCatesModel> listModel, Activity activity)
	{
		super(listModel, activity);
		pActivty =(SuperMarketDetailActivity)activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_supermarket_cate, null);
		}
		
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_supermarket_cate_img);
		//TextView tvName = ViewHolder.get(convertView, R.id.item_supermarket_cate_name);
		//TextView tvDesc = ViewHolder.get(convertView, R.id.item_supermarket_cate_desc);

		final SmCatesModel model = getItem(position);
		if (model != null)
		{
			//SDViewBinder.setImageView(ivImage, model.getImgurl());
			ivImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
			SDViewBinder.setImageView(ivImage, model.getImgurl());
			//SDViewBinder.setTextView(tvName, model.getName());
			//SDViewBinder.setTextView(tvDesc, model.getComment());
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					
					Intent intent = new Intent(App.getApplication(), SuperMarketSubCateActivity.class);
					intent.putExtra(SuperMarketSubCateFragment.EXTRA_BRAND_ID, pActivty.getSmId()); //sm id
					intent.putExtra(SuperMarketSubCateFragment.EXTRA_TITLE_TXT, pActivty.getSmName()); //sm name
					intent.putExtra(SuperMarketSubCateFragment.EXTRA_CATE_ID, Integer.toString( model.getId())); 
					intent.putExtra(SuperMarketSubCateFragment.EXTRA_CATE_NAME, model.getName()); 
					
					mActivity.startActivity(intent);
					
				}
			});
		}

		return convertView;
	}
}
