package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.MerchantListActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.WebViewActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.constant.ApkConstant;
import com.wenzhoujie.fragment.MerchantListFragment;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.act.GuessGoodsActModel;
import com.wenzhoujie.model.act.MoreCateActModel;
import com.wenzhoujie.model.act.MoreCateSubActModel;
import com.wenzhoujie.model.act.ShangQuanActModel;
import com.lingou.www.R;
import com.wenzhoujie.utils.ViewHolder;

public class PopupWinAdapter extends SDBaseAdapter<ShangQuanActModel> {

	
	public PopupWinAdapter(List<ShangQuanActModel> listModel, Activity activity) {
		super(listModel, activity);
		// TODO Auto-generated constructor stub
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.my_txtbtn_item, null);
		}
		
		TextView tvName = ViewHolder.get(convertView, R.id.tvt);
		
		final ShangQuanActModel model = getItem(position);
		if (model != null)
		{
			
			SDViewBinder.setTextView(tvName, model.getName());
			
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != null)
					{
						Intent intent = new Intent(App.getApplication(), MerchantListActivity.class);
						
						//intent.putExtra(MerchantListFragment.EXTRA_KEY_WORD, model.getName());
						intent.putExtra(MerchantListFragment.TITILE_NAME,"商家");
						intent.putExtra(MerchantListFragment.EXTRA_CATE_SEC_NAME,model.getName());
						//intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, model.getPid());
						intent.putExtra(MerchantListFragment.EXTRA_CATE_SEC_ID, model.getId());
						mActivity.startActivity(intent);

					}
				}
			});
		}

		return convertView;
	}
}
