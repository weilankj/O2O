package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.MerchantListActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.WebViewActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.constant.ApkConstant;
import com.wenzhoujie.fragment.MerchantListFragment;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.act.GuessGoodsActModel;
import com.wenzhoujie.model.act.MoreCateActModel;
import com.wenzhoujie.model.act.MoreCateSubActModel;
import com.lingou.www.R;
import com.wenzhoujie.utils.ViewHolder;

public class MoreCateAllAdapter extends SDBaseAdapter<MoreCateSubActModel> {

	String catePrtId = "0";//父节点id
	
	public MoreCateAllAdapter(List<MoreCateSubActModel> listModel, Activity activity) {
		super(listModel, activity);
		// TODO Auto-generated constructor stub
	}

	public void setCatePrtID(String id)
	{
		catePrtId = id;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.view_more_tags_frag, null);
		}
		
		TextView tvName = ViewHolder.get(convertView, R.id.more_tags_txt_frag);
		
		final MoreCateSubActModel model = getItem(position);
		if (model != null)
		{
			
			SDViewBinder.setTextView(tvName, model.getName());
			
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != null)
					{
						/*Intent intent = new Intent(App.getApplication(), TuanDetailActivity.class);
						intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getId());
						intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 1);
						mActivity.startActivity(intent);*/
						/*String JCUrl = ApkConstant.SERVER_URL+"/youhui.php?ctl=store&cid="+catePrtId+"&tid="+model.getId();
						Intent intent = new Intent(App.getApplication(), WebViewActivity.class);
						intent.putExtra(WebViewActivity.EXTRA_URL, JCUrl);
						mActivity.startActivity(intent);*/
						
						Intent intent = new Intent();
						intent.setClass(App.getApplication(), MerchantListActivity.class);
						intent.putExtra(MerchantListFragment.EXTRA_TITLE_TXT, "商家列表");
						intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, catePrtId);
						//intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, model.getName());
						intent.putExtra(MerchantListFragment.EXTRA_KEY_WORD, model.getName());
						
						//SDToast.showToast(model.getName());
						
						mActivity.startActivity(intent);
					}
				}
			});
		}

		return convertView;
	}
}
