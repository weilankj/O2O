package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.customview.SD2LvCategoryView.SD2LvCategoryViewAdapterInterface;
import com.wenzhoujie.model.Bcate_listBcate_typeModel;
import com.wenzhoujie.model.Bcate_listModel;
import com.wenzhoujie.utils.ViewHolder;

public class CategoryCateLeftAdapter extends SDBaseAdapter<Bcate_listModel> implements SD2LvCategoryViewAdapterInterface
{

	private int mDefaultIndex;

	public CategoryCateLeftAdapter(List<Bcate_listModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_category_left, null);
		}
		TextView tvTitle = ViewHolder.get(convertView, R.id.item_category_left_tv_title);
		TextView tvArrowRight = ViewHolder.get(convertView, R.id.item_category_left_tv_arrow_right);

		Bcate_listModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvTitle, model.getName());
			if (model.isSelect())
			{
				convertView.setBackgroundColor(SDResourcesUtil.getColor(R.color.bg_gray_categoryview_item_select));
			} else
			{
				convertView.setBackgroundColor(SDResourcesUtil.getColor(R.color.white));
			}
			if (model.isHasChild())
			{
				tvArrowRight.setVisibility(View.VISIBLE);
			} else
			{
				tvArrowRight.setVisibility(View.GONE);
			}
		}

		return convertView;
	}

	@Override
	public void setPositionSelectState(int position, boolean select, boolean notify)
	{
		getItem(position).setSelect(select);
		if (notify)
		{
			notifyDataSetChanged();
		}
	}

	@Override
	public String getTitleNameFromPosition(int position)
	{
		return getItem(position).getName();
	}

	@Override
	public BaseAdapter getAdapter()
	{
		return this;
	}

	@Override
	public Object getSelectModelFromPosition(int position)
	{
		return getItem(position);
	}

	@Override
	public int getTitleIndex()
	{
		return mDefaultIndex;
	}

	@Override
	public Object getRightListModelFromPosition_left(int position)
	{
		return getItem(position).getBcate_type();
	}

	@Override
	public void updateRightListModel_right(Object rightListModel)
	{

	}

	@Override
	public void setPositionSelectState_left(int positionLeft, int positionRight, boolean select)
	{
		List<Bcate_listBcate_typeModel> listRight = getItem(positionLeft).getBcate_type();
		if (listRight != null && positionRight >= 0 && positionRight < listRight.size())
		{
			listRight.get(positionRight).setSelect(select);
		}
	}

	@Override
	public int getItemCount()
	{
		return getCount();
	}

	public void setDefaultSelectId(String id)
	{
		if (!TextUtils.isEmpty(id) && mListModel != null)
		{
			int i = 0;
			for (Bcate_listModel model : mListModel)
			{
				if (id.equals(model.getId()))
				{
					model.setSelect(true);
					mDefaultIndex = i;
					break;
				}
				i++;
			}
		}
	}

}
