package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.HomeMoreCategoryActivity;
import com.wenzhoujie.MerchantListActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.TuanListActivity;
import com.wenzhoujie.WebViewActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.constant.ApkConstant;
import com.wenzhoujie.customview.SDGridViewInScroll;
import com.wenzhoujie.fragment.MerchantListFragment;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.act.GuessGoodsActModel;
import com.wenzhoujie.model.act.MoreCateActModel;
import com.wenzhoujie.model.act.MoreCateSubActModel;
import com.lingou.www.R;
import com.wenzhoujie.utils.ViewHolder;

public class MoreCateAdapter extends SDBaseAdapter<MoreCateSubActModel> {

	String catePrtId = "0";//父节点id
	SDGridViewInScroll nowView = null;//当前的视图
	Activity nowActy = null;
	List<MoreCateSubActModel> nowList = null;
	int nowViewId = 0;
	int splitNum = 4;
	
	public MoreCateAdapter(List<MoreCateSubActModel> listModel, Activity activity) {
		super(listModel, activity);
		nowActy = activity;
		
		// TODO Auto-generated constructor stub
	}

	public void setCatePrtID(String id,List<MoreCateSubActModel> alllistModel,int ViewID,int sNum)
	{//父id,完整的模型列表,当前视图的ID,不完整显示的个数
		nowList = alllistModel;
		catePrtId = id;
		nowViewId = ViewID;
		splitNum = sNum; 
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if( position < splitNum-1 )
		{
			if (convertView == null)
			{
				convertView = mInflater.inflate(R.layout.view_more_tags_frag, null);
			}
			TextView tvName = ViewHolder.get(convertView, R.id.more_tags_txt_frag);
			final MoreCateSubActModel model = getItem(position);
			
			
			if (model != null)
			{
				
				SDViewBinder.setTextView(tvName, model.getName());
				
				convertView.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (model.getId() != null)
						{
							/*Intent intent = new Intent(App.getApplication(), TuanDetailActivity.class);
							intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getId());
							intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 1);
							mActivity.startActivity(intent);*/
							
							//String JCUrl = ApkConstant.SERVER_URL+"/youhui.php?ctl=store&cid="+catePrtId+"&tid="+model.getId();
							//Intent intent = new Intent(App.getApplication(), WebViewActivity.class);
							//intent.putExtra(WebViewActivity.EXTRA_URL, JCUrl);
							//mActivity.startActivity(intent);
							
							Intent intent = new Intent();
							intent.setClass(App.getApplication(), MerchantListActivity.class);
							intent.putExtra(MerchantListFragment.EXTRA_TITLE_TXT, "商家列表");
							intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, catePrtId);
							intent.putExtra(MerchantListFragment.EXTRA_CATE_TYPE_ID, model.getId());
							intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, model.getName());
							intent.putExtra(MerchantListFragment.EXTRA_KEY_WORD, model.getName());
							
							//SDToast.showToast(model.getName());
							
							mActivity.startActivity(intent);
						}
					}
				});
			}
		}
		else if(position== splitNum-1 )
		{
			if (convertView == null)
			{
				convertView = mInflater.inflate(R.layout.view_more_tags_show_frag, null);//view_more_tags_show_frag
			}
			
			final MoreCateSubActModel model = getItem(position);
			
			if (model != null)
			{
				convertView.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						//System.out.println("catePrtId--"+catePrtId);
						//if (model.getId() != null)
						{
							
							//if(catePrtId=="1")
							{
								//SDGridViewInScroll tmView = (SDGridViewInScroll) findViewById(R.id.s_more_canyin_frag);
								//moreSubLst=moreLst.get(1).getSub_cate_info();
								nowView  = (SDGridViewInScroll) nowActy.findViewById(nowViewId);
								//nowView.removeAllViews();
								MoreCateAllAdapter adapter1 = new MoreCateAllAdapter(nowList,nowActy );
								adapter1.setCatePrtID(nowList.get(0).getId());
								//int listSize1 = moreSubLst.size();
								nowView.setAdapter(adapter1);
								nowView.deferNotifyDataSetChanged();
								//System.out.println("nowView.setAdapter(adapter1);");
							}
							
							
							
						}
					}
				});
			}
		}
		else
		{
			if (convertView == null)
			{
				convertView = mInflater.inflate(R.layout.view_more_tags_frag, null);
				convertView.setVisibility(View.GONE);
			}
		}
	
		
		return convertView;
	}
}
