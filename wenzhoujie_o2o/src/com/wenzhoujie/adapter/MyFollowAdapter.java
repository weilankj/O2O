package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.ShareDetailActivity;
import com.wenzhoujie.UserCenterActivity;
import com.wenzhoujie.model.FollowActItemModel;
import com.wenzhoujie.model.FollowActItemRelay_shareModel;
import com.wenzhoujie.model.UItemExpressModel;
import com.wenzhoujie.model.UItemImageModel;
import com.wenzhoujie.utils.SDMyImageGetterUtil;
import com.wenzhoujie.utils.ViewHolder;

public class MyFollowAdapter extends SDBaseAdapter<FollowActItemModel>
{

	public MyFollowAdapter(List<FollowActItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_share_content, null);

		}

		LinearLayout llFenxiang_Linear = ViewHolder.get(convertView, R.id.item_share_content_ll_original_fenxiang);
		ImageView ivUser_avatar = ViewHolder.get(convertView, R.id.item_share_content_iv_user_avatar);
		TextView tvUser_name = ViewHolder.get(convertView, R.id.item_share_content_tv_user_name);
		TextView tvList_time = ViewHolder.get(convertView, R.id.item_share_content_tv_time);
		TextView tvList_content = ViewHolder.get(convertView, R.id.item_share_content_tv_content);
		TextView tvFenxiang_name = ViewHolder.get(convertView, R.id.item_share_content_tv_original_user_name);
		TextView tvList_likeCount = ViewHolder.get(convertView, R.id.item_share_content_tv_like_count);
		TextView tvList_commentsCount = ViewHolder.get(convertView, R.id.item_share_content_tv_comment_count);
		TextView tvList_relayCount = ViewHolder.get(convertView, R.id.item_share_content_tv_replay_count);

		// ========================================imageviews===========================
		ImageView ivUserImage0 = ViewHolder.get(convertView, R.id.item_share_content_iv_image_0);
		ivUserImage0.setVisibility(View.GONE);

		ImageView ivUserImage1 = ViewHolder.get(convertView, R.id.item_share_content_iv_image_1);
		ivUserImage1.setVisibility(View.GONE);

		ImageView ivUserImage2 = ViewHolder.get(convertView, R.id.item_share_content_iv_image_2);
		ivUserImage2.setVisibility(View.GONE);

		ImageView ivReplayImage0 = ViewHolder.get(convertView, R.id.item_share_content_iv_original_image_0);
		ivReplayImage0.setVisibility(View.GONE);

		ImageView ivReplayImage1 = ViewHolder.get(convertView, R.id.item_share_content_iv_original_image_1);
		ivReplayImage1.setVisibility(View.GONE);

		ImageView ivReplayImage2 = ViewHolder.get(convertView, R.id.item_share_content_iv_original_image_2);
		ivReplayImage2.setVisibility(View.GONE);

		final FollowActItemModel model = getItem(position);
		List<UItemExpressModel> listParse_expresModel = model.getParse_expres();

		if (model != null)
		{
			SDViewBinder.setImageView(ivUser_avatar, model.getUser_avatar()); // 用户头像
			ivUser_avatar.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent();
					intent.setClass(mActivity, UserCenterActivity.class);
					intent.putExtra(UserCenterActivity.EXTRA_UID_ID, model.getUid());
					mActivity.startActivity(intent);
				}
			});

			SDViewBinder.setTextView(tvUser_name, model.getUser_name(), "未找到"); // 用户名
			SDViewBinder.setTextView(tvList_time, model.getTime(), "未找到"); // 发表时间

			// 设置内容
			String userContent = model.getContent();
			if (listParse_expresModel.size() > 0) // 内容包含表情
			{
				for (int i1 = 0; i1 < listParse_expresModel.size(); i1++)
				{
					String oldString = listParse_expresModel.get(i1).getKey();
					String newString = "<img src='" + listParse_expresModel.get(i1).getValue() + "'/>";
					userContent = userContent.replace(oldString, newString);
					Spanned text = Html.fromHtml(userContent, new SDMyImageGetterUtil(mActivity, tvList_content), null);
					SDViewBinder.setTextView(tvList_content, text);
				}
			} else
			{
				SDViewBinder.setTextView(tvList_content, userContent);
			}
			bindShareImageContent(model.getImgs(), ivUserImage0, ivUserImage1, ivUserImage2); // 设置图片

			// ============================判断是否是回复的内容，并决定是否绑定数据=====================

			if (model.isIs_relay_format_boolean()) // 是回复内容
			{
				llFenxiang_Linear.setVisibility(View.VISIBLE);
				final FollowActItemRelay_shareModel replay_shareModel = model.getRelay_share(); // 被回复的model
				if (replay_shareModel != null)
				{
					String replayUsername = replay_shareModel.getUser_name(); // 被回复的用户名
					String replayContent = replay_shareModel.getContent(); // 被回复的内容
					if (replayUsername != null && replayContent != null)
					{
						String replayUsernameAndContent = "<font color=#41598a>" + replayUsername + "</font>" + "<font color=#000000>" + " : " + replayContent + "</font>";

						List<UItemExpressModel> listReplayExpressModel = replay_shareModel.getParse_expres();
						if (listReplayExpressModel.size() > 0) // 被回复的内容中包含表情
						{
							for (int i = 0; i < listReplayExpressModel.size(); i++)
							{
								String oldString = listReplayExpressModel.get(i).getKey();
								String newString = "<img src='" + listReplayExpressModel.get(i).getValue() + "'/>";
								replayUsernameAndContent = replayUsernameAndContent.replace(oldString, newString);
								Spanned replayTextSpan = Html.fromHtml(replayUsernameAndContent, new SDMyImageGetterUtil(mActivity, tvFenxiang_name), null);
								SDViewBinder.setTextView(tvFenxiang_name, replayTextSpan);
							}
						} else
						{
							tvFenxiang_name.setText(Html.fromHtml(replayUsernameAndContent));
						}
					}
					bindShareImageContent(replay_shareModel.getImgs(), ivReplayImage0, ivReplayImage1, ivReplayImage2); // 设置被回复中的图片
				}

				llFenxiang_Linear.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						Intent intent = new Intent();
						intent.setClass(mActivity, ShareDetailActivity.class);
						intent.putExtra(ShareDetailActivity.EXTRA_SHARE_ID, replay_shareModel.getShare_id());
						mActivity.startActivity(intent);
					}
				});
			} else
			{
				llFenxiang_Linear.setVisibility(View.GONE);
			}

			// 底部转发次数和喜欢评论次数设置
			// 喜欢数
			SDViewBinder.setTextView(tvList_likeCount, model.getCollect_count());
			// 评论数
			SDViewBinder.setTextView(tvList_commentsCount, model.getComment_count());
			// 转发数
			SDViewBinder.setTextView(tvList_relayCount, model.getRelay_count());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Intent intent = new Intent();
					intent.setClass(mActivity, ShareDetailActivity.class);
					intent.putExtra(ShareDetailActivity.EXTRA_SHARE_ID, model.getShare_id());
					mActivity.startActivity(intent);
				}
			});

		}
		return convertView;
	}

	private void bindShareImageContent(List<UItemImageModel> listShareImages, ImageView... ivImages)
	{
		if (listShareImages != null && listShareImages.size() > 0) // 被回复的内容中包含图片
		{
			UItemImageModel imageModel = null;
			ImageView ivImage = null;
			for (int i = 0; i < listShareImages.size(); i++)
			{
				imageModel = listShareImages.get(i);
				ivImage = ivImages[i];
				if (ivImage != null)
				{
					ivImage.setVisibility(View.VISIBLE);
					SDViewBinder.setImageView(ivImage, imageModel.getSmall_img());
				}
			}
		}
	}

}
