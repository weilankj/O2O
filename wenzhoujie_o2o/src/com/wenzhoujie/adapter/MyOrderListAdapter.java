package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.OrderDetailActivity;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.dialog.SDDialogConfirm;
import com.wenzhoujie.library.dialog.SDDialogCustom;
import com.wenzhoujie.library.dialog.SDDialogCustom.SDDialogCustomListener;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.wenzhoujie.model.DealOrderItemModel;
import com.wenzhoujie.model.My_order_listActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewHolder;

public class MyOrderListAdapter extends
		SDBaseAdapter<My_order_listActItemModel> {

	public static final int REQUEST_CODE_ORDER_DETAIL = 1;

	public MyOrderListAdapter(List<My_order_listActItemModel> listModel,
			Activity activity) {
		super(listModel, activity);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_lv_my_order_list,
					null);
		}
		TextView tvOrderSn = ViewHolder.get(convertView,
				R.id.item_lv_my_order_list_tv_order_code); // sn码
		TextView tvOrderPrice = ViewHolder.get(convertView,
				R.id.item_lv_my_order_list_tv_order_price); // 订单总价
		TextView tvOrderGoodsNumber = ViewHolder.get(convertView,
				R.id.item_lv_my_order_list_tv_goods_number); // 商品数量
		TextView tvOrderStatus = ViewHolder.get(convertView,
				R.id.item_lv_my_order_list_tv_order_status); // 订单状态
		TextView tvOrderDate = ViewHolder.get(convertView,
				R.id.item_lv_my_order_list_tv_create_time_format); // 下单时间
		LinearLayout llGoods = ViewHolder.get(convertView,
				R.id.item_lv_my_order_list_ll_goods_list); // 商品列表
		LinearLayout llGoodDetail = ViewHolder.get(convertView,
				R.id.item_lv_my_order_list_ll_goods_detail); // 商品列表
		TextView tv_pay = ViewHolder.get(convertView, R.id.tv_pay);
		TextView tv_cancel = ViewHolder.get(convertView, R.id.tv_cancel);

		final My_order_listActItemModel model = getItem(position);
		if (model != null) {
			SDViewBinder.setTextView(tvOrderSn, model.getOrder_sn());
			SDViewBinder.setTextView(tvOrderPrice,
					"￥ "+model.getTotal_price());
			SDViewBinder.setTextView(tvOrderGoodsNumber,
					"共有" + SDTypeParseUtil.getIntFromString(model.getC(), 0)
							+ "件商品");
			SDViewBinder.setTextView(tvOrderStatus, model.getStatus_format());
			SDViewBinder
					.setTextView(tvOrderDate, model.getCreate_time());

			List<DealOrderItemModel> listModel = model.getDeal_order_item();
			if (listModel != null && listModel.size() > 0) {
				llGoods.removeAllViews();
				OrderGoodsListAdapter adapter = new OrderGoodsListAdapter(
						listModel, true, mActivity);
				for (int i = 0; i < adapter.getCount(); i++) {
					View view = adapter.getView(i, null, null);
					llGoods.addView(view);
				}

			}
			llGoodDetail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mActivity,
							OrderDetailActivity.class);
					intent.putExtra(OrderDetailActivity.EXTRA_ORDER_ID,
							model.getId());
					mActivity.startActivityForResult(intent,
							REQUEST_CODE_ORDER_DETAIL);
				}
			});

			if (model.getPay_status() == 0) {
				SDViewUtil.show(tv_pay);
				tv_pay.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(mActivity,
								OrderDetailActivity.class);
						intent.putExtra(OrderDetailActivity.EXTRA_ORDER_ID,
								model.getId());
						mActivity.startActivity(intent);
					}
				});
			} else {
				SDViewUtil.hide(tv_pay);
			}

			if (SDViewBinder.setViewsVisibility(tv_cancel,
					model.hasCancelButton())) {
				SDViewBinder
						.setTextView(tv_cancel, model.getCancelButtonText());
				tv_cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						deleteOrder(model, position);
					}
				});
			}

		}

		return convertView;
	}

	private void deleteOrder(final My_order_listActItemModel model,
			final int position) {
		if (model == null) {
			return;
		}

		SDDialogConfirm dialog = new SDDialogConfirm();
		dialog.setTextContent("确定删除订单？");
		dialog.setmListener(new SDDialogCustomListener() {

			@Override
			public void onClickConfirm(View v, SDDialogCustom dialog) {
				requestDeleteOrder(model, position);
			}

			@Override
			public void onClickCancel(View v, SDDialogCustom dialog) {
			}

			@Override
			public void onDismiss(DialogInterface iDialog, SDDialogCustom dialog) {

			}
		});
		dialog.show();
	}

	private void requestDeleteOrder(final My_order_listActItemModel model,
			final int position) {
		RequestModel requestModel = new RequestModel();
		requestModel.putAct("uc_order_cancel");
		requestModel.put("id", model.getId());
		requestModel.putUser();
		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("正在删除...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				BaseActModel actModel = JsonUtil.json2Object(
						responseInfo.result, BaseActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					int status = SDTypeParseUtil.getIntFromString(
							actModel.getStatus(), 0);
					if (status == 1) {
						mListModel.remove(position);
						notifyDataSetChanged();
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(requestModel, handler);
	}
}
