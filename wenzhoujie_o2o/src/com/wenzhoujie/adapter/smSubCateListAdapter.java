package com.wenzhoujie.adapter;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.google.gson.Gson;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.ConfirmOrderActivity;
import com.wenzhoujie.LoginNewActivity;
import com.wenzhoujie.MerchantDetailNewActivity;
import com.wenzhoujie.ShopCartActivity;
import com.wenzhoujie.SuperMarketDetailActivity;
import com.wenzhoujie.SuperMarketSubCateActivity;
import com.wenzhoujie.SuperMarketSubCateItemsActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.customview.SDGridViewInScroll;
import com.wenzhoujie.fragment.SuperMarketSubCateFragment;
import com.wenzhoujie.fragment.TuanDetailSecondFragment;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.CartGoodsModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.MerchantShopItem;
import com.wenzhoujie.model.MerchantlistActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.smSubCateItemModel;
import com.wenzhoujie.model.smSubCateListModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.model.act.smSubCatelistActModel;
import com.wenzhoujie.utils.SDActivityUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewHolder;

public class smSubCateListAdapter extends SDBaseAdapter<smSubCateItemModel>
{
	public smSubCateListAdapter(List<smSubCateItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
			convertView = mInflater.inflate(R.layout.frag_sm_subcate_four, null);
		ImageView moreBtn = ViewHolder.get(convertView, R.id.item_smitem_morebtn); // 
		TextView smSubtitle = ViewHolder.get(convertView, R.id.item_smitem_name); // 
		SDGridViewInScroll gvContent = ViewHolder.get(convertView, R.id.frag_supermarket_items_gv);
		
		final smSubCateItemModel model = getItem(position);
		if (model != null)
		{

			//SDViewBinder.setImageView(moreBtn, model.getLogo());
			SDViewBinder.setTextView(smSubtitle, model.getName());
			moreBtn.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					SuperMarketSubCateActivity nowActivity = (SuperMarketSubCateActivity)mActivity;
					SuperMarketSubCateFragment nowFrag = nowActivity.getFrag();
					Intent mintent = new Intent();
					mintent.putExtra(SuperMarketSubCateItemsActivity.EXTRA_MERCHANT_ID, nowFrag.getNowSmId());
					mintent.putExtra(SuperMarketSubCateItemsActivity.EXTRA_MERCHANT_TITLE, nowFrag.getNowSmName());
					mintent.putExtra(SuperMarketSubCateItemsActivity.EXTRA_CATE_ID,nowFrag.getNowCateId());
					mintent.putExtra(SuperMarketSubCateItemsActivity.EXTRA_SUBCATE_ID, Integer.toString(model.getId()) );
					mintent.putExtra(SuperMarketSubCateItemsActivity.EXTRA_SUBCATE_NAME, model.getName() );
					mintent.setClass(App.getApplication(), SuperMarketSubCateItemsActivity.class);
					mActivity.startActivity(mintent);
				}
			});
			
			SuperMarketCateGoodOneAdapter mItemsAdapter = new SuperMarketCateGoodOneAdapter(model.getItems(), mActivity);
			gvContent.setAdapter(mItemsAdapter);
			mItemsAdapter.updateListViewData(model.getItems());
		}
		return convertView;
	}
}
