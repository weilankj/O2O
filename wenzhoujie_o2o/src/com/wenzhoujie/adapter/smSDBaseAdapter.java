package com.wenzhoujie.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class smSDBaseAdapter<T> extends BaseAdapter
{

	protected List<T> mListModel = new ArrayList<T>();

	protected LayoutInflater mInflater = null;
	protected Activity mActivity = null;
	protected EnumAdapterMode mMode = EnumAdapterMode.SINGLE;

	/** 单选模式时候被选中的项 */
	protected int mSelectedPosition = -1;
	/** 多选模式时候被选中的项集合 */
	protected List<T> mListSelectedModel = new ArrayList<T>();

	public EnumAdapterMode getmMode()
	{
		return mMode;
	}

	public void setmMode(EnumAdapterMode mode)
	{
		this.mMode = mode;
	}

	public int getmSelectedPosition()
	{
		return mSelectedPosition;
	}

	public List<T> getmSelectedPositionList()
	{
		return mListSelectedModel;
	}

	public void setmSelectedPosition(int position, boolean selected)
	{
		if (mListModel != null && position >= 0 && position < mListModel.size())
		{
			switch (getmMode())
			{
			case SINGLE:
				if (mSelectedPosition >= 0)
				{
					onSelectedChange(mSelectedPosition, false, false);
				}
				if (selected)
				{
					mSelectedPosition = position;
				} else
				{
					mSelectedPosition = -1;
				}
				onSelectedChange(position, selected, true);
				break;
			case MULTI:
				T model = mListModel.get(position);
				if (selected)
				{
					if (!mListSelectedModel.contains(model))
					{
						mListSelectedModel.add(model);
						onSelectedChange(position, true, true);
					}
				} else
				{
					if (mListSelectedModel.contains(model))
					{
						mListSelectedModel.remove(model);
						onSelectedChange(position, false, true);
					}
				}
				break;
			default:
				break;
			}

		}
	}

	protected void onSelectedChange(int position, boolean selected, boolean notify)
	{

	}

	public smSDBaseAdapter(List<T> listModel, Activity activity)
	{
		if (listModel != null)
		{
			this.mListModel = listModel;
		}
		this.mActivity = activity;
		this.mInflater = mActivity.getLayoutInflater();
	}

	public List<T> getData()
	{
		return mListModel;
	}

	public void updateListViewData(List<T> listModel)
	{
		if (listModel != null)
		{
			this.mListModel = listModel;
		} else
		{
			this.mListModel = new ArrayList<T>();
		}
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		if (mListModel != null)
		{
			return mListModel.size();
		} else
		{
			return 0;
		}
	}

	@Override
	public T getItem(int position)
	{
		if (mListModel != null && position >= 0 && mListModel.size() > position)
		{
			return mListModel.get(position);
		} else
		{
			return null;
		}
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		return getViewItem(position, convertView, parent, getItem(position));
	}

	public View getViewItem(int position, View convertView, ViewGroup parent, T model)
	{
		return null;
	}

	public enum EnumAdapterMode
	{
		/** 单选模式 */
		SINGLE,
		/** 多选模式 */
		MULTI;
	}

}
