package com.wenzhoujie.adapter;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.InitActCitylistModel;
import com.wenzhoujie.utils.PinyinComparator;
import com.wenzhoujie.utils.ViewHolder;
import com.wenzhoujie.work.AppRuntimeWorker;

public class CityListAdapter extends SDBaseAdapter<InitActCitylistModel>
{

	private Map<Integer, Integer> mMapLettersAsciisFirstPostion = new HashMap<Integer, Integer>();
	private PinyinComparator mComparator = new PinyinComparator();

	public CityListAdapter(List<InitActCitylistModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_lv_citylist, null);
		}

		TextView tvLetter = ViewHolder.get(convertView, R.id.item_lv_citylist_tv_letter);
		TextView tvName = ViewHolder.get(convertView, R.id.item_lv_citylist_tv_city_name);
		LinearLayout ll_content = ViewHolder.get(convertView, R.id.ll_content);

		final InitActCitylistModel model = getItem(position);
		if (model != null)
		{
			// 根据position获取分类的首字母的Char ascii值
			int modelFirstLettersAscii = getModelFirstLettersAscii(model);

			// 如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
			if (position == getLettersAsciisFirstPosition(modelFirstLettersAscii))
			{
				tvLetter.setVisibility(View.VISIBLE);
				tvLetter.setText(model.getSortLetters());
			} else
			{
				tvLetter.setVisibility(View.GONE);
			}
			ll_content.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (AppRuntimeWorker.setCity_name(model.getName()))
					{
						mActivity.finish();
					} else
					{
						SDToast.showToast("设置城市失败");
					}
				}
			});
			SDViewBinder.setTextView(tvName, model.getName());

		}

		return convertView;
	}

	public int getModelFirstLettersAscii(InitActCitylistModel model)
	{
		if (model != null)
		{
			String letters = model.getSortLetters();
			if (!TextUtils.isEmpty(letters) && letters.length() > 0)
			{
				return letters.charAt(0);
			} else
			{
				return 0;
			}
		} else
		{
			return 0;
		}
	}

	public int getLettersAsciisFirstPosition(int lettersAscii)
	{
		Integer position = mMapLettersAsciisFirstPostion.get(lettersAscii);
		if (position == null)
		{
			boolean isFound = false;
			for (int i = 0; i < mListModel.size(); i++)
			{
				String sortStr = mListModel.get(i).getSortLetters();
				char firstChar = sortStr.toUpperCase().charAt(0);
				if (firstChar == lettersAscii)
				{
					mMapLettersAsciisFirstPostion.put(lettersAscii, i);
					isFound = true;
					position = i;
					break;
				}
			}
			if (!isFound)
			{
				position = -1;
				;
			}
		}
		return position;
	}

	@Override
	public void notifyDataSetChanged()
	{
		if (mListModel != null)
		{
			Collections.sort(mListModel, mComparator);
		}
		super.notifyDataSetChanged();
	}

}
