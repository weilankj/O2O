package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.UserfrequentedlistActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class FrequentedGoAdapter extends SDBaseAdapter<UserfrequentedlistActItemModel>
{

	private SubscribeMessageAdapterListener mListener = null;

	public FrequentedGoAdapter(List<UserfrequentedlistActItemModel> listModel, Activity activity, SubscribeMessageAdapterListener listener)
	{
		super(listModel, activity);
		this.mListener = listener;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_lv_frequented_go, null);
		}

		TextView tvTitle = ViewHolder.get(convertView, R.id.item_lv_frequented_go_tv_title);
		TextView tvAddress = ViewHolder.get(convertView, R.id.item_lv_frequented_go_tv_address);
		TextView tvDelete = ViewHolder.get(convertView, R.id.item_lv_frequented_go_tv_delete);

		final UserfrequentedlistActItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvTitle, model.getTitle());
			SDViewBinder.setTextView(tvAddress, model.getAddr());
		}

		tvDelete.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (mListener != null)
				{
					mListener.onDeleteClick(model);
				}
			}
		});

		return convertView;
	}

	public interface SubscribeMessageAdapterListener
	{
		public void onDeleteClick(UserfrequentedlistActItemModel model);

	}

}
