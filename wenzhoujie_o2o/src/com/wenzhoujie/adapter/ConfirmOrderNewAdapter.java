package com.wenzhoujie.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.R.attr;
import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.CartGoodsModel;
import com.wenzhoujie.model.ShopGoodsModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.ViewHolder;

public class ConfirmOrderNewAdapter extends SDBaseAdapter<CartGoodsModel>
{

	public ConfirmOrderNewAdapter(List<CartGoodsModel> listModel, Activity activity)
	{
		super(listModel, activity);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_order_goods_list, null);
		}

		View viewDiv = ViewHolder.get(convertView, R.id.item_order_goods_list_view_div); // 商品图片
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_order_goods_list_iv_goods_image); // 商品图片
		TextView tvTitle = ViewHolder.get(convertView, R.id.item_order_goods_list_tv_goods_title); // 商品标题
		TextView tvNumber = ViewHolder.get(convertView, R.id.item_order_goods_list_tv_number); // 商品数量
		TextView tvAttr = ViewHolder.get(convertView, R.id.item_order_goods_list_tv_attr); // 商品属性
		TextView tvPriceSingle = ViewHolder.get(convertView, R.id.item_order_goods_list_tv_price_single); // 商品单价
		TextView tvPriceTotal = ViewHolder.get(convertView, R.id.item_order_goods_list_tv_price_total); // 商品总价
		
		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		CartGoodsModel model = getItem(position);
		GoodsdescActModel goodsModel = model.getGoods();
		if (model != null)
		{
			String attrContents = model.getAttr_contents();
			if (!TextUtils.isEmpty(attrContents))
			{
				tvTitle.setMaxLines(1);
			}
			SDViewBinder.setImageView(ivImage,goodsModel.getImage());
			SDViewBinder.setTextView(tvTitle, goodsModel.getTitle());
			SDViewBinder.setTextView(tvNumber, model.getNum());
			if(attrContents.equals(""))
			{
				tvAttr.setVisibility(View.GONE);
			}else
			{
				SDViewBinder.setTextViewsVisibility(tvAttr, "商品属性："+attrContents);
			}
			SDViewBinder.setTextView(tvPriceSingle, model.getPrice_format_string());
			SDViewBinder.setTextView(tvPriceTotal, model.getTotal_format_string());
		}

		return convertView;
	}

	public String stringTo2double(String num) {
		Double total_price_double = Double.parseDouble(num);
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format(total_price_double);
	}
}
