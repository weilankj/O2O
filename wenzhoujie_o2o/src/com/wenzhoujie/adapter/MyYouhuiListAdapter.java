package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.MyyouhuilistActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class MyYouhuiListAdapter extends SDBaseAdapter<MyyouhuilistActItemModel>
{

	public MyYouhuiListAdapter(List<MyyouhuilistActItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_my_youhui_list, null);
		}

		ImageView ivImage = ViewHolder.get(convertView, R.id.item_my_youhui_list_iv_image);
		TextView tvTitle = ViewHolder.get(convertView, R.id.item_my_youhui_list_tv_title);
		TextView tvName = ViewHolder.get(convertView, R.id.item_my_youhui_list_tv_name);
		TextView tvNumber = ViewHolder.get(convertView, R.id.item_my_youhui_list_tv_number);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_my_youhui_list_tv_time);

		MyyouhuilistActItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getMerchant_logo());
			SDViewBinder.setTextView(tvTitle, model.getTitle());
			SDViewBinder.setTextView(tvName, model.getContent());
			SDViewBinder.setTextView(tvNumber, model.getYl_sn());
			SDViewBinder.setTextView(tvTime, model.getYl_create_time_format());
		}

		return convertView;
	}

}
