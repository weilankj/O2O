package com.wenzhoujie.wxpay;

import org.json.JSONObject;

public interface WXPayListener {
	public void onStart();

	public void onFailure(String msg);

	public void onFinish(JSONObject result);
}
