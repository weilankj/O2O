package com.wenzhoujie;

import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.MyFollowFragment;
import com.wenzhoujie.utils.IocUtil;

/**
 * 关注的人activity
 * 
 * @author js02
 * 
 */
public class MyFollowActivity extends BaseActivity
{
	private MyFollowFragment mFragMyFollow;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_follow);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		addFragments();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("我关注的人");
	}

	private void addFragments()
	{
		mFragMyFollow = new MyFollowFragment();
		replaceFragment(mFragMyFollow, R.id.act_my_follow_fl_content);
	}

}