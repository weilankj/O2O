package com.wenzhoujie;

import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.WebViewFragment;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.Show_articleActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;

/**
 * 公告内容
 * 
 * @author js02
 * 
 */
public class NewsDetailActivity extends BaseActivity
{

	/** 公告id */
	public static final String EXTRA_NEWS_ID = "extra_news_id";

	private WebViewFragment mFragWebview = null;

	private String mStrNewsId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedSlideFinishLayout = false;
		setmTitleType(TitleType.TITLE_SIMPLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_news_detail);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData();
		initTitle();
		requestData();
	}

	private void getIntentData()
	{
		mStrNewsId = getIntent().getStringExtra(EXTRA_NEWS_ID);
	}

	private void requestData()
	{
		if (mStrNewsId != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "show_article");
			model.put("id", mStrNewsId);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Show_articleActModel actModel = JsonUtil.json2Object(responseInfo.result, Show_articleActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							String content = actModel.getContent();
							addWebViewFragment(content);
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}

	}

	private void addWebViewFragment(String content)
	{
		if (!AppHelper.isEmptyString(content))
		{
			mFragWebview = new WebViewFragment();
			mFragWebview.setHtmlContent(content);
			replaceFragment(mFragWebview, R.id.act_news_detail_fl_content);
		}
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("公告内容");
	}

}