package com.wenzhoujie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDCollectionUtil;
import com.wenzhoujie.utils.SDDialogUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.work.RegionSaver;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.dao.Region_confModelDao;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.model.*;
import com.wenzhoujie.model.act.*;

public class AddDeliveryAddressActivity extends BaseActivity implements OnClickListener
{
	public static final String EXTRA_DELIVERY_MODEL = "extra_delivery_model";
	public static final int RESULT_CODE_ADD_ADDRESS_SUCCESS = 1;
	public static final int RESULT_CODE_DELETE_ADDRESS_SUCCESS = 2;

	@ViewInject(id = R.id.act_add_delivery_address_et_consignee)
	private ClearEditText mEtConsignee = null;

	@ViewInject(id = R.id.act_add_delivery_address_et_phone)
	private ClearEditText mEtPhone = null;

	@ViewInject(id = R.id.act_add_delivery_address_tv_delivery)
	private TextView mTvDelivery = null;

	@ViewInject(id = R.id.act_add_delivery_address_et_address_detail)
	private ClearEditText mEtAddressDetail = null;

	@ViewInject(id = R.id.act_add_delivery_address_et_postcode)
	private ClearEditText mEtPostcode = null;

	@ViewInject(id = R.id.act_add_delivery_address_tv_save)
	private TextView mTvSave = null;

	private DeliveryModel mDeliveryModel = null;

	private List<Region_confModel> mListRegionCountry = new ArrayList<Region_confModel>(); // 国家
	private List<Region_confModel> mListRegionProvince = new ArrayList<Region_confModel>(); // 省
	private List<Region_confModel> mListRegionCity = new ArrayList<Region_confModel>(); // 市
	private List<Region_confModel> mListRegionArea = new ArrayList<Region_confModel>(); // 区域

	private ArrayAdapter<Region_confModel> mAdapterCountry, mAdapterProvince, mAdapterCity, mAdapterArea;
	private Spinner mSpnCountry, mSpnProvince, mSpnCity, mSpnArea;

	private int mSelectIndexCountry, mSelectIndexProvince, mSelectIndexCity, mSelectIndexArea;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_add_delivery_address);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		initTitle();
		initViewsState();
		registeClick();
		bindData();
	}

	private void bindData()
	{
		if (mDeliveryModel != null)
		{
			if (!TextUtils.isEmpty(mDeliveryModel.getConsignee()))
			{
				mEtConsignee.setText(mDeliveryModel.getConsignee());
			}

			if (!TextUtils.isEmpty(mDeliveryModel.getPhone()))
			{
				mEtPhone.setText(mDeliveryModel.getPhone());
			}

			if (!TextUtils.isEmpty(mDeliveryModel.getDelivery()))
			{
				mTvDelivery.setText(mDeliveryModel.getDelivery());
			}

			if (!TextUtils.isEmpty(mDeliveryModel.getDelivery_detail()))
			{
				mEtAddressDetail.setText(mDeliveryModel.getDelivery_detail());
			}

			if (!TextUtils.isEmpty(mDeliveryModel.getPostcode()))
			{
				mEtPostcode.setText(mDeliveryModel.getPostcode());
			}
		}

	}

	private void initViewsState()
	{
		if (mDeliveryModel == null)
		{
			mTvSave.setText("添加");
		} else
		{
			mTvSave.setText("保存");
		}

	}

	private void initIntentData()
	{
		mDeliveryModel = (DeliveryModel) getIntent().getSerializableExtra(EXTRA_DELIVERY_MODEL);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				clickDeleteAddress();
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

		if (mDeliveryModel == null)
		{
			mTitleSimple.setTitleTop("创建收货地址");
		} else
		{
			mTitleSimple.setTitleTop("编辑收货地址");
			mTitleSimple.setRightText("删除");
		}
	}

	/**
	 * 删除地址
	 */
	protected void clickDeleteAddress()
	{
		if (mDeliveryModel != null)
		{
			// TODO 开始删除收货地址
			Map<String, Object> mapData = getRequestMap(mDeliveryModel, "del_addr");
			if (mapData != null)
			{
				RequestModel model = new RequestModel(mapData);
				RequestCallBack<String> handler = new RequestCallBack<String>()
				{
					@Override
					public void onStart()
					{
						AppHelper.showLoadingDialog("请稍候...");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo)
					{
						setResult(RESULT_CODE_DELETE_ADDRESS_SUCCESS);
						finish();
						/*
						BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
						if (!SDInterfaceUtil.isActModelNull(actModel))
						{
							if (actModel.getResponse_code() == 1)
							{
								setResult(RESULT_CODE_DELETE_ADDRESS_SUCCESS);
								finish();
							}
						}
						*/
					}

					@Override
					public void onFailure(HttpException error, String msg)
					{

					}

					@Override
					public void onFinish()
					{
						AppHelper.hideLoadingDialog();
					}

				};
				
				InterfaceServer.getInstance().requestInterface(model, handler);
			}
		}

	}

	/**
	 * 获得请求参数
	 * 
	 * @param model
	 * @param act
	 * @return
	 */
	private Map<String, Object> getRequestMap(DeliveryModel model, String act)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (model != null && user != null)
		{
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("act", act);// "add_addr");
			map.put("email", user.getUser_name());
			map.put("pwd", user.getUser_pwd());

			map.put("id", model.getId());

			map.put("region_lv1", model.getRegion_lv1());// 国家
			map.put("region_lv2", model.getRegion_lv2());// 省
			map.put("region_lv3", model.getRegion_lv3());// 城市
			map.put("region_lv4", model.getRegion_lv4());// 地区/县

			map.put("consignee", model.getConsignee());// 联系人姓名
			map.put("delivery_detail", model.getDelivery_detail());// 详细地址
			map.put("phone", model.getPhone());// 手机号码
			map.put("postcode", model.getPostcode());// 邮编
			return map;
		} else
		{
			return null;
		}
	}

	private void registeClick()
	{
		mTvDelivery.setOnClickListener(this);
		mTvSave.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_add_delivery_address_tv_delivery:
			clickDelivery();
			break;

		case R.id.act_add_delivery_address_tv_save:
			clickSave();
			break;

		default:
			break;
		}
	}

	/**
	 * 保存地址
	 */
	private void clickSave()
	{
		if (validateParam())
		{
			Map<String, Object> mapData = getRequestMap(mDeliveryModel, "add_addr");
			RequestModel model = new RequestModel(mapData);
			
			RequestCallBack<String> handler = new RequestCallBack<String>()
					{
						@Override
						public void onStart()
						{
							AppHelper.showLoadingDialog("请稍候...");
						}

						@Override
						public void onSuccess(ResponseInfo<String> responseInfo)
						{
							System.out.println("Save: "+responseInfo.result);
							setResult(RESULT_CODE_ADD_ADDRESS_SUCCESS);
							finish();
							/*
							Add_addrActModel actModel = (Add_addrActModel)JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
							
							if (!SDInterfaceUtil.isActModelNull(actModel))
							{
								System.out.println("Save: "+responseInfo.result+" Code: "+actModel.getResponse_code());
								if (actModel.getResponse_code() == 1)
								{
									setResult(RESULT_CODE_ADD_ADDRESS_SUCCESS);
									finish();
								}
							}
							*/
						}

						@Override
						public void onFailure(HttpException error, String msg)
						{
							System.out.println("Error: "+msg);
						}

						@Override
						public void onFinish()
						{
							AppHelper.hideLoadingDialog();
						}

					};
			
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	/**
	 * 验证输入参数
	 * 
	 * @return
	 */
	private boolean validateParam()
	{
		if (mDeliveryModel == null) // 是添加模式
		{
			mDeliveryModel = new DeliveryModel();
		}

		String consignee = mEtConsignee.getText().toString().trim();
		if (TextUtils.isEmpty(consignee)) // 名字为空
		{
			SDToast.showToast("姓名不能为空");
			return false;
		} else
		{
			if (consignee.length() < 2)
			{
				SDToast.showToast("姓名不能少于2个字");
				return false;
			}
		}
		mDeliveryModel.setConsignee(consignee);

		String phone = mEtPhone.getText().toString().trim();
		if (TextUtils.isEmpty(phone)) // 手机号为空
		{
			SDToast.showToast("电话号码不能为空");
			return false;
		} else
		{
			if (phone.length() < 7)
			{
				SDToast.showToast("电话号码不能少于7位");
				return false;
			}
		}
		mDeliveryModel.setPhone(phone);

		if (TextUtils.isEmpty(mDeliveryModel.getRegion_lv1()) || TextUtils.isEmpty(mDeliveryModel.getRegion_lv2()) || TextUtils.isEmpty(mDeliveryModel.getRegion_lv3())
				|| TextUtils.isEmpty(mDeliveryModel.getRegion_lv4()))
		{
			SDToast.showToast("请选择省市地区");
			return false;
		}

		String addressDetail = mEtAddressDetail.getText().toString().trim();
		if (TextUtils.isEmpty(addressDetail)) // 详细地址为空
		{
			SDToast.showToast("详细地址不能为空");
			return false;
		}
		mDeliveryModel.setDelivery_detail(addressDetail);

		String postCode = mEtPostcode.getText().toString().trim();
		if (TextUtils.isEmpty(postCode)) // 邮政编码为空
		{
			SDToast.showToast("邮政编码不能为空");
			return false;
		} else
		{
			if (postCode.length() < 6)
			{
				SDToast.showToast("邮政编码不能少于6位");
				return false;
			}
		}
		mDeliveryModel.setPostcode(postCode);

		return true;
	}

	/**
	 * 加载省市区地址列表
	 */
	private void clickDelivery()
	{
		loadRegionsDataFromDb();
		if (mListRegionCountry != null && mListRegionCountry.size() > 0)
		{
			showSelectRegionDialog();
		} else
		{
			requestRegionList();
		}

	}

	/**
	 * 弹出选择区域窗口
	 */
	private void showSelectRegionDialog()
	{
		findSelectIndexs();
		View view = getLayoutInflater().inflate(R.layout.dialog_select_region, null);

		mSpnCountry = (Spinner) view.findViewById(R.id.dialog_select_region_spn_country);
		mSpnProvince = (Spinner) view.findViewById(R.id.dialog_select_region_spn_province);
		mSpnCity = (Spinner) view.findViewById(R.id.dialog_select_region_spn_city);
		mSpnArea = (Spinner) view.findViewById(R.id.dialog_select_region_spn_area);

		// mSpnCountry.setPrompt("请选择");
		// mSpnProvince.setPrompt("请选择");
		// mSpnCity.setPrompt("请选择");
		// mSpnArea.setPrompt("请选择");

		if (mListRegionCountry != null)
		{
			mAdapterCountry = new ArrayAdapter<Region_confModel>(getApplicationContext(), android.R.layout.simple_spinner_item, mListRegionCountry);
			mAdapterCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpnCountry.setAdapter(mAdapterCountry);
			if (mSelectIndexCountry < mListRegionCountry.size())
			{
				mSpnCountry.setSelection(mSelectIndexCountry, true);
			}
		}

		if (mListRegionProvince != null)
		{
			mAdapterProvince = new ArrayAdapter<Region_confModel>(getApplicationContext(), android.R.layout.simple_spinner_item, mListRegionProvince);
			mAdapterProvince.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpnProvince.setAdapter(mAdapterProvince);
			if (mSelectIndexProvince < mListRegionProvince.size())
			{
				mSpnProvince.setSelection(mSelectIndexProvince, true);
			}
		}

		if (mListRegionCity != null)
		{
			mAdapterCity = new ArrayAdapter<Region_confModel>(getApplicationContext(), android.R.layout.simple_spinner_item, mListRegionCity);
			mAdapterCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpnCity.setAdapter(mAdapterCity);
			if (mSelectIndexCity < mListRegionCity.size())
			{
				mSpnCity.setSelection(mSelectIndexCity, true);
			}
		}

		if (mListRegionArea != null)
		{
			mAdapterArea = new ArrayAdapter<Region_confModel>(getApplicationContext(), android.R.layout.simple_spinner_item, mListRegionArea);
			mAdapterArea.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpnArea.setAdapter(mAdapterArea);
			if (mSelectIndexArea < mListRegionArea.size())
			{
				mSpnArea.setSelection(mSelectIndexArea, true);
			}
		}

		mSpnCountry.setOnItemSelectedListener(new RegionSpinnersOnItemSelectedListener());
		mSpnProvince.setOnItemSelectedListener(new RegionSpinnersOnItemSelectedListener());
		mSpnCity.setOnItemSelectedListener(new RegionSpinnersOnItemSelectedListener());
		mSpnArea.setOnItemSelectedListener(new RegionSpinnersOnItemSelectedListener());

		final Dialog dialog = SDDialogUtil.showView("所属区域选择", view, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if (dialog != null)
				{
					dialog.dismiss();
				}
				if (mDeliveryModel == null) // 是添加模式
				{
					mDeliveryModel = new DeliveryModel();
				}
				if (mAdapterArea != null && mAdapterCity != null && mAdapterCountry != null && mAdapterProvince != null)
				{
					Region_confModel modelArea = (Region_confModel) mSpnArea.getSelectedItem();
					Region_confModel modelCity = (Region_confModel) mSpnCity.getSelectedItem();
					Region_confModel modelCountry = (Region_confModel) mSpnCountry.getSelectedItem();
					Region_confModel modelProvince = (Region_confModel) mSpnProvince.getSelectedItem();
					if (modelArea != null && modelCity != null && modelCountry != null && modelProvince != null)
					{
						String nameCountry = modelCountry.getName();
						String nameProvince = modelProvince.getName();
						String nameCity = modelCity.getName();
						String nameArea = modelArea.getName();
						if (!TextUtils.isEmpty(nameCountry) && !TextUtils.isEmpty(nameProvince) && !TextUtils.isEmpty(nameCity) && !TextUtils.isEmpty(nameArea))
						{
							mTvDelivery.setText(nameCountry + " " + nameProvince + " " + nameCity + " " + nameArea);
						}

						String idCountry = modelCountry.getId();
						String idProvince = modelProvince.getId();
						String idCity = modelCity.getId();
						String idArea = modelArea.getId();
						if (!TextUtils.isEmpty(idCountry) && !TextUtils.isEmpty(idProvince) && !TextUtils.isEmpty(idCity) && !TextUtils.isEmpty(idArea))
						{
							mDeliveryModel.setRegion_lv1(idCountry);
							mDeliveryModel.setRegion_lv2(idProvince);
							mDeliveryModel.setRegion_lv3(idCity);
							mDeliveryModel.setRegion_lv4(idArea);
						}

					}
				}
			}
		}, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if (dialog != null)
				{
					dialog.dismiss();
				}
			}
		});

	}

	/**
	 * 地区列表item选中监听
	 * 
	 * @author js02
	 * 
	 */
	class RegionSpinnersOnItemSelectedListener implements OnItemSelectedListener
	{
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
		{
			Region_confModel model = null;
			switch (parent.getId())
			{
			case R.id.dialog_select_region_spn_country:
				if (mAdapterCountry != null)
				{
					model = mAdapterCountry.getItem((int) id);
					loadRegionDataBySpnSelectChange(model, 3);
				}
				break;
			case R.id.dialog_select_region_spn_province:
				if (mAdapterProvince != null)
				{
					model = mAdapterProvince.getItem((int) id);
					loadRegionDataBySpnSelectChange(model, 2);
				}
				break;
			case R.id.dialog_select_region_spn_city:
				if (mAdapterCity != null)
				{
					model = mAdapterCity.getItem((int) id);
					loadRegionDataBySpnSelectChange(model, 1);
				}
				break;
			case R.id.dialog_select_region_spn_area:
				if (mAdapterArea != null)
				{
					model = mAdapterArea.getItem((int) id);
				}
				break;

			default:
				break;
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent)
		{
			// TODO Auto-generated method stub

		}
	}

	/**
	 * 请求地区列表文件接口
	 */
	private void requestRegionList()
	{
		RequestModel model = new RequestModel();
		model.put("act", "down_region_conf");
		
		RequestCallBack<String> handler = new RequestCallBack<String>()
				{

					@Override
					public void onStart()
					{
						AppHelper.showLoadingDialog("请稍候，正在请求地区列表数据...");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo)
					{
						Down_region_confActModel actModel = JsonUtil.json2Object(responseInfo.result, Down_region_confActModel.class);
						if (!SDInterfaceUtil.isActModelNull(actModel))
						{
							if (actModel.getResponse_code() == 1)
							{
								RegionSaver regionSaver = new RegionSaver();
								regionSaver.setmListener(new AddDeliveryAddressActivity_RegionSaverListener());
								regionSaver.save(actModel);
							}
						}
					}

					@Override
					public void onFinish()
					{
						AppHelper.hideLoadingDialog();
					}
				};

		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	/**
	 * 从数据库获取地区列表数据
	 */
	private void loadRegionsDataFromDb()
	{
		if (SDCollectionUtil.isListHasData(mListRegionArea) && SDCollectionUtil.isListHasData(mListRegionCity) && SDCollectionUtil.isListHasData(mListRegionCountry)
				&& SDCollectionUtil.isListHasData(mListRegionProvince))
		{

		} else
		// 如果当前有的集合中没有数据才从数据库中获取
		{
			List<Region_confModel> listModels = Region_confModelDao.getInstance().getCountryList();
			if (listModels != null)
			{
				mListRegionCountry.clear();
				mListRegionCountry.addAll(listModels);
				if (mListRegionCountry != null && mListRegionCountry.size() > 0)
				{
					loadRegionDataBySpnSelectChange(mListRegionCountry.get(0), 3);
				}
			}
		}
	}

	/**
	 * 找到选中项
	 */
	public void findSelectIndexs()
	{

		if (mDeliveryModel != null)
		{
			String idCountry = mDeliveryModel.getRegion_lv1();
			String idProvince = mDeliveryModel.getRegion_lv2();
			String idCity = mDeliveryModel.getRegion_lv3();
			String idArea = mDeliveryModel.getRegion_lv4();

			if (idCountry != null && idProvince != null && idCity != null && idArea != null)
			{
				List<Region_confModel> listModelsProvince = Region_confModelDao.getInstance().getListByPid(idCountry);
				if (listModelsProvince != null)
				{
					mListRegionProvince.clear();
					mListRegionProvince.addAll(listModelsProvince);
				}

				List<Region_confModel> listModelsCity = Region_confModelDao.getInstance().getListByPid(idProvince);
				if (listModelsCity != null)
				{
					mListRegionCity.clear();
					mListRegionCity.addAll(listModelsCity);
				}

				List<Region_confModel> listModelsArea = Region_confModelDao.getInstance().getListByPid(idCity);
				if (listModelsArea != null)
				{
					mListRegionArea.clear();
					mListRegionArea.addAll(listModelsArea);
				}

				Region_confModel model = null;
				if (mListRegionCountry != null && mListRegionCountry.size() > 0)
				{
					for (int i = 0; i < mListRegionCountry.size(); i++)
					{
						model = mListRegionCountry.get(i);
						if (model != null && idCountry.equals(model.getId()))
						{
							mSelectIndexCountry = i;
							break;
						}
					}
				}
				if (mListRegionProvince != null && mListRegionProvince.size() > 0)
				{
					for (int i = 0; i < mListRegionProvince.size(); i++)
					{
						model = mListRegionProvince.get(i);
						if (model != null && idProvince.equals(model.getId()))
						{
							mSelectIndexProvince = i;
							break;
						}
					}
				}
				if (mListRegionCity != null && mListRegionCity.size() > 0)
				{
					for (int i = 0; i < mListRegionCity.size(); i++)
					{
						model = mListRegionCity.get(i);
						if (model != null && idCity.equals(model.getId()))
						{
							mSelectIndexCity = i;
							break;
						}
					}
				}
				if (mListRegionArea != null && mListRegionArea.size() > 0)
				{
					for (int i = 0; i < mListRegionArea.size(); i++)
					{
						model = mListRegionArea.get(i);
						if (model != null && idArea.equals(model.getId()))
						{
							mSelectIndexArea = i;
							break;
						}
					}
				}
			}
		}
	}

	private void loadRegionDataBySpnSelectChange(Region_confModel model, int level)
	{
		if (model != null && level >= 1 && level <= 3)
		{
			List<Region_confModel> listModels = null;
			switch (level)
			{
			case 1:
				listModels = Region_confModelDao.getInstance().getListByParentModel(model);
				if (listModels != null)
				{
					mListRegionArea.clear();
					mListRegionArea.addAll(listModels);
				}
				if (mAdapterArea != null)
				{
					mAdapterArea.notifyDataSetChanged();
					mSpnArea.setSelection(0, true);
				}
				break;
			case 2:
				listModels = Region_confModelDao.getInstance().getListByParentModel(model);
				if (listModels != null)
				{
					mListRegionCity.clear();
					mListRegionCity.addAll(listModels);
				}
				if (mAdapterCity != null)
				{
					mAdapterCity.notifyDataSetChanged();
					mSpnCity.setSelection(0, true);
				}
				if (mListRegionCity != null && mListRegionCity.size() > 0)
				{
					loadRegionDataBySpnSelectChange(mListRegionCity.get(0), --level);
				}
				break;
			case 3:
				listModels = Region_confModelDao.getInstance().getListByParentModel(model);
				if (listModels != null)
				{
					mListRegionProvince.clear();
					mListRegionProvince.addAll(listModels);
				}
				if (mAdapterProvince != null)
				{
					mAdapterProvince.notifyDataSetChanged();
					mSpnProvince.setSelection(0, true);
				}
				if (mListRegionProvince != null && mListRegionProvince.size() > 0)
				{
					loadRegionDataBySpnSelectChange(mListRegionProvince.get(0), --level);
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 下载地区列表文件并存到数据库之后的操作
	 * 
	 */
	class AddDeliveryAddressActivity_RegionSaverListener implements com.wenzhoujie.work.RegionSaver.RegionSaverListener
	{

		@Override
		public void onDownLoadFail()
		{
			//hideLoadingDialog();
			SDToast.showToast("下载区域文件失败，无法加载区域列表");
		}

		@Override
		public void onDownLoadSuccess()
		{

		}

		@Override
		public void onInsertFail()
		{
			//hideLoadingDialog();
			SDToast.showToast("解压区域文件或者插入数据失败，无法加载区域列表");
		}

		@Override
		public void onInsertSuccess()
		{
			//hideLoadingDialog();
			loadRegionsDataFromDb();
			showSelectRegionDialog();
		}

	}

}