package com.wenzhoujie.work;

import java.util.List;

import android.text.TextUtils;

import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.dao.InitActModelDao;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.model.InitActCitylistModel;
import com.wenzhoujie.model.InitActNewslistModel;
import com.wenzhoujie.model.QuansModel;
import com.wenzhoujie.model.act.InitActModel;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class AppRuntimeWorker
{

	public static InitActModel getInitActModel()
	{
		return InitActModelDao.queryModel();
	}

	/**
	 * 1：有配送地区选择项；0：无
	 * 
	 * @return
	 */
	public static int getHas_region()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			return SDTypeParseUtil.getIntFromString(model.getHas_region(), 0);
		} else
		{
			return 0;
		}
	}

	/**
	 * 1：会员只有一个配送地址；0：会员可以有多个配送地址
	 * 
	 * @return
	 */
	public static int getOnly_one_delivery()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			return SDTypeParseUtil.getIntFromString(model.getOnly_one_delivery(), 0);
		} else
		{
			return 0;
		}
	}

	public static String getSina_app_key()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getSina_app_key();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getSina_app_secret()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getSina_app_secret();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getSina_redirectUrl()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getSina_bind_url();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getTencent_app_key()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getTencent_app_key();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getTencent_app_secret()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getTencent_app_secret();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getTencent_bind_url()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getTencent_bind_url();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getQq_app_key()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getQq_app_key();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getQq_app_secret()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getQq_app_secret();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getWx_app_key()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getWx_app_key();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	public static String getWx_app_secret()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getWx_app_secret();
			if (!TextUtils.isEmpty(content))
			{
				return content;
			}
		}
		return "";
	}

	/**
	 * 获得商圈list（马尾区，仓山区，台江区等）
	 * 
	 * @return
	 */
	public static List<QuansModel> getQuanlist()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			List<QuansModel> content = model.getQuanlist();
			return content;
		}
		return null;
	}

	/**
	 * 获得公告列表
	 * 
	 * @return
	 */
	public static List<InitActNewslistModel> getNewslist()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			List<InitActNewslistModel> content = model.getNewslist();
			return content;
		}
		return null;
	}

	/**
	 * 获得城市列表
	 * 
	 * @return
	 */
	public static List<InitActCitylistModel> getCitylist()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			List<InitActCitylistModel> content = model.getCitylist();
			return content;
		}
		return null;
	}

	/**
	 * 获得热门城市列表
	 * 
	 * @return
	 */
	public static List<InitActCitylistModel> getHotCityList()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			List<InitActCitylistModel> content = model.getHoCitytList();
			return content;
		}
		return null;
	}

	/**
	 * 获得关于内容
	 * 
	 * @return
	 */
	public static String getAbout_info()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getAbout_info();
			return content;
		}
		return "";
	}

	/**
	 * 获得当前城市
	 * 
	 * @return
	 */
	public static String getCity_name()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			String content = model.getCity_name();
			return content;
		}
		return "";
	}

	/**
	 * 设置当前城市，也会设置当前城市id
	 * 
	 * @param cityName
	 * @return
	 */
	public static boolean setCity_name(String cityName)
	{
		int cityId = getCityIdByCityName(cityName);
		InitActModel model = getInitActModel();
		model.setCity_name(cityName);
		model.setCity_id(cityId);
		boolean insertSuccess = InitActModelDao.insertOrUpdateModel(model);
		if (insertSuccess)
		{
			SDEventManager.post(EnumEventTag.CITY_CHANGE.ordinal());
		}
		return insertSuccess;
	}

	/**
	 * 获得当前城市id
	 * 
	 * @return
	 */
	public static int getCity_id() {
		int cityId = 0;
		InitActModel model = getInitActModel();
		if (model != null) {
			cityId = model.getCity_id();
		}
		return cityId;
	}

	/**
	 * 根据城市名字获得城市id，如果未找到返回字符串0
	 * 
	 * @return
	 */
	public static int getCityIdByCityName(String cityName)
	{
		int cityId = -1;
		if (!TextUtils.isEmpty(cityName))
		{
			List<InitActCitylistModel> listCity = getCitylist();
			if (listCity != null && listCity.size() > 0)
			{
				InitActCitylistModel cityModel = null;
				for (int i = 0; i < listCity.size(); i++)
				{
					cityModel = listCity.get(i);
					if (cityModel != null)
					{
						if (cityName.equals(cityModel.getName()))
						{
							cityId = cityModel.getId();
						}
					}
				}
			}
		}
		return cityId;
	}

	public static int getServerRegionVersion()
	{
		InitActModel model = getInitActModel();
		if (model != null)
		{
			return SDTypeParseUtil.getIntFromString(model.getRegion_version(), -1);
		}
		return -1;
	}

}
