package com.wenzhoujie.work;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.wenzhoujie.dao.Region_confModelDao;
import com.wenzhoujie.dao.DbHelper;
import com.wenzhoujie.model.act.Down_region_confActModel;
import com.wenzhoujie.utils.SDHandlerUtil;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.XZip;
import com.ta.util.download.DownLoadCallback;
import com.ta.util.download.DownloadManager;

public class RegionSaver
{

	private RegionSaverListener mListener = null;

	public RegionSaverListener getmListener()
	{
		return mListener;
	}

	public void setmListener(RegionSaverListener mListener)
	{
		this.mListener = mListener;
	}

	public void save(Down_region_confActModel model)
	{
		if (model != null)
		{
			int fileExist = SDTypeParseUtil.getIntFromString(model.getFile_exists(), 0);
			if (fileExist == 1 && !TextUtils.isEmpty(model.getFile_url()))
			{
				DownloadManager.getDownloadManager().setDownLoadCallback(new DownLoadCallback()
				{

					@Override
					public void onSuccess(String url, File file)
					{
						// TODO Auto-generated method stub
						SDToast.showToast("下载成功");
						if (mListener != null)
						{
							mListener.onDownLoadSuccess();
						}
						new Thread(new SaveRegionRunnable(file)).start();
						super.onSuccess(url, file);
					}

					@Override
					public void onFailure(String url, String strMsg)
					{
						if (mListener != null)
						{
							mListener.onDownLoadFail();
						}
						super.onFailure(url, strMsg);
					}
				});
				DownloadManager.getDownloadManager().addHandler(model.getFile_url(), null);
			}
		}
	}

	class SaveRegionRunnable implements Runnable
	{
		private File zipFile = null;

		public SaveRegionRunnable(File zipFile)
		{
			super();
			this.zipFile = zipFile;
		}

		@Override
		public void run()
		{
			unZipFile(zipFile);
		}

	}

	private void unZipFile(File zipFile)
	{
		boolean insertSuccess = false;
		if (zipFile != null && zipFile.exists())
		{
			SQLiteDatabase db = null;
			try
			{
				System.out.println("Path: "+zipFile.getAbsolutePath());
				
				InputStream in = XZip.UpZip(zipFile.getAbsolutePath(), "region_conf.txt");
				BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
				String lineString = null;
				Region_confModelDao.getInstance().deleteAllData();
				db = DbHelper.getInstance().getWritableDatabase();
				db.beginTransaction();
				//db.execSQL("create table region_conf(id INT PRIMARY KEY, pid INT, name CHAR(50), postcode CHAR(50), py CHAR(50));");
				while ((lineString = br.readLine()) != null)
				{
					db.execSQL(lineString);
				}
				db.setTransactionSuccessful();
				SDToast.showToast("解压保存成功");
				insertSuccess = true;
			} catch (Exception e)
			{
				if (mListener != null)
				{
					mListener.onInsertFail();
				}
				e.printStackTrace();
			} finally
			{
				if (db != null)
				{
					db.endTransaction();
				}
			}
			if (insertSuccess)
			{
				if (mListener != null)
				{
					SDHandlerUtil.runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							mListener.onInsertSuccess();
						}
					});
				}
			}
		}
	}

	public interface RegionSaverListener
	{
		public void onDownLoadFail();

		public void onDownLoadSuccess();

		public void onInsertFail();

		public void onInsertSuccess();

	}

}
