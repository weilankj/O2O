package com.wenzhoujie.work;

import android.content.DialogInterface;
import android.view.View;

import com.wenzhoujie.app.App;
import com.wenzhoujie.common.CommonInterface;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.library.dialog.SDDialogConfirm;
import com.wenzhoujie.library.dialog.SDDialogCustom;
import com.wenzhoujie.library.dialog.SDDialogCustom.SDDialogCustomListener;
import com.wenzhoujie.library.utils.LogUtil;
import com.wenzhoujie.library.utils.SDTimer;
import com.wenzhoujie.library.utils.SDTimer.SDTimerListener;
import com.wenzhoujie.listener.RequestInitListener;
import com.wenzhoujie.model.act.InitActModel;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.sunday.eventbus.SDEventManager;
import com.ta.util.netstate.TANetChangeObserver;
import com.ta.util.netstate.TANetWorkUtil;
import com.ta.util.netstate.TANetWorkUtil.netType;
import com.ta.util.netstate.TANetworkStateReceiver;

public class RetryInitWorker implements TANetChangeObserver
{

	private static final int RETRY_TIME_SPAN = 1000 * 30;
	private static final int RETRY_MAX_COUNT = 10;

	private static RetryInitWorker mInstance;
	private SDTimer mTimer = new SDTimer();
	private boolean mIsInRetryInit = false;
	private boolean mIsInitSuccess = false;
	private int mRetryCount = 0;

	private RetryInitWorker()
	{
		TANetworkStateReceiver.registerObserver(this);
	}

	public static RetryInitWorker getInstance()
	{
		if (mInstance == null)
		{
			syncInit();
		}
		return mInstance;
	}

	private synchronized static void syncInit()
	{
		if (mInstance == null)
		{
			mInstance = new RetryInitWorker();
		}
	}

	public void start()
	{
		if (mIsInRetryInit || mIsInitSuccess)
		{
			return;
		}
		mIsInRetryInit = true;
		mRetryCount = 0;
		mTimer.startWork(0, RETRY_TIME_SPAN, new SDTimerListener()
		{

			@Override
			public void onWorkMain()
			{
				// 达到最大重试次数，并且没有初始化成功
				if (mRetryCount >= RETRY_MAX_COUNT && !mIsInitSuccess)
				{
					stop();
					showRetryFailDialog();
					return;
				}
				mRetryCount++;
				LogUtil.i("retry:" + mIsInitSuccess);
				if (TANetWorkUtil.isNetworkAvailable(App.getApplication()))
				{
					if (!mIsInitSuccess)
					{
						CommonInterface.requestInit(mListener);
					} else
					{
						stop();
						SDEventManager.post(EnumEventTag.RETRY_INIT_SUCCESS.ordinal());
					}
				} else
				{
					stop();
				}
			}

			@Override
			public void onWork()
			{
			}
		});
	}

	protected void showRetryFailDialog()
	{
		SDDialogConfirm dialog = new SDDialogConfirm();
		dialog.setCancelable(false);
		dialog.setTextContent("已经尝试初始化" + RETRY_MAX_COUNT + "次失败，是否继续重试？");
		dialog.setTextConfirm("重试").setmListener(new SDDialogCustomListener()
		{

			@Override
			public void onDismiss(DialogInterface iDialog,SDDialogCustom dialog)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onClickConfirm(View v, SDDialogCustom dialog)
			{
				start();

			}

			@Override
			public void onClickCancel(View v, SDDialogCustom dialog)
			{
				stop();
			}
		});
		dialog.show();
	}

	public void stop()
	{
		mTimer.stopWork();
		mIsInRetryInit = false;
		LogUtil.i("stop retry");
	}

	private RequestInitListener mListener = new RequestInitListener()
	{

		@Override
		public void onSuccess(ResponseInfo<String> responseInfo, InitActModel model)
		{
			mIsInitSuccess = true;
		}

		@Override
		public void onStart()
		{

		}

		@Override
		public void onFinish()
		{

		}

		@Override
		public void onFailure(HttpException error, String msg)
		{

		}
	};

	@Override
	public void onConnect(netType type)
	{
		if (!mIsInitSuccess)
		{
			start();
		}
	}

	@Override
	public void onDisConnect()
	{
		// TODO Auto-generated method stub

	}

}
