package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.TransitRouteLine;
import com.baidu.mapapi.search.route.WalkingRouteLine;
import com.wenzhoujie.library.customview.SlidingDrawer;
import com.wenzhoujie.library.customview.SlidingDrawer.OnDrawerCloseListener;
import com.wenzhoujie.library.customview.SlidingDrawer.OnDrawerOpenListener;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.wenzhoujie.adapter.RouteStepDetaiAdapter;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.model.MapBaseRouteModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 线路详情
 * 
 * @author js02
 * 
 */
public class RouteDetailActivity extends BaseBaiduMapActivity
{

	public static final String EXTRA_MODEL_MAPBASEROUTEMODEL = "extra_model_mapbaseroutemodel";

	public static final String EXTRA_ROUTE_BUS_INDEX = "extra_route_bus_index";
	public static final String EXTRA_ROUTE_DRIVING_INDEX = "extra_route_driving_index";
	public static final String EXTRA_ROUTE_WALKING_INDEX = "extra_route_walking_index";

	@ViewInject(id = R.id.act_route_detail_sd_drawer)
	private SlidingDrawer mSdDrawer;

	@ViewInject(id = R.id.act_route_detail_iv_drawer_arrow)
	private ImageView mIvDrawerArrow;

	@ViewInject(id = R.id.act_route_detail_ll_drawer_handle)
	private LinearLayout mLlDrawerHandle;

	@ViewInject(id = R.id.act_route_detail_tv_name)
	private TextView mTvName;

	@ViewInject(id = R.id.act_route_detail_tv_time)
	private TextView mTvTime;

	@ViewInject(id = R.id.act_route_detail_tv_distance)
	private TextView mTvDistance;

	@ViewInject(id = R.id.act_route_detail_ll_drawer_content)
	private LinearLayout mLlDrawerContent;

	@ViewInject(id = R.id.act_route_detail_lv_content)
	private ListView mLvContent;

	private List<String> mListStep = new ArrayList<String>();

	private RouteStepDetaiAdapter mAdapter = null;

	private MapBaseRouteModel mModel = null;

	private int mBusIndex;
	private int mDrivingIndex;
	private int mWalkingIndex;

	private LatLng mLlStart = null;
	private LatLng mLlEnd = null;

	private RouteLine mRouteLine = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		setmTitleType(TitleType.TITLE_SIMPLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_route_detail);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		getIntentData();
		initSlidingDrawer();
		bindDefaultData();
	}

	private void initSlidingDrawer()
	{
		mSdDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener()
		{

			@Override
			public void onDrawerOpened()
			{
				mIvDrawerArrow.setImageResource(R.drawable.ic_arrow_down_roange_open);
			}
		});

		mSdDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener()
		{

			@Override
			public void onDrawerClosed()
			{
				mIvDrawerArrow.setImageResource(R.drawable.ic_arrow_up_roange_open);
			}
		});

	}

	@Override
	public void onMapLoaded()
	{
		super.onMapLoaded();
		startLocation(false, true);
	}

	private void bindDefaultData()
	{
		mAdapter = new RouteStepDetaiAdapter(mListStep, this);
		mLvContent.setAdapter(mAdapter);
		mLvContent.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long id)
			{
				mAdapter.setSelectPos((int) id);
				clickNode((int) id);
			}
		});
		clickNode(0);
		mSdDrawer.setTopOffset(SDViewUtil.getScreenHeight() / 2);
		mSdDrawer.open();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				startLocation(true);
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("线路详情");
		mTitleSimple.setRightText("当前位置");
	}

	private void getIntentData()
	{
		mModel = (MapBaseRouteModel) getIntent().getSerializableExtra(EXTRA_MODEL_MAPBASEROUTEMODEL);
		if (mModel != null)
		{
			SDViewBinder.setTextView(mTvName, mModel.getName());
			SDViewBinder.setTextView(mTvTime, mModel.getTime());
			SDViewBinder.setTextView(mTvDistance, mModel.getDistance());
			List<String> listStep = mModel.getListStep();
			if (listStep != null)
			{
				mListStep = listStep;
			}
		}

		mBusIndex = getIntent().getIntExtra(EXTRA_ROUTE_BUS_INDEX, -1);
		mDrivingIndex = getIntent().getIntExtra(EXTRA_ROUTE_DRIVING_INDEX, -1);
		mWalkingIndex = getIntent().getIntExtra(EXTRA_ROUTE_WALKING_INDEX, -1);

		if (mBusIndex >= 0)
		{
			addBusRoute(mBusIndex);
		}

		if (mDrivingIndex >= 0)
		{
			addDrivingRoute(mDrivingIndex);
		}

		if (mWalkingIndex >= 0)
		{
			addWalkingRoute(mWalkingIndex);
		}

		if (mLlStart != null)
		{
			focusMapTo(mLlStart, true);
		}
	}

	private void addBusRoute(int index)
	{
		List<TransitRouteLine> listRoute = BaiduMapManager.getInstance().getmListTransitRouteLine();
		if (listRoute != null && listRoute.size() > index && index >= 0)
		{
			TransitRouteLine line = listRoute.get(index);
			if (line != null)
			{
				mLlStart = BaiduMapManager.getInstance().getLatLngFromRouteLineStep(line, 0);
				mLlEnd = BaiduMapManager.getInstance().getLatLngFromRouteLineStep(line, -1);
				mRouteLine = line;
				addRouteOverlayBus(line);
			}
		}
	}

	private void addDrivingRoute(int index)
	{
		List<DrivingRouteLine> listRoute = BaiduMapManager.getInstance().getmListDrivingRouteLine();
		if (listRoute != null && listRoute.size() > index && index >= 0)
		{
			DrivingRouteLine line = listRoute.get(index);
			if (line != null)
			{
				mLlStart = BaiduMapManager.getInstance().getLatLngFromRouteLineStep(line, 0);
				mLlEnd = BaiduMapManager.getInstance().getLatLngFromRouteLineStep(line, -1);
				mRouteLine = line;
				addRouteOverlayDriving(line);
			}
		}
	}

	private void addWalkingRoute(int index)
	{
		List<WalkingRouteLine> listRoute = BaiduMapManager.getInstance().getmListWalkingRouteLine();
		if (listRoute != null && listRoute.size() > index && index >= 0)
		{
			WalkingRouteLine line = listRoute.get(index);
			if (line != null)
			{
				mLlStart = BaiduMapManager.getInstance().getLatLngFromRouteLineStep(line, 0);
				mLlEnd = BaiduMapManager.getInstance().getLatLngFromRouteLineStep(line, -1);
				mRouteLine = line;
				addRouteOverlayWalking(line);
			}
		}
	}

	@Override
	public boolean onBusRouteNodeClick(int index)
	{
		clickNode(index);
		return true;
	}

	@Override
	public boolean onDrivingRouteNodeClick(int index)
	{
		clickNode(index);
		return true;
	}

	@Override
	public boolean onWalkingRouteNodeClick(int index)
	{
		clickNode(index);
		return true;
	}

	private void clickNode(int index)
	{
		if (mListStep != null && mListStep.size() > 0 && mListStep.size() > index)
		{
			String name = mListStep.get(index);
			LatLng ll = BaiduMapManager.getInstance().getLatLngFromRouteLineStep(mRouteLine, index);
			if (name != null && ll != null)
			{
				View view = getLayoutInflater().inflate(R.layout.pop_route_node, null);
				TextView tvContent = (TextView) view.findViewById(R.id.pop_route_node_tv_content);
				tvContent.setText(name);

				if (mInfoWindow != null)
				{
					mBaiduMap.hideInfoWindow();
				}
				mInfoWindow = new InfoWindow(view, ll, null);
				mBaiduMap.showInfoWindow(mInfoWindow);
				focusMapTo(ll, true);
			}
		}

	}

}
