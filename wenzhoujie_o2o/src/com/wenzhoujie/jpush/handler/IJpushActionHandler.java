package com.wenzhoujie.jpush.handler;

import android.content.Intent;

public interface IJpushActionHandler
{

	public void handle(Intent intent);

}
