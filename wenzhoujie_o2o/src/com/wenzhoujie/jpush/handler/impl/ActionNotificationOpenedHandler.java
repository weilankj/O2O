package com.wenzhoujie.jpush.handler.impl;

import android.content.Intent;
import android.os.Bundle;
import cn.jpush.android.api.JPushInterface;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wenzhoujie.library.utils.SDPackageUtil;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.EventsDetailActivity;
import com.wenzhoujie.MerchantDetailNewActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.WebViewActivity;
import com.wenzhoujie.YouHuiDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.constant.Constant.PushType;
import com.wenzhoujie.event.EventTagJpush;
import com.wenzhoujie.jpush.handler.IJpushActionHandler;

public class ActionNotificationOpenedHandler implements IJpushActionHandler
{

	@Override
	public void handle(Intent intent)
	{
		Bundle bundle = intent.getExtras();

		if (bundle != null)
		{
			String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
			if (extras != null)
			{
				JSONObject obj = JSON.parseObject(extras);
				if (obj != null)
				{
					int type = obj.getIntValue("type");
					String data = obj.getString("data");
					String title = obj.getString("title");
					switch (type)
					{
					case PushType.NORMAL:
						SDPackageUtil.startCurrentApp();
						break;
					case PushType.PROJECT_ID:
						SDPackageUtil.startCurrentApp();
						break;
					case PushType.ARTICLE_ID:
						break;
					case PushType.URL:
						Intent iUrl = new Intent(App.getApplication(), WebViewActivity.class);
						iUrl.putExtra(WebViewActivity.EXTRA_TITLE, title);
						iUrl.putExtra(WebViewActivity.EXTRA_URL, data);
						iUrl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startWebViewActivity(iUrl);
						break;
					case PushType.TUANDETAIL_ID:
						SDEventManager.post(EventTagJpush.EVENT_TUANDETAIL_ID);
						Intent iTuan = new Intent();
						iTuan.setClass(App.getApplication(), TuanDetailActivity.class);
						iTuan.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, data);
						iTuan.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 1);
						iTuan.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						App.getApplication().startActivity(iTuan);
						break;
					case PushType.ECSHOPDETAIL_ID:
						SDEventManager.post(EventTagJpush.EVENT_TUANDETAIL_ID);
						Intent iEcshop = new Intent();
						iEcshop.setClass(App.getApplication(), TuanDetailActivity.class);
						iEcshop.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, data);
						iEcshop.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 0);
						iEcshop.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						App.getApplication().startActivity(iEcshop);
						break;
					case PushType.EVENTSDETAIL_ID:
						SDEventManager.post(EventTagJpush.EVENT_EVENTSDETAIL_ID);
						Intent iEvents = new Intent();
						iEvents.setClass(App.getApplication(), EventsDetailActivity.class);
						iEvents.putExtra(EventsDetailActivity.EXTRA_EVENTS_ID, data);
						iEvents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						App.getApplication().startActivity(iEvents);
						break;
					case PushType.FAVORABLEDETAIL_ID:
						SDEventManager.post(EventTagJpush.EVENT_FAVORABLEDETAIL_ID);
						Intent iFavorable = new Intent();
						iFavorable.setClass(App.getApplication(), YouHuiDetailActivity.class);
						iFavorable.putExtra(YouHuiDetailActivity.EXTRA_YOUHUI_ID, data);
						iFavorable.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						App.getApplication().startActivity(iFavorable);
						break;
					case PushType.MERCHANTDETAIL_ID:
						SDEventManager.post(EventTagJpush.EVENT_MERCHANTDETAIL_ID);
						Intent iMerchant = new Intent();
						iMerchant.setClass(App.getApplication(), MerchantDetailNewActivity.class);
						iMerchant.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, data);
						iMerchant.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						App.getApplication().startActivity(iMerchant);
						break;
					case PushType.VOUCHERDETAIL_ID:
						SDEventManager.post(EventTagJpush.EVENT_TUANDETAIL_ID);
						Intent iVoucher = new Intent();
						iVoucher.setClass(App.getApplication(), TuanDetailActivity.class);
						iVoucher.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, data);
						iVoucher.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 1);
						iVoucher.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						App.getApplication().startActivity(iVoucher);
						break;
					default:
						SDPackageUtil.startCurrentApp();
						break;
					}
				} else
				{
					SDPackageUtil.startCurrentApp();
				}
			} else
			{
				SDPackageUtil.startCurrentApp();
			}

		}
	}

	private void startWebViewActivity(Intent intent)
	{
		App.getApplication().startActivity(intent);
	}

}
