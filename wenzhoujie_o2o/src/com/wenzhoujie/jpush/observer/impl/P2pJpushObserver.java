package com.wenzhoujie.jpush.observer.impl;

import android.content.Intent;

import com.wenzhoujie.jpush.handler.impl.ActionConnectionChangeHandler;
import com.wenzhoujie.jpush.handler.impl.ActionMessageReceivedHandler;
import com.wenzhoujie.jpush.handler.impl.ActionNotificationOpenedHandler;
import com.wenzhoujie.jpush.handler.impl.ActionNotificationReceivedHandler;
import com.wenzhoujie.jpush.handler.impl.ActionRegistrationIdHandler;
import com.wenzhoujie.jpush.handler.impl.ActionRichpushCallbackHandler;
import com.wenzhoujie.jpush.handler.impl.ActionUnKnowHandler;
import com.wenzhoujie.jpush.observer.IJpushObserver;

public class P2pJpushObserver implements IJpushObserver
{

	@Override
	public void onActionRegistrationId(Intent intent)
	{
		new ActionRegistrationIdHandler().handle(intent);
	}

	@Override
	public void onActionMessageReceived(Intent intent)
	{
		new ActionMessageReceivedHandler().handle(intent);
	}

	@Override
	public void onActionNotificationReceived(Intent intent)
	{
		new ActionNotificationReceivedHandler().handle(intent);
	}

	@Override
	public void onActionNotificationOpened(Intent intent)
	{
		new ActionNotificationOpenedHandler().handle(intent);
	}

	@Override
	public void onActionRichpushCallback(Intent intent)
	{
		new ActionRichpushCallbackHandler().handle(intent);
	}

	@Override
	public void onActionConnectionChange(Intent intent)
	{
		new ActionConnectionChangeHandler().handle(intent);
	}

	@Override
	public void onActionUnKnow(Intent intent)
	{
		new ActionUnKnowHandler().handle(intent);
	}

}
