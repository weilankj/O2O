package com.wenzhoujie;

import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lingou.www.R;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.UserShareByUidFragment;
import com.wenzhoujie.utils.IocUtil;

/**
 * 我发表的activity
 * 
 * @author js02
 * 
 */
public class MyPublishActivity extends BaseActivity
{

	private UserShareByUidFragment mFragMyPublish = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_publish);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		addFragments();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("我发表的");
	}

	private void addFragments()
	{
		mFragMyPublish = new UserShareByUidFragment();
		mFragMyPublish.setUid(AppHelper.getLocalUser().getUser_id());
		replaceFragment(mFragMyPublish, R.id.act_my_publish_fl_content);
	}

}