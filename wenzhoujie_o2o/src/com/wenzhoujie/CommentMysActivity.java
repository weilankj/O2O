package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.CommentMysAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.CommentMysActivityItemModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.CommentMysActivityModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class CommentMysActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_comment_mys_lv_list)
	private PullToRefreshListView mLvList = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();
	private List<CommentMysActivityItemModel> mCommentMysItemModel = new ArrayList<CommentMysActivityItemModel>();
	private CommentMysAdapter mAdapter = null;

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_comment_mys);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultLvData();
		initPullRefreshLv();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("评论我的");
	}

	private void bindDefaultLvData()
	{
		mAdapter = new CommentMysAdapter(mCommentMysItemModel, CommentMysActivity.this);
		mLvList.setAdapter(mAdapter);
	}

	private void initPullRefreshLv()
	{
		mLvList.setMode(Mode.BOTH);
		mLvList.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mSearcher.getPageModel().setPage(1);
				requestData(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{

				mCurPage++;
				if (mCurPage > mTotalPage)
				{
					SDToast.showToast("没有更多数据了");
					mLvList.onRefreshComplete();
				} else
				{
					mSearcher.getPageModel().setPage(mCurPage);
					requestData(true);
				}
			}
		});

		mLvList.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				final CommentMysActivityItemModel commentMysItemModel = mAdapter.getItem((int) id);
				Builder b = new AlertDialog.Builder(CommentMysActivity.this);
				b.setItems(new String[] { "回复", "删除" }, new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						// TODO
						switch (which)
						{
						case 0: // 回复
							Dialog d = new MyDialog(CommentMysActivity.this, R.style.MyDialog, commentMysItemModel.getShare_id());
							d.setCanceledOnTouchOutside(true);
							d.show();
							break;
						case 1: // 删除
							DelteComment(commentMysItemModel.getComment_id());
							break;

						default:
							break;
						}
					}
				});
				b.create().show();
			}
		});

		mLvList.setRefreshing();
	}

	private void requestData(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "ucommentlist");
		model.put("email", mSearcher.getEmail());
		model.put("pwd", mSearcher.getPwd());
		model.put("page", mSearcher.getPageModel().getPage());
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				CommentMysActivityModel actModel = JsonUtil.json2Object(responseInfo.result, CommentMysActivityModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (actModel.getPage() != null)
					{
						mCurPage = actModel.getPage().getPage();
						mTotalPage = actModel.getPage().getPage_total();
					}
					if (actModel.getItem() != null)
					{
						if (!isLoadMore)
						{
							mCommentMysItemModel.clear();
						}
						mCommentMysItemModel.addAll(actModel.getItem());
						mAdapter.updateListViewData(mCommentMysItemModel);
					} else
					{
						if (!isLoadMore)
						{
							SDToast.showToast("未找到数据");
							mAdapter.updateListViewData(null);
						} else
						{
							SDToast.showToast("未找到更多数据");
						}
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				mLvList.onRefreshComplete();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	/**
	 * 自定义对话框的类
	 */
	class MyDialog extends Dialog
	{

		private String share_id;

		public MyDialog(Context context, int theme, String share_id)
		{
			super(context, theme);
			this.share_id = share_id;
		}

		@Override
		protected void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			this.setContentView(R.layout.dialog);
			// 退出按钮和返回按钮
			Button submit_button, return_button;
			final EditText content_text;
			// 初始化
			submit_button = (Button) findViewById(R.id.submit_button);
			return_button = (Button) findViewById(R.id.return_button);
			content_text = (EditText) findViewById(R.id.dlg_input_content);

			// 发表
			submit_button.setOnClickListener(new View.OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					if (content_text.getText().toString() == null || "".equals(content_text.getText().toString()))
					{
						Toast.makeText(CommentMysActivity.this, "点评内容为空,发布失败!", Toast.LENGTH_LONG).show();
					} else
					{
						mSearcher.setContent(content_text.getText().toString());
						requestComment(share_id);
						mLvList.setRefreshing();
						dismiss();
					}
				}
			});

			// 返回
			return_button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					// 关闭对话框
					dismiss();
				}
			});
		}

	}

	/**
	 * 发表评论
	 */
	protected void requestComment(String share_id)
	{

		RequestModel model = new RequestModel();
		model.put("act", "addcomment");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("share_id", share_id);
		model.put("source", "来自android客户端");
		model.put("is_relay", 1);
		model.put("parent_id", 0);
		model.put("content", mSearcher.getContent());
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				SDToast.showToast("发布评论成功。");
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				mLvList.setRefreshing();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	/**
	 * 删除评论
	 */
	protected void DelteComment(String comment_id)
	{

		RequestModel model = new RequestModel();
		model.put("act", "delcomment");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("id", comment_id);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				JSONObject json = JSON.parseObject(responseInfo.result);
				if (json.getIntValue("return") == 1)
				{
					Toast.makeText(getApplicationContext(), "温馨提示，删除成功", Toast.LENGTH_SHORT).show();
					mLvList.setRefreshing();
				} else
				{
					Toast.makeText(getApplicationContext(), json.getString("info"), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{

			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}