package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;

import com.wenzhoujie.common.CommonInterface;
import com.wenzhoujie.library.utils.SDHandlerUtil;
import com.wenzhoujie.listener.RequestInitListener;
import com.wenzhoujie.model.act.InitActModel;
import com.wenzhoujie.work.RetryInitWorker;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lingou.www.R;

/**
 * 初始化Activity
 * 
 * @author yhz
 * @createtime 2014-7-3
 */
public class InitActivity extends BaseActivity
{

	private static final boolean NEED_DELAY = true;
	private static final long DEFAULT_DELAY = 1000 * 1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedAnimation = false;
		mIsNeedSlideFinishLayout = false;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_init);
		init();
	}

	private void init()
	{
		requestInitInterface();
	}

	private void requestInitInterface()
	{
		CommonInterface.requestInit(new RequestInitListener()
		{

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo, InitActModel model)
			{

			}

			@Override
			public void onStart()
			{

			}

			@Override
			public void onFinish()
			{
				startMainActivity();
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{
				RetryInitWorker.getInstance().start(); // 如果初始化失败重试
			}
		});

	}

	private void startMainActivity()
	{
		long delay = 0;
		if (NEED_DELAY)
		{
			delay = DEFAULT_DELAY;
		}
		SDHandlerUtil.runOnUiThreadDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				startActivity(new Intent(InitActivity.this, MainActivity.class));
				finish();
			}
		}, delay);
	}
}
