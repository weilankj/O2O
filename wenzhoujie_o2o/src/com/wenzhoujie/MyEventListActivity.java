package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleTwoRightButton.SDTitleTwoRightButtonListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.EventsAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.EventsListActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.EventsListActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 我参加的活动列表
 * 
 * @author js02
 * 
 */
public class MyEventListActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_my_event_list_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	@ViewInject(id = R.id.act_my_event_list_ll_empty)
	private LinearLayout mLlEmpty = null;

	private List<EventsListActItemModel> mListModel = new ArrayList<EventsListActItemModel>();
	private EventsAdapter mAdapter = null;

	private int pageTotal;
	// =================提交服务器参数
	private int page;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_TWO_RIGHT_BUTTON);
		setContentView(R.layout.act_my_event_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new EventsAdapter(mListModel, this);
		mPtrlvContent.setAdapter(mAdapter);
	}

	protected void requestMyEvent(final boolean isLoadMore)
	{

		if (!AppHelper.isLogin())
		{
			return;
		}

		RequestModel model = new RequestModel();
		model.putAct("my_eventlist");
		model.putUser();
		model.putLocation();
		model.putPage(page);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				EventsListActModel model = JsonUtil.json2Object(responseInfo.result, EventsListActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							page = model.getPage().getPage();
							pageTotal = model.getPage().getPage_total();
						}
						SDViewUtil.updateAdapterByList(mListModel, model.getItem(), mAdapter, isLoadMore);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mPtrlvContent.onRefreshComplete();
				SDViewUtil.toggleEmptyMsgByList(mListModel, mLlEmpty);
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void initPullListView()
	{
		mPtrlvContent.setMode(Mode.BOTH);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page = 1;
				requestMyEvent(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++page > pageTotal && pageTotal > 0)
				{
					SDToast.showToast("没有更多数据了");
					mPtrlvContent.onRefreshComplete();
				} else
				{
					requestMyEvent(true);
				}
			}
		});
		mPtrlvContent.setRefreshing();
	}

	private void initTitle()
	{

		mTitleTwoRightBtns.setmListener(new SDTitleTwoRightButtonListener()
		{

			@Override
			public void onRightButton2CLick_SDTitleTwoRightButton(View v)
			{
			}

			@Override
			public void onRightButton1CLick_SDTitleTwoRightButton(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleTwoRightButton(View v)
			{
				finish();
			}
		});

		mTitleTwoRightBtns.setTitleTop("我的活动");
	}

}