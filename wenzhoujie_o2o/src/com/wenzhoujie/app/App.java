package com.wenzhoujie.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Application;
import cn.jpush.android.api.JPushInterface;

import com.wenzhoujie.library.SDLibrary;
import com.wenzhoujie.library.common.SDActivityManager;
import com.wenzhoujie.library.config.SDLibraryConfig;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.sunday.eventbus.SDEventManager;
import com.sunday.eventbus.SDEventObserver;
import com.ta.util.netstate.TANetChangeObserver;
import com.ta.util.netstate.TANetWorkUtil;
import com.ta.util.netstate.TANetWorkUtil.netType;
import com.ta.util.netstate.TANetworkStateReceiver;
import com.wenzhoujie.BaseActivity;
import com.wenzhoujie.MainActivity;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.common.CommonInterface;
import com.wenzhoujie.common.ImageLoaderManager;
import com.wenzhoujie.constant.ApkConstant;
import com.wenzhoujie.constant.Constant.LoadImageType;
import com.wenzhoujie.dao.LocalUserModelDao;
import com.wenzhoujie.dao.SettingModelDao;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.jpush.observer.IJpushObserver;
import com.wenzhoujie.jpush.observer.impl.P2pJpushObserver;
import com.wenzhoujie.model.CartGoodsArrayList;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RuntimeConfigModel;
import com.wenzhoujie.model.RuntimeSettingModel;
import com.wenzhoujie.model.SettingModel;
import com.wenzhoujie.model.ShopGoodsModel;
import com.wenzhoujie.receiver.JPushReceiver;
import com.wenzhoujie.utils.CrashHandler;

public class App extends Application implements SDEventObserver, TANetChangeObserver
{

	private static App mApp = null;

	private LocalUserModel mLocalUser = null;

	private CartGoodsArrayList mListCartGoodsModel = new CartGoodsArrayList();
	
	private List<ShopGoodsModel> ShopModelList=new ArrayList<ShopGoodsModel>();

	private RuntimeSettingModel mRuntimeSettingModel = new RuntimeSettingModel();

	private IJpushObserver mJpushObserver = null; // 极光
	public List<Class<? extends BaseActivity>> mListClassNotFinishWhenLoginState0 = new ArrayList<Class<? extends BaseActivity>>();
	public RuntimeConfigModel mRuntimeConfig = new RuntimeConfigModel();

	public RuntimeSettingModel getmRuntimeSettingModel()
	{
		return mRuntimeSettingModel;
	}

	public void setmRuntimeSettingModel(RuntimeSettingModel mRuntimeSettingModel)
	{
		this.mRuntimeSettingModel = mRuntimeSettingModel;
	}

	public CartGoodsArrayList getListCartGoodsModel()
	{
		return mListCartGoodsModel;
	}

	public void setListCartGoodsModel(CartGoodsArrayList listCartGoodsModel)
	{
		this.mListCartGoodsModel = listCartGoodsModel;
	}

	public LocalUserModel getmLocalUser()
	{
		return mLocalUser;
	}

	public void setmLocalUser(LocalUserModel localUser)
	{
		if (localUser != null)
		{
			this.mLocalUser = localUser;
			LocalUserModelDao.saveModel(localUser);
		}
	}

	@Override
	public void onCreate()
	{
		// TODO Auto-generated method stub
		super.onCreate();
		init();
	}

	private void init()
	{
		mApp = this;
		ImageLoaderManager.initImageLoader();
		initSDLibrary();
		initAppCrashHandler();
		initBaiduMap();
		initLoginState();
		SDEventManager.register(this);
		TANetworkStateReceiver.registerObserver(this);
		initSettingModel();
		initJpush();
		addClassesNotFinishWhenLoginState0();
	}

	private void initSDLibrary()
	{
		SDLibrary.getInstance().init(getApplication());

		SDLibraryConfig config = new SDLibraryConfig();

		config.setmCornerRadiusResId(R.dimen.corner);
		config.setmGrayPressColorResId(R.color.gray_press);
		config.setmMainColorPressResId(R.color.main_color_press);
		config.setmMainColorResId(R.color.main_color);
		config.setmStrokeColorResId(R.color.stroke);
		config.setmStrokeWidth(SDViewUtil.dp2px(1));
		config.setmTitleColorResId(R.color.bg_title_bar);
		config.setmTitleColorPressedResId(R.color.bg_title_bar_pressed);
		config.setmTitleHeightResId(R.dimen.height_title_bar);
		config.setmIconArrowLeftResId(R.drawable.ic_arrow_left_back);

		SDLibrary.getInstance().initConfig(config);
	}

	private void addClassesNotFinishWhenLoginState0()
	{
		mListClassNotFinishWhenLoginState0.add(MainActivity.class);
	}

	private void initJpush()
	{
		mJpushObserver = new P2pJpushObserver();
		JPushReceiver.registerObserver(mJpushObserver);
		JPushInterface.setDebugMode(true);
		JPushInterface.init(this);
	}

	private void initAppCrashHandler()
	{
		if (!ApkConstant.DEBUG)
		{
			CrashHandler crashHandler = CrashHandler.getInstance();
			crashHandler.init(getApplicationContext());
		}
	}

	private void initSettingModel()
	{
		SettingModel settingModel = new SettingModel();
		if (SettingModelDao.getInstance().insertOrCreateModel(settingModel)) // 插入成功或者数据库已经存在记录
		{
			SettingModel queryModel = SettingModelDao.queryModel();
			if (queryModel != null)
			{
				settingModel = queryModel;
			}
		}
		onConnect(TANetWorkUtil.getAPNType(getApplication()));
		getmRuntimeSettingModel().setCanPushMessage(settingModel.getCanPushMessage());
	}

	private void initBaiduMap()
	{
		BaiduMapManager.getInstance().init(this);
	}

	private void initLoginState()
	{
		this.mLocalUser = LocalUserModelDao.getModel();
		CommonInterface.refreshLocalUser();
	}

	public static App getApplication()
	{
		return mApp;
	}

	public void exitApp(boolean isBackground)
	{
		SDActivityManager.getInstance().finishAllActivity();
		SDEventManager.post(EnumEventTag.EXIT_APP.ordinal());
		if (isBackground)
		{

		} else
		{
			System.exit(0);
		}
	}

	public void clearAppsLocalUserModel()
	{
		LocalUserModelDao.deleteAllModel();
		this.mLocalUser = null;
	}

	public static String getStringById(int resId)
	{
		return getApplication().getString(resId);
	}

	@Override
	public void onConnect(netType type)
	{
		switch (SettingModelDao.getLoadImageType())
		{
		case LoadImageType.ONLY_WIFI:
			switch (type)
			{
			case wifi:
				getmRuntimeSettingModel().setCanLoadImage(1);
				break;
			default:
				getmRuntimeSettingModel().setCanLoadImage(0);
				break;
			}
			break;
		case LoadImageType.ALL:
			getmRuntimeSettingModel().setCanLoadImage(1);
			break;

		default:
			break;
		}

		switch (getmRuntimeSettingModel().getCanLoadImage())
		{
		case 0:
			SDViewBinder.mCanLoadImageFromUrl = false;
			break;
		case 1:
			SDViewBinder.mCanLoadImageFromUrl = true;
			break;

		default:
			break;
		}

	}

	@Override
	public void onDisConnect()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onTerminate()
	{
		SDEventManager.unregister(this);
		super.onTerminate();
	}

	@Override
	public void onEvent(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case LOGOUT:
			clearAppsLocalUserModel();
			mListCartGoodsModel.clear();
			break;

		default:
			break;
		}
	}

	@Override
	public void onEventBackgroundThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventAsync(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	public List<ShopGoodsModel> getShopModelList() {
		return ShopModelList;
	}

	public void setShopModelList(List<ShopGoodsModel> shopModelList) {
		ShopModelList = shopModelList;
	}

}
