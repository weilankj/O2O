package com.wenzhoujie.model;

/**
 * 用于保存app运行期间的一些配置信息
 * 
 * @author js02
 * 
 */
public class RuntimeConfigModel
{

	/** MainActivity是否已经被启动 */
	private boolean isMainActivityStarted = false;
	/** 是否需要刷新团购详情页面 */
	private boolean isNeedRefreshTuanDetail = false;
	/** 是否需要刷新商家详情页面 */
	private boolean isNeedRefreshMerchantDetail = false;

	public boolean isNeedRefreshTuanDetail()
	{
		return isNeedRefreshTuanDetail;
	}

	public void setNeedRefreshTuanDetail(boolean isNeedRefreshTuanDetail)
	{
		this.isNeedRefreshTuanDetail = isNeedRefreshTuanDetail;
	}

	public boolean isNeedRefreshMerchantDetail()
	{
		return isNeedRefreshMerchantDetail;
	}

	public void setNeedRefreshMerchantDetail(boolean isNeedRefreshMerchantDetail)
	{
		this.isNeedRefreshMerchantDetail = isNeedRefreshMerchantDetail;
	}

	public boolean isMainActivityStarted()
	{
		return isMainActivityStarted;
	}

	public void setMainActivityStarted(boolean isMainActivityStarted)
	{
		this.isMainActivityStarted = isMainActivityStarted;
	}

}
