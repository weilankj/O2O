package com.wenzhoujie.model;

import java.io.Serializable;

import android.text.TextUtils;

public class CouponlistActItemModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = null;
	private String sn = null;
	private String password = null;
	private String begin_time = null;
	private String end_time = null;
	private String is_valid = null;
	private String user_id = null;
	private String deal_id = null;
	private String order_id = null;
	private String order_deal_id = null;
	private String is_new = null;
	private String supplier_id = null;
	private String confirm_account = null;
	private String is_delete = null;
	private String confirm_time = null;
	private String mail_count = null;
	private String sms_count = null;
	private String is_balance = null;
	private String balance_memo = null;
	private String balance_price = null;
	private String balance_time = null;
	private String refund_status = null;
	private String expire_refund = null;
	private String any_refund = null;
	private String coupon_price = null;
	private String coupon_score = null;
	private String deal_type = null;
	private String coupon_money = null;
	private String add_balance_price = null;
	private String createTime = null;
	private String endTime = null;
	private String useTime = null;
	private String beginTime = null;
	private String dealIcon = null;
	private String lessTime = null;
	private String spName = null;
	private String spTel = null;
	private String spAddress = null;
	private String couponSn = null;
	private String couponPw = null;
	private String dealName = null;
	private String qrcode = null;

	// ==================add
	private String sn_format_string = null;
	private String overdue_time_format = null;

	public String getQrcode()
	{
		return qrcode;
	}

	public void setQrcode(String qrcode)
	{
		this.qrcode = qrcode;
	}

	public String getOverdue_time_format()
	{
		return overdue_time_format;
	}

	public void setOverdue_time_format(String overdue_time_format)
	{
		this.overdue_time_format = overdue_time_format;
	}

	/**
	 * 在set lesstime，endtime和usetime的时候要调用此方法
	 */
	private void formatOverdueTime()
	{
		if (!TextUtils.isEmpty(getUseTime()))
		{
			setOverdue_time_format("使用时间：" + getUseTime());
		} else
		{
			if (!TextUtils.isEmpty(getLessTime()) && !TextUtils.isEmpty(getEndTime()))
			{
				setOverdue_time_format("过期时间：" + getEndTime());
			}
		}
	}

	public String getSn_format_string()
	{
		return sn_format_string;
	}

	public void setSn_format_string(String sn_format_string)
	{
		this.sn_format_string = sn_format_string;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getSn()
	{
		return sn;
	}

	public void setSn(String sn)
	{
		this.sn = sn;
		if (!TextUtils.isEmpty(sn))
		{
			setSn_format_string("序列号：" + sn);
		}
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getBegin_time()
	{
		return begin_time;
	}

	public void setBegin_time(String begin_time)
	{
		this.begin_time = begin_time;
	}

	public String getEnd_time()
	{
		return end_time;
	}

	public void setEnd_time(String end_time)
	{
		this.end_time = end_time;
	}

	public String getIs_valid()
	{
		return is_valid;
	}

	public void setIs_valid(String is_valid)
	{
		this.is_valid = is_valid;
	}

	public String getUser_id()
	{
		return user_id;
	}

	public void setUser_id(String user_id)
	{
		this.user_id = user_id;
	}

	public String getDeal_id()
	{
		return deal_id;
	}

	public void setDeal_id(String deal_id)
	{
		this.deal_id = deal_id;
	}

	public String getOrder_id()
	{
		return order_id;
	}

	public void setOrder_id(String order_id)
	{
		this.order_id = order_id;
	}

	public String getOrder_deal_id()
	{
		return order_deal_id;
	}

	public void setOrder_deal_id(String order_deal_id)
	{
		this.order_deal_id = order_deal_id;
	}

	public String getIs_new()
	{
		return is_new;
	}

	public void setIs_new(String is_new)
	{
		this.is_new = is_new;
	}

	public String getSupplier_id()
	{
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id)
	{
		this.supplier_id = supplier_id;
	}

	public String getConfirm_account()
	{
		return confirm_account;
	}

	public void setConfirm_account(String confirm_account)
	{
		this.confirm_account = confirm_account;
	}

	public String getIs_delete()
	{
		return is_delete;
	}

	public void setIs_delete(String is_delete)
	{
		this.is_delete = is_delete;
	}

	public String getConfirm_time()
	{
		return confirm_time;
	}

	public void setConfirm_time(String confirm_time)
	{
		this.confirm_time = confirm_time;
	}

	public String getMail_count()
	{
		return mail_count;
	}

	public void setMail_count(String mail_count)
	{
		this.mail_count = mail_count;
	}

	public String getSms_count()
	{
		return sms_count;
	}

	public void setSms_count(String sms_count)
	{
		this.sms_count = sms_count;
	}

	public String getIs_balance()
	{
		return is_balance;
	}

	public void setIs_balance(String is_balance)
	{
		this.is_balance = is_balance;
	}

	public String getBalance_memo()
	{
		return balance_memo;
	}

	public void setBalance_memo(String balance_memo)
	{
		this.balance_memo = balance_memo;
	}

	public String getBalance_price()
	{
		return balance_price;
	}

	public void setBalance_price(String balance_price)
	{
		this.balance_price = balance_price;
	}

	public String getBalance_time()
	{
		return balance_time;
	}

	public void setBalance_time(String balance_time)
	{
		this.balance_time = balance_time;
	}

	public String getRefund_status()
	{
		return refund_status;
	}

	public void setRefund_status(String refund_status)
	{
		this.refund_status = refund_status;
	}

	public String getExpire_refund()
	{
		return expire_refund;
	}

	public void setExpire_refund(String expire_refund)
	{
		this.expire_refund = expire_refund;
	}

	public String getAny_refund()
	{
		return any_refund;
	}

	public void setAny_refund(String any_refund)
	{
		this.any_refund = any_refund;
	}

	public String getCoupon_price()
	{
		return coupon_price;
	}

	public void setCoupon_price(String coupon_price)
	{
		this.coupon_price = coupon_price;
	}

	public String getCoupon_score()
	{
		return coupon_score;
	}

	public void setCoupon_score(String coupon_score)
	{
		this.coupon_score = coupon_score;
	}

	public String getDeal_type()
	{
		return deal_type;
	}

	public void setDeal_type(String deal_type)
	{
		this.deal_type = deal_type;
	}

	public String getCoupon_money()
	{
		return coupon_money;
	}

	public void setCoupon_money(String coupon_money)
	{
		this.coupon_money = coupon_money;
	}

	public String getAdd_balance_price()
	{
		return add_balance_price;
	}

	public void setAdd_balance_price(String add_balance_price)
	{
		this.add_balance_price = add_balance_price;
	}

	public String getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(String createTime)
	{
		this.createTime = createTime;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
		formatOverdueTime();
	}

	public String getUseTime()
	{
		return useTime;
	}

	public void setUseTime(String useTime)
	{
		this.useTime = useTime;
		formatOverdueTime();
	}

	public String getBeginTime()
	{
		return beginTime;
	}

	public void setBeginTime(String beginTime)
	{
		this.beginTime = beginTime;
	}

	public String getDealIcon()
	{
		return dealIcon;
	}

	public void setDealIcon(String dealIcon)
	{
		this.dealIcon = dealIcon;
	}

	public String getLessTime()
	{
		return lessTime;
	}

	public void setLessTime(String lessTime)
	{
		this.lessTime = lessTime;
		formatOverdueTime();
	}

	public String getSpName()
	{
		return spName;
	}

	public void setSpName(String spName)
	{
		this.spName = spName;
	}

	public String getSpTel()
	{
		return spTel;
	}

	public void setSpTel(String spTel)
	{
		this.spTel = spTel;
	}

	public String getSpAddress()
	{
		return spAddress;
	}

	public void setSpAddress(String spAddress)
	{
		this.spAddress = spAddress;
	}

	public String getCouponSn()
	{
		return couponSn;
	}

	public void setCouponSn(String couponSn)
	{
		this.couponSn = couponSn;
	}

	public String getCouponPw()
	{
		return couponPw;
	}

	public void setCouponPw(String couponPw)
	{
		this.couponPw = couponPw;
	}

	public String getDealName()
	{
		return dealName;
	}

	public void setDealName(String dealName)
	{
		this.dealName = dealName;
	}

}