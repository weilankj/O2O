package com.wenzhoujie.model;

public class WxpayModel {

	private String body = null;				//内容
	private String out_trade_no = null;		//交易流水号
											//其它参数由客户端实现
	
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	public String getOut_trade_no() {
		return out_trade_no;
	}
	
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	
}
