package com.wenzhoujie.model;

import java.util.List;

import org.json.JSONArray;

public class ECshoplistActItemModel
{
	private String goods_id; // 商品ID，有可能是字符串类型的
	private String sub_name; // 简短名称
	private String title;// 商品名称
	private String city_name; // => 上海
	private String image;// 图片地址
							// http://localhost:8888/groupon/Public/upload/goods/small/201104/4d9d79cf7dbd9.jpg

	private int start_date;// => 1302135840 团购开始时间
	private int end_date;// => 1367281440 团购结束时间
	private float discount;// => 5.0 折扣
	private String ori_price_format;// 原价格式字符串
	private String cur_price_format;// 当前价格格式字符串
	private String address;// => 商家地址
	private String num_unit;// => 单位(如：件，公斤)
	private int limit_num;// limit_num;//购买数据限制
	private int has_attr;// 0:无商品属性；1：额商品属性;是否有商品属性
	private int has_mcod;// has_mcod:1:商品支持，现金支付(货到付款); 0:不支持
	private int has_cart;// 1:可以跟其它商品一起放入购物车购买；0：不能放入购物车，只能独立购买
	private int change_cart_request_server;// 0:提交，1:不提交；编辑购买车商品时，需要提交到服务器端，让服务器端通过一些判断返回一些信息回来(如：满多少钱，可以免运费等一些提示)
	private String goods_brief;
	private JSONArray galleryObject;
	private int has_delivery;// 0:无配送方式;1:有配送方式
	private List<Object> gallery; // 所有图库的地址列表
	private String goods_desc;
	private String saving_format;
	private String sp_detail;
	private String less_time;
	private double ypoint;
	private double xpoint;
	private String distance;

	// ----------add-------
	private String buy_count;// 购买数量
	private String ori_price;// => 100 原价
	private String cur_price;// => 50 当前价格

	public String getGoods_id()
	{
		return goods_id;
	}

	public void setGoods_id(String goods_id)
	{
		this.goods_id = goods_id;
	}

	public String getSub_name()
	{
		return sub_name;
	}

	public void setSub_name(String sub_name)
	{
		this.sub_name = sub_name;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public String getBuy_count()
	{
		return buy_count;
	}

	public void setBuy_count(String buy_count)
	{
		this.buy_count = buy_count;
	}

	public int getStart_date()
	{
		return start_date;
	}

	public void setStart_date(int start_date)
	{
		this.start_date = start_date;
	}

	public int getEnd_date()
	{
		return end_date;
	}

	public void setEnd_date(int end_date)
	{
		this.end_date = end_date;
	}

	public String getOri_price()
	{
		return ori_price;
	}

	public void setOri_price(String ori_price)
	{
		this.ori_price = ori_price + "元";
	}

	public String getCur_price()
	{
		return cur_price;
	}

	public void setCur_price(String cur_price)
	{
		this.cur_price = cur_price;
	}

	public float getDiscount()
	{
		return discount;
	}

	public void setDiscount(float discount)
	{
		this.discount = discount;
	}

	public String getOri_price_format()
	{
		return ori_price_format;
	}

	public void setOri_price_format(String ori_price_format)
	{
		this.ori_price_format = ori_price_format;
	}

	public String getCur_price_format()
	{
		return cur_price_format;
	}

	public void setCur_price_format(String cur_price_format)
	{
		this.cur_price_format = cur_price_format;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getNum_unit()
	{
		return num_unit;
	}

	public void setNum_unit(String num_unit)
	{
		this.num_unit = num_unit;
	}

	public int getLimit_num()
	{
		return limit_num;
	}

	public void setLimit_num(int limit_num)
	{
		this.limit_num = limit_num;
	}

	public int getHas_attr()
	{
		return has_attr;
	}

	public void setHas_attr(int has_attr)
	{
		this.has_attr = has_attr;
	}

	public int getHas_mcod()
	{
		return has_mcod;
	}

	public void setHas_mcod(int has_mcod)
	{
		this.has_mcod = has_mcod;
	}

	public int getHas_cart()
	{
		return has_cart;
	}

	public void setHas_cart(int has_cart)
	{
		this.has_cart = has_cart;
	}

	public int getChange_cart_request_server()
	{
		return change_cart_request_server;
	}

	public void setChange_cart_request_server(int change_cart_request_server)
	{
		this.change_cart_request_server = change_cart_request_server;
	}

	public String getGoods_brief()
	{
		return goods_brief;
	}

	public void setGoods_brief(String goods_brief)
	{
		this.goods_brief = goods_brief;
	}

	public JSONArray getGalleryObject()
	{
		return galleryObject;
	}

	public void setGalleryObject(JSONArray galleryObject)
	{
		this.galleryObject = galleryObject;
	}

	public int getHas_delivery()
	{
		return has_delivery;
	}

	public void setHas_delivery(int has_delivery)
	{
		this.has_delivery = has_delivery;
	}

	public List<Object> getGallery()
	{
		return gallery;
	}

	public void setGallery(List<Object> gallery)
	{
		this.gallery = gallery;
	}

	public String getGoods_desc()
	{
		return goods_desc;
	}

	public void setGoods_desc(String goods_desc)
	{
		this.goods_desc = goods_desc;
	}

	public String getSaving_format()
	{
		return saving_format;
	}

	public void setSaving_format(String saving_format)
	{
		this.saving_format = saving_format;
	}

	public String getSp_detail()
	{
		return sp_detail;
	}

	public void setSp_detail(String sp_detail)
	{
		this.sp_detail = sp_detail;
	}

	public String getLess_time()
	{
		return less_time;
	}

	public void setLess_time(String less_time)
	{
		this.less_time = less_time;
	}

	public double getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(double ypoint)
	{
		this.ypoint = ypoint;
	}

	public double getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(double xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
	}

}