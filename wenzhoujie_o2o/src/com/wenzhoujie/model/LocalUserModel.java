package com.wenzhoujie.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.app.App;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.utils.AESUtil;

public class LocalUserModel implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int _id;

	private String user_id;
	private String user_email;
	private String user_name;
	private String user_pwd;
	private String user_avatar;
	private String is_account;
	private String user_money;
	private String user_money_format;
	private String user_score;
	private String nick_name;
	
	public static void dealLoginSuccess(String userId, String UserEmail, String UserName, String UserPwd, String UserAvatar, String isAccount, String UserMoney,
			String UserMoneyFormat, String User_Score, boolean postEvent, String Nick_Name)
	{
		LocalUserModel model = new LocalUserModel();
		model.setUser_id(userId);
		model.setUser_email(UserEmail);
		model.setUser_name(UserName);
		model.setNick_name(Nick_Name);
		model.setUser_pwd(UserPwd);
		model.setUser_avatar(UserAvatar);
		model.setIs_account(isAccount);
		model.setUser_money(UserMoney);
		model.setUser_money_format(UserMoneyFormat);
		model.setUser_score(User_Score);
		dealLoginSuccess(model, postEvent);
	}

	public static void dealLoginSuccess(String userId, String UserEmail, String UserName, String UserPwd, String UserAvatar, String isAccount, String UserMoney,
			String UserMoneyFormat, String User_Score, String Nick_Name)
	{
		dealLoginSuccess(userId, UserEmail, UserName, UserPwd, UserAvatar, isAccount, UserMoney, UserMoneyFormat, User_Score, true,Nick_Name);
	}

	public static void dealLoginSuccess(LocalUserModel model, boolean postEvent)
	{
		App.getApplication().setmLocalUser(model);
		if (postEvent)
		{
			SDEventManager.post(EnumEventTag.LOGIN_NORMAL_SUCCESS.ordinal());
		}
	}

	public String getUser_money()
	{
		return user_money;
	}

	public void setUser_money(String user_money)
	{
		this.user_money = user_money;
	}

	public String getUser_money_format()
	{
		return user_money_format;
	}

	public void setUser_money_format(String user_money_format)
	{
		this.user_money_format = user_money_format;
	}

	public int get_id()
	{
		return _id;
	}

	public void set_id(int _id)
	{
		this._id = _id;
	}

	public String getUser_id()
	{
		return user_id;
	}

	public void setUser_id(String user_id)
	{
		this.user_id = user_id;
	}

	public String getUser_email()
	{
		return user_email;
	}

	public void setUser_email(String user_email)
	{
		this.user_email = user_email;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}
	
	public String getNick_name()
	{
		return nick_name;
	}

	public void setNick_name(String nick_name)
	{
		this.nick_name = nick_name;
	}

	public String getUser_pwd()
	{
		return user_pwd;
	}

	public void setUser_pwd(String user_pwd)
	{
		this.user_pwd = user_pwd;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getIs_account()
	{
		return is_account;
	}

	public void setIs_account(String is_account)
	{
		this.is_account = is_account;
	}

	public String getUser_score()
	{
		return user_score;
	}

	public void setUser_score(String user_score)
	{
		this.user_score = user_score;
	}

	public LocalUserModel deepClone()
	{
		try
		{
			// 将对象写到流里
			ByteArrayOutputStream bo = new ByteArrayOutputStream();
			ObjectOutputStream oo = new ObjectOutputStream(bo);
			oo.writeObject(this);
			// 从流里读出来
			ByteArrayInputStream bi = new ByteArrayInputStream(bo.toByteArray());
			ObjectInputStream oi = new ObjectInputStream(bi);
			return (LocalUserModel) (oi.readObject());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void encryptModel()
	{
		if (this.is_account != null)
		{
			this.is_account = AESUtil.encrypt(this.is_account);
		}
		if (this.user_avatar != null)
		{
			this.user_avatar = AESUtil.encrypt(this.user_avatar);
		}
		if (this.user_email != null)
		{
			this.user_email = AESUtil.encrypt(this.user_email);
		}
		if (this.user_id != null)
		{
			this.user_id = AESUtil.encrypt(this.user_id);
		}
		if (this.user_name != null)
		{
			this.user_name = AESUtil.encrypt(this.user_name);
		}
		if (this.nick_name != null)
		{
			this.nick_name = AESUtil.encrypt(this.nick_name);
		}
		if (this.user_pwd != null)
		{
			this.user_pwd = AESUtil.encrypt(this.user_pwd);
		}
		if (this.user_money != null)
		{
			this.user_money = AESUtil.encrypt(this.user_money);
		}
		if (this.user_money_format != null)
		{
			this.user_money_format = AESUtil.encrypt(this.user_money_format);
		}
		if (this.user_score != null)
		{
			this.user_score = AESUtil.encrypt(this.user_score);
		}
	}

	public void decryptModel()
	{
		if (this.is_account != null)
		{
			this.is_account = AESUtil.decrypt(this.is_account);
		}
		if (this.user_avatar != null)
		{
			this.user_avatar = AESUtil.decrypt(this.user_avatar);
		}
		if (this.user_email != null)
		{
			this.user_email = AESUtil.decrypt(this.user_email);
		}
		if (this.user_id != null)
		{
			this.user_id = AESUtil.decrypt(this.user_id);
		}
		if (this.user_name != null)
		{
			this.user_name = AESUtil.decrypt(this.user_name);
		}
		if (this.nick_name != null)
		{
			this.nick_name = AESUtil.decrypt(this.nick_name);
		}
		if (this.user_pwd != null)
		{
			this.user_pwd = AESUtil.decrypt(this.user_pwd);
		}
		if (this.user_money != null)
		{
			this.user_money = AESUtil.decrypt(this.user_money);
		}
		if (this.user_money_format != null)
		{
			this.user_money_format = AESUtil.decrypt(this.user_money_format);
		}
		if (this.user_score != null)
		{
			this.user_score = AESUtil.decrypt(this.user_score);
		}
	}

}
