package com.wenzhoujie.model;

import java.util.List;

//超市专页，返回商品信息
public class smSubCateItemModel {
	private int id;
	private String name;
	private List<MerchantShopItem> items;

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<MerchantShopItem> getItems() {
		return items;
	}
	public void setItems(List<MerchantShopItem> items) {
		this.items = items;
	}

}
