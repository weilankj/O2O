package com.wenzhoujie.model;

import java.util.List;

public class smGoodsCates
{
	private String id = null;
	private String name = null;
	private List<smSmallCates> sub_cate = null;

	// /////////////////////////add
	private boolean isSelect = false;
	private boolean isHasChild = false;

	public boolean isHasChild()
	{
		return isHasChild;
	}

	public void setHasChild(boolean isHasChild)
	{
		this.isHasChild = isHasChild;
	}

	public boolean isSelect()
	{
		return isSelect;
	}

	public void setSelect(boolean isSelect)
	{
		this.isSelect = isSelect;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<smSmallCates> getSub_cate()
	{
		return sub_cate;
	}

	public void setSub_cate(List<smSmallCates> sub_cate)
	{
		this.sub_cate = sub_cate;
		if (sub_cate != null && sub_cate.size() > 1)
		{
			setHasChild(true);
		} else
		{
			setHasChild(false);
		}
	}

}