package com.wenzhoujie.model;

public class UHome_userModel
{
	private String uid = null;
	private String email = null;
	private String user_name = null;
	private String user_avatar = null;
	private String fans = null;
	private int follows;
	private String photos = null;
	private String favs = null;
	private String goods = null;
	private String bfavs = null;
	private String is_follow = null;

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getFans()
	{
		return fans;
	}

	public void setFans(String fans)
	{
		this.fans = fans;
	}

	public int getFollows()
	{
		return follows;
	}

	public void setFollows(int follows)
	{
		this.follows = follows;
	}

	public String getPhotos()
	{
		return photos;
	}

	public void setPhotos(String photos)
	{
		this.photos = photos;
	}

	public String getFavs()
	{
		return favs;
	}

	public void setFavs(String favs)
	{
		this.favs = favs;
	}

	public String getGoods()
	{
		return goods;
	}

	public void setGoods(String goods)
	{
		this.goods = goods;
	}

	public String getBfavs()
	{
		return bfavs;
	}

	public void setBfavs(String bfavs)
	{
		this.bfavs = bfavs;
	}

	public String getIs_follow()
	{
		return is_follow;
	}

	public void setIs_follow(String is_follow)
	{
		this.is_follow = is_follow;
	}
}