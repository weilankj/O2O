package com.wenzhoujie.model;

import java.util.List;

import com.wenzhoujie.model.act.BaseActModel;

public class Article_listActModel extends BaseActModel
{
	private List<Article_listListModel> list = null;

	private PageModel page;

	public List<Article_listListModel> getList()
	{
		return list;
	}

	public void setList(List<Article_listListModel> list)
	{
		this.list = list;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}
