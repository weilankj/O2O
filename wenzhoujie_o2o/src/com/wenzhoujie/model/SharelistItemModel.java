package com.wenzhoujie.model;

import com.wenzhoujie.utils.SDTypeParseUtil;

public class SharelistItemModel
{

	private String share_id;
	private String img;
	private String height;
	private String width;
	// ============add
	private int heightFormat;
	private int widthFormat;

	public int getWidthFormat()
	{
		return widthFormat;
	}

	public void setWidthFormat(int widthFormat)
	{
		this.widthFormat = widthFormat;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
		setWidthFormat(SDTypeParseUtil.getIntFromString(width, 0));
	}

	public int getHeightFormat()
	{
		return heightFormat;
	}

	public void setHeightFormat(int heightFormat)
	{
		this.heightFormat = heightFormat;
	}

	public String getShare_id()
	{
		return share_id;
	}

	public void setShare_id(String share_id)
	{
		this.share_id = share_id;
	}

	public String getImg()
	{
		return img;
	}

	public void setImg(String img)
	{
		this.img = img;
	}

	public String getHeight()
	{
		return height;
	}

	public void setHeight(String height)
	{
		this.height = height;
		setHeightFormat(SDTypeParseUtil.getIntFromString(height, 0));
	}

}
