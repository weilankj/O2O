package com.wenzhoujie.model;

import java.util.ArrayList;
import java.util.List;

import com.wenzhoujie.app.App;

public class CartGoodsArrayList extends ArrayList<CartGoodsModel>
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean add(CartGoodsModel model)
	{

		if (model != null)
		{
			if (model.isHas_cart()) // 可以加入购物车
			{
				List<CartGoodsModel> listModel = App.getApplication().getListCartGoodsModel();
				for (int i = 0; i < listModel.size(); i++)
				{
					CartGoodsModel item = listModel.get(i);
					if (item.equals(model))
					{
						item.addNumber();
						return false;
					}
				}
				return super.add(model);
			} else
			{
				return false;
			}
		} else
		{
			return false;
		}

		// if (model != null && model.getGoods() != null &&
		// model.getGoods().getHas_cart() != null)
		// {
		// if (model.getGoods().getHas_cart().equals("1")) // 可以加入购物车
		// {
		// List<CartGoodsModel> listModel =
		// App.getApplication().getListCartGoodsModel();
		// for (int i = 0; i < listModel.size(); i++)
		// {
		// CartGoodsModel item = listModel.get(i);
		// if (item.getGoods_id() != null &&
		// item.getGoods_id().equals(model.getGoods_id())) // 商品id相同
		// {
		// if ((item.getAttr_id_a() != null &&
		// item.getAttr_id_a().equals(model.getAttr_id_a())) &&
		// (item.getAttr_id_b() != null &&
		// item.getAttr_id_b().equals(model.getAttr_id_b())))
		// {
		// try
		// {
		// int limit_num = Integer.parseInt(item.getLimit_num());
		// int newNum = Integer.parseInt(model.getNum());
		// int oldNum = Integer.parseInt(item.getNum());
		// if (limit_num > 0 && limit_num < (oldNum + newNum))
		// {
		// item.setNum(item.getLimit_num());
		// }else
		// {
		// item.setNum(String.valueOf(oldNum + newNum));
		// }
		// return false;
		// } catch (Exception e)
		// {
		// // TODO: handle exception
		// }
		// }
		// }
		// }
		// return super.add(model);
		// }
		// }
		// return false;
	}

}
