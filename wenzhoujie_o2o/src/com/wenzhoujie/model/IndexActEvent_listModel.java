package com.wenzhoujie.model;

import java.io.Serializable;

public class IndexActEvent_listModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String icon;
	private String event_begin_time;
	private String event_end_time;
	private String event_begin_time_format;
	private String event_end_time_format;
	private String sheng_time_format;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public String getEvent_begin_time()
	{
		return event_begin_time;
	}

	public void setEvent_begin_time(String event_begin_time)
	{
		this.event_begin_time = event_begin_time;
	}

	public String getEvent_end_time()
	{
		return event_end_time;
	}

	public void setEvent_end_time(String event_end_time)
	{
		this.event_end_time = event_end_time;
	}

	public String getEvent_begin_time_format()
	{
		return event_begin_time_format;
	}

	public void setEvent_begin_time_format(String event_begin_time_format)
	{
		this.event_begin_time_format = event_begin_time_format;
	}

	public String getEvent_end_time_format()
	{
		return event_end_time_format;
	}

	public void setEvent_end_time_format(String event_end_time_format)
	{
		this.event_end_time_format = event_end_time_format;
	}

	public String getSheng_time_format()
	{
		return sheng_time_format;
	}

	public void setSheng_time_format(String sheng_time_format)
	{
		this.sheng_time_format = sheng_time_format;
	}

}
