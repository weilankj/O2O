package com.wenzhoujie.model;

import com.wenzhoujie.model.act.BaseActModel;

public class Show_articleActModel extends BaseActModel
{

	private String id;

	private String title;
	private String content;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

}
