package com.wenzhoujie.model;

//超市专页，返回商品信息
public class MerchantShopItem {
	private int id;
	private String name;
	private String img;
	private float origin_price;
	private float current_price;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public float getOrigin_price() {
		return origin_price;
	}
	public void setOrigin_price(float origin_price) {
		this.origin_price = origin_price;
	}
	public float getCurrent_price() {
		return current_price;
	}
	public void setCurrent_price(float current_price) {
		this.current_price = current_price;
	}
}
