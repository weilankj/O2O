package com.wenzhoujie.model;

public class MyyouhuilistActItemModel
{
	private String id = null;
	private String title = null;
	private String logo = null;
	private String merchant_logo = null;
	private String create_time = null;
	private String create_time_format = null;
	private String yl_create_time = null;
	private String yl_create_time_format = null;
	private String yl_confirm_time = null;
	private String yl_confirm_time_format = null;
	private String yl_sn = null;
	private String content = null;
	private String is_sc = null;
	private String info = null;
	private String begin_time_format = null;
	private String end_time = null;
	private String begin_time = null;
	private String used = null;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getLogo()
	{
		return logo;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public String getMerchant_logo()
	{
		return merchant_logo;
	}

	public void setMerchant_logo(String merchant_logo)
	{
		this.merchant_logo = merchant_logo;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getCreate_time_format()
	{
		return create_time_format;
	}

	public void setCreate_time_format(String create_time_format)
	{
		this.create_time_format = create_time_format;
	}

	public String getYl_create_time()
	{
		return yl_create_time;
	}

	public void setYl_create_time(String yl_create_time)
	{
		this.yl_create_time = yl_create_time;
	}

	public String getYl_create_time_format()
	{
		return yl_create_time_format;
	}

	public void setYl_create_time_format(String yl_create_time_format)
	{
		this.yl_create_time_format = yl_create_time_format;
	}

	public String getYl_confirm_time()
	{
		return yl_confirm_time;
	}

	public void setYl_confirm_time(String yl_confirm_time)
	{
		this.yl_confirm_time = yl_confirm_time;
	}

	public String getYl_confirm_time_format()
	{
		return yl_confirm_time_format;
	}

	public void setYl_confirm_time_format(String yl_confirm_time_format)
	{
		this.yl_confirm_time_format = yl_confirm_time_format;
	}

	public String getYl_sn()
	{
		return yl_sn;
	}

	public void setYl_sn(String yl_sn)
	{
		this.yl_sn = yl_sn;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getIs_sc()
	{
		return is_sc;
	}

	public void setIs_sc(String is_sc)
	{
		this.is_sc = is_sc;
	}

	public String getInfo()
	{
		return info;
	}

	public void setInfo(String info)
	{
		this.info = info;
	}

	public String getBegin_time_format()
	{
		return begin_time_format;
	}

	public void setBegin_time_format(String begin_time_format)
	{
		this.begin_time_format = begin_time_format;
	}

	public String getEnd_time()
	{
		return end_time;
	}

	public void setEnd_time(String end_time)
	{
		this.end_time = end_time;
	}

	public String getBegin_time()
	{
		return begin_time;
	}

	public void setBegin_time(String begin_time)
	{
		this.begin_time = begin_time;
	}

	public String getUsed()
	{
		return used;
	}

	public void setUsed(String used)
	{
		this.used = used;
	}

}