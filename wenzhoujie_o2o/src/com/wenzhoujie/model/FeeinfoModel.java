package com.wenzhoujie.model;

public class FeeinfoModel
{

	private String item = null;
	private String value = null;

	public String getItem()
	{
		return item;
	}

	public void setItem(String item)
	{
		this.item = item;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

}
