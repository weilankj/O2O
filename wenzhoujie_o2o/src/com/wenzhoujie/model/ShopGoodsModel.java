package com.wenzhoujie.model;

public class ShopGoodsModel {
	private String add_balance_price;
	private String attr;
	private String attr_str;
	private String buy_type;
	private String create_time;
	private String deal_id;
	private String icon;
	private String id;
	private String name;
	private String number;
	private String return_money;
	private String return_score;
	private String return_total_money;
	private String return_total_score;
	private String session_id;
	private String sub_name;
	private String supplier_id;
	private String total_price;
	private String unit_price;
	private String update_time;
	private String user_id;
	private String verify_code;
	public String getAdd_balance_price() {
		return add_balance_price;
	}
	public void setAdd_balance_price(String add_balance_price) {
		this.add_balance_price = add_balance_price;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public String getAttr_str() {
		return attr_str;
	}
	public void setAttr_str(String attr_str) {
		this.attr_str = attr_str;
	}
	public String getBuy_type() {
		return buy_type;
	}
	public void setBuy_type(String buy_type) {
		this.buy_type = buy_type;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getDeal_id() {
		return deal_id;
	}
	public void setDeal_id(String deal_id) {
		this.deal_id = deal_id;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getReturn_money() {
		return return_money;
	}
	public void setReturn_money(String return_money) {
		this.return_money = return_money;
	}
	public String getReturn_score() {
		return return_score;
	}
	public void setReturn_score(String return_score) {
		this.return_score = return_score;
	}
	public String getReturn_total_money() {
		return return_total_money;
	}
	public void setReturn_total_money(String return_total_money) {
		this.return_total_money = return_total_money;
	}
	public String getReturn_total_score() {
		return return_total_score;
	}
	public void setReturn_total_score(String return_total_score) {
		this.return_total_score = return_total_score;
	}
	public String getSession_id() {
		return session_id;
	}
	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	public String getSub_name() {
		return sub_name;
	}
	public void setSub_name(String sub_name) {
		this.sub_name = sub_name;
	}
	public String getSupplier_id() {
		return supplier_id;
	}
	public void setSupplier_id(String supplier_id) {
		this.supplier_id = supplier_id;
	}
	public String getTotal_price() {
		return total_price;
	}
	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}
	public String getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(String unit_price) {
		this.unit_price = unit_price;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getVerify_code() {
		return verify_code;
	}
	public void setVerify_code(String verify_code) {
		this.verify_code = verify_code;
	}
}
