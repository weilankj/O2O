package com.wenzhoujie.model;

import android.text.TextUtils;

public class FavorableListActItemModel
{
	private String id = null;
	private String title = null;
	private String logo = null;
	private String logo_1 = null;
	private String logo_2 = null;
	private String image_3_w = null;
	private String image_3_h = null;
	private String merchant_logo = null;
	private String create_time = null;
	private String create_time_format = null;
	private String xpoint = null;
	private String ypoint = null;
	private String address = null;
	private String content = null;
	private String is_sc = null;
	private String distance;

	// //////////////////////add

	private String begin_time_format_string = null;

	public String getBegin_time_format_string()
	{
		return begin_time_format_string;
	}

	public void setBegin_time_format_string(String begin_time_format_string)
	{
		this.begin_time_format_string = begin_time_format_string;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getLogo()
	{
		return logo;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public String getLogo_1()
	{
		return logo_1;
	}

	public void setLogo_1(String logo_1)
	{
		this.logo_1 = logo_1;
	}

	public String getLogo_2()
	{
		return logo_2;
	}

	public void setLogo_2(String logo_2)
	{
		this.logo_2 = logo_2;
	}

	public String getImage_3_w()
	{
		return image_3_w;
	}

	public void setImage_3_w(String image_3_w)
	{
		this.image_3_w = image_3_w;
	}

	public String getImage_3_h()
	{
		return image_3_h;
	}

	public void setImage_3_h(String image_3_h)
	{
		this.image_3_h = image_3_h;
	}

	public String getMerchant_logo()
	{
		return merchant_logo;
	}

	public void setMerchant_logo(String merchant_logo)
	{
		this.merchant_logo = merchant_logo;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getCreate_time_format()
	{
		return create_time_format;
	}

	public void setCreate_time_format(String create_time_format)
	{
		this.create_time_format = create_time_format;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getIs_sc()
	{
		return is_sc;
	}

	public void setIs_sc(String is_sc)
	{
		this.is_sc = is_sc;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		if (Integer.valueOf(distance) > 5000)
		{
			this.distance = "大于5公里";
		} else
		{
			this.distance = distance + "米";
		}

	}

	public String getComment_count()
	{
		return comment_count;
	}

	public void setComment_count(String comment_count)
	{
		this.comment_count = comment_count;
	}

	public String getMerchant_id()
	{
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id)
	{
		this.merchant_id = merchant_id;
	}

	public String getView_count()
	{
		return view_count;
	}

	public void setView_count(String view_count)
	{
		this.view_count = view_count;
	}

	public String getSms_count()
	{
		return sms_count;
	}

	public void setSms_count(String sms_count)
	{
		this.sms_count = sms_count;
	}

	public String getPrint_count()
	{
		return print_count;
	}

	public void setPrint_count(String print_count)
	{
		this.print_count = print_count;
	}

	public String getYouhui_type()
	{
		return youhui_type;
	}

	public void setYouhui_type(String youhui_type)
	{
		this.youhui_type = youhui_type;
	}

	public String getTotal_num()
	{
		return total_num;
	}

	public void setTotal_num(String total_num)
	{
		this.total_num = total_num;
	}

	public String getUse_notice()
	{
		return use_notice;
	}

	public void setUse_notice(String use_notice)
	{
		this.use_notice = use_notice;
	}

	public String getBegin_time_format()
	{
		return begin_time_format;
	}

	public void setBegin_time_format(String begin_time_format)
	{
		this.begin_time_format = begin_time_format;
		if (!TextUtils.isEmpty(begin_time_format))
		{
			this.begin_time_format_string = "发布时间:" + begin_time_format;
		}
	}

	public String getEnd_time_format()
	{
		return end_time_format;
	}

	public void setEnd_time_format(String end_time_format)
	{
		this.end_time_format = "有效期：" + end_time_format;
	}

	public String getYcq()
	{
		return ycq;
	}

	public void setYcq(String ycq)
	{
		this.ycq = ycq;
	}

	public String getAdv_url()
	{
		return adv_url;
	}

	public void setAdv_url(String adv_url)
	{
		this.adv_url = adv_url;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	private String comment_count = null;
	private String merchant_id = null;
	private String view_count = null;
	private String sms_count = null;
	private String print_count = null;
	private String youhui_type = null;
	private String total_num = null;
	private String use_notice = null;
	private String begin_time_format = null;
	private String end_time_format = null;
	private String ycq = null;
	private String adv_url = null;
	private String city_name = null;
}