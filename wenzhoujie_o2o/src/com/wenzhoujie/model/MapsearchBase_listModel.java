package com.wenzhoujie.model;

import java.io.Serializable;

import com.wenzhoujie.utils.SDTypeParseUtil;

public class MapsearchBase_listModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String icon;
	private String address;
	private String xpoint;
	private String ypoint;
	private String supplier_name;
	private String type;
	private String distance;
	// ==========================add
	private int typeFormat;
	private double xpointFormat;
	private double ypointFormat;

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public double getXpointFormat()
	{
		return xpointFormat;
	}

	public void setXpointFormat(double xpointFormat)
	{
		this.xpointFormat = xpointFormat;
	}

	public double getYpointFormat()
	{
		return ypointFormat;
	}

	public void setYpointFormat(double ypointFormat)
	{
		this.ypointFormat = ypointFormat;
	}

	public int getTypeFormat()
	{
		return typeFormat;
	}

	public void setTypeFormat(int typeFormat)
	{
		this.typeFormat = typeFormat;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
		setXpointFormat(SDTypeParseUtil.getDoubleFromString(xpoint, 0));
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
		setYpointFormat(SDTypeParseUtil.getDoubleFromString(ypoint, 0));
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
		setTypeFormat(SDTypeParseUtil.getIntFromString(type, 0));
	}

	public String getSupplier_name()
	{
		return supplier_name;
	}

	public void setSupplier_name(String supplier_name)
	{
		this.supplier_name = supplier_name;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}
