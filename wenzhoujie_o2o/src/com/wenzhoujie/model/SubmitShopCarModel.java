package com.wenzhoujie.model;

public class SubmitShopCarModel {
	
	private String goods_id;
	private String attr_id_a = "";
	private String attr_id_b = "";
	private String attr_value_a;
	private String attr_value_b;
	private String num;
	public String getGoods_id() {
		return goods_id;
	}
	public void setGoods_id(String goods_id) {
		this.goods_id = goods_id;
	}
	public String getAttr_id_a() {
		return attr_id_a;
	}
	public void setAttr_id_a(String attr_id_a) {
		this.attr_id_a = attr_id_a;
	}
	public String getAttr_id_b() {
		return attr_id_b;
	}
	public void setAttr_id_b(String attr_id_b) {
		this.attr_id_b = attr_id_b;
	}
	public String getAttr_value_a() {
		return attr_value_a;
	}
	public void setAttr_value_a(String attr_value_a) {
		this.attr_value_a = attr_value_a;
	}
	public String getAttr_value_b() {
		return attr_value_b;
	}
	public void setAttr_value_b(String attr_value_b) {
		this.attr_value_b = attr_value_b;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
}
