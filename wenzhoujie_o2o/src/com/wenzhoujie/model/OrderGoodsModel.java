package com.wenzhoujie.model;

public class OrderGoodsModel
{
	public static final int EMPTY_STATE = -999;
	
	private String id = null;
	private String goods_id = null;
	private String name = null;
	private String num = null;
	private String price = null;
	private String price_format = null;
	private String total_money = null;
	private String total_money_format = null;
	private String attr_content = null;
	private String image = null;
	
	private int delivery_status; // 0:未发货 1:已发货 5.无需发货
	private int is_arrival; // 0:未收货1:已收货2:没收到货(维权)
	private int is_refund; // 是否支持退款，由商品表同步而来，0不支持 1支持
	private int refund_status; // 0未退款 1退款中 2已退款 3退款被拒

	// add
	private int order_status; // 订单状态
	private int pay_status;
	private int refundState = EMPTY_STATE;
	private int deliveryState = EMPTY_STATE;
	
	/**
	 * 
	 * @return true:实体商品，false:团购商品
	 */
	public boolean isShop()
	{
		boolean isShop = true;
		if (delivery_status == 5)
		{
			isShop = false;
		}
		return isShop;
	}
	
	/**
	 * 
	 * @return 0:可以退款，1:退款中，2:已退款，3:退款被拒
	 */
	public int getRefundState()
	{
		if (refundState == EMPTY_STATE)
		{
			if (delivery_status == 0)
			{
				if (pay_status == 2)
				{
					if (is_refund == 1)
					{
						if (refund_status == 0)
						{
							refundState = 0;
						} else if (refund_status == 1)
						{
							refundState = 1;
						} else if (refund_status == 2)
						{
							refundState = 2;
						} else if (refund_status == 3)
						{
							refundState = 3;
						}
					}
				}
			} else if (delivery_status == 5)
			{
				if (pay_status == 2)
				{
					if (is_refund == 1)
					{
						if (order_status == 0)
						{
							refundState = 0;
						} else if (order_status == 1)
						{
							if (refund_status == 1)
							{
								refundState = 1;
							} else if (refund_status == 2)
							{
								refundState = 2;
							} else if (refund_status == 3)
							{
								refundState = 3;
							}
						}
					}
				}
			}
		}
		return refundState;
	}
	
	/**
	 * 
	 * @return 0:未发货， 1:已发货，但是未收到货(查询物流操作,确认收货操作，没收到货操作)，2:已收货，3:维权中
	 */
	public int getDeliveryState()
	{
		if (deliveryState == EMPTY_STATE)
		{
			if (order_status == 0)
			{
				if (delivery_status != 5)
				{
					if (delivery_status == 0)
					{
						deliveryState = 0;
					} else if (delivery_status == 1)
					{
						if (is_arrival == 0)
						{
							deliveryState = 1;
						} else if (is_arrival == 1)
						{
							deliveryState = 2;
						} else if (is_arrival == 2)
						{
							deliveryState = 3;
						}
					}
				}
			}
		}
		return deliveryState;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getGoods_id()
	{
		return goods_id;
	}

	public void setGoods_id(String goods_id)
	{
		this.goods_id = goods_id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNum()
	{
		return num;
	}

	public void setNum(String num)
	{
		this.num = num;
	}

	public String getPrice()
	{
		return price;
	}

	public void setPrice(String price)
	{
		this.price = price;
	}

	public String getPrice_format()
	{
		return price_format;
	}

	public void setPrice_format(String price_format)
	{
		this.price_format = price_format;
	}

	public String getTotal_money()
	{
		return total_money;
	}

	public void setTotal_money(String total_money)
	{
		this.total_money = total_money;
	}

	public String getTotal_money_format()
	{
		return total_money_format;
	}

	public void setTotal_money_format(String total_money_format)
	{
		this.total_money_format = total_money_format;
	}

	public String getAttr_content()
	{
		return attr_content;
	}

	public void setAttr_content(String attr_content)
	{
		this.attr_content = attr_content;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}
	
	public int getRefund_status()
	{
		return refund_status;
	}

	public void setRefund_status(int refund_status)
	{
		this.refund_status = refund_status;
	}

}