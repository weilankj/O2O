package com.wenzhoujie.model;

import java.util.List;

public class PrivateLettersItemActivityMsg_listModel
{
	private String miid = null;
	private String mlid = null;
	private String uid = null;
	private String message = null;
	private String time = null;
	private String tuid = null;
	private String tuser_name = null;
	private String tuser_avatar = null;
	private String content = null;
	private String user_name = null;
	private String user_avatar = null;
	private List<UItemExpressModel> parse_expres = null;

	public List<UItemExpressModel> getParse_expres()
	{
		return parse_expres;
	}

	public void setParse_expres(List<UItemExpressModel> parse_expres)
	{
		this.parse_expres = parse_expres;
	}

	public String getMiid()
	{
		return miid;
	}

	public void setMiid(String miid)
	{
		this.miid = miid;
	}

	public String getMlid()
	{
		return mlid;
	}

	public void setMlid(String mlid)
	{
		this.mlid = mlid;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public String getTuid()
	{
		return tuid;
	}

	public void setTuid(String tuid)
	{
		this.tuid = tuid;
	}

	public String getTuser_name()
	{
		return tuser_name;
	}

	public void setTuser_name(String tuser_name)
	{
		this.tuser_name = tuser_name;
	}

	public String getTuser_avatar()
	{
		return tuser_avatar;
	}

	public void setTuser_avatar(String tuser_avatar)
	{
		this.tuser_avatar = tuser_avatar;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}
}