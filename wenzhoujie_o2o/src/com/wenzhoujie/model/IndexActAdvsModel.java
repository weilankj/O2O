package com.wenzhoujie.model;

import java.io.Serializable;

public class IndexActAdvsModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String img;
	private String type;
	private IndexActAdvsDataModel data;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getImg()
	{
		return img;
	}

	public void setImg(String img)
	{
		this.img = img;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public IndexActAdvsDataModel getData()
	{
		return data;
	}

	public void setData(IndexActAdvsDataModel data)
	{
		this.data = data;
	}

}
