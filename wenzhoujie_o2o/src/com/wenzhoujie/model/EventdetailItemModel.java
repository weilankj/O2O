package com.wenzhoujie.model;

import java.util.List;

public class EventdetailItemModel
{
	private String id = null;
	private String name = null;
	private String icon = null;
	private String event_begin_time = null;
	private String event_end_time = null;
	private String submit_begin_time = null;
	private String submit_end_time = null;
	private String user_id = null;
	private String content = null;
	private String cate_id = null;
	private String city_id = null;
	private String address = null;
	private String xpoint = null;
	private String ypoint = null;
	private String locate_match = null;
	private String locate_match_row = null;
	private String cate_match = null;
	private String cate_match_row = null;
	private String name_match = null;
	private String name_match_row = null;
	private String submit_count = null;
	private String reply_count = null;
	private String brief = null;
	private String sort = null;
	private String is_effect = null;
	private String click_count = null;
	private String is_recommend = null;
	private String supplier_id = null;
	private String publish_wait = null;
	private List<EventdetailItemField_listModel> field_list = null;
	private List<EventdetailItemCommentsModel> comments = null;

	// ------add------
	private int is_submit;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public String getEvent_begin_time()
	{
		return event_begin_time;
	}

	public void setEvent_begin_time(String event_begin_time)
	{
		this.event_begin_time = event_begin_time;
	}

	public String getEvent_end_time()
	{
		return event_end_time;
	}

	public void setEvent_end_time(String event_end_time)
	{
		this.event_end_time = event_end_time;
	}

	public String getSubmit_begin_time()
	{
		return submit_begin_time;
	}

	public void setSubmit_begin_time(String submit_begin_time)
	{
		this.submit_begin_time = submit_begin_time;
	}

	public String getSubmit_end_time()
	{
		return submit_end_time;
	}

	public void setSubmit_end_time(String submit_end_time)
	{
		this.submit_end_time = submit_end_time;
	}

	public String getUser_id()
	{
		return user_id;
	}

	public void setUser_id(String user_id)
	{
		this.user_id = user_id;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getCate_id()
	{
		return cate_id;
	}

	public void setCate_id(String cate_id)
	{
		this.cate_id = cate_id;
	}

	public String getCity_id()
	{
		return city_id;
	}

	public void setCity_id(String city_id)
	{
		this.city_id = city_id;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
	}

	public String getLocate_match()
	{
		return locate_match;
	}

	public void setLocate_match(String locate_match)
	{
		this.locate_match = locate_match;
	}

	public String getLocate_match_row()
	{
		return locate_match_row;
	}

	public void setLocate_match_row(String locate_match_row)
	{
		this.locate_match_row = locate_match_row;
	}

	public String getCate_match()
	{
		return cate_match;
	}

	public void setCate_match(String cate_match)
	{
		this.cate_match = cate_match;
	}

	public String getCate_match_row()
	{
		return cate_match_row;
	}

	public void setCate_match_row(String cate_match_row)
	{
		this.cate_match_row = cate_match_row;
	}

	public String getName_match()
	{
		return name_match;
	}

	public void setName_match(String name_match)
	{
		this.name_match = name_match;
	}

	public String getName_match_row()
	{
		return name_match_row;
	}

	public void setName_match_row(String name_match_row)
	{
		this.name_match_row = name_match_row;
	}

	public String getSubmit_count()
	{
		return submit_count;
	}

	public void setSubmit_count(String submit_count)
	{
		this.submit_count = submit_count;
	}

	public String getReply_count()
	{
		return reply_count;
	}

	public void setReply_count(String reply_count)
	{
		this.reply_count = reply_count;
	}

	public String getBrief()
	{
		return brief;
	}

	public void setBrief(String brief)
	{
		this.brief = brief;
	}

	public String getSort()
	{
		return sort;
	}

	public void setSort(String sort)
	{
		this.sort = sort;
	}

	public String getIs_effect()
	{
		return is_effect;
	}

	public void setIs_effect(String is_effect)
	{
		this.is_effect = is_effect;
	}

	public String getClick_count()
	{
		return click_count;
	}

	public void setClick_count(String click_count)
	{
		this.click_count = click_count;
	}

	public String getIs_recommend()
	{
		return is_recommend;
	}

	public void setIs_recommend(String is_recommend)
	{
		this.is_recommend = is_recommend;
	}

	public String getSupplier_id()
	{
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id)
	{
		this.supplier_id = supplier_id;
	}

	public String getPublish_wait()
	{
		return publish_wait;
	}

	public void setPublish_wait(String publish_wait)
	{
		this.publish_wait = publish_wait;
	}

	public List<EventdetailItemField_listModel> getField_list()
	{
		return field_list;
	}

	public void setField_list(List<EventdetailItemField_listModel> field_list)
	{
		this.field_list = field_list;
	}

	public int getIs_submit()
	{
		return is_submit;
	}

	public void setIs_submit(int is_submit)
	{
		this.is_submit = is_submit;
	}

	public List<EventdetailItemCommentsModel> getComments()
	{
		return comments;
	}

	public void setComments(List<EventdetailItemCommentsModel> comments)
	{
		this.comments = comments;
	}
}