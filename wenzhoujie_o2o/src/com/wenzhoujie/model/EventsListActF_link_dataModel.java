package com.wenzhoujie.model;

import java.util.List;

public class EventsListActF_link_dataModel
{
	private String id = null;
	private String name = null;
	private String sort = null;
	private String is_effect = null;
	private List<EventsListActF_link_dataLinksModel> links = null;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSort()
	{
		return sort;
	}

	public void setSort(String sort)
	{
		this.sort = sort;
	}

	public String getIs_effect()
	{
		return is_effect;
	}

	public void setIs_effect(String is_effect)
	{
		this.is_effect = is_effect;
	}

	public List<EventsListActF_link_dataLinksModel> getLinks()
	{
		return links;
	}

	public void setLinks(List<EventsListActF_link_dataLinksModel> links)
	{
		this.links = links;
	}
}