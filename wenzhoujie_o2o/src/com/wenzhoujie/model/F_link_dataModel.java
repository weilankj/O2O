package com.wenzhoujie.model;

import java.io.Serializable;
import java.util.List;

public class F_link_dataModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = null;
	private String name = null;
	private String sort = null;
	private String is_effect = null;
	private List<F_link_dataLinksModel> links = null;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSort()
	{
		return sort;
	}

	public void setSort(String sort)
	{
		this.sort = sort;
	}

	public String getIs_effect()
	{
		return is_effect;
	}

	public void setIs_effect(String is_effect)
	{
		this.is_effect = is_effect;
	}

	public List<F_link_dataLinksModel> getLinks()
	{
		return links;
	}

	public void setLinks(List<F_link_dataLinksModel> links)
	{
		this.links = links;
	}

}