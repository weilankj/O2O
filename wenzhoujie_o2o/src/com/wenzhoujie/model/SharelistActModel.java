package com.wenzhoujie.model;

import java.util.List;

import com.wenzhoujie.model.act.BaseActModel;

public class SharelistActModel extends BaseActModel
{

	private String tag;
	private String cid;
	private List<SharelistItemModelnew> item;
	private List<IndexActAdvsModel> advs;
	private PageModel page;

	public String getTag()
	{
		return tag;
	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}

	public String getCid()
	{
		return cid;
	}

	public void setCid(String cid)
	{
		this.cid = cid;
	}

	public List<SharelistItemModelnew> getItem()
	{
		return item;
	}

	public void setItem(List<SharelistItemModelnew> item)
	{
		this.item = item;
	}

	public List<IndexActAdvsModel> getAdvs()
	{
		return advs;
	}

	public void setAdvs(List<IndexActAdvsModel> advs)
	{
		this.advs = advs;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}
