package com.wenzhoujie.model;

import java.util.List;

import android.view.View;

public class EventdetailItemField_listModel
{
	private int id;
	private String event_id = null;
	private String field_show_name = null;
	private int field_type;
	private List<String> value_scope = null;
	private String sort = null;

	// ==============add
	private View view;

	public View getView()
	{
		return view;
	}

	public void setView(View view)
	{
		this.view = view;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getEvent_id()
	{
		return event_id;
	}

	public void setEvent_id(String event_id)
	{
		this.event_id = event_id;
	}

	public String getField_show_name()
	{
		return field_show_name;
	}

	public void setField_show_name(String field_show_name)
	{
		this.field_show_name = field_show_name;
	}

	public int getField_type()
	{
		return field_type;
	}

	public void setField_type(int field_type)
	{
		this.field_type = field_type;
	}

	public List<String> getValue_scope()
	{
		return value_scope;
	}

	public void setValue_scope(List<String> value_scope)
	{
		this.value_scope = value_scope;
	}

	public String getSort()
	{
		return sort;
	}

	public void setSort(String sort)
	{
		this.sort = sort;
	}
}