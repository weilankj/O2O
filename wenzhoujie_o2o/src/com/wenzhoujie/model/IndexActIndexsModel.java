package com.wenzhoujie.model;

import java.io.Serializable;

public class IndexActIndexsModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = null;
	private String name = null;
	private String vice_name = null;
	private String desc = null;
	private String is_hot = null;
	private String is_new = null;
	private String img = null;
	private String type = null;
	private IndexActAdvsDataModel data = null;

	// /////////////////add
	private String temp = null;

	public void setTempName(String tempName)
	{
		this.temp = this.name;
		this.name = tempName;
	}

	public void restoreOriginalName()
	{
		this.name = this.temp;
	}

	public IndexActAdvsDataModel getData()
	{
		return data;
	}

	public void setData(IndexActAdvsDataModel data)
	{
		this.data = data;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getVice_name()
	{
		return vice_name;
	}

	public void setVice_name(String vice_name)
	{
		this.vice_name = vice_name;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public String getIs_hot()
	{
		return is_hot;
	}

	public void setIs_hot(String is_hot)
	{
		this.is_hot = is_hot;
	}

	public String getIs_new()
	{
		return is_new;
	}

	public void setIs_new(String is_new)
	{
		this.is_new = is_new;
	}

	public String getImg()
	{
		return img;
	}

	public void setImg(String img)
	{
		this.img = img;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

}