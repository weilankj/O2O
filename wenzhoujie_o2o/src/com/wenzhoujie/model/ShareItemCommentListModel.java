package com.wenzhoujie.model;

import java.util.List;

import com.wenzhoujie.model.act.BaseActModel;

public class ShareItemCommentListModel extends BaseActModel
{
	private String comment_id = null;
	private String share_id = null;
	private String uid = null;
	private String parent_id = null;
	private String content = null;
	private String create_time = null;
	private String user_name = null;
	private String user_avatar = null;
	private String time = null;
	private List<ShareItemExpressModel> parse_expres = null;
	private String parse_user = null;

	public String getComment_id()
	{
		return comment_id;
	}

	public void setComment_id(String comment_id)
	{
		this.comment_id = comment_id;
	}

	public String getShare_id()
	{
		return share_id;
	}

	public void setShare_id(String share_id)
	{
		this.share_id = share_id;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getParent_id()
	{
		return parent_id;
	}

	public void setParent_id(String parent_id)
	{
		this.parent_id = parent_id;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public List<ShareItemExpressModel> getParse_expres()
	{
		return parse_expres;
	}

	public void setParse_expres(List<ShareItemExpressModel> parse_expres)
	{
		this.parse_expres = parse_expres;
	}

	public String getParse_user()
	{
		return parse_user;
	}

	public void setParse_user(String parse_user)
	{
		this.parse_user = parse_user;
	}
}