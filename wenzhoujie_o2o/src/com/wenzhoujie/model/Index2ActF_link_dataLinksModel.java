package com.wenzhoujie.model;

public class Index2ActF_link_dataLinksModel
{
	private String id = null;
	private String name = null;
	private String group_id = null;
	private String url = null;
	private String is_effect = null;
	private String sort = null;
	private String img = null;
	private String description = null;
	private String count = null;
	private String show_index = null;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getGroup_id()
	{
		return group_id;
	}

	public void setGroup_id(String group_id)
	{
		this.group_id = group_id;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getIs_effect()
	{
		return is_effect;
	}

	public void setIs_effect(String is_effect)
	{
		this.is_effect = is_effect;
	}

	public String getSort()
	{
		return sort;
	}

	public void setSort(String sort)
	{
		this.sort = sort;
	}

	public String getImg()
	{
		return img;
	}

	public void setImg(String img)
	{
		this.img = img;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getCount()
	{
		return count;
	}

	public void setCount(String count)
	{
		this.count = count;
	}

	public String getShow_index()
	{
		return show_index;
	}

	public void setShow_index(String show_index)
	{
		this.show_index = show_index;
	}

}