package com.wenzhoujie.model;

import java.io.Serializable;

public class IndexActSupplier_listModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = null;
	private String name = null;
	private String preview = null;
	private int shop_type;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getPreview()
	{
		return preview;
	}

	public void setPreview(String preview)
	{
		this.preview = preview;
	}
	
	public int getShop_type()
	{
		return shop_type;
	}

	public void setShop_type(int shop_type)
	{
		this.shop_type = shop_type;
	}

}