package com.wenzhoujie.model;

import java.util.List;

public class Bcate_listModel
{
	private String id = null;
	private String name = null;
	private List<Bcate_listBcate_typeModel> bcate_type = null;

	// /////////////////////////add
	private boolean isSelect = false;
	private boolean isHasChild = false;

	public boolean isHasChild()
	{
		return isHasChild;
	}

	public void setHasChild(boolean isHasChild)
	{
		this.isHasChild = isHasChild;
	}

	public boolean isSelect()
	{
		return isSelect;
	}

	public void setSelect(boolean isSelect)
	{
		this.isSelect = isSelect;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<Bcate_listBcate_typeModel> getBcate_type()
	{
		return bcate_type;
	}

	public void setBcate_type(List<Bcate_listBcate_typeModel> bcate_type)
	{
		this.bcate_type = bcate_type;
		if (bcate_type != null && bcate_type.size() > 1)
		{
			setHasChild(true);
		} else
		{
			setHasChild(false);
		}
	}

}