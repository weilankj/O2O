package com.wenzhoujie.model;

import java.util.List;

import com.wenzhoujie.utils.SDTypeParseUtil;

public class SharelistItemModelnew
{

	private String share_id;
	private String img;
	public String org_img;
	private String height;
	private String width;
	// ============add
	private int heightFormat;
	private int widthFormat;
	private String content = null;
	private String reply_count = null;
	private String create_time = null;
	private String useravatar = null;
	private String nick_name = null;
	private String user_name = null;
	private List<ShareItemExpressModel> parse_expres = null;
	
	public List<ShareItemExpressModel> getParse_expres()
	{
		return parse_expres;
	}

	public void setParse_expres(List<ShareItemExpressModel> parse_expres)
	{
		this.parse_expres = parse_expres;
	}
	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}
	public String getUseravatar()
	{
		return useravatar;
	}

	public void setUseravatar(String useravatar)
	{
		this.useravatar = useravatar;
	}
	public String getReply_count()
	{
		return reply_count;
	}

	public void setReply_count(String reply_count)
	{
		this.reply_count = reply_count;
	}
	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}
	
	public String getNick_name()
	{
		return nick_name;
	}

	public void setNick_name(String nick_name)
	{
		this.nick_name = nick_name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public int getWidthFormat()
	{
		return widthFormat;
	}

	public void setWidthFormat(int widthFormat)
	{
		this.widthFormat = widthFormat;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
		setWidthFormat(SDTypeParseUtil.getIntFromString(width, 0));
	}

	public int getHeightFormat()
	{
		return heightFormat;
	}

	public void setHeightFormat(int heightFormat)
	{
		this.heightFormat = heightFormat;
	}

	public String getShare_id()
	{
		return share_id;
	}

	public void setShare_id(String share_id)
	{
		this.share_id = share_id;
	}

	public String getImg()
	{
		return img;
	}

	public void setImg(String img)
	{
		this.img = img;
	}

	public String getHeight()
	{
		return height;
	}

	public void setHeight(String height)
	{
		this.height = height;
		setHeightFormat(SDTypeParseUtil.getIntFromString(height, 0));
	}

}
