package com.wenzhoujie.model;

import java.io.Serializable;

import android.text.TextUtils;

import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.utils.SDDistanceUtil;
import com.wenzhoujie.utils.SDFormatUtil;

public class IndexActDeal_listModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String auto_order;
	private String sub_name;
	private String brief;
	private String cate_id;
	private String supplier_id;
	private String current_price;
	private String origin_price;
	private String img;
	private String begin_time;
	private String end_time;
	private String buy_type;
	private String buy_count;
	private String end_time_format;
	private String begin_time_format;
	private String ypoint;
	private String xpoint;
	private String is_hot = null;
	private String save_price;
	private String discount;
	private String distance;
	private int biz_shop_cate_id;
	// /////////////////////////////////////////手工添加字段
	private String current_price_format = null;
	private String origin_price_format = null;
	private String buy_count_format = null;
	private String distance_format = null;

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
		calculateDistance();
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
		calculateDistance();
	}

	public void calculateDistance()
	{
		double calDistance = BaiduMapManager.getInstance().getDistanceFromMyLocation(ypoint, xpoint);
		setDistance_format(SDDistanceUtil.getFormatDistance(calDistance));
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;

	}

	public String getDistance_format()
	{
		return distance_format;
	}

	public void setDistance_format(String distance_format)
	{
		this.distance_format = distance_format;
	}

	public String getDiscount()
	{
		return discount;
	}

	public void setDiscount(String discount)
	{
		this.discount = discount;
	}

	public String getIs_hot()
	{
		return is_hot;
	}

	public void setIs_hot(String is_hot)
	{
		this.is_hot = is_hot;
	}

	public String getSave_price()
	{
		return save_price;
	}

	public void setSave_price(String save_price)
	{
		this.save_price = save_price;
	}

	public String getBuy_count_format()
	{
		return buy_count_format;
	}

	public void setBuy_count_format(String buy_count_format)
	{
		this.buy_count_format = buy_count_format;
	}

	public String getCurrent_price_format()
	{
		return current_price_format;
	}

	public void setCurrent_price_format(String current_price_format)
	{
		this.current_price_format = current_price_format;
	}

	public String getOrigin_price_format()
	{
		return origin_price_format;
	}

	public void setOrigin_price_format(String origin_price_format)
	{
		this.origin_price_format = origin_price_format;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAuto_order()
	{
		return auto_order;
	}

	public void setAuto_order(String auto_order)
	{
		this.auto_order = auto_order;
	}

	public String getSub_name()
	{
		return sub_name;
	}

	public void setSub_name(String sub_name)
	{
		this.sub_name = sub_name;
	}

	public String getBrief()
	{
		return brief;
	}

	public void setBrief(String brief)
	{
		this.brief = brief;
	}

	public String getCate_id()
	{
		return cate_id;
	}

	public void setCate_id(String cate_id)
	{
		this.cate_id = cate_id;
	}

	public String getSupplier_id()
	{
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id)
	{
		this.supplier_id = supplier_id;
	}

	public String getCurrent_price()
	{
		return current_price;
	}

	public void setCurrent_price(String current_price)
	{
		this.current_price = current_price;
		this.current_price_format = SDFormatUtil.formatMoneyChina(current_price);
	}

	public String getOrigin_price()
	{
		return origin_price;
	}

	public void setOrigin_price(String origin_price)
	{
		this.origin_price = origin_price;
		this.origin_price_format = SDFormatUtil.formatMoneyChina(origin_price);
	}

	public String getImg()
	{
		return img;
	}

	public void setImg(String img)
	{
		this.img = img;
	}

	public String getBegin_time()
	{
		return begin_time;
	}

	public void setBegin_time(String begin_time)
	{
		this.begin_time = begin_time;
	}

	public String getEnd_time()
	{
		return end_time;
	}

	public void setEnd_time(String end_time)
	{
		this.end_time = end_time;
	}

	public String getBuy_type()
	{
		return buy_type;
	}

	public void setBuy_type(String buy_type)
	{
		this.buy_type = buy_type;
	}

	public String getBuy_count()
	{
		return buy_count;
	}

	public void setBuy_count(String buy_count)
	{
		this.buy_count = buy_count;
		if (!TextUtils.isEmpty(buy_count))
		{
			this.buy_count_format = "售出" + buy_count;
		}
	}

	public String getEnd_time_format()
	{
		return end_time_format;
	}

	public void setEnd_time_format(String end_time_format)
	{
		this.end_time_format = end_time_format;
	}

	public String getBegin_time_format()
	{
		return begin_time_format;
	}

	public void setBegin_time_format(String begin_time_format)
	{
		this.begin_time_format = begin_time_format;
	}

	public int getBiz_shop_cate_id() {
		return biz_shop_cate_id;
	}

	public void setBiz_shop_cate_id(int biz_shop_cate_id) {
		this.biz_shop_cate_id = biz_shop_cate_id;
	}

}
