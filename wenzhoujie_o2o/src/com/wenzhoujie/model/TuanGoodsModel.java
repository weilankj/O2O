package com.wenzhoujie.model;

import com.wenzhoujie.utils.SDDistanceUtil;
import com.wenzhoujie.utils.SDFormatUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class TuanGoodsModel
{
	private String id = null;
	private String name = null;
	private String sub_name = null;
	private String icon = null;
	private String origin_price = null;
	private String current_price = null;
	private String buy_count = null;
	private String auto_order = null;
	private String is_taday = null;
	private String goods_brief = null;
	private String distance;
	// -----------add-----------

	private int auto_order_fromat_int = 0;
	private int is_taday_format_int = 0;
	private String buy_count_format_string = null;
	private String origin_price_fromat_string = null;
	private String current_price_fromat_string = null;
	private String deal_id = null;
	private String distance_format;

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
		setDistance_format(SDDistanceUtil.getFormatDistance(SDTypeParseUtil.getDoubleFromString(distance, 0)));
	}

	public String getDistance_format()
	{
		return distance_format;
	}

	public void setDistance_format(String distance_format)
	{
		this.distance_format = distance_format;
	}

	public String getDeal_id()
	{
		return deal_id;
	}

	public void setDeal_id(String deal_id)
	{
		this.deal_id = deal_id;
	}

	public String getCurrent_price_fromat_string()
	{
		return current_price_fromat_string;
	}

	public void setCurrent_price_fromat_string(String current_price_fromat_string)
	{
		this.current_price_fromat_string = current_price_fromat_string;
	}

	public String getGoods_brief()
	{
		return goods_brief;
	}

	public void setGoods_brief(String goods_brief)
	{
		this.goods_brief = goods_brief;
	}

	public String getOrigin_price_fromat_string()
	{
		return origin_price_fromat_string;
	}

	public void setOrigin_price_fromat_string(String origin_price_fromat_string)
	{
		this.origin_price_fromat_string = origin_price_fromat_string;
	}

	public String getBuy_count_format_string()
	{
		return buy_count_format_string;
	}

	public void setBuy_count_format_string(String buy_count_format_string)
	{
		this.buy_count_format_string = buy_count_format_string;
	}

	public int getIs_taday_format_int()
	{
		return is_taday_format_int;
	}

	public void setIs_taday_format_int(int is_taday_format_int)
	{
		this.is_taday_format_int = is_taday_format_int;
	}

	public int getAuto_order_fromat_int()
	{
		return auto_order_fromat_int;
	}

	public void setAuto_order_fromat_int(int auto_order_fromat_int)
	{
		this.auto_order_fromat_int = auto_order_fromat_int;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSub_name()
	{
		return sub_name;
	}

	public void setSub_name(String sub_name)
	{
		this.sub_name = sub_name;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public String getOrigin_price()
	{
		return origin_price;
	}

	public void setOrigin_price(String origin_price)
	{
		this.origin_price = origin_price;
		this.origin_price_fromat_string = SDFormatUtil.formatMoneyChina(origin_price);
	}

	public String getCurrent_price()
	{
		return current_price;
	}

	public void setCurrent_price(String current_price)
	{
		this.current_price = current_price;
		this.current_price_fromat_string = SDFormatUtil.formatMoneyChina(current_price);
	}

	public String getBuy_count()
	{
		return buy_count;
	}

	public void setBuy_count(String buy_count)
	{
		this.buy_count = buy_count;
		this.buy_count_format_string = "已售" + SDTypeParseUtil.getIntFromString(buy_count, 0);
	}

	public String getAuto_order()
	{
		return auto_order;
	}

	public void setAuto_order(String auto_order)
	{
		this.auto_order = auto_order;
		this.auto_order_fromat_int = SDTypeParseUtil.getIntFromString(auto_order, 0);
	}

	public String getIs_taday()
	{
		return is_taday;
	}

	public void setIs_taday(String is_taday)
	{
		this.is_taday = is_taday;
		this.is_taday_format_int = SDTypeParseUtil.getIntFromString(is_taday, 0);
	}

}