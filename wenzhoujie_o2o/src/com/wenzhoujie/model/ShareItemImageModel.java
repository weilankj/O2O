package com.wenzhoujie.model;

public class ShareItemImageModel
{
	private String share_id = null;
	private String id = null;
	private String img = null;
	private String small_img = null;
	private String type = null;
	private String price_format = null;
	private String Taoke_url = null;
	private String data_name = null;

	// ------add------
	private int img_width;
	private int img_height;

	public String getData_name()
	{
		return data_name;
	}

	public void setData_name(String data_name)
	{
		this.data_name = data_name;
	}

	public String getTaoke_url()
	{
		return Taoke_url;
	}

	public void setTaoke_url(String taoke_url)
	{
		Taoke_url = taoke_url;
	}

	public String getPrice_format()
	{
		return price_format;
	}

	public void setPrice_format(String price_format)
	{
		this.price_format = price_format;
	}

	public String getShare_id()
	{
		return share_id;
	}

	public void setShare_id(String share_id)
	{
		this.share_id = share_id;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getImg()
	{
		return img;
	}

	public void setImg(String img)
	{
		this.img = img;
	}

	public String getSmall_img()
	{
		return small_img;
	}

	public void setSmall_img(String small_img)
	{
		this.small_img = small_img;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public int getImg_width()
	{
		return img_width;
	}

	public void setImg_width(int img_width)
	{
		this.img_width = img_width;
	}

	public int getImg_height()
	{
		return img_height;
	}

	public void setImg_height(int img_height)
	{
		this.img_height = img_height;
	}
}