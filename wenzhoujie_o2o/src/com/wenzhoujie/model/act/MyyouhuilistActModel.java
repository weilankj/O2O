package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.MyyouhuilistActItemModel;
import com.wenzhoujie.model.PageModel;

public class MyyouhuilistActModel extends BaseActModel
{
	private List<MyyouhuilistActItemModel> item = null;
	private String count = null;
	private PageModel page = null;
	private String now = null;

	public List<MyyouhuilistActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<MyyouhuilistActItemModel> item)
	{
		this.item = item;
	}

	public String getCount()
	{
		return count;
	}

	public void setCount(String count)
	{
		this.count = count;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public String getNow()
	{
		return now;
	}

	public void setNow(String now)
	{
		this.now = now;
	}

}