package com.wenzhoujie.model.act;

import com.wenzhoujie.model.ShareItemModel;

public class ShareActModel extends BaseActModel
{
	private ShareItemModel item = null;

	public ShareItemModel getItem()
	{
		return item;
	}

	public void setItem(ShareItemModel item)
	{
		this.item = item;
	}
}