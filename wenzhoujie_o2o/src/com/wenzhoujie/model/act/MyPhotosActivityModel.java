package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.MyPhotosActivityItemModel;
import com.wenzhoujie.model.PageModel;

public class MyPhotosActivityModel extends BaseActModel
{
	private PageModel page = null;
	private List<MyPhotosActivityItemModel> item = null;

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public List<MyPhotosActivityItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<MyPhotosActivityItemModel> item)
	{
		this.item = item;
	}
}