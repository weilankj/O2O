package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.UHome_userModel;
import com.wenzhoujie.model.UItemModel;
import com.wenzhoujie.model.UUserModel;

public class UActModel extends BaseActModel
{
	private UUserModel user = null;
	private UHome_userModel home_user = null;
	private List<UItemModel> item = null;
	private PageModel page = null;

	public UUserModel getUser()
	{
		return user;
	}

	public void setUser(UUserModel user)
	{
		this.user = user;
	}

	public UHome_userModel getHome_user()
	{
		return home_user;
	}

	public void setHome_user(UHome_userModel home_user)
	{
		this.home_user = home_user;
	}

	public List<UItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<UItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}