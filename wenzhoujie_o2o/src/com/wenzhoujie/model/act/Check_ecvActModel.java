package com.wenzhoujie.model.act;

public class Check_ecvActModel extends BaseActModel
{
	private String check_ecv_state = null;

	public String getCheck_ecv_state()
	{
		return check_ecv_state;
	}

	public void setCheck_ecv_state(String check_ecv_state)
	{
		this.check_ecv_state = check_ecv_state;
	}

}
