package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.Bcate_listModel;
import com.wenzhoujie.model.FavorableListActF_link_dataModel;
import com.wenzhoujie.model.FavorableListActItemModel;
import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.Quan_listModel;

public class FavorableListActModel extends BaseActModel
{
	private String email = null;
	private List<FavorableListActF_link_dataModel> f_link_data = null;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public List<FavorableListActF_link_dataModel> getF_link_data()
	{
		return f_link_data;
	}

	public void setF_link_data(List<FavorableListActF_link_dataModel> f_link_data)
	{
		this.f_link_data = f_link_data;
	}

	public List<Bcate_listModel> getBcate_list()
	{
		return bcate_list;
	}

	public void setBcate_list(List<Bcate_listModel> bcate_list)
	{
		this.bcate_list = bcate_list;
	}

	public List<Quan_listModel> getQuan_list()
	{
		return quan_list;
	}

	public void setQuan_list(List<Quan_listModel> quan_list)
	{
		this.quan_list = quan_list;
	}

	public List<FavorableListActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<FavorableListActItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public String getPage_title()
	{
		return page_title;
	}

	public void setPage_title(String page_title)
	{
		this.page_title = page_title;
	}

	private List<Bcate_listModel> bcate_list = null;
	private List<Quan_listModel> quan_list = null;
	private List<FavorableListActItemModel> item = null;
	private PageModel page = null;
	private String page_title = null;
}