package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.VoucherActBcate_listModel;
import com.wenzhoujie.model.VoucherActItemModel;

public class VoucherActModel extends BaseActModel
{
	private List<VoucherActItemModel> item = null;
	private PageModel page;

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public List<VoucherActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<VoucherActItemModel> item)
	{
		this.item = item;
	}

	public List<VoucherActBcate_listModel> getBcate_list()
	{
		return bcate_list;
	}

	public void setBcate_list(List<VoucherActBcate_listModel> bcate_list)
	{
		this.bcate_list = bcate_list;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public String getPage_title()
	{
		return page_title;
	}

	public void setPage_title(String page_title)
	{
		this.page_title = page_title;
	}

	private List<VoucherActBcate_listModel> bcate_list = null;
	private String email = null;
	private String city_name = null;
	private String page_title = null;
}