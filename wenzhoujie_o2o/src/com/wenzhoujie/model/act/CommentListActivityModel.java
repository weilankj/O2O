package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.CommentListActivityItemModel;
import com.wenzhoujie.model.PageModel;

public class CommentListActivityModel extends BaseActModel
{
	private List<CommentListActivityItemModel> item = null;
	private PageModel page = null;

	public List<CommentListActivityItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<CommentListActivityItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}