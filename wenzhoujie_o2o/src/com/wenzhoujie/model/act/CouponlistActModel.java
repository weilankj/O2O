package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.CouponlistActItemModel;
import com.wenzhoujie.model.PageModel;

public class CouponlistActModel extends BaseActModel
{
	private List<CouponlistActItemModel> item = null;
	private String count = null;
	private PageModel page = null;

	public List<CouponlistActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<CouponlistActItemModel> item)
	{
		this.item = item;
	}

	public String getCount()
	{
		return count;
	}

	public void setCount(String count)
	{
		this.count = count;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}