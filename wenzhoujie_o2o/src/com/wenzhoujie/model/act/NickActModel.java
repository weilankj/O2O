package com.wenzhoujie.model.act;

public class NickActModel extends BaseActModel
{
	private String uid = null;
	private String email = null;
	private String user_name = null;
	private String nick_name = null;
	private String user_avatar = null;

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getNick_name()
	{
		return nick_name;
	}

	public void setNick_name(String nick_name)
	{
		this.nick_name = nick_name;
	}
	
	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

}
