package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.util.List;

public class MoreCateActModel extends BaseActModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String top_cate_name;
	private List<MoreCateSubActModel> sub_cate_info;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTop_cate_name() {
		return top_cate_name;
	}
	public void setTop_cate_name(String top_cate_name) {
		this.top_cate_name = top_cate_name;
	}
	public List<MoreCateSubActModel> getSub_cate_info() {
		return sub_cate_info;
	}
	public void setSub_cate_info(List<MoreCateSubActModel> sub_cate_info) {
		this.sub_cate_info = sub_cate_info;
	}

	
	
	
}
