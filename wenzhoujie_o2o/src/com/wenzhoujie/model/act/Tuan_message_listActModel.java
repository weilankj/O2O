package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.PageModel;

public class Tuan_message_listActModel extends BaseActModel
{
	private List<GoodsCommentModel> message_list = null;

	private String message_count = null;

	private PageModel page = null;

	private String star_1 = null;
	private String star_2 = null;
	private String star_3 = null;
	private String star_4 = null;
	private String star_5 = null;
	private String star_dp_width_1 = null;
	private String star_dp_width_2 = null;
	private String star_dp_width_3 = null;
	private String star_dp_width_4 = null;
	private String star_dp_width_5 = null;
	private String buy_dp_avg = null;
	private String allow_dp = null;

	public String getStar_1()
	{
		return star_1;
	}

	public void setStar_1(String star_1)
	{
		this.star_1 = star_1;
	}

	public String getStar_2()
	{
		return star_2;
	}

	public void setStar_2(String star_2)
	{
		this.star_2 = star_2;
	}

	public String getStar_3()
	{
		return star_3;
	}

	public void setStar_3(String star_3)
	{
		this.star_3 = star_3;
	}

	public String getStar_4()
	{
		return star_4;
	}

	public void setStar_4(String star_4)
	{
		this.star_4 = star_4;
	}

	public String getStar_5()
	{
		return star_5;
	}

	public void setStar_5(String star_5)
	{
		this.star_5 = star_5;
	}

	public String getStar_dp_width_1()
	{
		return star_dp_width_1;
	}

	public void setStar_dp_width_1(String star_dp_width_1)
	{
		this.star_dp_width_1 = star_dp_width_1;
	}

	public String getStar_dp_width_2()
	{
		return star_dp_width_2;
	}

	public void setStar_dp_width_2(String star_dp_width_2)
	{
		this.star_dp_width_2 = star_dp_width_2;
	}

	public String getStar_dp_width_3()
	{
		return star_dp_width_3;
	}

	public void setStar_dp_width_3(String star_dp_width_3)
	{
		this.star_dp_width_3 = star_dp_width_3;
	}

	public String getStar_dp_width_4()
	{
		return star_dp_width_4;
	}

	public void setStar_dp_width_4(String star_dp_width_4)
	{
		this.star_dp_width_4 = star_dp_width_4;
	}

	public String getStar_dp_width_5()
	{
		return star_dp_width_5;
	}

	public void setStar_dp_width_5(String star_dp_width_5)
	{
		this.star_dp_width_5 = star_dp_width_5;
	}

	public String getBuy_dp_avg()
	{
		return buy_dp_avg;
	}

	public void setBuy_dp_avg(String buy_dp_avg)
	{
		this.buy_dp_avg = buy_dp_avg;
	}

	public String getAllow_dp()
	{
		return allow_dp;
	}

	public void setAllow_dp(String allow_dp)
	{
		this.allow_dp = allow_dp;
	}

	public List<GoodsCommentModel> getMessage_list()
	{
		return message_list;
	}

	public void setMessage_list(List<GoodsCommentModel> message_list)
	{
		this.message_list = message_list;
	}

	public String getMessage_count()
	{
		return message_count;
	}

	public void setMessage_count(String message_count)
	{
		this.message_count = message_count;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}
