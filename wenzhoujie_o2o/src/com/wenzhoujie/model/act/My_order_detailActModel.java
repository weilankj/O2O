package com.wenzhoujie.model.act;

import java.util.List;

import android.text.TextUtils;

import com.wenzhoujie.model.DealOrderItemModel;
import com.wenzhoujie.model.DeliveryModel;
import com.wenzhoujie.model.FeeinfoModel;
import com.wenzhoujie.model.OrderDetailCoupon_listModel;
import com.wenzhoujie.model.OrderGoodsModel;
import com.wenzhoujie.model.Order_parmModel;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class My_order_detailActModel extends BaseActModel
{
	private String id = null;
	private String sn = null;
	private String create_time = null;
	private String create_time_format = null;
	private String total_money = null;
	private String money = null;
	private String total_money_format = null;
	private String money_format = null;
	private String num = null;

	private List<DealOrderItemModel> orderGoods = null;

	private DeliveryModel deliveryAddr = null;

	private String content = null;
	private String send_mobile = null;
	private String tax_title = null;
	private String tax_id = null;
	private String deliver_time_id = null;
	private String payment_id = null;

	private Order_parmModel order_parm = null;
	private List<FeeinfoModel> feeinfo = null;
	private String evc_sn = null;
	private String evc_pwd = null;
	private String delivery_id = null;
	private int has_cancel = 0;
	private int has_edit_delivery = 0;
	private int has_edit_delivery_time = 0;
	private int has_edit_invoice = 0;
	private int has_edit_ecv = 0;
	private int has_edit_message = 0;
	private int has_edit_moblie = 0;
	private int has_pay = 0;
	private double pay_money = 0;
	private String use_user_money = null;
	private String kd_sn;// 快递单跟踪号
	private String kd_com;// 快递公司代码
	private List<OrderDetailCoupon_listModel> coupon_list;

	// 参数资料查看：http://code.google.com/p/kuaidi-api/wiki/Open_API_API_URL

	// =======================================
	private double use_user_money_format_double = 0;
	private String status_format = null;
	
	private int delivery_status;
	private int pay_status;
	private int order_status;
	private int refund_status;

	public String getStatus_format() {
		status_format="";
		//付款状态
		switch (pay_status) {
		case 0:
			status_format+="未支付";
			break;
		case 1:
			status_format+="部分付款";
			break;
		case 2:
			status_format+="全部付款";
			break;

		default:
			break;
		}
		//发货状态
		switch (delivery_status) {
		case 0:
			status_format+="、未发货";
			break;
		case 1:
			status_format+="、部分发货";
			break;
		case 2:
			status_format+="、全部发货";
			break;
		case 3:
			status_format+="、无需发货的订单";
			break;
		default:
			break;
		}
		//订单状态
		switch (order_status) {
		case 0:
			status_format+="、未结单";
			break;
		case 1:
			status_format+="、已结单";
			break;
		default:
			break;
		}
		//退款状态
		switch (refund_status) {
		case 1:
			status_format+="、退款申请中";
			break;
		case 2:
			status_format+="、退款已处理";
			break;
		default:
			break;
		}
		return status_format;
	}

	public void setStatus_format(String status_format)
	{
		this.status_format = status_format;
	}

	public List<OrderDetailCoupon_listModel> getCoupon_list()
	{
		return coupon_list;
	}

	public void setCoupon_list(List<OrderDetailCoupon_listModel> coupon_list)
	{
		this.coupon_list = coupon_list;
	}

	public double getUse_user_money_format_double()
	{
		return use_user_money_format_double;
	}

	public void setUse_user_money_format_double(double use_user_money_format_double)
	{
		this.use_user_money_format_double = use_user_money_format_double;
	}

	public String getKd_sn()
	{
		return kd_sn;
	}

	public String getKd_com()
	{
		return kd_com;
	}

	public void setKd_com(String kd_com)
	{
		this.kd_com = kd_com;
	}

	public void setKd_sn(String kd_sn)
	{
		this.kd_sn = kd_sn;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getSn()
	{
		return sn;
	}

	public void setSn(String sn)
	{
		this.sn = sn;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getCreate_time_format()
	{
		return create_time_format;
	}

	public void setCreate_time_format(String create_time_format)
	{
		this.create_time_format = create_time_format;
	}

	public String getTotal_money()
	{
		return total_money;
	}

	public void setTotal_money(String total_money)
	{
		this.total_money = total_money;
	}

	public String getMoney()
	{
		return money;
	}

	public void setMoney(String money)
	{
		this.money = money;
	}

	public String getTotal_money_format()
	{
		return total_money_format;
	}

	public void setTotal_money_format(String total_money_format)
	{
		this.total_money_format = total_money_format;
	}

	public String getMoney_format()
	{
		return money_format;
	}

	public void setMoney_format(String money_format)
	{
		this.money_format = money_format;
	}

	public int getDelivery_status() {
		return delivery_status;
	}

	public void setDelivery_status(int delivery_status) {
		this.delivery_status = delivery_status;
	}

	public String getNum()
	{
		return num;
	}

	public void setNum(String num)
	{
		this.num = num;
	}

	public List<DealOrderItemModel> getOrderGoods()
	{
		return orderGoods;
	}

	public void setOrderGoods(List<DealOrderItemModel> orderGoods)
	{
		this.orderGoods = orderGoods;
	}

	public DeliveryModel getDeliveryAddr()
	{
		return deliveryAddr;
	}

	public void setDeliveryAddr(DeliveryModel deliveryAddr)
	{
		this.deliveryAddr = deliveryAddr;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getSend_mobile()
	{
		return send_mobile;
	}

	public void setSend_mobile(String send_mobile)
	{
		this.send_mobile = send_mobile;
	}

	public String getTax_title()
	{
		return tax_title;
	}

	public void setTax_title(String tax_title)
	{
		this.tax_title = tax_title;
	}

	public String getTax_id()
	{
		return tax_id;
	}

	public void setTax_id(String tax_id)
	{
		this.tax_id = tax_id;
	}

	public String getDeliver_time_id()
	{
		return deliver_time_id;
	}

	public void setDeliver_time_id(String deliver_time_id)
	{
		this.deliver_time_id = deliver_time_id;
	}

	public String getPayment_id()
	{
		return payment_id;
	}

	public void setPayment_id(String payment_id)
	{
		this.payment_id = payment_id;
	}

	public Order_parmModel getOrder_parm()
	{
		return order_parm;
	}

	public void setOrder_parm(Order_parmModel order_parm)
	{
		this.order_parm = order_parm;
	}

	public String getEvc_sn()
	{
		return evc_sn;
	}

	public void setEvc_sn(String evc_sn)
	{
		this.evc_sn = evc_sn;
	}

	public String getEvc_pwd()
	{
		return evc_pwd;
	}

	public void setEvc_pwd(String evc_pwd)
	{
		this.evc_pwd = evc_pwd;
	}

	public String getDelivery_id()
	{
		return delivery_id;
	}

	public void setDelivery_id(String delivery_id)
	{
		this.delivery_id = delivery_id;
	}

	public int getHas_cancel()
	{
		return has_cancel;
	}

	public void setHas_cancel(int has_cancel)
	{
		this.has_cancel = has_cancel;
	}

	public int getHas_edit_delivery()
	{
		return has_edit_delivery;
	}

	public void setHas_edit_delivery(int has_edit_delivery)
	{
		this.has_edit_delivery = has_edit_delivery;
	}

	public int getHas_edit_delivery_time()
	{
		return has_edit_delivery_time;
	}

	public void setHas_edit_delivery_time(int has_edit_delivery_time)
	{
		this.has_edit_delivery_time = has_edit_delivery_time;
	}

	public int getHas_edit_invoice()
	{
		return has_edit_invoice;
	}

	public void setHas_edit_invoice(int has_edit_invoice)
	{
		this.has_edit_invoice = has_edit_invoice;
	}

	public int getHas_edit_ecv()
	{
		return has_edit_ecv;
	}

	public void setHas_edit_ecv(int has_edit_ecv)
	{
		this.has_edit_ecv = has_edit_ecv;
	}

	public int getHas_edit_message()
	{
		return has_edit_message;
	}

	public void setHas_edit_message(int has_edit_message)
	{
		this.has_edit_message = has_edit_message;
	}

	public int getHas_edit_moblie()
	{
		return has_edit_moblie;
	}

	public void setHas_edit_moblie(int has_edit_moblie)
	{
		this.has_edit_moblie = has_edit_moblie;
	}

	public int getHas_pay()
	{
		return has_pay;
	}

	public void setHas_pay(int has_pay)
	{
		this.has_pay = has_pay;
	}

	public double getPay_money()
	{
		return pay_money;
	}

	public void setPay_money(double pay_money)
	{
		this.pay_money = pay_money;
	}

	public String getUse_user_money()
	{
		return use_user_money;
	}

	public void setUse_user_money(String use_user_money)
	{
		this.use_user_money = use_user_money;
		if (!TextUtils.isEmpty(use_user_money))
		{
			this.use_user_money_format_double = SDTypeParseUtil.getDoubleFromString(use_user_money, 0);
		} else
		{
			this.use_user_money_format_double = 0;
		}
	}

	public List<FeeinfoModel> getFeeinfo()
	{
		return feeinfo;
	}

	public void setFeeinfo(List<FeeinfoModel> feeinfo)
	{
		this.feeinfo = feeinfo;
	}

	public int getPay_status() {
		return pay_status;
	}

	public void setPay_status(int pay_status) {
		this.pay_status = pay_status;
	}

	public int getOrder_status() {
		return order_status;
	}

	public void setOrder_status(int order_status) {
		this.order_status = order_status;
	}

	public int getRefund_status() {
		return refund_status;
	}

	public void setRefund_status(int refund_status) {
		this.refund_status = refund_status;
	}

}
