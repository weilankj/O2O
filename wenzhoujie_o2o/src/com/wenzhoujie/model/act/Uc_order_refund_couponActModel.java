package com.wenzhoujie.model.act;

import java.util.ArrayList;
import java.util.List;

import com.wenzhoujie.model.DealOrderItemModel;
import com.wenzhoujie.model.OrderCoupon_listModel;
import com.wenzhoujie.utils.SDCollectionUtil;

public class Uc_order_refund_couponActModel extends BaseActModel
{
	private DealOrderItemModel item;
	private List<OrderCoupon_listModel> coupon_list;

	public List<OrderCoupon_listModel> getCoupon_list()
	{
		return coupon_list;
	}

	public void setCoupon_list(List<OrderCoupon_listModel> coupon_list)
	{
		this.coupon_list = coupon_list;
		updateCouponInfo();
	}

	public List<DealOrderItemModel> getListItem()
	{
		List<DealOrderItemModel> listItem = new ArrayList<DealOrderItemModel>();
		if (item != null)
		{
			listItem.add(item);
		}
		return listItem;
	}

	public DealOrderItemModel getItem()
	{
		return item;
	}

	public void setItem(DealOrderItemModel item)
	{
		this.item = item;
		updateCouponInfo();
	}

	private void updateCouponInfo()
	{
		if (!SDCollectionUtil.isEmpty(coupon_list) && item != null)
		{
			for (OrderCoupon_listModel coupon : coupon_list)
			{
				coupon.setNumber(item.getNumr());
			}
		}
	}

}
