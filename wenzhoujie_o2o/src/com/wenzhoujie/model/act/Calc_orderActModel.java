package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.FeeinfoModel;
import com.wenzhoujie.model.Order_parmModel;

public class Calc_orderActModel extends BaseActModel
{

	private Order_parmModel order_parm = null;

	private List<FeeinfoModel> feeinfo = null;

	private double use_user_money = 0;
	private double pay_money = 0;

	public Order_parmModel getOrder_parm()
	{
		return order_parm;
	}

	public void setOrder_parm(Order_parmModel order_parm)
	{
		this.order_parm = order_parm;
	}

	public List<FeeinfoModel> getFeeinfo()
	{
		return feeinfo;
	}

	public void setFeeinfo(List<FeeinfoModel> feeinfo)
	{
		this.feeinfo = feeinfo;
	}

	public double getUse_user_money()
	{
		return use_user_money;
	}

	public void setUse_user_money(double use_user_money)
	{
		this.use_user_money = use_user_money;
	}

	public double getPay_money()
	{
		return pay_money;
	}

	public void setPay_money(double pay_money)
	{
		this.pay_money = pay_money;
	}

}
