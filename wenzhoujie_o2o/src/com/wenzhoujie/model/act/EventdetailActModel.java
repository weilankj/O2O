package com.wenzhoujie.model.act;

import com.wenzhoujie.model.EventdetailItemModel;
import com.wenzhoujie.model.PageModel;

public class EventdetailActModel extends BaseActModel
{
	private PageModel page = null;
	private EventdetailItemModel item = null;

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public EventdetailItemModel getItem()
	{
		return item;
	}

	public void setItem(EventdetailItemModel item)
	{
		this.item = item;
	}
}