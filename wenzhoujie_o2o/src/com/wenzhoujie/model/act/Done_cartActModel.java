package com.wenzhoujie.model.act;

public class Done_cartActModel extends BaseActModel
{

	private String order_id = null;
	private String pay_status = null;

	public String getOrder_id()
	{
		return order_id;
	}

	public void setOrder_id(String order_id)
	{
		this.order_id = order_id;
	}

	public String getPay_status()
	{
		return pay_status;
	}

	public void setPay_status(String pay_status)
	{
		this.pay_status = pay_status;
	}

}
