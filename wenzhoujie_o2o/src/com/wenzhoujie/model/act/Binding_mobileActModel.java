package com.wenzhoujie.model.act;

import com.wenzhoujie.utils.SDTypeParseUtil;

public class Binding_mobileActModel extends BaseActModel
{
	private String is_binding = null;
	private String mobile = null;

	// /////////////////////////add
	private int is_binding_format_int = 0;

	public int getIs_binding_format_int()
	{
		return is_binding_format_int;
	}

	public void setIs_binding_format_int(int is_binding_format_int)
	{
		this.is_binding_format_int = is_binding_format_int;
	}

	public String getIs_binding()
	{
		return is_binding;
	}

	public void setIs_binding(String is_binding)
	{
		this.is_binding = is_binding;
		this.is_binding_format_int = SDTypeParseUtil.getIntFromString(is_binding, 0);
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

}
