package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.DeliveryModel;
import com.wenzhoujie.model.FeeinfoModel;
import com.wenzhoujie.model.Order_parmModel;

public class Calc_cartActModel extends BaseActModel
{
	private int first_calc;
	private DeliveryModel delivery;
	private String send_mobile;
	private int select_delivery_id;
	private String use_user_money;
	private double pay_money;
	private List<FeeinfoModel> feeinfo;
	private Order_parmModel order_parm;
	private String mobile_user_id;
	private String mobile_user_name;
	private String mobile_user_pwd;

	public String getMobile_user_id()
	{
		return mobile_user_id;
	}

	public void setMobile_user_id(String mobile_user_id)
	{
		this.mobile_user_id = mobile_user_id;
	}

	public String getMobile_user_name()
	{
		return mobile_user_name;
	}

	public void setMobile_user_name(String mobile_user_name)
	{
		this.mobile_user_name = mobile_user_name;
	}

	public String getMobile_user_pwd()
	{
		return mobile_user_pwd;
	}

	public void setMobile_user_pwd(String mobile_user_pwd)
	{
		this.mobile_user_pwd = mobile_user_pwd;
	}

	public int getFirst_calc()
	{
		return first_calc;
	}

	public void setFirst_calc(int first_calc)
	{
		this.first_calc = first_calc;
	}

	public DeliveryModel getDelivery()
	{
		return delivery;
	}

	public void setDelivery(DeliveryModel delivery)
	{
		this.delivery = delivery;
	}

	public String getSend_mobile()
	{
		return send_mobile;
	}

	public void setSend_mobile(String send_mobile)
	{
		this.send_mobile = send_mobile;
	}

	public int getSelect_delivery_id()
	{
		return select_delivery_id;
	}

	public void setSelect_delivery_id(int select_delivery_id)
	{
		this.select_delivery_id = select_delivery_id;
	}

	public String getUse_user_money()
	{
		return use_user_money;
	}

	public void setUse_user_money(String use_user_money)
	{
		this.use_user_money = use_user_money;
	}

	public double getPay_money()
	{
		return pay_money;
	}

	public void setPay_money(double pay_money)
	{
		this.pay_money = pay_money;
	}

	public List<FeeinfoModel> getFeeinfo()
	{
		return feeinfo;
	}

	public void setFeeinfo(List<FeeinfoModel> feeinfo)
	{
		this.feeinfo = feeinfo;
	}

	public Order_parmModel getOrder_parm()
	{
		return order_parm;
	}

	public void setOrder_parm(Order_parmModel order_parm)
	{
		this.order_parm = order_parm;
	}

}
