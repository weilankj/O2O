package com.wenzhoujie.model.act;

public class SyncbindActModel extends BaseActModel
{
	private String login_type = null;

	public String getLogin_type()
	{
		return login_type;
	}

	public void setLogin_type(String login_type)
	{
		this.login_type = login_type;
	}

}
