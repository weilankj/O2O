package com.wenzhoujie.model.act;

public class Collect_dealActModel extends BaseActModel
{
	private int collect_status = 0;
	private int is_collect = 0;

	public int getCollect_status()
	{
		return collect_status;
	}

	public void setCollect_status(int collect_status)
	{
		this.collect_status = collect_status;
	}

	public int getIs_collect()
	{
		return is_collect;
	}

	public void setIs_collect(int is_collect)
	{
		this.is_collect = is_collect;
	}

}
