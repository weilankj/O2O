package com.wenzhoujie.model.act;

import java.io.Serializable;

public class MoreCateSubActModel extends BaseActModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String count;
	/*private String tel;
	private String good_dp_count;
	private String preview;*/
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return count;
	}
	public void setAddress(String count) {
		this.count = count;
	}
	/*public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getGood_dp_count() {
		return good_dp_count;
	}
	public void setGood_dp_count(String good_dp_count) {
		this.good_dp_count = good_dp_count;
	}
	public String getPreview() {
		return preview;
	}
	public void setPreview(String preview) {
		this.preview = preview;
	}
	*/
	
}
