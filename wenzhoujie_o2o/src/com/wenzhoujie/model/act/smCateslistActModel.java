package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.smGoodsCates;


public class smCateslistActModel extends BaseActModel
{

	private List<smGoodsCates> cate_list = null;
	

	public List<smGoodsCates> getCate_list()
	{
		return cate_list;
	}

	public void setCate_list(List<smGoodsCates> cate_list)
	{
		this.cate_list = cate_list;
	}

	

}