package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.CommentlistItemModel;
import com.wenzhoujie.model.PageModel;

public class CommentlistActModel extends BaseActModel
{
	private List<CommentlistItemModel> item = null;
	private PageModel page = null;

	public List<CommentlistItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<CommentlistItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}