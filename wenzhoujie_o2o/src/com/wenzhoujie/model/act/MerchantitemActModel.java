package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.util.List;

import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.GoodsdescSupplier_location_listModel;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.IndexActYouhui_listModel;

public class MerchantitemActModel extends BaseActModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String avg_point;
	private String logo;
	private String xpoint;
	private String ypoint;
	private String api_address;
	private String address;
	private String dp_count;
	private String good_rate;
	private String deal_cate_id;
	private String tel;
	private String is_dy;
	private String city_name;
	private String comment_count;
	private String event_count;
	private String youhui_count;
	private String brand_id;
	private String distance;
	private String brief;
	private String width;
	private String supplier_name;
	private String supplier_id;
	private String is_auto_order;
	private List<IndexActDeal_listModel> tuan_list;
	private String tuan_count;
	private List<IndexActDeal_listModel> goods_list;
	private List<GoodsCommentModel> comment_list;
	private List<IndexActYouhui_listModel> youhui_list;
	private List<GoodsdescSupplier_location_listModel> other_supplier_location;
	private String page_title;
	private int biz_shop_cate_id;
	private int shop_type;

	public List<IndexActYouhui_listModel> getYouhui_list()
	{
		return youhui_list;
	}

	public void setYouhui_list(List<IndexActYouhui_listModel> youhui_list)
	{
		this.youhui_list = youhui_list;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAvg_point()
	{
		return avg_point;
	}

	public void setAvg_point(String avg_point)
	{
		this.avg_point = avg_point;
	}

	public String getLogo()
	{
		return logo;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
	}

	public String getApi_address()
	{
		return api_address;
	}

	public void setApi_address(String api_address)
	{
		this.api_address = api_address;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getDp_count()
	{
		return dp_count;
	}

	public void setDp_count(String dp_count)
	{
		this.dp_count = dp_count;
	}

	public String getGood_rate()
	{
		return good_rate;
	}

	public void setGood_rate(String good_rate)
	{
		this.good_rate = good_rate;
	}

	public String getDeal_cate_id()
	{
		return deal_cate_id;
	}

	public void setDeal_cate_id(String deal_cate_id)
	{
		this.deal_cate_id = deal_cate_id;
	}

	public String getTel()
	{
		return tel;
	}

	public void setTel(String tel)
	{
		this.tel = tel;
	}

	public String getIs_dy()
	{
		return is_dy;
	}

	public void setIs_dy(String is_dy)
	{
		this.is_dy = is_dy;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public String getComment_count()
	{
		return comment_count;
	}

	public void setComment_count(String comment_count)
	{
		this.comment_count = comment_count;
	}

	public String getEvent_count()
	{
		return event_count;
	}

	public void setEvent_count(String event_count)
	{
		this.event_count = event_count;
	}

	public String getYouhui_count()
	{
		return youhui_count;
	}

	public void setYouhui_count(String youhui_count)
	{
		this.youhui_count = youhui_count;
	}

	public String getBrand_id()
	{
		return brand_id;
	}

	public void setBrand_id(String brand_id)
	{
		this.brand_id = brand_id;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
	}

	public String getBrief()
	{
		return brief;
	}

	public void setBrief(String brief)
	{
		this.brief = brief;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
	}

	public String getSupplier_name()
	{
		return supplier_name;
	}

	public void setSupplier_name(String supplier_name)
	{
		this.supplier_name = supplier_name;
	}

	public String getSupplier_id()
	{
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id)
	{
		this.supplier_id = supplier_id;
	}

	public String getIs_auto_order()
	{
		return is_auto_order;
	}

	public void setIs_auto_order(String is_auto_order)
	{
		this.is_auto_order = is_auto_order;
	}

	public List<GoodsdescSupplier_location_listModel> getOther_supplier_location()
	{
		return other_supplier_location;
	}

	public void setOther_supplier_location(List<GoodsdescSupplier_location_listModel> other_supplier_location)
	{
		this.other_supplier_location = other_supplier_location;
	}

	public List<IndexActDeal_listModel> getTuan_list()
	{
		return tuan_list;
	}

	public void setTuan_list(List<IndexActDeal_listModel> tuan_list)
	{
		this.tuan_list = tuan_list;
	}

	public String getTuan_count()
	{
		return tuan_count;
	}

	public void setTuan_count(String tuan_count)
	{
		this.tuan_count = tuan_count;
	}

	public List<IndexActDeal_listModel> getGoods_list()
	{
		return goods_list;
	}

	public void setGoods_list(List<IndexActDeal_listModel> goods_list)
	{
		this.goods_list = goods_list;
	}

	public List<GoodsCommentModel> getComment_list()
	{
		return comment_list;
	}

	public void setComment_list(List<GoodsCommentModel> comment_list)
	{
		this.comment_list = comment_list;
	}

	public String getPage_title()
	{
		return page_title;
	}

	public void setPage_title(String page_title)
	{
		this.page_title = page_title;
	}

	public int getBiz_shop_cate_id() {
		return biz_shop_cate_id;
	}

	public void setBiz_shop_cate_id(int biz_shop_cate_id) {
		this.biz_shop_cate_id = biz_shop_cate_id;
	}

	public int getShop_type() {
		return shop_type;
	}

	public void setShop_type(int shop_type) {
		this.shop_type = shop_type;
	}
}
