package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.Bcate_listModel;

/**
 * 
 * @author yhz
 * @create time 2014-8-14
 */
public class MapActModel extends BaseActModel
{
	private List<Bcate_listModel> bcate_list = null;

	public List<Bcate_listModel> getBcate_list()
	{
		return bcate_list;
	}

	public void setBcate_list(List<Bcate_listModel> bcate_list)
	{
		this.bcate_list = bcate_list;
	}

}
