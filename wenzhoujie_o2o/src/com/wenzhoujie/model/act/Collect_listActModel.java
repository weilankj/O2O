package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.TuanGoodsModel;

public class Collect_listActModel extends BaseActModel
{

	private List<TuanGoodsModel> collect_list = null;
	private PageModel page = null;

	public List<TuanGoodsModel> getCollect_list()
	{
		return collect_list;
	}

	public void setCollect_list(List<TuanGoodsModel> collect_list)
	{
		this.collect_list = collect_list;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}
