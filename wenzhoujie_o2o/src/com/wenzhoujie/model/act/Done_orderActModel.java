package com.wenzhoujie.model.act;

public class Done_orderActModel extends BaseActModel
{

	private String has_pay = null;
	private String order_id = null;

	public String getHas_pay()
	{
		return has_pay;
	}

	public void setHas_pay(String has_pay)
	{
		this.has_pay = has_pay;
	}

	public String getOrder_id()
	{
		return order_id;
	}

	public void setOrder_id(String order_id)
	{
		this.order_id = order_id;
	}

}
