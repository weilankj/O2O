package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.PrivateLettersActivityMsg_listModel;
import com.wenzhoujie.model.PrivateLettersActivitySys_MsgsModel;

public class PrivateLettersActivityModel extends BaseActModel
{
	private List<PrivateLettersActivityMsg_listModel> msg_list = null;
	private List<PrivateLettersActivitySys_MsgsModel> sys_msgs = null;

	public List<PrivateLettersActivitySys_MsgsModel> getSys_msgs()
	{
		return sys_msgs;
	}

	public void setSys_msgs(List<PrivateLettersActivitySys_MsgsModel> sys_msgs)
	{
		this.sys_msgs = sys_msgs;
	}

	private PageModel page = null;

	public List<PrivateLettersActivityMsg_listModel> getMsg_list()
	{
		return msg_list;
	}

	public void setMsg_list(List<PrivateLettersActivityMsg_listModel> msg_list)
	{
		this.msg_list = msg_list;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}