package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.GetbrandlistActItemModel;
import com.wenzhoujie.model.PageModel;

public class GetbrandlistActModel extends BaseActModel
{

	private List<GetbrandlistActItemModel> item = null;

	private PageModel page = null;

	public List<GetbrandlistActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<GetbrandlistActItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}
