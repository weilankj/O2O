package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.FollowActItemModel;
import com.wenzhoujie.model.PageModel;

public class FollowActModel extends BaseActModel
{
	private List<FollowActItemModel> item = null;
	private PageModel page = null;

	public List<FollowActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<FollowActItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}