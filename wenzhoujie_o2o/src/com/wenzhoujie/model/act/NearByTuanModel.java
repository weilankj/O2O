package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.NearByTuanItemModel;
import com.wenzhoujie.model.PageModel;

public class NearByTuanModel extends BaseActModel
{
	private List<NearByTuanItemModel> item = null;
	private PageModel page = null;

	public List<NearByTuanItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<NearByTuanItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}