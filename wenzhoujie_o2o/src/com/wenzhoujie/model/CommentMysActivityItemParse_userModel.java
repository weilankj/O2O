package com.wenzhoujie.model;

public class CommentMysActivityItemParse_userModel
{
	private String key = null;
	private String value = null;

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
}