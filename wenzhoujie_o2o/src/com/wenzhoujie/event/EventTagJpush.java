package com.wenzhoujie.event;

public class EventTagJpush
{
	/** 团购 */
	public static final String EVENT_TUANDETAIL_ID = "event_tuandetail_id";
	/** 商城 */
	public static final String EVENT_ECSHOPDETAIL_ID = "event_ecshopdetail_id";
	/** 活动 */
	public static final String EVENT_EVENTSDETAIL_ID = "event_eventsdetail_id";
	/** 优惠 */
	public static final String EVENT_FAVORABLEDETAIL_ID = "event_favorabledetail_id";
	/** 商家 */
	public static final String EVENT_MERCHANTDETAIL_ID = "event_merchantdetail_id";
	/** 代金券 */
	public static final String EVENT_VOUCHERDETAIL_ID = "event_voucherdetail_id";
}
