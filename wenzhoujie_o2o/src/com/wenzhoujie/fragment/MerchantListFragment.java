package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.wenzhoujie.library.title.SDTitleTwoRightButton.SDTitleTwoRightButtonListener;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.lingou.www.R.color;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.HomeSearchActivity;
import com.wenzhoujie.MainActivity;
import com.wenzhoujie.MapSearchActivity;
import com.wenzhoujie.adapter.CategoryCateLeftAdapter;
import com.wenzhoujie.adapter.CategoryCateRightAdapter;
import com.wenzhoujie.adapter.CategoryOrderAdapter;
import com.wenzhoujie.adapter.CategoryQuanLeftAdapterNew;
import com.wenzhoujie.adapter.CategoryQuanRightAdapterNew;
import com.wenzhoujie.adapter.MerchantListAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.constant.Constant.CategoryOrderTypeName;
import com.wenzhoujie.constant.Constant.CategoryOrderTypeValue;
import com.wenzhoujie.constant.Constant.SearchTypeMap;
import com.wenzhoujie.constant.Constant.SearchTypeNormal;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.customview.SD2LvCategoryView;
import com.wenzhoujie.customview.SDBottomNavigatorBaseItem;
import com.wenzhoujie.customview.SDLvCategoryView;
import com.wenzhoujie.customview.SDLvCategoryView.SDLvCategoryViewListener;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.Bcate_listBcate_typeModel;
import com.wenzhoujie.model.Bcate_listModel;
import com.wenzhoujie.model.CategoryOrderModel;
import com.wenzhoujie.model.MerchantlistActItemModel;
import com.wenzhoujie.model.Quan_listModel;
import com.wenzhoujie.model.Quan_listQuan_subModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.MerchantlistActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDViewNavigatorManager;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.work.AppRuntimeWorker;

public class MerchantListFragment extends BaseFragment implements OnClickListener
{
	public static final String EXTRA_TITLE_TXT = "extra_title_txt";
	public static final String EXTRA_CATE_NAME = "extra_cate_name";
	public static final String EXTRA_FROM_TXT = "extra_from_txt";
	
	public static final String EXTRA_CATE_TYPE_ID = "extra_cate_type_id";
	
	public static final String TITILE_NAME = "title_name";
	public static final String EXTRA_KEY_WORD = "extra_key_word";
	/** 商家id */
	public static final String EXTRA_BRAND_ID = "extra_brand_id";

	public static final String EXTRA_CATE_ID = "extra_cate_id";
	//地区id
	public static final String EXTRA_CATE_SEC_ID = "extra_cate_sec_id";
	//地区名称
	public static final String EXTRA_CATE_SEC_NAME = "extra_cate_sec_name";
	@ViewInject(id = R.id.frag_merchant_list_cv_left)
	private SD2LvCategoryView mCvLeft = null;

	@ViewInject(id = R.id.frag_merchant_list_cv_middle)
	private SD2LvCategoryView mCvMiddle = null;

	@ViewInject(id = R.id.frag_merchant_list_cv_right)
	private SDLvCategoryView mCvRight = null;

	@ViewInject(id = R.id.frag_merchant_list_ll_empty)
	private LinearLayout mLlEmpty = null;

	@ViewInject(id = R.id.frag_merchant_list_ll_current_location)
	private LinearLayout mLlCurrentLocation = null;

	@ViewInject(id = R.id.frag_merchant_list_tv_current_address)
	private TextView mTvAddress = null;

	@ViewInject(id = R.id.frag_merchant_list_tv_location)
	private ImageView mIvLocation = null;

	@ViewInject(id = R.id.frag_merchant_list_ll_current_search)
	private LinearLayout mLlCurrentSearch = null;

	@ViewInject(id = R.id.frag_merchant_list_tv_current_keyword)
	private TextView mTvCurrentKeyword = null;

	@ViewInject(id = R.id.frag_merchant_list_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	private MerchantListAdapter mAdapter = null;
	private List<MerchantlistActItemModel> mListModel = new ArrayList<MerchantlistActItemModel>();
	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();

	private boolean isFirstBindCategoryViewData = true;

	// --------搜索条件-----
	// private SearchConditionModel mSearcher = new SearchConditionModel();

	private int pageTotal;
	// =======================提交到服务器参数
	private String title_name;
	private String city_name;
	private String city_id;
	private String quan_id;
	private String quyu_name;
	/** 一级分类id */
	private String cate_id;
	/** 商家id */
	private String brand_id;
	private String keyword;
	private int page;
	/** 二级分类id */
	private String cata_type_id;
	private String order_type;
	/** 是否加载支持自主下单功能的商家1:是，0:否 */
	private String is_auto_order;
	private String latitude_top;
	private String latitude_bottom;
	private String longitude_left;
	private String longitude_right;
	/** 纬度 */
	private String m_latitude;
	/** 经度 */
	private String m_longitude;
	/** 来自哪里的点击 */
	private String from_txt;

	private String title_txt;
	private String cata_type_name;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_merchant_list, container, false);
		view = setmTitleType(TitleType.TITLE_TWO_RIGHT_BUTTON, view);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		getIntentData();
		initTitle();
		bindDefaultLvData();
		bindLocationData();
		initCategoryView();
		initCategoryViewNavigatorManager();
		registeClick();
		initPullRefreshLv();
		
	}

	private void bindLocationData()
	{
		String addrShort = BaiduMapManager.getInstance().getCurAddressShort();
		if (TextUtils.isEmpty(addrShort))
		{
			locationAddress();
		}
	}

	private void initTitle()
	{

		mTitleTwoRightBtns.setmListener(new SDTitleTwoRightButtonListener()
		{

			@Override
			public void onRightButton2CLick_SDTitleTwoRightButton(View v)
			{
				Intent intent = new Intent(getActivity(), HomeSearchActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(HomeSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeNormal.MERCHANT);
				startActivity(intent);
			}

			@Override
			public void onRightButton1CLick_SDTitleTwoRightButton(View v)
			{
				startNearbyMapSearchActivity();
			}

			@Override
			public void onLeftButtonCLick_SDTitleTwoRightButton(View v)
			{
				if (getActivity() instanceof MainActivity)
				{
					startNearbyMapSearchActivity();
				} else
				{
					getActivity().finish();
				}
			}
		});

		if (getActivity() instanceof MainActivity)
		{
			mTitleTwoRightBtns.setLeftText(null);
			mTitleTwoRightBtns.setLeftImage(R.drawable.ic_location_home_top);
		} else
		{
			mTitleTwoRightBtns.setLeftImage(R.drawable.ic_arrow_left_back);
			mTitleTwoRightBtns.setRightImage1(R.drawable.ic_location_home_top);
		}
		mTitleTwoRightBtns.setRightImage2(R.drawable.ic_search_home_top);

		String city = AppRuntimeWorker.getCity_name();
		if(title_txt!=null)
		{
			mTitleTwoRightBtns.setTitleTop(title_txt + " - " + city);
		}
		else if (title_name!=null)
		{
			mTitleTwoRightBtns.setTitleTop(title_name + " - " + city);
		}else
			{
			if (!TextUtils.isEmpty(city))
			{
				mTitleTwoRightBtns.setTitleTop(SDResourcesUtil.getString(R.string.supplier) + " - " + city);
			}
			else
			{
				mTitleTwoRightBtns.setTitleTop(SDResourcesUtil.getString(R.string.supplier));
			}
		}
	}

	private void startNearbyMapSearchActivity()
	{
		Intent intent = new Intent(getActivity(), MapSearchActivity.class);
		intent.putExtra(MapSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeMap.MERCHANT);
		startActivity(intent);
	}

	private void getIntentData()
	{
		from_txt = "";
		keyword = getActivity().getIntent().getStringExtra(EXTRA_KEY_WORD);
		brand_id = getActivity().getIntent().getStringExtra(EXTRA_BRAND_ID);
		cate_id = getActivity().getIntent().getStringExtra(EXTRA_CATE_ID);
		title_name = getActivity().getIntent().getStringExtra(TITILE_NAME);
		
		quan_id = getActivity().getIntent().getStringExtra(EXTRA_CATE_SEC_ID);
		quyu_name = getActivity().getIntent().getStringExtra(EXTRA_CATE_SEC_NAME);
		from_txt = getActivity().getIntent().getStringExtra(EXTRA_FROM_TXT);
		
		title_txt = getActivity().getIntent().getStringExtra(EXTRA_TITLE_TXT);
		cata_type_name = getActivity().getIntent().getStringExtra(EXTRA_CATE_NAME);
		
		cata_type_id = getActivity().getIntent().getStringExtra(EXTRA_CATE_TYPE_ID);
		
		
		if (TextUtils.isEmpty(keyword))
		{
			mLlCurrentLocation.setVisibility(View.VISIBLE);
			mLlCurrentSearch.setVisibility(View.GONE);
			if (BaiduMapManager.getInstance().getCurAddress() != null)
			{
				mTvAddress.setText(BaiduMapManager.getInstance().getCurAddressShort());
			}
		} else
		{
			mLlCurrentLocation.setVisibility(View.GONE);
			mLlCurrentSearch.setVisibility(View.VISIBLE);
			mTvCurrentKeyword.setText(keyword);
		}
	}

	private void initPullRefreshLv()
	{
		mPtrlvContent.setMode(Mode.BOTH);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page = 1;
				requestData(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page++;
				if (page > pageTotal && pageTotal > 0)
				{
					SDToast.showToast("没有更多数据了");
					mPtrlvContent.onRefreshComplete();
				} else
				{
					requestData(true);
				}
			}
		});
		mPtrlvContent.setRefreshing();
		if(quyu_name!=null)
		{
			mCvMiddle.setTitle(quyu_name);
		}
	}

	private void bindDefaultLvData()
	{
		mAdapter = new MerchantListAdapter(mListModel, getActivity());
		mPtrlvContent.setAdapter(mAdapter);
	}

	private void initCategoryViewNavigatorManager()
	{
		SDBottomNavigatorBaseItem[] items = new SDBottomNavigatorBaseItem[] { mCvLeft, mCvMiddle, mCvRight };
		mViewManager.setItems(items);
		mViewManager.setmMode(SDViewNavigatorManager.Mode.CAN_NONE_SELECT);
	}

	private void initCategoryView()
	{
		mCvLeft.setmBackgroundNormal(R.drawable.bg_choosebar_press_down);
		mCvLeft.setmBackgroundSelect(R.drawable.bg_choosebar_press_up);
		mCvLeft.setmTextColorNormal(getResources().getColor(color.text_home_item_content));
		mCvLeft.setmTextColorSelect(getResources().getColor(color.main_color));
		mCvLeft.setmListener(new SD2LvCategoryView.SD2LvCategoryViewListener()
		{

			@Override
			public void onRightItemSelect(int leftIndex, int rightIndex, Object leftModel, Object rightModel)
			{
				Bcate_listModel left = (Bcate_listModel) leftModel;
				Bcate_listBcate_typeModel right = (Bcate_listBcate_typeModel) rightModel;
				cate_id = left.getId();
				
				cata_type_id = right.getId();
				mPtrlvContent.setRefreshing();
				cata_type_name=null;
			}

			@Override
			public void onLeftItemSelect(int leftIndex, Object leftModel, boolean isNotifyDirect)
			{
				if (isNotifyDirect)
				{
					Bcate_listModel left = (Bcate_listModel) leftModel;
					Bcate_listBcate_typeModel right = left.getBcate_type().get(0);
					cate_id = left.getId();
					cata_type_id = right.getId();
					
					mPtrlvContent.setRefreshing();
					cata_type_name=null;
				}
			}
		});

		mCvMiddle.setmBackgroundNormal(R.drawable.bg_choosebar_press_down_2);
		mCvMiddle.setmBackgroundSelect(R.drawable.bg_choosebar_press_up_2);
		mCvMiddle.setmTextColorNormal(getResources().getColor(color.text_home_item_content));
		mCvMiddle.setmTextColorSelect(getResources().getColor(color.main_color));
		mCvMiddle.setmListener(new SD2LvCategoryView.SD2LvCategoryViewListener()
		{
			@Override
			public void onRightItemSelect(int leftIndex, int rightIndex, Object leftModel, Object rightModel)
			{
				Quan_listQuan_subModel right = (Quan_listQuan_subModel) rightModel;
				//quan_id = right.getId();
				//System.out.println("quan_id"+quan_id);
				/*if(quan_id!=null)
				{
					right.setId(quan_id);
				}
				else*/
				{
					quan_id = right.getId();
					quyu_name=null;
				}
				mPtrlvContent.setRefreshing();
			}

			@Override
			public void onLeftItemSelect(int leftIndex, Object leftModel, boolean isNotifyDirect)
			{
				if (isNotifyDirect)
				{
					Quan_listModel left = (Quan_listModel) leftModel;
					Quan_listQuan_subModel right = left.getQuan_sub().get(0);
					//quan_id = right.getId();
					//System.out.println("quan_id"+quan_id);
					/*if(quan_id!=null)
					{
						right.setId(quan_id);
					}
					else*/
					{
						quan_id = right.getId();
						quyu_name=null;
					}
					mPtrlvContent.setRefreshing();
				}
			}
		});
		//Quan_listQuan_subModel right = (Quan_listQuan_subModel) rightModel;
			
		
		mCvRight.setmBackgroundNormal(R.drawable.bg_choosebar_press_down_3);
		mCvRight.setmBackgroundSelect(R.drawable.bg_choosebar_press_up_3);
		mCvRight.setmTextColorNormal(getResources().getColor(color.text_home_item_content));
		mCvRight.setmTextColorSelect(getResources().getColor(color.main_color));
		mCvRight.setmListener(new SDLvCategoryViewListener()
		{
			@Override
			public void onItemSelect(int index, Object model)
			{
				if (model instanceof CategoryOrderModel)
				{
					CategoryOrderModel orderModel = (CategoryOrderModel) model;
					order_type = orderModel.getValue();
					mPtrlvContent.setRefreshing();
				}
			}
		});

		List<CategoryOrderModel> listOrderModel = new ArrayList<CategoryOrderModel>();
		CategoryOrderModel orderModelDefault = new CategoryOrderModel();
		orderModelDefault.setName(CategoryOrderTypeName.DEFAULT);
		orderModelDefault.setValue(CategoryOrderTypeValue.DEFAULT);
		listOrderModel.add(orderModelDefault);

		CategoryOrderModel orderModelNearBy = new CategoryOrderModel();
		orderModelNearBy.setName(CategoryOrderTypeName.NEARBY);
		orderModelNearBy.setValue(CategoryOrderTypeValue.NEARBY);
		listOrderModel.add(orderModelNearBy);

		CategoryOrderModel orderModelPoint = new CategoryOrderModel();
		orderModelPoint.setName(CategoryOrderTypeName.AVG_POINT);
		orderModelPoint.setValue(CategoryOrderTypeValue.AVG_POINT);
		listOrderModel.add(orderModelPoint);
		/*
		 * CategoryOrderModel orderModelNewest = new CategoryOrderModel();
		 * orderModelNewest.setName(CategoryOrderTypeName.NEWEST);
		 * orderModelNewest.setValue(CategoryOrderTypeValue.NEWEST);
		 * listOrderModel.add(orderModelNewest);
		 * 
		 * CategoryOrderModel orderModelBuyCount = new CategoryOrderModel();
		 * orderModelBuyCount.setName(CategoryOrderTypeName.BUY_COUNT);
		 * orderModelBuyCount.setValue(CategoryOrderTypeValue.BUY_COUNT);
		 * listOrderModel.add(orderModelBuyCount);
		 * 
		 * CategoryOrderModel orderModelPriceAsc = new CategoryOrderModel();
		 * orderModelPriceAsc.setName(CategoryOrderTypeName.PRICE_ASC);
		 * orderModelPriceAsc.setValue(CategoryOrderTypeValue.PRICE_ASC);
		 * listOrderModel.add(orderModelPriceAsc);
		 * 
		 * CategoryOrderModel orderModelPriceDesc = new CategoryOrderModel();
		 * orderModelPriceDesc.setName(CategoryOrderTypeName.PRICE_DESC);
		 * orderModelPriceDesc.setValue(CategoryOrderTypeValue.PRICE_DESC);
		 * listOrderModel.add(orderModelPriceDesc);
		 */
		CategoryOrderAdapter adapter = new CategoryOrderAdapter(listOrderModel, getActivity());
		mCvRight.setAdapter(adapter);

	}

	private void requestData(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "merchantlist");
		model.put("keyword", keyword);
		model.put("m_latitude", BaiduMapManager.getInstance().getLatitude());
		model.put("m_longitude", BaiduMapManager.getInstance().getLongitude());
		model.put("order_type", order_type); // 排序类型
		model.put("cata_type_id", cata_type_id);// 小分类ID
		model.put("cate_id", cate_id);// 大分类ID
		model.put("brand_id", brand_id);//商家id
		model.put("city_id", AppRuntimeWorker.getCity_id());
		model.put("quan_id", quan_id);
		model.put("page", page);
		model.put("from", from_txt);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				MerchantlistActModel actModel = JsonUtil.json2Object(responseInfo.result, MerchantlistActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (actModel.getResponse_code() == 1)
					{
						if (isFirstBindCategoryViewData)
						{
							bindLeftCategoryViewData(actModel.getBcate_list());
							bindMiddleCategoryViewData(actModel.getQuan_list());
							isFirstBindCategoryViewData = false;
						}

						if (actModel.getPage() != null)
						{
							page = actModel.getPage().getPage();
							pageTotal = actModel.getPage().getPage_total();
						}
						SDViewUtil.updateAdapterByList(mListModel, actModel.getItem(), mAdapter, isLoadMore);
					}
				}
				if(quyu_name!=null)
				{
					mCvMiddle.setTitle(quyu_name);
				}
				if(cata_type_name!=null)
				{
					mCvLeft.setTitle(cata_type_name);
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				dealFinishRequest();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	protected void dealFinishRequest()
	{
		AppHelper.hideLoadingDialog();
		mPtrlvContent.onRefreshComplete();
		SDViewUtil.toggleEmptyMsgByList(mListModel, mLlEmpty);
	}

	private void bindLeftCategoryViewData(List<Bcate_listModel> listModel)
	{
		if (listModel != null && listModel.size() > 0)
		{
			Bcate_listModel leftModel = listModel.get(0);
			if (leftModel != null)
			{
				List<Bcate_listBcate_typeModel> listRight = leftModel.getBcate_type();

				CategoryCateLeftAdapter adapterLeft = new CategoryCateLeftAdapter(listModel, getActivity());
				//adapterLeft.setDefaultSelectId(cate_id);

				CategoryCateRightAdapter adapterRight = new CategoryCateRightAdapter(listRight, getActivity());

				mCvLeft.setLeftAdapter(adapterLeft);
				mCvLeft.setRightAdapter(adapterRight);
				mCvLeft.setAdapterFinish();
				
				/*cata_type_id = cate_id;*/
				mPtrlvContent.setRefreshing();
			}
		}
	}

	private void bindMiddleCategoryViewData(List<Quan_listModel> listModel)
	{
		if (listModel != null && listModel.size() > 0)
		{
			Quan_listModel leftModel = listModel.get(0);
			if (leftModel != null)
			{
				List<Quan_listQuan_subModel> listRight = leftModel.getQuan_sub();

				CategoryQuanLeftAdapterNew adapterLeft = new CategoryQuanLeftAdapterNew(listModel, getActivity());
				CategoryQuanRightAdapterNew adapterRight = new CategoryQuanRightAdapterNew(listRight, getActivity());
				mCvMiddle.setLeftAdapter(adapterLeft);
				mCvMiddle.setRightAdapter(adapterRight);
				mCvMiddle.setAdapterFinish();
			}
		}
	}

	private void registeClick()
	{
		mIvLocation.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		case R.id.frag_merchant_list_tv_location:
			clickTv_locaiton();
			break;

		default:
			break;
		}
	}

	private void clickTv_locaiton()
	{
		locationAddress();
	}

	/**
	 * 定位地址
	 */
	private void locationAddress()
	{
		// 开始定位
		mIvLocation.setVisibility(View.GONE);
		setCurrentLocationCity("定位中...", false);
		BaiduMapManager.getInstance().startLocation(new BDLocationListener()
		{
			@Override
			public void onReceivePoi(BDLocation arg0)
			{

			}

			@Override
			public void onReceiveLocation(BDLocation location)
			{
				if (location != null)
				{
					setCurrentLocationCity(BaiduMapManager.getInstance().getCurAddressShort(), true);
				} else
				{
					setCurrentLocationCity("定位失败，点击重试", false);
				}
				// 定位回调完关闭，否则一直定位浪费电
				BaiduMapManager.getInstance().destoryLocation();
				mIvLocation.setVisibility(View.VISIBLE);
			}
		});
	}

	private void setCurrentLocationCity(String string, boolean isLocationSuccess)
	{
		if (!AppHelper.isEmptyString(string))
		{
			if (mTvAddress != null)
			{
				mTvAddress.setText(string);
				if (isLocationSuccess)
				{
					// 这个刷新有BUG会保留原始状态
					mPtrlvContent.setRefreshing();
				}
			}
		}
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case CITY_CHANGE:
			initTitle();
			mPtrlvContent.setRefreshing();
			break;

		default:
			break;
		}
	}

}