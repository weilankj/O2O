package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.MyCouponDetailActivity;
import com.wenzhoujie.adapter.MyCouponsListAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.CouponlistActItemModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.CouponlistActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class MyCouponListFragment extends BaseFragment
{
	@ViewInject(id = R.id.frag_my_coupon_list_ptrlv_coupons)
	private PullToRefreshListView mPtrlvCoupons = null;

	@ViewInject(id = R.id.frag_my_coupon_list_iv_empty)
	private ImageView mIvEmpty = null;

	private List<CouponlistActItemModel> mListModel = new ArrayList<CouponlistActItemModel>();
	private MyCouponsListAdapter mAdapter = null;

	private int mPage = 1;
	private int mPageTotal = 0;

	/** 1: 快过期 2:可以使用 3:失败 0:所有 */
	private String mStrStatus = null;

	public String getmStrStatus()
	{
		return mStrStatus;
	}

	public void setmStrStatus(String mStrStatus)
	{
		this.mStrStatus = mStrStatus;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_my_coupon_list, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new MyCouponsListAdapter(mListModel, getActivity());
		mPtrlvCoupons.setAdapter(mAdapter);
	}

	private void initPullToRefreshListView()
	{
		mPtrlvCoupons.setMode(Mode.BOTH);
		mPtrlvCoupons.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refreshData();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				loadMoreData();
			}
		});
		mPtrlvCoupons.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				if (mAdapter != null)
				{
					CouponlistActItemModel model = mAdapter.getItem((int) id);
					Intent intent = new Intent();
					intent.setClass(getActivity(), MyCouponDetailActivity.class);
					intent.putExtra(MyCouponDetailActivity.EXTRA_COUPONLISTACTITEMMODEL, model);
					getActivity().startActivity(intent);
				}

			}
		});

		mPtrlvCoupons.setRefreshing();
	}

	protected void refreshData()
	{
		mPage = 1;
		requestCoupons(false);

	}

	protected void loadMoreData()
	{
		mPage++;
		if (mPage > mPageTotal && mPageTotal != 0)
		{
			SDToast.showToast("没有更多数据了");
			mPtrlvCoupons.onRefreshComplete();
		} else
		{
			requestCoupons(true);
		}

	}

	private void requestCoupons(final boolean isLoadMore)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null && mStrStatus != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "couponlist");
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			model.put("tag", mStrStatus); // 1: 快过期 2:可以使用 3:失败 0:所有
			model.put("page", mPage);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					CouponlistActModel actModel = JsonUtil.json2Object(responseInfo.result, CouponlistActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							if (actModel.getPage() != null)
							{
								mPage = actModel.getPage().getPage();
								mPageTotal = actModel.getPage().getPage_total();
							}

							if (actModel.getItem() != null && actModel.getItem().size() > 0)
							{
								if (!isLoadMore)
								{
									mListModel.clear();
								}
								mListModel.addAll(actModel.getItem());
							} else
							{
								mListModel.clear();
								SDToast.showToast("未找到数据");
							}
							mAdapter.updateListViewData(mListModel);

						}

					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
					mPtrlvCoupons.onRefreshComplete();
					SDViewUtil.toggleEmptyMsgByList(mListModel, mIvEmpty);
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}
}