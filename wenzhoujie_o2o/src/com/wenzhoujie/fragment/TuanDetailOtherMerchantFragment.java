package com.wenzhoujie.fragment;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.adapter.GoodsDetailMerchantAdapter;
import com.wenzhoujie.customview.SDMoreView;
import com.wenzhoujie.customview.SDMoreViewBaseAdapter;
import com.wenzhoujie.model.GoodsdescSupplier_location_listModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 团购详情 （其他门店fragment）
 * 
 * @author js02
 * 
 */
public class TuanDetailOtherMerchantFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_tuan_detail_other_merchant_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_tuan_detail_other_merchant_smv_more)
	private SDMoreView mSmvMore = null;

	private GoodsdescActModel mGoodsModel = null;

	public void setmGoodsModel(GoodsdescActModel mGoodsModel)
	{
		this.mGoodsModel = mGoodsModel;
		if (mGoodsModel != null)
		{
			this.mListModel = mGoodsModel.getSupplier_location_list();
		}
	}

	private List<GoodsdescSupplier_location_listModel> mListModel = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_tuan_detail_other_merchant, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindDataByGoodsModel(mListModel);
	}

	private void bindDataByGoodsModel(final List<GoodsdescSupplier_location_listModel> listModel)
	{
		if (listModel != null && listModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			final GoodsDetailMerchantAdapter adapter = new GoodsDetailMerchantAdapter(listModel, getActivity());
			mSmvMore.setAdapter(new SDMoreViewBaseAdapter()
			{

				@Override
				public View getView(int pos)
				{
					return adapter.getView(pos, null, null);
				}

				@Override
				public int getMaxShowCount()
				{
					return 3;
				}

				@Override
				public int getListCount()
				{
					return listModel.size();
				}

				@Override
				public String getClickMoreString()
				{
					return "查看更多";
				}

				@Override
				public String getClickBackString()
				{
					return "点击收回";
				}

				@Override
				public boolean clickMore(View v)
				{
					return false;
				}
			});
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		default:
			break;
		}
	}

}