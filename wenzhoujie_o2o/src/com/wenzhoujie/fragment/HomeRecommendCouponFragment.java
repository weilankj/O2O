package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.YouHuiListActivity;
import com.wenzhoujie.adapter.HomeRecommendCouponAdapter;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.model.IndexActYouhui_listModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 首页推荐优惠券
 * 
 * @author js02
 * 
 */
public class HomeRecommendCouponFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_home_recommend_coupon_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_home_recommend_coupon_ll_coupon)
	private LinearLayout mLlCoupon = null;

	@ViewInject(id = R.id.frag_home_recommend_coupon_ll_all_coupon)
	private LinearLayout mLlAllCoupon = null;

	private List<IndexActYouhui_listModel> mListModel = new ArrayList<IndexActYouhui_listModel>();

	private IndexActNewModel mIndexModel = null;

	public void setmIndexModel(IndexActNewModel indexModel)
	{
		this.mIndexModel = indexModel;
		if (mIndexModel != null)
		{
			mListModel = mIndexModel.getYouhui_list();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_home_recommend_coupon, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		registeClick();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlCoupon.removeAllViews();
			HomeRecommendCouponAdapter adapter = new HomeRecommendCouponAdapter(mListModel, getActivity());
			int listSize = mListModel.size();
			for (int i = 0; i < listSize; i++)
			{
				View view = adapter.getView(i, null, null);
				mLlCoupon.addView(view);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	private void registeClick()
	{
		mLlAllCoupon.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_home_recommend_coupon_ll_all_coupon:
			clickAllCoupon();
			break;
		default:
			break;
		}
	}

	private void clickAllCoupon()
	{
		startActivity(new Intent(getActivity(), YouHuiListActivity.class));
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case LOCATION_SUCCESS:
			evnetLocationSuccess();
			break;

		default:
			break;
		}
	}

	private void evnetLocationSuccess()
	{
		if (mListModel != null)
		{
			for (IndexActYouhui_listModel model : mListModel)
			{
				model.calculateDistance();
				bindData();
			}
		}

	}

}