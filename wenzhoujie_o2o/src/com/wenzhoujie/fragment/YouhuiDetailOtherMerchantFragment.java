package com.wenzhoujie.fragment;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.adapter.YouHuiDetailMerchantAdapter;
import com.wenzhoujie.customview.SDMoreView;
import com.wenzhoujie.customview.SDMoreViewBaseAdapter;
import com.wenzhoujie.model.YouhuiitemActMerchantModel;
import com.wenzhoujie.model.act.YouhuiitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 优惠详情 （其他门店fragment）
 * 
 * @author js02
 * 
 */
public class YouhuiDetailOtherMerchantFragment extends BaseFragment
{

	@ViewInject(id = R.id.ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_youhui_detail_other_merchant_smv_more)
	private SDMoreView mSmvMore = null;

	private List<YouhuiitemActMerchantModel> mListModel = null;

	private YouhuiitemActModel mYouhuiitemActModel = null;

	public void setmYouhuiitemActModel(YouhuiitemActModel mYouhuiitemActModel)
	{
		this.mYouhuiitemActModel = mYouhuiitemActModel;
		if (mYouhuiitemActModel != null)
		{
			this.mListModel = mYouhuiitemActModel.getList_merchant();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_youhui_detail_other_merchant, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			final YouHuiDetailMerchantAdapter adapter = new YouHuiDetailMerchantAdapter(mListModel, getActivity());
			mSmvMore.setAdapter(new SDMoreViewBaseAdapter()
			{

				@Override
				public View getView(int pos)
				{
					return adapter.getView(pos, null, null);
				}

				@Override
				public int getMaxShowCount()
				{
					return 3;
				}

				@Override
				public int getListCount()
				{
					return mListModel.size();
				}

				@Override
				public String getClickMoreString()
				{
					return "查看更多";
				}

				@Override
				public String getClickBackString()
				{
					return "点击收回";
				}

				@Override
				public boolean clickMore(View v)
				{
					return false;
				}
			});
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

}