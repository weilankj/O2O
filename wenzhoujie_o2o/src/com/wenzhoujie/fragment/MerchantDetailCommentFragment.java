package com.wenzhoujie.fragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.MerchantCommentListActivity;
import com.wenzhoujie.adapter.TuanDetailCommontsAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.act.MerchantitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 商家详细页（评价fragment）
 * 
 * @author js02
 * 
 */
public class MerchantDetailCommentFragment extends BaseFragment implements OnClickListener
{
	@ViewInject(id = R.id.frag_merchant_detail_comment_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_merchant_detail_comment_ll_comment_content)
	private LinearLayout mLlCommentContent = null;

	@ViewInject(id = R.id.frag_merchant_detail_comment_ll_more_comment)
	private LinearLayout mLlMoreComment = null;

	@ViewInject(id = R.id.frag_merchant_detail_comment_tv_comment)
	private TextView mTvCommnent = null;

	private MerchantitemActModel mMerchantitemActModel = null;

	private List<GoodsCommentModel> mListComments = null;

	public void setmMerchantitemActModel(MerchantitemActModel merchantitemActModel)
	{
		this.mMerchantitemActModel = merchantitemActModel;
		this.mListComments = mMerchantitemActModel.getComment_list();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_merchant_detail_comment, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		registeClick();
	}

	private void registeClick()
	{
		mLlMoreComment.setOnClickListener(this);
		mTvCommnent.setOnClickListener(this);
	}

	private void bindData()
	{
		if (mListComments != null && mListComments.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mTvCommnent.setVisibility(View.GONE);
			mLlCommentContent.removeAllViews();
			TuanDetailCommontsAdapter adapter = new TuanDetailCommontsAdapter(mListComments, getActivity());
			for (int i = 0; i < mListComments.size(); i++)
			{
				mLlCommentContent.addView(adapter.getView(i, null, null));
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
			mTvCommnent.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_merchant_detail_comment_ll_more_comment:
			clickMoreComment();
			break;
		case R.id.frag_merchant_detail_comment_tv_comment:
			clickMoreComment();
			break;

		default:
			break;
		}

	}

	private void clickMoreComment()
	{
		if (mMerchantitemActModel != null && !TextUtils.isEmpty(mMerchantitemActModel.getId()))
		{
			Intent intent = new Intent();
			intent.putExtra(MerchantCommentListActivity.EXTRA_MERCHANT_ID, mMerchantitemActModel.getId());
			intent.setClass(App.getApplication(), MerchantCommentListActivity.class);
			startActivity(intent);
		}

	}

}