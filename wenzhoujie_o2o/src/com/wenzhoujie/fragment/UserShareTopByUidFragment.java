package com.wenzhoujie.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.MyPhotosAndLovesActivity;
import com.wenzhoujie.UserFansAndMyFollowsActivity;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.UHome_userModel;
import com.wenzhoujie.model.act.UActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class UserShareTopByUidFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_user_share_top_by_uid_iv_user_avatar)
	private ImageView mIvUserAvatar = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_user_name)
	private TextView mTvUserName = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_fans_count)
	private LinearLayout mLlFansCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_fans_count)
	private TextView mTvFansCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_photos_count)
	private LinearLayout mLlPhotosCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_photos_count)
	private TextView mTvPhotosCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_like_count)
	private LinearLayout mLlLikeCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_like_count)
	private TextView mTvLikeCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_follow_count)
	private LinearLayout mLlFollowCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_follow_count)
	private TextView mTvFollowCount = null;

	private UActModel mUActModel = null;

	private String uid;

	private UserShareTopByUidFragmentListener mListener = null;

	public void setmListener(UserShareTopByUidFragmentListener mListener)
	{
		this.mListener = mListener;
	}

	public void setUActModel(UActModel uActModel)
	{
		this.mUActModel = uActModel;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_user_share_top_by_uid, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		registeClick();
		requestData();
	}

	public void refreshData()
	{
		requestData();
	}

	private void requestData()
	{
		if (mUActModel != null)
		{
			bindData(mUActModel);
			return;
		}

		if (AppHelper.isEmptyString(uid))
		{
			SDToast.showToast("要查看的用户id为空");
			return;
		}

		if (AppHelper.getLocalUser() == null)
		{
			SDToast.showToast("未登录");
			return;
		}

		RequestModel model = new RequestModel();
		model.put("act", "u");
		model.put("uid", uid);
		model.put("email", AppHelper.getLocalUser().getUser_name());
		model.put("pwd", AppHelper.getLocalUser().getUser_pwd());
		model.put("page", 1);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				UActModel model = JsonUtil.json2Object(responseInfo.result, UActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					if (model.getResponse_code() == 1)
					{
						bindData(model);
						if (mListener != null)
						{
							mListener.onSuccess(model);
						}
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{

			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void bindData(UActModel model)
	{
		if (model != null)
		{
			UHome_userModel p = model.getHome_user();
			if (p != null)
			{
				// 加载个人头像
				SDViewBinder.setImageView(mIvUserAvatar, p.getUser_avatar());
				// 设置个人名称
				mTvUserName.setText(p.getUser_name());
				// 粉丝数
				mTvFansCount.setText(String.valueOf(p.getFans()));
				// 相册数
				mTvPhotosCount.setText(String.valueOf(p.getPhotos()));
				// 喜欢数
				mTvLikeCount.setText(String.valueOf(p.getFavs()));
				// 关注数
				mTvFollowCount.setText(String.valueOf(p.getFollows()));
			}
		}

	}

	private void registeClick()
	{
		mLlFansCount.setOnClickListener(this);
		mLlPhotosCount.setOnClickListener(this);
		mLlLikeCount.setOnClickListener(this);
		mLlFollowCount.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_user_share_top_by_uid_ll_fans_count:
			clickFansCount();
			break;
		case R.id.frag_user_share_top_by_uid_ll_photos_count:
			clickPhotosCount();
			break;
		case R.id.frag_user_share_top_by_uid_ll_like_count:
			clickLikeCount();
			break;
		case R.id.frag_user_share_top_by_uid_ll_follow_count:
			clickFollowCount();
			break;
		default:
			break;
		}
	}

	/**
	 * 粉丝
	 */
	private void clickFansCount()
	{
		if (!AppHelper.isEmptyString(uid))
		{
			Intent intent = new Intent();
			intent.setClass(getActivity(), UserFansAndMyFollowsActivity.class);
			intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_ACT, "fanslist");
			intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_UID, uid);
			intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_TYPE, false);
			startActivity(intent);
		}
	}

	/**
	 * 相册
	 */
	private void clickPhotosCount()
	{
		if (!AppHelper.isEmptyString(uid))
		{
			Intent intent = new Intent();
			intent.setClass(getActivity(), MyPhotosAndLovesActivity.class);
			intent.putExtra(MyPhotosAndLovesActivity.EXTRA_UID, uid);
			intent.putExtra(MyPhotosAndLovesActivity.EXTRA_ACT, "photolist");
			intent.putExtra(MyPhotosAndLovesActivity.EXTRA_TITLE, "相册");
			startActivity(intent);
		}
	}

	/**
	 * 喜欢
	 */
	private void clickLikeCount()
	{
		if (!AppHelper.isEmptyString(uid))
		{

		}
		Intent intent = new Intent();
		intent.setClass(getActivity(), MyPhotosAndLovesActivity.class);
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_UID, uid);
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_ACT, "favlist");
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_TITLE, "喜欢");
		startActivity(intent);
	}

	/**
	 * 关注
	 */
	private void clickFollowCount()
	{
		if (!AppHelper.isEmptyString(uid))
		{
			Intent intent = new Intent();
			intent.setClass(getActivity(), UserFansAndMyFollowsActivity.class);
			intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_ACT, "followlist");
			intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_UID, uid);
			intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_TYPE, false);
			startActivity(intent);
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		refreshData();
	}

	public interface UserShareTopByUidFragmentListener
	{
		public void onSuccess(UActModel model);
	}

}