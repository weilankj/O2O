package com.wenzhoujie.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.EventdetailItemModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class EventDetailTopFragment extends BaseFragment
{

	@ViewInject(id = R.id.frag_event_detail_top_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_event_detail_top_tv_brief)
	private TextView mTvBrief = null;

	@ViewInject(id = R.id.frag_event_detail_top_tv_name)
	private TextView mTvName = null;

	private EventdetailItemModel mEventsDetailActivityItemModel = null;

	public void setEventsModel(EventdetailItemModel mEventsDetailActivityItemModel)
	{
		this.mEventsDetailActivityItemModel = mEventsDetailActivityItemModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_event_detail_top, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		if (mEventsDetailActivityItemModel != null && !TextUtils.isEmpty(mEventsDetailActivityItemModel.getBrief()))
		{
			mLlAll.setVisibility(View.VISIBLE);
			SDViewBinder.setTextView(mTvName, mEventsDetailActivityItemModel.getName());
			SDViewBinder.setTextView(mTvBrief, mEventsDetailActivityItemModel.getBrief());
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

}