package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.ECshopListActivity;
import com.wenzhoujie.adapter.ChaoshiCateAdapter;
import com.wenzhoujie.adapter.SearchTypeAdapter;
import com.wenzhoujie.model.MerchantdetailClassifyModel;
import com.wenzhoujie.model.act.MerchantitemChaoshiActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 超市分类
 * 
 * @author kek
 * 
 *   
 */
public class ChaoshiCateFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_chaoshi_cate_all)
	private LinearLayout fCsc = null;

	@ViewInject(id = R.id.frag_chaoshi_cate_view)
	private GridView cView = null;
	//private GridView mGridView = null;
	
	private ChaoshiCateAdapter adapter =null;
	
	private MerchantitemChaoshiActModel mIndexModel = null;

	private List<MerchantdetailClassifyModel> mListModel = new ArrayList<MerchantdetailClassifyModel>();

	public void setmIndexModel(MerchantitemChaoshiActModel indexModel)
	{
		this.mIndexModel = indexModel;
		//this.mListModel = mIndexModel.getCates();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_chaoshi_cate, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		//registeClick();
	}

	private void bindData()
	{
		//cView = (GridView) findViewById(R.id.frag_chaoshi_cate_view);

		ChaoshiCateAdapter adapter = new ChaoshiCateAdapter(mListModel, getActivity());
		cView.setAdapter(adapter);
		
		
//		if (mListModel != null && mListModel.size() > 0)
//		{
//			fCsc.setVisibility(View.VISIBLE);
//			cView.removeAllViews();
//			ChaoshiCateAdapter adapter = new ChaoshiCateAdapter(mListModel, getActivity());
//			int listSize = mListModel.size();
//			for (int i = 0; i < listSize; i++)
//			{
//				View view = adapter.getView(i, null, null);
//				cView.addView(view);
//			}
//		} else
//		{
//			fCsc.setVisibility(View.GONE);
//		}
	}

	private void registeClick()
	{
		cView.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_home_recommend_goods_ll_all_goods:
			clickAllGoods();
			break;
		default:
			break;
		}
	}

	private void clickAllGoods()
	{
		startActivity(new Intent(getActivity(), ECshopListActivity.class));
	}

}