package com.wenzhoujie.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wenzhoujie.library.customview.SDTabItemCorner;
import com.wenzhoujie.library.customview.SDViewAttr;
import com.wenzhoujie.library.customview.SDViewBase.EnumTabPosition;
import com.wenzhoujie.library.customview.SDViewNavigatorManager;
import com.wenzhoujie.library.customview.SDViewNavigatorManager.SDViewNavigatorManagerListener;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.wenzhoujie.LoginNewActivity;
import com.wenzhoujie.MineActivity;
import com.wenzhoujie.TakephotosActivity;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.config.O2oConfig;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.PhotosFragment.PhotosFragmentListener;
import com.wenzhoujie.model.SharelistActModel;
import com.wenzhoujie.utils.IocUtil;

public class ShareFragment extends BaseFragment implements OnClickListener
{

	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();

	private HomeAdvsFragment mFragAdvs = null;

	private PhotosFragment mFragHot = null;
	private PhotosFragment mFragNew = null;

	private PhotosFragment mFragLastVisible = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_share, container, false);
		view = setmTitleType(TitleType.TITLE_SIMPLE, view);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		initTab();
		initTitle();
		registeClick();
	}

	private void initTitle()
	{

		mTitleSimple.setLeftImage(R.drawable.ic_write);
		mTitleSimple.setRightText("我的");
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				clickTopRight();
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				clickTopLeft();
			}
		});

	}

	private void initTab()
	{
		SDTabItemCorner tabHot = new SDTabItemCorner(getActivity());
		tabHot.getmAttr().setmBackgroundColorNormalResId(R.color.bg_title_bar);
		tabHot.getmAttr().setmBackgroundColorSelectedResId(R.color.white);
		tabHot.getmAttr().setmTextColorNormalResId(R.color.white);
		tabHot.getmAttr().setmTextColorSelectedResId(R.color.bg_title_bar);
		tabHot.getmAttr().setmStrokeColorResId(R.color.white);
		tabHot.getmAttr().setmStrokeWidth(SDViewUtil.dp2px(1));

		tabHot.setTabName("最热");
		tabHot.setTabTextSizeSp(14);
		tabHot.setmPosition(EnumTabPosition.LAST);

		SDTabItemCorner tabNew = new SDTabItemCorner(getActivity());
		tabNew.setmAttr((SDViewAttr) tabHot.getmAttr().clone());
		tabNew.setTabName("最新");
		tabNew.setTabTextSizeSp(14);
		tabNew.setmPosition(EnumTabPosition.FIRST);

		mTitleSimple.mTlMiddle.removeAllViews();
		mTitleSimple.mTlMiddle.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);

		mTitleSimple.mTlMiddle.addView(tabNew, params);
		mTitleSimple.mTlMiddle.addView(tabHot, params);

		SDViewUtil.setViewHeight(mTitleSimple.mTlMiddle, SDViewUtil.dp2px(35));
		SDViewUtil.setViewWidth(mTitleSimple.mTlMiddle, SDViewUtil.dp2px(120));

		mViewManager.setItems(tabNew, tabHot);
		mViewManager.setmListener(new SDViewNavigatorManagerListener()
		{
			@Override
			public void onItemClick(View v, int index)
			{
				O2oConfig.setLastshareIndex(index);
				switch (index)
				{
				case 0: // 最新
					clickNew();
					break;
				case 1: // 最热
					clickHot();
					break;
				default:
					break;
				}
			}
		});

		mViewManager.setSelectIndex(O2oConfig.getLastshareIndex(), tabHot, true);
	}

	protected void clickHot()
	{
		if (mFragHot == null)
		{
			mFragHot = new PhotosFragment();
			mFragHot.setIs_hot(1);
			mFragHot.setmListener(mListener);
			replaceFragment(mFragHot, R.id.frag_share_fl_hot);
		}
		toggleFragment(mFragHot);
	}

	protected void clickNew()
	{
		if (mFragNew == null)
		{
			mFragNew = new PhotosFragment();
			mFragNew.setIs_new(1);
			mFragNew.setmListener(mListener);
			replaceFragment(mFragNew, R.id.frag_share_fl_new);
		}
		toggleFragment(mFragNew);
	}

	private void toggleFragment(PhotosFragment fragment)
	{
		if (mFragLastVisible != null)
		{
			hideFragment(mFragLastVisible);
		}
		showFragment(fragment);
		mFragLastVisible = fragment;
	}

	private void registeClick()
	{
	}

	@Override
	public void onClick(View v)
	{
	}

	private void clickTopLeft()
	{
		if (AppHelper.getLocalUser() != null)
		{
			Intent intent = new Intent(getActivity(), TakephotosActivity.class);
			startActivity(intent);
		} else
		{
			Intent intent = new Intent(getActivity(), LoginNewActivity.class);
			startActivity(intent);
		}
	}

	private void clickTopRight()
	{
		if (AppHelper.getLocalUser() != null)
		{
			// TODO 跳到我的界面
			Intent intent = new Intent(getActivity(), MineActivity.class);
			startActivity(intent);
		} else
		{
			Intent intent = new Intent(getActivity(), LoginNewActivity.class);
			startActivity(intent);
		}
	}

	private PhotosFragmentListener mListener = new PhotosFragmentListener()
	{

		@Override
		public void onSuccess(SharelistActModel model, boolean isLoadMore)
		{
			if (!isLoadMore)
			{
				bindAdvImages(model);
			}
		}
	};

	/**
	 * 绑定广告图片
	 * 
	 * @param model
	 */
	private void bindAdvImages(SharelistActModel model)
	{
		mFragAdvs = new HomeAdvsFragment();
		mFragAdvs.setListIndexActAdvsModel(model.getAdvs());
		replaceFragment(mFragAdvs, R.id.frag_share_fl_advs);
	}

}