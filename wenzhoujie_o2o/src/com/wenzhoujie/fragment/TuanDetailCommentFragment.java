package com.wenzhoujie.fragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.TuanCommentListActivity;
import com.wenzhoujie.adapter.TuanDetailCommontsAdapter;
import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 团购详情，商品评论fragment
 * 
 * @author js02
 * 
 */
public class TuanDetailCommentFragment extends BaseFragment implements OnClickListener
{
	@ViewInject(id = R.id.frag_tuan_detail_five_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_tuan_detail_five_ll_comment_content)
	private LinearLayout mLlCommentContent = null;

	@ViewInject(id = R.id.frag_tuan_detail_five_ll_more_comment)
	private LinearLayout mLlMoreComment = null;

	private GoodsdescActModel mGoodsModel = null;

	public void setmGoodsModel(GoodsdescActModel mGoodsModel)
	{
		this.mGoodsModel = mGoodsModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_tuan_detail_comment, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindDataByGoodsModel(mGoodsModel);
		registeClick();
	}

	private void registeClick()
	{
		mLlMoreComment.setOnClickListener(this);
	}

	private void bindDataByGoodsModel(GoodsdescActModel model)
	{
		if (model != null)
		{
			List<GoodsCommentModel> listComments = model.getMessage_list();
			if (listComments != null && listComments.size() > 0)
			{
				mLlAll.setVisibility(View.VISIBLE);
				mLlCommentContent.removeAllViews();
				TuanDetailCommontsAdapter adapter = new TuanDetailCommontsAdapter(listComments, getActivity());
				for (int i = 0; i < listComments.size(); i++)
				{
					mLlCommentContent.addView(adapter.getView(i, null, null));
				}
			} else
			{
				mLlAll.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_tuan_detail_five_ll_more_comment:
			clickMoreComment();
			break;

		default:
			break;
		}

	}

	private void clickMoreComment()
	{
		if (mGoodsModel != null && mGoodsModel.getGoods_id() != null)
		{
			Intent intent = new Intent(getActivity(), TuanCommentListActivity.class);
			intent.putExtra(TuanCommentListActivity.EXTRA_TUAN_ID, mGoodsModel.getGoods_id());
			startActivity(intent);
		}
	}

}