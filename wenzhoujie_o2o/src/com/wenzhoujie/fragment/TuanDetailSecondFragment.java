package com.wenzhoujie.fragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.TuanCommentListActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.GoodsExt_labelModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDTimerDown;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.utils.SDTimerDown.SDTimerDownListener;

public class TuanDetailSecondFragment extends BaseFragment implements OnClickListener
{
	@ViewInject(id = R.id.frag_tuan_detail_second_tv_goods_name)
	private TextView mTvGoodsName = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_tv_goods_detail)
	private TextView mTvGoodsDetail = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_tv_buy_count)
	private TextView mTvBuyCount = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_tv_time_down)
	private TextView mTvTimeDown = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_ll_goods_support)
	private LinearLayout mLlGoodsSupport = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_rl_comment_detail)
	private RelativeLayout mRlCommentDetail = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_rb_rating_star)
	private RatingBar mRbRatingStar = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_tv_star_number)
	private TextView mTvStarNumber = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_tv_comment_count)
	private TextView mTvCommentCount = null;

	@ViewInject(id = R.id.frag_tuan_detail_second_iv_arrow_right)
	private ImageView mIvArrowRight = null;

	private SDTimerDown mCounter = new SDTimerDown();

	boolean mIsCanBuy = true;

	private GoodsdescActModel mGoodsModel = null;

	public void setmGoodsModel(GoodsdescActModel mGoodsModel)
	{
		this.mGoodsModel = mGoodsModel;
	}

	public boolean ismIsCanBuy()
	{
		return mIsCanBuy;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_tuan_detail_second, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		mRbRatingStar.setStepSize(0.5f);
		registeClick();
		bindDataByGoodsModel(mGoodsModel);

	}

	private void bindDataByGoodsModel(GoodsdescActModel goodsModel)
	{
		if (goodsModel != null)
		{
			// 商品名称
			SDViewBinder.setTextView(mTvGoodsName, goodsModel.getSub_name());
			// 商品描述
			String desc = goodsModel.getGoods_brief();
			if (!TextUtils.isEmpty(desc))
			{
				mTvGoodsDetail.setText(desc);
			}
			// 商品是否支持随时退货，支持过期退货等信息
			bindExtLabelData(goodsModel);

			// 已售
			mTvBuyCount.setText(SDResourcesUtil.getString(R.string.has_sold) + SDTypeParseUtil.getIntFromString(goodsModel.getBuy_count(), 0));
			// 倒计时

			String lessTime = goodsModel.getLess_time();
			if (!TextUtils.isEmpty(lessTime))
			{
				if (lessTime.equals("none")) // 商品数量无限制
				{
					mTvTimeDown.setText("无限时");
				} else
				{
					long timeSecond = SDTypeParseUtil.getLongFromString(goodsModel.getLess_time(), 0);
					if (timeSecond <= 0)
					{
						mIsCanBuy = false;
						mTvTimeDown.setText("已过期");
					} else
					{
						mCounter.startCount(mTvTimeDown, timeSecond, new SDTimerDownListener()
						{

							@Override
							public void onTickFinish()
							{
								mIsCanBuy = false;
							}

							@Override
							public void onTick()
							{
								// TODO Auto-generated method stub

							}

							@Override
							public void onStart()
							{
								// TODO Auto-generated method stub

							}
						});
					}
				}
			}

			// 设置ratingBar
			SDViewBinder.setRatingBar(mRbRatingStar, SDTypeParseUtil.getFloatFromString(goodsModel.getPoint(), 0));
			SDViewBinder.setTextView(mTvStarNumber, goodsModel.getPoint());
			// 评论人数
			mTvCommentCount.setText(SDTypeParseUtil.getIntFromString(goodsModel.getMessage_count(), 0) + "人评分");
		}
	}

	/**
	 * 绑定商品是否支持随时退货，支持过期退货等信息
	 * 
	 * @param goodsModel
	 */
	private void bindExtLabelData(GoodsdescActModel goodsModel)
	{
		List<GoodsExt_labelModel> listExtLabel = goodsModel.getExt_label();
		if (listExtLabel != null && listExtLabel.size() > 0)
		{
			mLlGoodsSupport.setVisibility(View.VISIBLE);
			mLlGoodsSupport.removeAllViews();
			int count = 0;
			for (GoodsExt_labelModel labelModel : listExtLabel)
			{
				if (count % 2 == 0) // 被2整除
				{
					if (labelModel != null)
					{
						View viewGoodsSupport = LayoutInflater.from(App.getApplication()).inflate(R.layout.item_goods_support, null);
						TextView tvExt1 = (TextView) viewGoodsSupport.findViewById(R.id.item_goods_support_tv_support1);
						TextView tvExt2 = (TextView) viewGoodsSupport.findViewById(R.id.item_goods_support_tv_support2);
						ImageView ivExt1 = (ImageView) viewGoodsSupport.findViewById(R.id.item_goods_support_iv_image1);
						ImageView ivExt2 = (ImageView) viewGoodsSupport.findViewById(R.id.item_goods_support_iv_image2);

						// 绑定第一项数据
						bindExtByLabelModel(labelModel, ivExt1, tvExt1);

						// 绑定第二项数据
						if (listExtLabel.size() > (count + 1))
						{
							GoodsExt_labelModel labelModelRight = listExtLabel.get(count + 1);
							if (labelModelRight != null)
							{
								bindExtByLabelModel(labelModelRight, ivExt2, tvExt2);
							}
						}
						mLlGoodsSupport.addView(viewGoodsSupport);
					}
				}
				count++;
			}
		} else
		{
			mLlGoodsSupport.setVisibility(View.GONE);
		}
	}

	private void bindExtByLabelModel(GoodsExt_labelModel labelModel, ImageView iv, TextView tv)
	{
		if (labelModel != null)
		{
			SDViewBinder.setTextView(tv, labelModel.getName());
			int type = SDTypeParseUtil.getIntFromString(labelModel.getType(), -1);
			switch (type)
			{
			case 0: // 支持随时退货
				if (!TextUtils.isEmpty(labelModel.getIco())) // 如果有传图标下来则用传下来的图标，否则用默认图标
				{
					SDViewBinder.setImageView(iv, labelModel.getIco());
				} else
				{
					iv.setImageResource(R.drawable.ic_any_refund);
				}
				break;
			case 1: // 支持过期退货
				if (!TextUtils.isEmpty(labelModel.getIco())) // 如果有传图标下来则用传下来的图标，否则用默认图标
				{
					SDViewBinder.setImageView(iv, labelModel.getIco());
				} else
				{
					iv.setImageResource(R.drawable.ic_expire_refund);
				}
				break;
			default:
				iv.setVisibility(View.INVISIBLE);
				tv.setVisibility(View.INVISIBLE);
				break;
			}
		}
	}

	private void registeClick()
	{
		mRlCommentDetail.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_tuan_detail_second_rl_comment_detail:
			clickCommentDetail();
			break;

		default:
			break;
		}
	}

	private void clickCommentDetail()
	{
		if (mGoodsModel != null && mGoodsModel.getGoods_id() != null)
		{
			Intent intent = new Intent(getActivity(), TuanCommentListActivity.class);
			intent.putExtra(TuanCommentListActivity.EXTRA_TUAN_ID, mGoodsModel.getGoods_id());
			startActivity(intent);
		}
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		if (mCounter != null)
		{
			mCounter.stopCount();
		}
	}

}