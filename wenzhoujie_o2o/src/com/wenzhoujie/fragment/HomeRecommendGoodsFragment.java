package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.ECshopListActivity;
import com.wenzhoujie.adapter.HomeRecommendGoodsAdapter;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 首页推荐商品
 * 
 * @author js02
 * 
 */
public class HomeRecommendGoodsFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_home_recommend_goods_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_home_recommend_goods_ll_deals)
	private LinearLayout mLlGoods = null;

	@ViewInject(id = R.id.frag_home_recommend_goods_ll_all_goods)
	private LinearLayout mLlAllGoods = null;

	private IndexActNewModel mIndexModel = null;

	private List<IndexActDeal_listModel> mListModel = new ArrayList<IndexActDeal_listModel>();

	public void setmIndexModel(IndexActNewModel indexModel)
	{
		this.mIndexModel = indexModel;
		this.mListModel = mIndexModel.getSupplier_deal_list();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_home_recommend_goods, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		registeClick();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlGoods.removeAllViews();
			HomeRecommendGoodsAdapter adapter = new HomeRecommendGoodsAdapter(mListModel, getActivity());
			int listSize = mListModel.size();
			for (int i = 0; i < listSize; i++)
			{
				View view = adapter.getView(i, null, null);
				mLlGoods.addView(view);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	private void registeClick()
	{
		mLlAllGoods.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_home_recommend_goods_ll_all_goods:
			clickAllGoods();
			break;
		default:
			break;
		}
	}

	private void clickAllGoods()
	{
		startActivity(new Intent(getActivity(), ECshopListActivity.class));
	}

}