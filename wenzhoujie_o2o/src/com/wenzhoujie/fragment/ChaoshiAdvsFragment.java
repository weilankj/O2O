package com.wenzhoujie.fragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.MainActivity;
import com.wenzhoujie.customview.AbSlidingPlayView;
import com.wenzhoujie.customview.AbSlidingPlayView.AbOnItemClickListener;
import com.wenzhoujie.customview.AbSlidingPlayView.AbOnTouchListener;
import com.wenzhoujie.model.IndexActAdvsModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 超市广告fragment
 * 
 * @author js02
 * 
 */
public class ChaoshiAdvsFragment extends BaseFragment
{
	/** 首页接口model */
	public static final String EXTRA_LIST_INDEX_ACT_ADVS_MODEL = "extra_list_index_act_advs_model";

	@ViewInject(id = R.id.act_chaoshi_advs_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.act_merchant_advs_spv_ad)
	private AbSlidingPlayView mSpvAd = null;

	private List<IndexActAdvsModel> mListModel = null;

	public void setListIndexActAdvsModel(List<IndexActAdvsModel> listModel)
	{
		mListModel = listModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.act_merchant_detail_chaoshi, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		// initBundleData();
		initAbSlidingPlayView();
		bindAdvs();
	}

	// private void initBundleData()
	// {
	// Bundle bundle = getArguments();
	// if (bundle != null)
	// {
	// mListModel = (List<IndexActAdvsModel>)
	// bundle.getSerializable(EXTRA_LIST_INDEX_ACT_ADVS_MODEL);
	// }
	// }

	/**
	 * 绑定头部广告
	 */
	private void bindAdvs()
	{
		mSpvAd.stopPlay();
		if (mListModel != null && mListModel.size() > 0)
		{
			mSpvAd.getViewPager().setCurrentItem(Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % mListModel.size());

			mLlAll.setVisibility(View.VISIBLE);
			mSpvAd.removeAllViews();
			IndexActAdvsModel model = null;
			for (int i = 0; i < mListModel.size(); i++)
			{
				model = mListModel.get(i);
				ImageView ivAdv = new ImageView(getActivity());
				ivAdv.setScaleType(ScaleType.FIT_CENTER);
				mSpvAd.addView(ivAdv);
				if (i == 0)
				{
					SDViewBinder.setImageFillScreenWidthByScale(ivAdv, mSpvAd, model.getImg());
				} else
				{
					SDViewBinder.setImageView(ivAdv, model.getImg());
				}
			}
			/*if(mListModel.size() <= 0)
			{
				mLlAll.setVisibility(View.GONE);
				mSpvAd.setVisibility(View.GONE);
			}*/
		} else
		{
			mLlAll.setVisibility(View.GONE);
			mSpvAd.setVisibility(View.GONE);
		}
	}

	@Override
	public void onResume()
	{
		if (mSpvAd != null)
		{
			mSpvAd.startPlay();
		}
		super.onResume();
	}

	@Override
	public void onPause()
	{
		if (mSpvAd != null)
		{
			mSpvAd.stopPlay();
		}
		super.onPause();
	}

	private void initAbSlidingPlayView()
	{
		mSpvAd.setNavHorizontalGravity(Gravity.CENTER);

		mSpvAd.setOnItemClickListener(new AbOnItemClickListener()
		{
			@Override
			public void onClick(int position)
			{
				if (mListModel != null && mListModel.size() > 0)
				{
					IndexActAdvsModel model = mListModel.get(position);
					clickAd(model);
				}
			}
		});

		mSpvAd.setOnTouchListener(new AbOnTouchListener()
		{

			@Override
			public void onTouch(MotionEvent event)
			{
				// switch (event.getAction())
				// {
				// case MotionEvent.ACTION_DOWN:
				// mSpvAd.stopPlay();
				// break;
				// case MotionEvent.ACTION_UP:
				// mSpvAd.startPlay();
				// break;
				//
				// default:
				// break;
				// }
			}
		});
	}

	/**
	 * 广告被点击
	 * 
	 * @param model
	 */
	private void clickAd(IndexActAdvsModel model)
	{
		if (model != null)
		{
			int type = SDTypeParseUtil.getIntFromString(model.getType(), -1);
			if (type == 7)
			{
				if (getActivity() instanceof MainActivity)
				{
					MainActivity mainActivity = (MainActivity) getActivity();
					mainActivity.setSelectIndex(2, null, true);
				}
				return;
			}
			Intent intent = IndexActNewModel.createIntentByType(type, model.getData(), true);
			if (intent != null)
			{
				try
				{
					startActivity(intent);
				} catch (Exception e)
				{
					// TODO: handle exception
				}
			}
		}
	}

}