package com.wenzhoujie.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.http.ResponseInfo;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.CityListActivity;
import com.wenzhoujie.HomeSearchActivity;
import com.wenzhoujie.MyCaptureActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.common.CommonInterface;
import com.wenzhoujie.constant.Constant.SearchTypeNormal;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.listener.RequestScanResultListener;
import com.wenzhoujie.model.Mobile_qrcodeActModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.work.AppRuntimeWorker;

/**
 * 首页标题栏fragment2
 * 
 * @author song
 * 
 */
public class HomeTitleBarFragment2 extends BaseFragment implements OnClickListener
{
	public static final int REQUEST_CODE_SCAN_CODE = 1;
	/** 0:优惠，2:团购，3:商家，4:活动，5:商城 */
	public static final String EXTRA_SEARCH_TYPE = "extra_search_type";

	@ViewInject(id = R.id.frag_more_rl_search_bar)
	private LinearLayout mRlSearchBar = null;

	@ViewInject(id = R.id.frag_home_title_bar_ll_earn)
	private LinearLayout mLlEarn = null;

	@ViewInject(id = R.id.frag_home_title_bar_tv_earn)
	private TextView mTvCurrentCity = null;

	@ViewInject(id = R.id.frag_more_et_search_text)
	private TextView mEtSearchText = null;

	@ViewInject(id = R.id.frag_more_btn_search)
	private Button mBtnSearch = null;

	@ViewInject(id = R.id.frag_home_title_bar_iv_earn_arrow)
	private ImageView mIvEarnArrow = null;

	@ViewInject(id = R.id.frag_home_title_bar_barcode)
	private Button bcBtnSearch = null;

	private int type = 2; // 搜索商家
	private boolean mIsScanActivityStartByThis = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_home_title_bar_new, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		registeClick();
		bindTitlebarCityNameData();
		clickSearchBtn();
	}

	private void bindTitlebarCityNameData() {
		// 设置当前默认城市
		SDViewBinder.setTextView(mTvCurrentCity, AppRuntimeWorker.getCity_name(), "未找到");
	}

	private void registeClick()
	{
		mLlEarn.setOnClickListener(this);
		bcBtnSearch.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId()) {
		case R.id.frag_home_title_bar_ll_earn:
			clickEarn(v);
			break;
		case R.id.frag_home_title_bar_barcode:
			clickQrcode();
			break;
		default:
			break;
		}
	}

	/**
	 * 扫一扫
	 */
	private void clickQrcode()
	{
		mIsScanActivityStartByThis = true;
		Intent intent = new Intent(App.getApplication(), MyCaptureActivity.class);
		startActivityForResult(intent, REQUEST_CODE_SCAN_CODE);
	}

	/**
	 * 点击区域
	 */
	private void clickEarn(View v)
	{
		Intent intent = new Intent(App.getApplication(), CityListActivity.class);
		startActivity(intent);
	}

	/**
	 * 搜索
	 */
	private void clickSearchBtn()
	{
		mEtSearchText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), HomeSearchActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(HomeSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeNormal.MERCHANT);
				startActivity(intent);
			}
		});
	}

	/**
	 * 处理扫描结果
	 * 
	 * @param scanResult
	 */
	private void requestScanResult(String scanResult)
	{
		if (AppHelper.isEmptyString(scanResult))
		{
			SDToast.showToast("扫描结果为空");
			return;
		}

		CommonInterface.requestScanResult(scanResult, new RequestScanResultListener()
		{
			@Override
			public void onSuccess(ResponseInfo<String> responseInfo, Mobile_qrcodeActModel model)
			{
				int type = model.getTypeFormat();
				if (type == 0) // 未解释成功
				{
					SDToast.showToast("对不起，解释二维码失败");
				} else
				{
					Intent intent = IndexActNewModel.createIntentByType(type, model.getData(), false);
					if (intent != null)
					{
						try
						{
							startActivity(intent);
						} catch (Exception e)
						{
							e.printStackTrace();
						}
					} else
					{
						SDToast.showToast("未知的类型：" + type);
					}
				}
			}

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("正在解释验证码");
			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
			}

			@Override
			public void onFailure()
			{
				SDToast.showToast("解释失败");
			}
		});
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case SCAN_CODE_SUCCESS:
			if (mIsScanActivityStartByThis)
			{
				mIsScanActivityStartByThis = false;
				Intent intent = (Intent) event.getEventData();
				String scanResult = intent.getStringExtra(MyCaptureActivity.EXTRA_RESULT_SUCCESS_STRING);
				requestScanResult(scanResult);
			}
			break;

		case RETRY_INIT_SUCCESS:
			bindTitlebarCityNameData();
			break;

		case CITY_CHANGE:
			bindTitlebarCityNameData();
			break;
		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case REQUEST_CODE_SCAN_CODE:
			if (resultCode == MyCaptureActivity.RESULT_CODE_SCAN_SUCCESS)
			{
				String scanResult = data.getStringExtra(MyCaptureActivity.EXTRA_RESULT_SUCCESS_STRING);
				requestScanResult(scanResult);
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}