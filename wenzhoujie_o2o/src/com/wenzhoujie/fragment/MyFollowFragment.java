package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.MyFollowAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.FollowActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.FollowActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 关注的人fragment
 * 
 * @author js02
 * 
 */
public class MyFollowFragment extends BaseFragment
{
	@ViewInject(id = R.id.frag_my_follow_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	private MyFollowAdapter mAdapter;
	private List<FollowActItemModel> mListModel = new ArrayList<FollowActItemModel>();

	private int page;
	private int pageTotal;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_my_follow, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindDefaultData();
		initPullListView();
	}

	private void initPullListView()
	{
		mPtrlvContent.setMode(Mode.BOTH);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page = 1;
				requestFollow(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++page > pageTotal && pageTotal > 0)
				{
					SDToast.showToast("没有更多数据了");
					mPtrlvContent.onRefreshComplete();
				} else
				{
					requestFollow(true);
				}
			}
		});
		mPtrlvContent.setRefreshing();

	}

	private void requestFollow(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "ufollow");
		model.put("email", AppHelper.getLocalUser().getUser_name());
		model.put("pwd", AppHelper.getLocalUser().getUser_pwd());
		model.put("page", page);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				FollowActModel model = JsonUtil.json2Object(responseInfo.result, FollowActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							page = model.getPage().getPage();
							pageTotal = model.getPage().getPage_total();
						}
						SDViewUtil.updateAdapterByList(mListModel, model.getItem(), mAdapter, isLoadMore);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mPtrlvContent.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void bindDefaultData()
	{
		mAdapter = new MyFollowAdapter(mListModel, getActivity());
		mPtrlvContent.setAdapter(mAdapter);
	}

}