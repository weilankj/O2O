package com.wenzhoujie.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.EventdetailItemCommentsModel;
import com.wenzhoujie.model.EventdetailItemModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDMyImageGetterUtil;
import com.wenzhoujie.utils.ViewHolder;
import com.wenzhoujie.utils.ViewInject;

public class EvevtsDetailCommentFragment extends BaseFragment
{
	@ViewInject(id = R.id.frag_events_detail_ll_comment)
	private LinearLayout mLlComment = null;

	private EventdetailItemModel mDetailItemModel = null;

	private Spanned text;

	public void setEventsModel(EventdetailItemModel mDetailItemModel)
	{
		this.mDetailItemModel = mDetailItemModel;

	}

	public EventdetailItemModel getEventsModel()
	{
		return this.mDetailItemModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_evevts_detail_second, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		initComment();
	}

	/**
	 * 评论
	 */
	private void initComment()
	{
		mLlComment.removeAllViews();
		TextView pinglun = new TextView(App.getApplication());
		pinglun.setPadding(10, 5, 5, 5);
		pinglun.setText("评论...");
		pinglun.setTextColor(Color.BLUE);

		mLlComment.addView(pinglun);
		mLlComment.setOrientation(LinearLayout.VERTICAL);
		loadcommentListLinear();
	}

	private void loadcommentListLinear()
	{

		if (mDetailItemModel.getComments() != null)
		{
			// 加载评论列表
			LayoutInflater inflater = LayoutInflater.from(App.getApplication());
			LinearLayout.LayoutParams paramsDiv = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 1);
			for (int l = 0; l < mDetailItemModel.getComments().size(); l++)
			{

				View rowView = inflater.inflate(R.layout.item_events_detail_comment, null);

				EventdetailItemCommentsModel a = mDetailItemModel.getComments().get(l);
				// 设置评论用户头像
				ImageView user_avatar = ViewHolder.get(rowView, R.id.item_events_detail_comment_iv_user_avatar);
				SDViewBinder.setImageView(user_avatar, a.getUser_avatar());

				// 设置评论用户名称
				TextView user_name = ViewHolder.get(rowView, R.id.item_events_detail_comment_tv_user_name);
				SDViewBinder.setTextView(user_name, a.getUser_name());

				// 设置评论内容(表情替换)
				if (a.getParse_expres().size() != 0)
				{
					String b = a.getContent();
					for (int i1 = 0; i1 < a.getParse_expres().size(); i1++)
					{

						String c = "\\[\\w*" + a.getParse_expres().get(i1).getKey() + "\\]";// expresList.get(i).getKey().toString();
						// \[[衰]\]
						String g = "<img src='" + a.getParse_expres().get(i1).getValue() + "'/>";
						String d = b.replaceAll(c, g);
						b = d;
						TextView user_content = ViewHolder.get(rowView, R.id.item_events_detail_comment_tv_user_content);
						text = Html.fromHtml(b, new SDMyImageGetterUtil(getActivity(), user_content), null);
						user_content.setText(text);
					}
				} else if (a.getParse_expres().size() == 0)
				{
					TextView user_content = ViewHolder.get(rowView, R.id.item_events_detail_comment_tv_user_content);
					String text1 = a.getContent();
					user_content.setText(text1);
				}

				// 设置评论时间
				TextView user_time = ViewHolder.get(rowView, R.id.item_events_detail_comment_tv_user_time);
				SDViewBinder.setTextView(user_time, a.getTime());

				mLlComment.addView(rowView);

				if (l != mDetailItemModel.getComments().size() - 1)
				{
					View viewDiv = new View(App.getApplication());
					viewDiv.setBackgroundColor(getResources().getColor(R.color.stroke));
					mLlComment.addView(viewDiv, paramsDiv);
				}
			}
		}

	}

}