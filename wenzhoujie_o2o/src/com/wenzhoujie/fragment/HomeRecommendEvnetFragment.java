package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.wenzhoujie.ECshopListActivity;
import com.wenzhoujie.EventListActivity;
import com.wenzhoujie.MainActivity;
import com.wenzhoujie.MerchantListActivity;
import com.wenzhoujie.TuanListActivity;
import com.wenzhoujie.adapter.HomeRecommendEventAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.IndexActEvent_listModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 首页推荐活动
 * 
 * @author js02
 * 
 */
public class HomeRecommendEvnetFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_home_recommend_event_ll_all)
	private LinearLayout mLlAll = null;

//	@ViewInject(id = R.id.frag_home_recommend_event_ll_events)
//	private LinearLayout mLlEvnents = null;
//
	//@ViewInject(id = R.id.frag_home_recommend_event_ll_all_events)
	//private TextView mLlAllEvents = null;
	
	@ViewInject(id = R.id.benDiTeSebtn)
	private ImageButton benDiTeSeBTN = null;
	
	@ViewInject(id = R.id.jinKouShiPinbtn)
	private ImageButton jinKouShiPinBTN = null;
	

	private List<IndexActEvent_listModel> mListModel = new ArrayList<IndexActEvent_listModel>();

	private IndexActNewModel mIndexModel = null;

	public void setmIndexModel(IndexActNewModel indexModel)
	{
		this.mIndexModel = indexModel;
		this.mListModel = mIndexModel.getEvent_list();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_home_recommend_event, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		//bindData();
		registeClick();
	}

	/*private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlEvnents.removeAllViews();
			HomeRecommendEventAdapter adapter = new HomeRecommendEventAdapter(mListModel, getActivity());
			int listSize = mListModel.size();
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, SDViewUtil.dp2px(5));
			for (int i = 0; i < listSize; i++)
			{
				View view = adapter.getView(i, null, null);
				mLlEvnents.addView(view);
				if (i != 0)
				{
					mLlEvnents.addView(new TextView(App.getApplication()), param);
				}
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}
*/
	private void registeClick()
	{
		//mLlAllEvents.setOnClickListener(this);
		benDiTeSeBTN.setOnClickListener(this);
		jinKouShiPinBTN.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

//		case R.id.frag_home_recommend_event_ll_all_events:
//			clickAllEvents();
//			break;
		case R.id.benDiTeSebtn:
			clickBenDiTeSe();
			break;	
		case R.id.jinKouShiPinbtn:
			clickJinKouShiPin();
			break;
	
		default:
			break;
		}
	}

	private void clickAllEvents()
	{
		startActivity(new Intent(getActivity(), EventListActivity.class));
	}
	
	private void clickBenDiTeSe()
	{
		//startActivity(new Intent(getActivity(), EventListActivity.class));
		Intent intent = new Intent();
		intent.setClass(App.getApplication(), MerchantListActivity.class);
		intent.putExtra(MerchantListFragment.EXTRA_TITLE_TXT, "商家列表");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, "3");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_TYPE_ID, "103");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, "本地特色");
		
		startActivity(intent);
	}
	
	private void clickJinKouShiPin()
	{
		//startActivity(new Intent(getActivity(), EventListActivity.class));
		Intent intent = new Intent();
		intent.setClass(App.getApplication(), MerchantListActivity.class);
		intent.putExtra(MerchantListFragment.EXTRA_TITLE_TXT, "商家列表");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, "3");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_TYPE_ID, "104");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, "进口食品");
		
		startActivity(intent);
	}
}