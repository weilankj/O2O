package com.wenzhoujie.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.lingou.www.R;
import com.wenzhoujie.model.EventdetailItemModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class EvevtsDetailWebViewFragment extends BaseFragment
{
	@ViewInject(id = R.id.frag_events_detail_webview_pgb_progress)
	private ProgressBar mWPgbProgress = null;

	@ViewInject(id = R.id.frag_events_detail_webview_wv)
	private WebView mWWv = null;

	private EventdetailItemModel mDetailItemModel = null;

	public void setEventsModel(EventdetailItemModel mDetailItemModel)
	{
		this.mDetailItemModel = mDetailItemModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_evevts_detail_first, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		initWebView();
		startLoadData();
	}

	private void startLoadData()
	{
		if (mDetailItemModel.getContent() != null)
		{
			loadHtmlContent(mDetailItemModel.getContent());
			return;
		}

	}

	private void initWebView()
	{
		WebSettings settings = mWWv.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setSupportZoom(true);
		settings.setBuiltInZoomControls(true);
		// 设置自适应屏幕
		// settings.setUseWideViewPort(true);
		// settings.setLoadWithOverviewMode(true);

		mWWv.setWebViewClient(new ProjectDetailWebviewActivity_WebViewClient());
		mWWv.setWebChromeClient(new ProjectDetailWebviewActivity_WebChromeClient());
	}

	private void loadHtmlContent(String htmlContent)
	{
		if (htmlContent != null)
		{
			mWWv.loadDataWithBaseURL("about:blank", htmlContent, "text/html", "utf-8", null);
		}
	}

	class ProjectDetailWebviewActivity_WebViewClient extends WebViewClient
	{

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon)
		{

			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url)
		{

			super.onPageFinished(view, url);
		}
	}

	class ProjectDetailWebviewActivity_WebChromeClient extends WebChromeClient
	{

		@Override
		public void onProgressChanged(WebView view, int newProgress)
		{
			if (newProgress == 100)
			{
				mWPgbProgress.setVisibility(View.GONE);
			} else
			{
				if (mWPgbProgress.getVisibility() == View.GONE)
				{
					mWPgbProgress.setVisibility(View.VISIBLE);
				}
				mWPgbProgress.setProgress(newProgress);
			}
			super.onProgressChanged(view, newProgress);
		}
	}

}