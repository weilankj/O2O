package com.wenzhoujie.fragment;

import java.util.List;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.model.Order_parmModel;
import com.wenzhoujie.model.Payment_listModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 订单详情页（支付方式fragment）
 * 
 * @author js02
 * 
 */
public class OrderDetailPaymentsFragment extends BaseFragment
{

	@ViewInject(id = R.id.frag_order_detail_payments_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_order_detail_payments_rg_payment_list)
	private RadioGroup mRgPaymentList = null;

	private Order_parmModel mModel = null;

	private OrderDetailPaymentsFragmentListener mListener = null;
	/** 默认选中的支付id */
	private String mSelectPaymentId = null;

	private String payment_id = null;
	private String payment_code = null;
	private boolean mHasPayment = false;

	public String getSelectPaymentId()
	{
		return mSelectPaymentId;
	}

	public void setSelectPaymentId(String selectPaymentId)
	{
		this.mSelectPaymentId = selectPaymentId;
	}

	public String getPayment_id()
	{
		return payment_id;
	}

	public String getPayment_code()
	{
		return payment_code;
	}

	public boolean getHasPayment()
	{
		return mHasPayment;
	}

	public void setListener(OrderDetailPaymentsFragmentListener listener)
	{
		this.mListener = listener;
	}

	public void setData(Order_parmModel model, boolean showPayment)
	{
		this.mModel = model;
		this.mHasPayment = showPayment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_order_detail_payments, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mHasPayment)
		{
			mLlAll.setVisibility(View.VISIBLE);
			if (mModel != null)
			{
				mRgPaymentList.removeAllViews();
				List<Payment_listModel> listModel = mModel.getPayment_list();
				if (listModel != null && listModel.size() > 0)
				{
					int hasMcod = mModel.getHas_mcod(); // 1：支持货到付款，0：不支持
					String selectPaymentId = mSelectPaymentId; // 默认选中的支付方式
					if (AppHelper.isEmptyString(selectPaymentId))
					{
						selectPaymentId = mModel.getSelect_payment_id();
					}
					for (int i = 0; i < listModel.size(); i++)
					{
						Payment_listModel paymentModel = listModel.get(i);
						RadioButton rbPayment = createPaymentRadioButton(paymentModel, selectPaymentId, i, hasMcod);
						if (rbPayment != null)
						{
							mRgPaymentList.addView(rbPayment);
						}
					}
					mRgPaymentList.setOnCheckedChangeListener(new PaymentCheckListener());
				}
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}

	}

	/**
	 * 动态创建支付方式
	 * 
	 * @param paymentModel
	 * @param selectPaymentId
	 * @param btnId
	 * @param hasMcod
	 * @return
	 */
	private RadioButton createPaymentRadioButton(Payment_listModel paymentModel, String selectPaymentId, int btnId, int hasMcod)
	{
		if (paymentModel != null)
		{
			String name = paymentModel.getName();
			String hasCacl = paymentModel.getHas_calc();
			String id = paymentModel.getId();
			String code = paymentModel.getCode();

			if (!AppHelper.isEmptyString(name) && !AppHelper.isEmptyString(hasCacl) && !AppHelper.isEmptyString(id) && !AppHelper.isEmptyString(code))
			{
				if (hasMcod == 0 && "mcod".equalsIgnoreCase(code)) // 不支持货到付款
				{
					return null;
				}
				RadioButton radiobtn = new RadioButton(App.getApplication());
				radiobtn.setText(name);
				radiobtn.setTextColor(getResources().getColor(R.color.text_home_item_content));
				radiobtn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
				radiobtn.setTag(paymentModel);
				radiobtn.setId(btnId);
				if (id.equals(selectPaymentId)) // 默认支付方式
				{
					radiobtn.setChecked(true);
					payment_id = id;
					payment_code = code;
				}
				return radiobtn;
			}
		}
		return null;
	}

	/**
	 * 支付方式选择监听
	 * 
	 * @author js02
	 * 
	 */
	class PaymentCheckListener implements OnCheckedChangeListener
	{
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId)
		{
			RadioButton rbButton = (RadioButton) group.findViewById(checkedId);
			if (rbButton != null)
			{
				Payment_listModel paymentModel = (Payment_listModel) rbButton.getTag();
				if (paymentModel != null)
				{
					if ("1".equals(paymentModel.getHas_calc()))
					{
						payment_id = paymentModel.getId();
						payment_code = paymentModel.getCode();
						if (mListener != null)
						{
							mListener.onPaymentChange();
						}
					}
				}
			}
		}
	}

	public interface OrderDetailPaymentsFragmentListener
	{
		public void onPaymentChange();
	}

}