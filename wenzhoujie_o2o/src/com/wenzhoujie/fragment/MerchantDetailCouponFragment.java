package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.adapter.HomeRecommendCouponAdapter;
import com.wenzhoujie.customview.SDMoreView;
import com.wenzhoujie.customview.SDMoreViewBaseAdapter;
import com.wenzhoujie.model.IndexActYouhui_listModel;
import com.wenzhoujie.model.act.MerchantitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 门店详情，推荐优惠券
 * 
 * @author js02
 * 
 */
public class MerchantDetailCouponFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_merchant_detail_coupon_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_merchant_detail_coupon_smv_more)
	private SDMoreView mSmvMore = null;

	private List<IndexActYouhui_listModel> mListModel = new ArrayList<IndexActYouhui_listModel>();

	private MerchantitemActModel mMerchantitemActModel = null;

	public void setmMerchantitemActModel(MerchantitemActModel merchantitemActModel)
	{
		this.mMerchantitemActModel = merchantitemActModel;
		this.mListModel = mMerchantitemActModel.getYouhui_list();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_merchant_detail_coupon, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			final HomeRecommendCouponAdapter adapter = new HomeRecommendCouponAdapter(mListModel, getActivity());
			mSmvMore.setAdapter(new SDMoreViewBaseAdapter()
			{

				@Override
				public View getView(int pos)
				{
					return adapter.getView(pos, null, null);
				}

				@Override
				public int getMaxShowCount()
				{
					return 3;
				}

				@Override
				public int getListCount()
				{
					return mListModel.size();
				}

				@Override
				public String getClickMoreString()
				{
					return "查看更多";
				}

				@Override
				public String getClickBackString()
				{
					return "点击收回";
				}

				@Override
				public boolean clickMore(View v)
				{
					return false;
				}
			});
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		default:
			break;
		}
	}
}