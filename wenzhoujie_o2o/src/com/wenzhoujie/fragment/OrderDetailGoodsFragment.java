package com.wenzhoujie.fragment;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.adapter.OrderGoodsListAdapter;
import com.wenzhoujie.model.DealOrderItemModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 订单详情页面(订单商品fragment)
 * 
 * @author js02
 * 
 */
public class OrderDetailGoodsFragment extends BaseFragment {

	@ViewInject(id = R.id.frag_order_detail_goods_ll_all)
	private LinearLayout mLlAll = null;

	private List<DealOrderItemModel> mListModel = null;
	private int diver_state = 0;

	public void setListOrderGoodsModel(List<DealOrderItemModel> listModel) {
		this.mListModel = listModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_order_detail_goods,
				container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init() {
		bindData();
	}

	private void bindData() {
		if (mListModel != null && mListModel.size() > 0) {
			mLlAll.setVisibility(View.VISIBLE);
			mLlAll.removeAllViews();
			OrderGoodsListAdapter goodsAdapter = new OrderGoodsListAdapter(
					mListModel, true, getActivity());
			for (int i = 0; i < mListModel.size(); i++) {
				mLlAll.addView(goodsAdapter.getView(i, null, null));
			}

		} else {
			mLlAll.setVisibility(View.GONE);
		}
	}

}