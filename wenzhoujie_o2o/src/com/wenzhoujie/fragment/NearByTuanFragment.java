package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.adapter.NearByTuanAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.NearByTuanItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.NearByTuanModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class NearByTuanFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_nearby_discount_lv)
	private PullToRefreshListView mIpLvVip = null;

	@ViewInject(id = R.id.frag_nearby_discount_iv_empty)
	private ImageView mIvEmpty = null;

	private NearByTuanAdapter mAdapter = null;
	private List<NearByTuanItemModel> mListGoodsModel = new ArrayList<NearByTuanItemModel>();

	private SearchConditionModel mSearcher = new SearchConditionModel();

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_nearby_discount, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{

		bindDefaultData();
		initPullListView();

	}

	private void bindDefaultData()
	{
		mAdapter = new NearByTuanAdapter(mListGoodsModel, getActivity());
		mIpLvVip.setAdapter(mAdapter);
		mIpLvVip.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				NearByTuanItemModel Model = mAdapter.getItem((int) id);
				if (Model != null && Model.getGoods_id() != null)
				{
					Intent intent = new Intent(getActivity(), TuanDetailActivity.class);
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, Model.getGoods_id());
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 1);
					startActivity(intent);
				}
			}

		});
	}

	protected void requestNearByVip(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "nearbygoodses");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("city_id", mSearcher.getCity_id());
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("latitude_top", mSearcher.getM_latitude() + 0.2);
		model.put("latitude_bottom", mSearcher.getM_latitude() - 0.2);
		model.put("longitude_left", mSearcher.getM_longitude() - 0.2);
		model.put("longitude_right", mSearcher.getM_longitude() + 0.2);
		model.put("m_latitude", mSearcher.getM_latitude());
		model.put("m_longitude", mSearcher.getM_longitude());
		model.put("cate_id", mSearcher.getCate_id());
		model.put("page", mCurPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				NearByTuanModel model = JsonUtil.json2Object(responseInfo.result, NearByTuanModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mCurPage = model.getPage().getPage();
							mTotalPage = model.getPage().getPage_total();
						}
						if (!isLoadMore)
						{
							mListGoodsModel.clear();
						}
						if (model.getItem() != null)
						{
							mListGoodsModel.addAll(model.getItem());
						} else
						{
							SDToast.showToast("没有更多数据了");
						}
						mAdapter.updateListViewData(mListGoodsModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				mIpLvVip.onRefreshComplete();
				SDViewUtil.toggleEmptyMsgByList(mListGoodsModel, mIvEmpty);
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void initPullListView()
	{
		mIpLvVip.setMode(Mode.BOTH);
		mIpLvVip.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestNearByVip(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					SDToast.showToast("没有更多数据了");
					mIpLvVip.onRefreshComplete();
				} else
				{
					requestNearByVip(true);
				}
			}
		});
		mIpLvVip.setRefreshing();
	}

	@Override
	public void onClick(View arg0)
	{
		// TODO Auto-generated method stub

	}

}