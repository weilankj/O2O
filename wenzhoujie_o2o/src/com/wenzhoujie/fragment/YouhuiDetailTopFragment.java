package com.wenzhoujie.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wenzhoujie.library.dialog.SDDialogConfirm;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.LoginNewActivity;
import com.wenzhoujie.YouHuiDetailActivity;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.model.act.YouhuiitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

public class YouhuiDetailTopFragment extends BaseFragment implements OnClickListener
{

	public static final int REQUEST_CODE_REQUEST_LOGIN = 1;

	@ViewInject(id = R.id.frag_youhui_detail_top_rl_logo)
	private RelativeLayout mRlLogo = null;

	@ViewInject(id = R.id.frag_youhui_detail_top_iv_logo)
	private ImageView mIvLogo = null;

	@ViewInject(id = R.id.frag_youhui_detail_top_tv_logo_desc)
	private TextView mTvLogoDesc = null;

	@ViewInject(id = R.id.frag_youhui_detail_top_tv_preview_times)
	private TextView mTvPreviewTimes = null;

	@ViewInject(id = R.id.frag_youhui_detail_top_tv_download_times)
	private TextView mTvDownloadTimes = null;

	@ViewInject(id = R.id.frag_youhui_detail_top_tv_buy_youhui)
	private TextView mTvBuyYouhui = null;

	private String mStrYouHuiId = null;

	private YouhuiDetailTopFragmentListener mListener = null;

	private YouhuiitemActModel mYouhuiitemActModel = null;

	public void setListener(YouhuiDetailTopFragmentListener listener)
	{
		this.mListener = listener;
	}

	public YouhuiitemActModel getmYouhuiitemActModel()
	{
		return mYouhuiitemActModel;
	}

	public void setmYouhuiitemActModel(YouhuiitemActModel mYouhuiitemActModel)
	{
		this.mYouhuiitemActModel = mYouhuiitemActModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_youhui_detail_top, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		initIntentData();
		initViewState();
		bindData();
		registeClick();
	}

	private void initIntentData()
	{
		Intent intent = getActivity().getIntent();
		mStrYouHuiId = intent.getStringExtra(YouHuiDetailActivity.EXTRA_YOUHUI_ID);
	}

	private void initViewState()
	{
		if (!AppHelper.isLogin())
		{
			mTvBuyYouhui.setText("登陆后领取");
		} else
		{
			mTvBuyYouhui.setText("领取");
		}
	}

	private void bindData()
	{
		if (mYouhuiitemActModel != null)
		{
			SDViewBinder.setImageFillScreenWidthByScale(mIvLogo, mRlLogo, mYouhuiitemActModel.getLogo_2());
			SDViewBinder.setTextView(mTvPreviewTimes, mYouhuiitemActModel.getView_count(), "0");
			SDViewBinder.setTextView(mTvDownloadTimes, mYouhuiitemActModel.getDown_count(), "0");
			SDViewBinder.setTextView(mTvLogoDesc, mYouhuiitemActModel.getYcq());
		}
	}

	private void registeClick()
	{
		mTvBuyYouhui.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_youhui_detail_top_tv_buy_youhui:
			clickBuyYouHui();
			break;
		default:
			break;
		}
	}

	private void clickBuyYouHui()
	{
		if (AppHelper.isLogin())
		{
			requestBuyYouHui();
		} else
		{
			Intent intent = new Intent(getActivity(), LoginNewActivity.class);
			startActivityForResult(intent, REQUEST_CODE_REQUEST_LOGIN);
		}

	}

	/**
	 * 请求领取优惠券接口
	 */
	private void requestBuyYouHui()
	{
		RequestModel model = new RequestModel();
		model.put("act", "fasongduanxin");
		model.put("email", AppHelper.getLocalUser().getUser_name());
		model.put("pwd", AppHelper.getLocalUser().getUser_pwd());
		model.put("id", mStrYouHuiId);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
				if (null != actModel)
				{
					int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
					if (status == 1)
					{
						if (mListener != null)
						{
							mListener.onRefresh();
						}
						if (!TextUtils.isEmpty(actModel.getInfo()))
						{
							showTipDialog(actModel.getInfo());
						}
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{

			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	protected void showTipDialog(String info)
	{
		new SDDialogConfirm().setTextContent(info).show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == LoginNewActivity.RESULT_CODE_LOGIN_SUCCESS)
		{
			initViewState();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public interface YouhuiDetailTopFragmentListener
	{
		public void onRefresh();
	}

}