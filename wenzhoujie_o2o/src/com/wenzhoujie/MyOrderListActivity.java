package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.adapter.MyOrderListAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.customview.SDTabItemCorner;
import com.wenzhoujie.library.customview.SDViewBase.EnumTabPosition;
import com.wenzhoujie.library.customview.SDViewNavigatorManager;
import com.wenzhoujie.library.customview.SDViewNavigatorManager.SDViewNavigatorManagerListener;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.My_order_listActItemModel;
import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.My_order_listActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class MyOrderListActivity extends BaseActivity
{
	/** 订单状态 0:未付款、部分付款 2：已付款 */
	public static final String EXTRA_PAY_STATUS = "extra_pay_status";

	public static final int REQUEST_CODE_ORDER_DETAIL = 1;

	@ViewInject(id = R.id.act_my_order_list_ll_tabs)
	private LinearLayout mLlTabs = null;

	@ViewInject(id = R.id.act_my_order_list_tab_all)
	private SDTabItemCorner mTabAll = null;

	@ViewInject(id = R.id.act_my_order_list_tab_not_user)
	private SDTabItemCorner mTabNotUse = null;

	@ViewInject(id = R.id.act_my_order_list_tab_not_comment)
	private SDTabItemCorner mTabNotComment = null;
	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();

	@ViewInject(id = R.id.act_my_order_list_lv_orders)
	private PullToRefreshListView mLvOrders = null;

	@ViewInject(id = R.id.act_my_order_list_ll_empty)
	private LinearLayout mLlEmpty = null;

	private List<My_order_listActItemModel> mListModel = new ArrayList<My_order_listActItemModel>();
	private MyOrderListAdapter mAdapter = null;

	private int mPayStatus;

	private int mPage = 1;
	private int mPageTotal = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_order_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		initTitle();
		initTabs();
		bindDefaultData();
		initPullToRefreshListView();

	}

	private void initTabs()
	{
		mTabAll.setTabName("全部");
		mTabAll.setTabTextSizeSp(14);
		mTabAll.setmPosition(EnumTabPosition.FIRST);

		mTabNotUse.setTabName("未消费");
		mTabNotUse.setTabTextSizeSp(14);
		mTabNotUse.setmPosition(EnumTabPosition.MIDDLE);

		mTabNotComment.setTabName("待评价");
		mTabNotComment.setTabTextSizeSp(14);
		mTabNotComment.setmPosition(EnumTabPosition.LAST);

		mViewManager.setItems(mTabAll, mTabNotUse, mTabNotComment);

		mViewManager.setmListener(new SDViewNavigatorManagerListener()
		{
			@Override
			public void onItemClick(View v, int index)
			{
				switch (index)
				{
				case 0: // 全部

					break;
				case 1: // 未消费

					break;
				case 2: // 待评价

					break;
				default:
					break;
				}
			}
		});
		mViewManager.setSelectIndex(0, mTabAll, false);
	}

	private void bindDefaultData()
	{
		mAdapter = new MyOrderListAdapter(mListModel, this);
		mLvOrders.setAdapter(mAdapter);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

		if (mPayStatus == 0)
		{
			mTitleSimple.setTitleTop("待付款订单");
			// mLlTabs.setVisibility(View.GONE);
		} else if (mPayStatus == 2)
		{
			mTitleSimple.setTitleTop("已付款订单");
			// mLlTabs.setVisibility(View.VISIBLE);
		}
	}

	private void initIntentData()
	{
		mPayStatus = getIntent().getIntExtra(EXTRA_PAY_STATUS, 0);
	}

	private void initPullToRefreshListView()
	{
		mLvOrders.setMode(Mode.BOTH);
		mLvOrders.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refreshData();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				loadMoreData();
			}
		});
		/*mLvOrders.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				if (mAdapter != null)
				{
					My_order_listActItemModel model = mAdapter.getItem((int) id);
					if (model != null && model.getId() != null)
					{
						Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
						intent.putExtra(OrderDetailActivity.EXTRA_ORDER_ID, model.getId());
						startActivityForResult(intent, REQUEST_CODE_ORDER_DETAIL);
					}
				}

			}

		});*/
		mLvOrders.setRefreshing();
	}

	/**
	 * 下拉刷新数据
	 */
	protected void refreshData()
	{
		mPage = 1;
		requestOrders(false);
	}

	/**
	 * 上拉加载更多
	 */
	protected void loadMoreData()
	{
		mPage++;
		if (mPage > mPageTotal && mPageTotal != 0)
		{
			SDToast.showToast("没有更多数据了");
			mLvOrders.onRefreshComplete();
		} else
		{
			requestOrders(true);
		}

	}

	private void requestOrders(final boolean isLoadMore)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "my_order_list");
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			model.put("pay_status", mPayStatus);
			model.put("page", mPage);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					My_order_listActModel actModel = JsonUtil.json2Object(responseInfo.result, My_order_listActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							PageModel page = actModel.getPage();
							if (page != null)
							{
								mPage = page.getPage();
								mPageTotal = page.getPage_total();
							}

							if (actModel.getItem() != null && actModel.getItem().size() > 0)
							{
								if (!isLoadMore)
								{
									mListModel.clear();
								}
								mListModel.addAll(actModel.getItem());
							} else
							{
								mListModel.clear();
								SDToast.showToast("未找到数据");
							}
							mAdapter.updateListViewData(mListModel);
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
					SDViewUtil.toggleEmptyMsgByList(mListModel, mLlEmpty);
					mLvOrders.onRefreshComplete();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case REQUEST_CODE_ORDER_DETAIL:
			if (resultCode == OrderDetailActivity.RESULT_CODE_DELETE_ORDER_SUCCESS)
			{
				mLvOrders.setRefreshing();
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case PAY_ORDER_SUCCESS:
			refreshData();
			break;
		case REFRESH_ORDER_LIST:
			refreshData();
			break;

		default:
			break;
		}
	}
}