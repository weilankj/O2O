package com.wenzhoujie;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.PwdActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 修改密码
 * 
 * @author js02
 * 
 */
public class ModifyPasswordActivity extends BaseActivity implements OnClickListener
{

	@ViewInject(id = R.id.act_modify_password_tv_username)
	private TextView mTvUsername = null;

	@ViewInject(id = R.id.act_modify_password_et_old_password)
	private ClearEditText mEtOldPassword = null;

	@ViewInject(id = R.id.act_modify_password_et_new_password)
	private ClearEditText mEtNewPassword = null;

	@ViewInject(id = R.id.act_modify_password_et_new_password_confirm)
	private ClearEditText mEtNewPasswordConfirm = null;

	@ViewInject(id = R.id.act_modify_password_tv_submit)
	private TextView mTvSubmit = null;

	private String mStrOldPassword = null;
	private String mStrNewPassword = null;
	private String mStrNewPasswordConfirm = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_modify_password);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		registeClick();

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("修改密码");
	}

	private void registeClick()
	{
		mTvSubmit.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_modify_password_tv_submit:
			clickSubmit();
			break;

		default:
			break;
		}
	}

	private void clickSubmit()
	{
		if (validateParams())
		{
			// TODO 请求更改密码接口
			final LocalUserModel user = App.getApplication().getmLocalUser();
			if (user != null)
			{
				RequestModel model = new RequestModel(false);
				model.put("act", "pwd");
				model.put("email", user.getUser_email());
				model.put("oldpassword", mStrOldPassword);
				model.put("newpassword", mStrNewPassword);
				model.put("pwd", user.getUser_pwd());
				RequestCallBack<String> handler = new RequestCallBack<String>()
				{

					@Override
					public void onStart()
					{
						AppHelper.showLoadingDialog("请稍候...");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo)
					{
						PwdActModel actModel = JsonUtil.json2Object(responseInfo.result, PwdActModel.class);
						if (!SDInterfaceUtil.isActModelNull(actModel))
						{
							if (actModel.getResponse_code() == 1)
							{
								user.setUser_pwd(mStrNewPassword);
								App.getApplication().setmLocalUser(user);
								finish();
							} else
							{

							}
						}
					}

					@Override
					public void onFailure(HttpException error, String msg)
					{

					}

					@Override
					public void onFinish()
					{
						AppHelper.hideLoadingDialog();
					}
				};
				InterfaceServer.getInstance().requestInterface(model, handler);
			} else
			{

			}

		}

	}

	private boolean validateParams()
	{
		mStrOldPassword = mEtOldPassword.getText().toString().trim();
		if (TextUtils.isEmpty(mStrOldPassword))
		{
			SDToast.showToast("旧密码不能为空");
			mEtOldPassword.requestFocus();
			return false;
		}

		mStrNewPassword = mEtNewPassword.getText().toString().trim();
		if (TextUtils.isEmpty(mStrNewPassword))
		{
			SDToast.showToast("新密码不能为空");
			mEtNewPassword.requestFocus();
			return false;
		}

		if (mStrNewPassword.length() < 4)
		{
			SDToast.showToast("密码不能低于4位");
			mEtNewPassword.requestFocus();
			return false;
		}

		mStrNewPasswordConfirm = mEtNewPasswordConfirm.getText().toString().trim();
		if (TextUtils.isEmpty(mStrNewPasswordConfirm))
		{
			SDToast.showToast("确认新密码不能为空");
			mEtNewPasswordConfirm.requestFocus();
			return false;
		}

		if (!mStrNewPassword.equals(mStrNewPasswordConfirm))
		{
			SDToast.showToast("两次新密码不一致");
			return false;
		}

		return true;
	}

}