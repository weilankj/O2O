package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class NewsShowActivity extends BaseActivity implements OnClickListener
{
	/** title */
	public static final String EXTRA_TITLE = "extra_title";
	/** content */
	public static final String EXTRA_CONTENT = "extra_content";

	@ViewInject(id = R.id.act_news_show_rl_layout2)
	private RelativeLayout mRlLayout2 = null;

	@ViewInject(id = R.id.act_news_show_tv_news_title)
	private TextView mTvNewsTitle = null;

	@ViewInject(id = R.id.act_news_show_sv_news_show_scroll)
	private ScrollView mSvNewsShowScroll = null;

	@ViewInject(id = R.id.act_news_show_wb_news_content)
	private WebView mWbNewsContent = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_news_show);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		initIntentData();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

	}

	private void initIntentData()
	{
		Intent intent = getIntent();

		if (intent.getStringExtra(EXTRA_TITLE).equals(""))
		{
			mTvNewsTitle.setVisibility(View.GONE);
			mTitleSimple.setTitleTop("关于我们");
		} else
		{
			mTvNewsTitle.setVisibility(View.VISIBLE);
			mTvNewsTitle.setText(intent.getStringExtra(EXTRA_TITLE));
		}

		try
		{
			mWbNewsContent.loadDataWithBaseURL(null, intent.getStringExtra(EXTRA_CONTENT), "text/html", "utf-8", null);
		} catch (Exception e)
		{
			// TODO: handle exception
		}

	}

	@Override
	public void onClick(View v)
	{
	}

}