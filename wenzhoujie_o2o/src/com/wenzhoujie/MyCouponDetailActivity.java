package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.model.CouponlistActItemModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class MyCouponDetailActivity extends BaseActivity
{
	/** CouponlistActItemModel 实体 */
	public static final String EXTRA_COUPONLISTACTITEMMODEL = "extra_couponlistactitemmodel";

	@ViewInject(id = R.id.act_my_coupon_detail_iv_coupon_image)
	private ImageView mIvCouponImage = null;

	@ViewInject(id = R.id.act_my_coupon_detail_tv_coupon_brief)
	private TextView mTvCouponBrief = null;

	@ViewInject(id = R.id.act_my_coupon_detail_tv_coupon_limit_time)
	private TextView mTvCouponLimitTime = null;

	@ViewInject(id = R.id.act_my_coupon_detail_tv_coupon_code)
	private TextView mTvCouponCode = null;

	@ViewInject(id = R.id.act_my_coupon_detail_iv_coupon_code_image)
	private ImageView mIvCouponCodeImage = null;

	@ViewInject(id = R.id.act_my_coupon_detail_tv_merchant_name)
	private TextView mTvMerchantName = null;

	@ViewInject(id = R.id.act_my_coupon_detail_tv_merchant_mobile)
	private TextView mTvMerchantMobile = null;

	@ViewInject(id = R.id.act_my_coupon_detail_tv_merchant_address)
	private TextView mTvMerchantAddress = null;

	private CouponlistActItemModel mModel = null;

	private boolean mIsCouponImageLoad = false;
	private boolean mIsCouponCodeImageLoad = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_coupon_detail);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData();
		initTitle();
		bindData();
	}

	private void getIntentData()
	{
		Intent intent = getIntent();
		mModel = (CouponlistActItemModel) intent.getSerializableExtra(EXTRA_COUPONLISTACTITEMMODEL);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop(SDResourcesUtil.getString(R.string.tuan_gou_coupon_detail));

	}

	private void bindData()
	{

		mIvCouponImage.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener()
		{
			@Override
			public boolean onPreDraw()
			{
				if (mModel != null && !mIsCouponImageLoad)
				{
					mIsCouponImageLoad = true;
					SDViewBinder.setImageViewByImagesScale(mIvCouponImage, mModel.getDealIcon());
				}
				return true;
			}
		});

		mIvCouponCodeImage.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener()
		{
			@Override
			public boolean onPreDraw()
			{
				if (mModel != null && !mIsCouponCodeImageLoad)
				{
					mIsCouponCodeImageLoad = true;
					SDViewBinder.setImageViewByImagesScale(mIvCouponCodeImage, mModel.getQrcode());
				}
				return true;
			}
		});

		if (mModel != null)
		{
			SDViewBinder.setTextView(mTvCouponBrief, mModel.getDealName());

			SDViewBinder.setTextView(mTvCouponLimitTime, mModel.getEndTime());
			SDViewBinder.setTextView(mTvCouponCode, mModel.getPassword());

			SDViewBinder.setTextView(mTvMerchantName, mModel.getSpName());
			SDViewBinder.setTextView(mTvMerchantMobile, mModel.getSpTel());
			SDViewBinder.setTextView(mTvMerchantAddress, mModel.getSpAddress());
		}
	}

}