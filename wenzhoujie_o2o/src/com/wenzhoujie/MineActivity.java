package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.UHome_userModel;
import com.wenzhoujie.model.act.UActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class MineActivity extends BaseActivity implements OnClickListener
{

	@ViewInject(id = R.id.act_mine_ll_my_follow)
	private LinearLayout mLlMyFollow = null;

	@ViewInject(id = R.id.act_mine_ll_my_publish)
	private LinearLayout mLlMypublish = null;

	@ViewInject(id = R.id.act_mine_ll_comment_me)
	private LinearLayout mLlCommentMe = null;

	@ViewInject(id = R.id.act_mine_ll_at_me)
	private LinearLayout mLlAtMe = null;

	@ViewInject(id = R.id.act_mine_ll_private_message)
	private LinearLayout mLlPrivateMessage = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_iv_user_avatar)
	private ImageView mIvUserAvatar = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_user_name)
	private TextView mTvUserName = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_fans_count)
	private LinearLayout mLlFansCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_fans_count)
	private TextView mTvFansCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_photos_count)
	private LinearLayout mLlPhotosCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_photos_count)
	private TextView mTvPhotosCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_like_count)
	private LinearLayout mLlLikeCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_like_count)
	private TextView mTvLikeCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_ll_follow_count)
	private LinearLayout mLlFollowCount = null;

	@ViewInject(id = R.id.frag_user_share_top_by_uid_tv_follow_count)
	private TextView mTvFollowCount = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_mine);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		registeClick();
		requestFollow();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("我的");
	}

	private void loadSetData(UHome_userModel p)
	{
		// 加载个人头像
		SDViewBinder.setImageView(mIvUserAvatar, p.getUser_avatar());
		// 设置个人名称
		mTvUserName.setText(p.getUser_name());
		// 粉丝数
		mTvFansCount.setText(String.valueOf(p.getFans()));
		// 相册数
		mTvPhotosCount.setText(String.valueOf(p.getPhotos()));
		// 喜欢数
		mTvLikeCount.setText(String.valueOf(p.getFavs()));
		// 关注数
		mTvFollowCount.setText(String.valueOf(p.getFollows()));

	}

	private void requestFollow()
	{
		RequestModel model = new RequestModel();
		model.put("act", "u");
		model.put("uid", mSearcher.getUid());
		model.put("email", mSearcher.getEmail());
		model.put("pwd", mSearcher.getPwd());
		model.put("page", 1);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				UActModel model = JsonUtil.json2Object(responseInfo.result, UActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:
						break;
					case 1:
						loadSetData(model.getHome_user());
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void registeClick()
	{

		mLlFansCount.setOnClickListener(this);
		mLlPhotosCount.setOnClickListener(this);
		mLlLikeCount.setOnClickListener(this);
		mLlFollowCount.setOnClickListener(this);

		mLlMyFollow.setOnClickListener(this);
		mLlMypublish.setOnClickListener(this);
		mLlCommentMe.setOnClickListener(this);
		mLlAtMe.setOnClickListener(this);
		mLlPrivateMessage.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_user_share_top_by_uid_ll_fans_count:
			clickFansCount();
			break;
		case R.id.frag_user_share_top_by_uid_ll_photos_count:
			clickPhotosCount();
			break;
		case R.id.frag_user_share_top_by_uid_ll_like_count:
			clickLikeCount();
			break;
		case R.id.frag_user_share_top_by_uid_ll_follow_count:
			clickFollowCount();
			break;

		case R.id.act_mine_ll_comment_me:
			clickCommentMe();
			break;

		case R.id.act_mine_ll_at_me:
			clickAtMe();
			break;

		case R.id.act_mine_ll_private_message:
			clickPrivateMessage();
			break;

		case R.id.act_mine_ll_my_follow:
			clickMyFollow();
			break;

		case R.id.act_mine_ll_my_publish:
			clickMypublish();
			break;

		default:
			break;
		}
	}

	/**
	 * 粉丝
	 */
	private void clickFansCount()
	{
		Intent intent = new Intent();
		intent.setClass(this, UserFansAndMyFollowsActivity.class);
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_ACT, "fanslist");
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_UID, AppHelper.getLocalUser().getUser_id());
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_TYPE, false);
		startActivity(intent);
	}

	/**
	 * 相册
	 */
	private void clickPhotosCount()
	{
		Intent intent = new Intent();
		intent.setClass(this, MyPhotosAndLovesActivity.class);
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_UID, AppHelper.getLocalUser().getUser_id());
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_ACT, "photolist");
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_TITLE, "相册");
		startActivity(intent);
	}

	/**
	 * 喜欢
	 */
	private void clickLikeCount()
	{
		Intent intent = new Intent();
		intent.setClass(this, MyPhotosAndLovesActivity.class);
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_UID, AppHelper.getLocalUser().getUser_id());
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_ACT, "favlist");
		intent.putExtra(MyPhotosAndLovesActivity.EXTRA_TITLE, "喜欢");
		startActivity(intent);
	}

	/**
	 * 关注
	 */
	private void clickFollowCount()
	{
		Intent intent = new Intent();
		intent.setClass(this, UserFansAndMyFollowsActivity.class);
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_ACT, "followlist");
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_UID, AppHelper.getLocalUser().getUser_id());
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_TYPE, false);
		startActivity(intent);
	}

	/**
	 * 我发表的
	 */
	private void clickMypublish()
	{
		startActivity(new Intent(this, MyPublishActivity.class));
	}

	/**
	 * 我关注的
	 */
	private void clickMyFollow()
	{
		startActivity(new Intent(this, MyFollowActivity.class));
	}

	/**
	 * 私信
	 */
	private void clickPrivateMessage()
	{
		startActivity(new Intent(this, PrivatelettersListActivity.class));

	}

	/**
	 * @我的
	 */
	private void clickAtMe()
	{
		startActivity(new Intent(this, FollowMeActivity.class));
	}

	/**
	 * 评论我的
	 */
	private void clickCommentMe()
	{
		startActivity(new Intent(this, CommentMysActivity.class));
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		requestFollow();
	}

}