package com.wenzhoujie;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.customview.SDSendValidateButton;
import com.wenzhoujie.library.customview.SDSendValidateButton.SDSendValidateButtonListener;
import com.wenzhoujie.library.dialog.SDDialogConfirm;
import com.wenzhoujie.library.dialog.SDDialogCustom;
import com.wenzhoujie.library.dialog.SDDialogCustom.SDDialogCustomListener;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.LoginPhoneFragment;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.model.act.Binding_mobileActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 绑定手机号
 * 
 * @author js02
 * 
 */
public class BindPhoneActivity extends BaseActivity implements OnClickListener
{

	public static final int RESULT_CODE_BIND_PHONE_SUCCESS = 10;

	@ViewInject(id = R.id.act_bind_phone_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.act_bind_phone_et_phone)
	private ClearEditText mEtPhone = null;

	@ViewInject(id = R.id.act_bind_phone_btn_sand_validate)
	private SDSendValidateButton mBtnSandValidate = null;

	@ViewInject(id = R.id.act_bind_phone_et_code)
	private ClearEditText mEtCode = null;

	@ViewInject(id = R.id.act_bind_phone_btn_submit)
	private Button mBtnSubmit = null;

	private String mStrNewPhone = null;// 要绑定的手机号码
	private String mStrCode = null; // 验证码

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_bind_phone);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		registeClick();
		initSendValidateButton();
		// requestIsBindPhone();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("绑定手机号");
	}

	private void registeClick()
	{
		mBtnSubmit.setOnClickListener(this);
	}

	/**
	 * 初始化发送验证码按钮
	 */
	private void initSendValidateButton()
	{
		mBtnSandValidate.setmBackgroundEnableResId(R.drawable.layer_main_color_normal);
		mBtnSandValidate.setmBackgroundDisableResId(R.drawable.layer_main_color_press);
		mBtnSandValidate.setmListener(new SDSendValidateButtonListener()
		{
			@Override
			public void onTick()
			{
			}

			@Override
			public void onClickSendValidateButton()
			{
				mStrNewPhone = mEtPhone.getText().toString();
				if (TextUtils.isEmpty(mStrNewPhone))
				{
					SDToast.showToast("请输入要绑定的手机号码");
					mEtPhone.requestFocus();
					return;
				}
				if (mStrNewPhone.length() != 11)
				{
					SDToast.showToast("请输入11位的手机号码");
					mEtPhone.requestFocus();
					return;
				}
				requestValidateCode();
			}
		});
	}

	/**
	 * 请求验证码接口
	 */
	protected void requestValidateCode()
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "verify_phone");
			model.put("mobile", mStrNewPhone);
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						mBtnSandValidate.startTickWork();
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	/**
	 * 请求是否绑定手机号码接口
	 */
	private void requestIsBindPhone()
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "binding_mobile");
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Binding_mobileActModel actModel = JsonUtil.json2Object(responseInfo.result, Binding_mobileActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							mLlAll.setVisibility(View.VISIBLE);
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_bind_phone_btn_submit:
			clickSubmit();
			break;
		default:
			break;
		}
	}

	private void clickSubmit()
	{
		if (validateParam())
		{
			requestBindPhone();
		}
	}

	/**
	 * 请求绑定手机号接口
	 */
	private void requestBindPhone()
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "do_binding_mobile");
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			model.put("new_mobile", mStrNewPhone);
			model.put("code", mStrCode);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							/* 0:绑定失败1:成功2:已经被他人绑定了 */
							int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
							switch (status)
							{
							case 0:

								break;
							case 1:
								setResult(RESULT_CODE_BIND_PHONE_SUCCESS);
								finish();
								break;
							case 2:
								showPhoneHasBindDialog();
								break;

							default:
								break;
							}
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	protected void showPhoneHasBindDialog()
	{
		new SDDialogConfirm().setTextContent(mEtPhone.getText().toString().trim() + "已经被绑定是否直接用该手机号登录?").setmListener(new SDDialogCustomListener()
		{
			
			@Override
			public void onDismiss(DialogInterface iDialog, SDDialogCustom dialog)
			{
			}
			
			@Override
			public void onClickConfirm(View v, SDDialogCustom dialog)
			{
				Intent intent = new Intent(BindPhoneActivity.this, LoginNewActivity.class);
				intent.putExtra(LoginNewActivity.EXTRA_SELECT_TAG_INDEX, 1);
				intent.putExtra(LoginPhoneFragment.EXTRA_PHONE_NUMBER, mEtPhone.getText().toString().trim());
				startActivity(intent);
				finish();
			}
			
			@Override
			public void onClickCancel(View v, SDDialogCustom dialog)
			{
			}
		}).show();
	}

	private boolean validateParam()
	{

		mStrNewPhone = mEtPhone.getText().toString();
		if (TextUtils.isEmpty(mStrNewPhone))
		{
			SDToast.showToast("请输入要绑定的手机号码");
			mEtPhone.requestFocus();
			return false;
		}
		if (mStrNewPhone.length() != 11)
		{
			SDToast.showToast("请输入11位的手机号码");
			mEtPhone.requestFocus();
			return false;
		}

		mStrCode = mEtCode.getText().toString();
		if (TextUtils.isEmpty(mStrCode))
		{
			SDToast.showToast("请输入验证码");
			mEtCode.requestFocus();
			return false;
		}

		return true;
	}

	@Override
	protected void onDestroy()
	{
		mBtnSandValidate.stopTickWork();
		super.onDestroy();
	}

}