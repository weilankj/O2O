package com.wenzhoujie;

import android.os.Bundle;

import com.lingou.www.R;
import com.wenzhoujie.fragment.MerchantListFragment;
import com.wenzhoujie.fragment.SuperMarketSubCateFragment;

/**
 * 
 * 
 *
 * 
 */
public class SuperMarketSubCateActivity extends BaseActivity
{

	private SuperMarketSubCateFragment mFragMerchant;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_ecshop_list);
		init();

	}

	private void init()
	{
		mFragMerchant = new SuperMarketSubCateFragment();
		replaceFragment(mFragMerchant, R.id.act_echsop_fl_fragment_content);
	}
	public SuperMarketSubCateFragment getFrag()
	{
		return mFragMerchant;
	}

}