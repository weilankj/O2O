package com.wenzhoujie.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lingou.www.R;

public class SDGoodsAttrItemView extends LinearLayout
{

	private TextView mTvTitle = null;

	private ImageView mIvTop = null;
	private ImageView mIvLeft = null;
	private ImageView mIvBottom = null;
	private ImageView mIvRight = null;

	public SDGoodsAttrItemView(Context context)
	{
		super(context);
		init();
	}

	public SDGoodsAttrItemView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.item_tuan_detail_attr_single, this, true);
		mIvTop = (ImageView) findViewById(R.id.item_tuan_detail_attr_single_iv_div_top);
		mIvLeft = (ImageView) findViewById(R.id.item_tuan_detail_attr_single_iv_div_left);
		mIvBottom = (ImageView) findViewById(R.id.item_tuan_detail_attr_single_iv_div_bottom);
		mIvRight = (ImageView) findViewById(R.id.item_tuan_detail_attr_single_iv_div_right);
		mTvTitle = (TextView) findViewById(R.id.item_tuan_detail_attr_single_tv_title);
	}

	public void setTitle(String title)
	{
		if (title != null)
		{
			mTvTitle.setText(title);
		}
	}

	public void hideTopDiv()
	{
		mIvTop.setVisibility(View.GONE);
	}

	public void hideLeftDiv()
	{
		mIvLeft.setVisibility(View.GONE);
	}

	public void hideRightDiv()
	{
		mIvRight.setVisibility(View.GONE);
	}

	public void hideBottomDiv()
	{
		mIvBottom.setVisibility(View.GONE);
	}

	public void showTopDiv()
	{
		mIvTop.setVisibility(View.VISIBLE);
	}

	public void showLeftDiv()
	{
		mIvLeft.setVisibility(View.VISIBLE);
	}

	public void showRightDiv()
	{
		mIvRight.setVisibility(View.VISIBLE);
	}

	public void showBottomDiv()
	{
		mIvBottom.setVisibility(View.VISIBLE);
	}

}
