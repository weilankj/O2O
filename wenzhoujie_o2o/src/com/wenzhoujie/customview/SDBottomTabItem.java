package com.wenzhoujie.customview;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lingou.www.R;

public class SDBottomTabItem extends SDBottomNavigatorBaseItem
{

	private ImageView mIvTitle = null;
	private TextView mTvTitle = null;
	private TextView mTvNumbr = null;

	private int mBackgroundNormalId = 0;
	private int mBackgroundPressId = 0;

	private int mImageNormalId = 0;
	private int mImagePressId = 0;

	private int mTextColorNormal = Color.parseColor("#c2c2c2"); 
	private int mTextColorPress = Color.parseColor("#f43d69");

	public void setTitleText(String title)
	{
		mTvTitle.setText(title);
	}

	public int getmTextColorNormal()
	{
		return mTextColorNormal;
	}

	public void setmTextColorNormal(int mTextColorNormal)
	{
		this.mTextColorNormal = mTextColorNormal;
	}

	public int getmTextColorPress()
	{
		return mTextColorPress;
	}

	public void setmTextColorPress(int mTextColorPress)
	{
		this.mTextColorPress = mTextColorPress;
	}

	public int getmImageNormalId()
	{
		return mImageNormalId;
	}

	public void setmImageNormalId(int mImageNormalId)
	{
		this.mImageNormalId = mImageNormalId;
	}

	public int getmImagePressId()
	{
		return mImagePressId;
	}

	public void setmImagePressId(int mImagePressId)
	{
		this.mImagePressId = mImagePressId;
	}

	public int getmBackgroundNormalId()
	{
		return mBackgroundNormalId;
	}

	public void setmBackgroundNormalId(int mBackgroundNormalId)
	{
		this.mBackgroundNormalId = mBackgroundNormalId;
	}

	public int getmBackgroundPressId()
	{
		return mBackgroundPressId;
	}

	public void setmBackgroundPressId(int mBackgroundPressId)
	{
		this.mBackgroundPressId = mBackgroundPressId;
	}

	public SDBottomTabItem(Context context)
	{
		super(context);
		init();
	}

	public SDBottomTabItem(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.view_tab_item, this, true);
		mIvTitle = (ImageView) findViewById(R.id.view_tab_item_iv_title);
		mTvTitle = (TextView) findViewById(R.id.view_tab_item_tv_title);
		mTvNumbr = (TextView) findViewById(R.id.view_tab_item_tv_number);
		mTvNumbr.setVisibility(View.GONE);
	}

	public void setTitleNumber(String number)
	{
		if (number != null && number.length() > 0)
		{
			mTvNumbr.setVisibility(View.VISIBLE);
			mTvNumbr.setText(number);
		} else
		{
			mTvNumbr.setVisibility(View.GONE);
		}
	}

	@Override
	public void setSelectedState(boolean select)
	{
		if (select)
		{
			mSelected = true;
			if (mImagePressId != 0)
			{
				mIvTitle.setImageResource(mImagePressId);
			}
			if (mBackgroundPressId != 0)
			{
				mIvTitle.setImageResource(mBackgroundPressId);
			}
			if (mTextColorPress != 0)
			{
				mTvTitle.setTextColor(mTextColorPress);
			}
		} else
		{
			mSelected = false;
			if (mImageNormalId != 0)
			{
				mIvTitle.setImageResource(mImageNormalId);
			}
			if (mBackgroundNormalId != 0)
			{
				mIvTitle.setImageResource(mBackgroundNormalId);
			}
			if (mTextColorNormal != 0)
			{
				mTvTitle.setTextColor(mTextColorNormal);
			}
		}

	}

	@Override
	public boolean getSelectedState()
	{
		// TODO Auto-generated method stub
		return mSelected;
	}

}
