package com.wenzhoujie.customview;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.wenzhoujie.adapter.smCategoryCateLeftAdapter;
import com.wenzhoujie.customview.SDLvCategoryView.SDLvCategoryViewAdapterInterface;

public class smSD2LvCategoryView extends SDBottomNavigatorBaseItem
{

	private static final int DEFAULT_INDEX = 0;

	private smSD2LvCategoryViewAdapterInterface mAdapterHelperLeft;
	private smSD2LvCategoryViewAdapterInterface mAdapterHelperRight;

	private ImageView mIvLeft = null;
	private ImageView mIvRight = null;
	private TextView mTvTitle = null;

	private ListView mLvLeft = null;
	private ListView mLvRight = null;

	private View mPopView = null;

	private int mCurrentIndexLeft = 0;
	private int mCurrentIndexRight = 0;

	private int mBackgroundNormal = 0;
	private int mBackgroundSelect = 0;
	private int mTextColorNormal = 0;
	private int mTextColorSelect = 0;

	private PopupWindow mPopCategory = null;

	private SD2LvCategoryViewListener mListener = null;

	private int mOldLeftIndex = 0;

	/** 是否点击左边直接消失popwindow */
	private boolean mIsNeedNotifyDirect = true;

	/** 当子分类的数量小于等于这个值的时候点击左边直接消失popwindow */
	private int mNotifyDirectRightCount = 1;

	public int getmTextColorNormal()
	{
		return mTextColorNormal;
	}

	public void setmTextColorNormal(int mTextColorNormal)
	{
		this.mTextColorNormal = mTextColorNormal;
	}

	public int getmTextColorSelect()
	{
		return mTextColorSelect;
	}

	public void setmTextColorSelect(int mTextColorSelect)
	{
		this.mTextColorSelect = mTextColorSelect;
	}

	public int getmBackgroundNormal()
	{
		return mBackgroundNormal;
	}

	public void setmBackgroundNormal(int mBackgroundNormal)
	{
		this.mBackgroundNormal = mBackgroundNormal;
	}

	public int getmBackgroundSelect()
	{
		return mBackgroundSelect;
	}

	public void setmBackgroundSelect(int mBackgroundSelect)
	{
		this.mBackgroundSelect = mBackgroundSelect;
	}

	public void setmListener(SD2LvCategoryViewListener mListener)
	{
		this.mListener = mListener;
	}

	public void setTitle(String title)
	{
//		if (title != null)
//		{
//			this.mTvTitle.setText(title);
//		}
	}

	public String getTitle()
	{
		return mTvTitle.getText().toString();
	}

	public smSD2LvCategoryView(Context context)
	{
		this(context, null);
	}

	public smSD2LvCategoryView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.view_category_tab, this, true);
		mTvTitle = (TextView) this.findViewById(R.id.view_category_tab_tv_title);
		mIvLeft = (ImageView) this.findViewById(R.id.view_category_tab_iv_left);
		mIvRight = (ImageView) this.findViewById(R.id.view_category_tab_iv_right);

		mPopView = LayoutInflater.from(getContext()).inflate(R.layout.pop_category_two_lv, null);
		mLvLeft = (ListView) mPopView.findViewById(R.id.pop_category_two_lv_lv_left);
		mLvRight = (ListView) mPopView.findViewById(R.id.pop_category_two_lv_lv_right);
		setGravity(Gravity.CENTER_VERTICAL);
		ensureAdapterNotNull();
	}

	private OnItemClickListener mLeftListViewItemClickListener = new OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			mAdapterHelperLeft.setPositionSelectState(mCurrentIndexLeft, false, false);
			mAdapterHelperLeft.setPositionSelectState((int) id, true, true);
			mCurrentIndexLeft = (int) id;

			Object rightListModel = mAdapterHelperLeft.getRightListModelFromPosition_left((int) id);
			mAdapterHelperRight.updateRightListModel_right(rightListModel);

			boolean isNotifyDirect = false;
			if (mIsNeedNotifyDirect)
			{
				int rightCount = mAdapterHelperRight.getItemCount();
				if (rightCount <= mNotifyDirectRightCount) // 点击左边直接消失popWindow
				{
					isNotifyDirect = true;
					String title = mAdapterHelperLeft.getTitleNameFromPosition(mCurrentIndexLeft);
					// ===================让右边第一项直接选中(考虑到有的用户数据右边没有子分类的时候右边只有一项"全部")，如果不需要，删除这段代码==============
					if (mAdapterHelperRight.getItemCount() > 0)
					{
						mAdapterHelperRight.setPositionSelectState(0, true, true);
					}
					// ================================================================================================
					removeOldRightSelect();
					disMissPopWindow(title);
				}
			}

			if (mListener != null)
			{
				Object leftModel = mAdapterHelperLeft.getSelectModelFromPosition((int) id);
				mListener.onLeftItemSelect(mCurrentIndexLeft, leftModel, isNotifyDirect);
			}
		}
	};

	private OnItemClickListener mRightListViewItemClickListener = new OnItemClickListener()
	{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			removeOldRightSelect();
			mAdapterHelperRight.setPositionSelectState((int) id, true, true);
			mCurrentIndexRight = (int) id;
			mOldLeftIndex = mCurrentIndexLeft;

			// ================================================================
			String title = null;
			if (mCurrentIndexRight <= (mNotifyDirectRightCount - 1))
			{
				if (mIsNeedNotifyDirect)
				{
					title = mAdapterHelperLeft.getTitleNameFromPosition(mCurrentIndexLeft);
				} else
				{
					title = mAdapterHelperRight.getTitleNameFromPosition(mCurrentIndexRight);
				}
			} else
			{
				title = mAdapterHelperRight.getTitleNameFromPosition(mCurrentIndexRight);
			}
			// ================================================================

			disMissPopWindow(title);

			if (mListener != null)
			{
				Object leftModel = mAdapterHelperLeft.getSelectModelFromPosition(mCurrentIndexLeft);
				Object rightModel = mAdapterHelperRight.getSelectModelFromPosition(mCurrentIndexRight);
				mListener.onRightItemSelect(mCurrentIndexLeft, mCurrentIndexRight, leftModel, rightModel);
			}
		}

	};

	private void removeOldRightSelect()
	{
		mAdapterHelperLeft.setPositionSelectState_left(mOldLeftIndex, mCurrentIndexRight, false);
		mAdapterHelperLeft.setPositionSelectState_left(mCurrentIndexLeft, mCurrentIndexRight, false);
	}

	public void setLeftAdapter(smSD2LvCategoryViewAdapterInterface adapterLeft)
	{
		this.mAdapterHelperLeft = adapterLeft;
		ensureAdapterNotNull();
		mLvLeft.setAdapter(mAdapterHelperLeft.getAdapter());
		mLvLeft.setOnItemClickListener(mLeftListViewItemClickListener);
	}

	public void setRightAdapter(smSD2LvCategoryViewAdapterInterface adapter)
	{
		this.mAdapterHelperRight = adapter;
		ensureAdapterNotNull();
		mLvRight.setAdapter(mAdapterHelperRight.getAdapter());
		mLvRight.setOnItemClickListener(mRightListViewItemClickListener);
	}

	public void setAdapterFinish()
	{
		//initTitle();
	}

	private void ensureAdapterNotNull()
	{
		if (mAdapterHelperLeft == null)
		{
			mAdapterHelperLeft = getEmptyAdapter();
		}

		if (mAdapterHelperRight == null)
		{
			mAdapterHelperRight = getEmptyAdapter();
		}

	}

	private smSD2LvCategoryViewAdapterInterface getEmptyAdapter()
	{
		return new smSD2LvCategoryViewAdapterInterface()
		{

			@Override
			public void setPositionSelectState(int position, boolean select, boolean notify)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public String getTitleNameFromPosition(int position)
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTitleIndex()
			{
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public Object getSelectModelFromPosition(int position)
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public BaseAdapter getAdapter()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void updateRightListModel_right(Object rightListModel)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void setPositionSelectState_left(int positionLeft, int positionRight, boolean select)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public Object getRightListModelFromPosition_left(int position)
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getItemCount()
			{
				// TODO Auto-generated method stub
				return 0;
			}
		};
	}

	private void initTitle()
	{
//		int titleIndex = mAdapterHelperLeft.getTitleIndex();
//		if (titleIndex >= 0)
//		{
//			mCurrentIndexLeft = titleIndex;
//		} else
//		{
//			mCurrentIndexLeft = DEFAULT_INDEX;
//		}
//
//		setTitle(mAdapterHelperLeft.getTitleNameFromPosition(mCurrentIndexLeft));
//		mAdapterHelperLeft.setPositionSelectState(mCurrentIndexLeft, true, true);
//
//		Object rightListModel = mAdapterHelperLeft.getRightListModelFromPosition_left(mCurrentIndexLeft);
//		mAdapterHelperRight.updateRightListModel_right(rightListModel);
//
//		if (mAdapterHelperRight.getItemCount() > 0)
//		{
//			mAdapterHelperRight.setPositionSelectState(0, true, true);
//		}
//
//		mLvLeft.setSelection(mCurrentIndexLeft);
	}

	public void showPopWindow()
	{
		if (mAdapterHelperLeft.getItemCount() > 0)
		{
			mPopCategory = new PopupWindow(mPopView, SDViewUtil.getScreenWidth(), SDViewUtil.getScreenHeight() / 2,
					true);
			mPopCategory.setBackgroundDrawable(new ColorDrawable());
			mPopCategory.setOutsideTouchable(true);
			mPopCategory.setFocusable(true);
			mPopCategory.setOnDismissListener(new PopDismissListener());
			mPopCategory.showAsDropDown(this, 0, 0);
		}
	}

	public void disMissPopWindow(String title)
	{
		if (mPopCategory != null)
		{
			mPopCategory.dismiss();
		}
		if (title != null)
		{
			setTitle(title);
		}
	}

	@Override
	public void setSelectedState(boolean select)
	{
		if (select)
		{
			if (mBackgroundSelect != 0)
			{
				//setBackgroundResource(mBackgroundSelect);
			}
			if (mTextColorSelect != 0)
			{
				//mTvTitle.setTextColor(mTextColorSelect);
			}
			showPopWindow();
		} else
		{
			if (mBackgroundNormal != 0)
			{
				//setBackgroundResource(mBackgroundNormal);
			}
			if (mTextColorNormal != 0)
			{
				//mTvTitle.setTextColor(mTextColorNormal);
			}
			if (mPopCategory != null)
			{
				mPopCategory.dismiss();
			}
		}
		mSelected = select;
	}

	class PopDismissListener implements OnDismissListener
	{
		@Override
		public void onDismiss()
		{
			setSelectedState(false);
		}
	}

	@Override
	public boolean getSelectedState()
	{
		return mSelected;
	}

	public interface SD2LvCategoryViewListener
	{
		public void onRightItemSelect(int leftIndex, int rightIndex, Object leftModel, Object rightModel);

		public void onLeftItemSelect(int leftIndex, Object leftModel, boolean isNotifyDirect);
	}

	public interface smSD2LvCategoryViewAdapterInterface extends SDLvCategoryViewAdapterInterface
	{
		public Object getRightListModelFromPosition_left(int position);

		public void updateRightListModel_right(Object rightListModel);

		public void setPositionSelectState_left(int positionLeft, int positionRight, boolean select);
	}

}
