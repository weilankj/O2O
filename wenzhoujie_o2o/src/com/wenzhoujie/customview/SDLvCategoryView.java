package com.wenzhoujie.customview;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;

public class SDLvCategoryView extends SDBottomNavigatorBaseItem
{

	private static final int DEFAULT_INDEX = 0;

	private ImageView mIvLeft = null;
	private ImageView mIvRight = null;
	private TextView mTvTitle = null;

	private ListView mLvContent = null;
	private SDLvCategoryViewAdapterInterface mAdapterHelper = null;

	private View mPopView = null;

	private int mBackgroundNormal = 0;
	private int mBackgroundSelect = 0;
	private int mTextColorNormal = 0;
	private int mTextColorSelect = 0;

	private PopupWindow mPwCategory = null;

	private int mCurrentIndex = DEFAULT_INDEX;

	private SDLvCategoryViewListener mListener = null;

	public SDLvCategoryViewListener getmListener()
	{
		return mListener;
	}

	public void setmListener(SDLvCategoryViewListener mListener)
	{
		this.mListener = mListener;
	}

	public int getmTextColorNormal()
	{
		return mTextColorNormal;
	}

	public void setmTextColorNormal(int mTextColorNormal)
	{
		this.mTextColorNormal = mTextColorNormal;
	}

	public int getmTextColorSelect()
	{
		return mTextColorSelect;
	}

	public void setmTextColorSelect(int mTextColorSelect)
	{
		this.mTextColorSelect = mTextColorSelect;
	}

	public int getmBackgroundNormal()
	{
		return mBackgroundNormal;
	}

	public void setmBackgroundNormal(int mBackgroundNormal)
	{
		this.mBackgroundNormal = mBackgroundNormal;
	}

	public int getmBackgroundSelect()
	{
		return mBackgroundSelect;
	}

	public void setmBackgroundSelect(int mBackgroundSelect)
	{
		this.mBackgroundSelect = mBackgroundSelect;
	}

	public void setTitle(String title)
	{
		if (title != null)
		{
			this.mTvTitle.setText(title);
		}
	}

	public SDLvCategoryView(Context context)
	{
		this(context, null);
	}

	public SDLvCategoryView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.view_category_tab, this, true);
		mTvTitle = (TextView) this.findViewById(R.id.view_category_tab_tv_title);
		mIvLeft = (ImageView) this.findViewById(R.id.view_category_tab_iv_left);
		mIvRight = (ImageView) this.findViewById(R.id.view_category_tab_iv_right);

		mPopView = LayoutInflater.from(getContext()).inflate(R.layout.pop_category_single_lv, null);
		mLvContent = (ListView) mPopView.findViewById(R.id.pop_category_single_lv_lv);

		ensureAdapterNotNull();
	}

	public void setAdapter(SDLvCategoryViewAdapterInterface adapter)
	{
		this.mAdapterHelper = adapter;
		ensureAdapterNotNull();
		initTitle();
		mLvContent.setOnItemClickListener(new ListViewItemClickListener());
		mLvContent.setAdapter(mAdapterHelper.getAdapter());
	}

	private void initTitle()
	{
		int titleIndex = mAdapterHelper.getTitleIndex();
		if (titleIndex >= 0)
		{
			mCurrentIndex = titleIndex;
		} else
		{
			mCurrentIndex = DEFAULT_INDEX;
		}
		mAdapterHelper.setPositionSelectState(mCurrentIndex, true, false);
		setTitle(mAdapterHelper.getTitleNameFromPosition(mCurrentIndex));
	}

	private void ensureAdapterNotNull()
	{
		if (mAdapterHelper == null)
		{
			mAdapterHelper = getEmptyAdapter();
		}
	}

	private SDLvCategoryViewAdapterInterface getEmptyAdapter()
	{
		return new SDLvCategoryViewAdapterInterface()
		{

			@Override
			public void setPositionSelectState(int position, boolean select, boolean notify)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public String getTitleNameFromPosition(int position)
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTitleIndex()
			{
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public Object getSelectModelFromPosition(int position)
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getItemCount()
			{
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public BaseAdapter getAdapter()
			{
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

	class ListViewItemClickListener implements OnItemClickListener
	{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			mAdapterHelper.setPositionSelectState(mCurrentIndex, false, false);
			mAdapterHelper.setPositionSelectState((int) id, true, true);
			mCurrentIndex = (int) id;

			setTitle(mAdapterHelper.getTitleNameFromPosition(mCurrentIndex));

			if (mPwCategory != null)
			{
				mPwCategory.dismiss();
			}
			if (mListener != null)
			{
				mListener.onItemSelect(mCurrentIndex, mAdapterHelper.getSelectModelFromPosition(mCurrentIndex));
			}
		}
	}

	@Override
	public void setSelectedState(boolean select)
	{
		if (select)
		{
			if (mBackgroundSelect != 0)
			{
				setBackgroundResource(mBackgroundSelect);
			}
			if (mTextColorSelect != 0)
			{
				mTvTitle.setTextColor(mTextColorSelect);
			}
			if (mAdapterHelper.getItemCount() > 0)
			{
				mPwCategory = new PopupWindow(mPopView, SDViewUtil.getScreenWidth(), SDViewUtil.getScreenHeight() / 2, true);
				mPwCategory.setBackgroundDrawable(new ColorDrawable());
				mPwCategory.setOutsideTouchable(true);
				mPwCategory.setFocusable(true);
				mPwCategory.setOnDismissListener(new PopDismissListener());
				mPwCategory.showAsDropDown(this, 0, 0);
			}
		} else
		{
			if (mBackgroundNormal != 0)
			{
				setBackgroundResource(mBackgroundNormal);
			}
			if (mTextColorNormal != 0)
			{
				mTvTitle.setTextColor(mTextColorNormal);
			}
			if (mPwCategory != null)
			{
				mPwCategory.dismiss();
			}
		}
		mSelected = select;
	}

	class PopDismissListener implements OnDismissListener
	{
		@Override
		public void onDismiss()
		{
			setSelectedState(false);
		}

	}

	@Override
	public boolean getSelectedState()
	{
		return mSelected;
	}

	public interface SDLvCategoryViewListener
	{
		public void onItemSelect(int index, Object model);
	}

	public interface SDLvCategoryViewAdapterInterface
	{

		public void setPositionSelectState(int position, boolean select, boolean notify);

		public String getTitleNameFromPosition(int position);

		public BaseAdapter getAdapter();

		public Object getSelectModelFromPosition(int position);

		public int getTitleIndex();

		public int getItemCount();
	}

}
