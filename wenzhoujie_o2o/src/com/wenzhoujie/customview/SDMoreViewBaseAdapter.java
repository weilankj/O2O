package com.wenzhoujie.customview;

import android.view.View;

public abstract class SDMoreViewBaseAdapter
{

	public abstract int getMaxShowCount();

	public abstract int getListCount();

	public abstract String getClickMoreString();

	public abstract String getClickBackString();

	public abstract View getView(int pos);

	public abstract boolean clickMore(View v);
}
