package com.wenzhoujie.dao;

import java.util.List;

import com.lidroid.xutils.db.sqlite.WhereBuilder;
import com.wenzhoujie.common.DbManagerX;
import com.wenzhoujie.constant.Constant.LoadImageType;
import com.wenzhoujie.model.SettingModel;

public class SettingModelDao
{
	private static SettingModelDao mInstance = null;

	private SettingModelDao()
	{
	}

	public static SettingModelDao getInstance()
	{
		if (mInstance == null)
		{
			mInstance = new SettingModelDao();
		}
		return mInstance;
	}

	public boolean insertOrCreateModel(SettingModel model)
	{
		if (model != null)
		{
			if (hasSettingModel())
			{
				return true;
			} else
			{
				try
				{
					DbManagerX.getDbUtils().save(model);
					return true;
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	public static SettingModel queryModel()
	{
		try
		{
			List<SettingModel> listModel = DbManagerX.getDbUtils().findAll(SettingModel.class);
			if (listModel != null && listModel.size() == 1)
			{
				return listModel.get(0);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static int getLoadImageType()
	{
		SettingModel model = queryModel();
		if (model != null)
		{
			return model.getLoadImageType();
		} else
		{
			return LoadImageType.ALL;
		}
	}

	public int canPushMessage()
	{
		SettingModel model = queryModel();
		if (model != null)
		{
			return model.getCanPushMessage();
		} else
		{
			return 1;
		}
	}

	public boolean hasSettingModel()
	{
		SettingModel model = queryModel();
		if (model != null)
		{
			return true;
		} else
		{
			return false;
		}
	}

	public boolean updateLoadImageType(int loadImageType)
	{
		SettingModel model = queryModel();
		if (model != null)
		{
			try
			{
				model.setLoadImageType(loadImageType);
				DbManagerX.getDbUtils().update(model, WhereBuilder.b("_id", "=", model.get_id()));
				return true;
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean updateCanPushMessage(int canPushMessage)
	{
		SettingModel model = queryModel();
		if (model != null)
		{
			try
			{
				model.setCanPushMessage(canPushMessage);
				DbManagerX.getDbUtils().update(model, WhereBuilder.b("_id", "=", model.get_id()));
				return true;
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}

}
