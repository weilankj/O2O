package com.wenzhoujie.dao;

import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.wenzhoujie.app.App;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.model.JsonDbModel;
import com.wenzhoujie.model.Region_confModel;
import com.wenzhoujie.model.SettingModel;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbHelper extends OrmLiteSqliteOpenHelper
{

	private static final String DATABASE_NAME = "wzj.db";
	private static final int DATABASE_VERSION = 1;

	private static DbHelper mInstance = null;

	public static DbHelper getInstance()
	{
		//syncInit();
		if (mInstance == null)
		{//System.out.println("mInstance == null======================");
			syncInit();
		}
		//System.out.println("mInstance != null======================"+mInstance);
		return mInstance;
	}

	private synchronized static void syncInit()
	{
		if (mInstance == null)
		{
			mInstance = new DbHelper();
			
		}
	}

	private DbHelper()
	{
		super(App.getApplication(), DATABASE_NAME, null, DATABASE_VERSION);
		System.out.println("建立DbHelper的数据库======================");
	}

	@Override
	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource)
	{	System.out.println("建立DbHelper表格======================");
		//SDToast.showToast("建立DbHelper======================");
		try
		{
			//TableUtils.createTable(connectionSource, JsonDbModel.class);
			TableUtils.createTable(connectionSource, Region_confModel.class);
			//TableUtils.createTable(connectionSource, SettingModel.class);
			System.out.println("建立数据表格完成======================");
			//SDToast.showToast("建立数据库======================");
			
		} catch (Exception e)
		{System.out.println("建立数据表格失败======================");
		//SDToast.showToast("建立数据库失败======================");
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion)
	{

	}

	@Override
	public <D extends Dao<T, ?>, T> D getDao(Class<T> clazz)
	{
		try
		{
			return super.getDao(clazz);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
