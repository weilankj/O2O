package com.wenzhoujie.listener;

import com.wenzhoujie.model.act.UActModel;

public interface GetUserShareByUidListener
{
	public void onStart();

	public void onSuccess(UActModel model);

	public void onFailure();

	public void onFinish();

	public void onUnLogin();

	public void onUidIsEmpty();
}
