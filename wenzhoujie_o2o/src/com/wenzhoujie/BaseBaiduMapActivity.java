package com.wenzhoujie;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ZoomControls;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMapClickListener;
import com.baidu.mapapi.map.BaiduMap.OnMapLoadedCallback;
import com.baidu.mapapi.map.BaiduMap.OnMapStatusChangeListener;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.overlayutil.DrivingRouteOverlay;
import com.baidu.mapapi.overlayutil.TransitRouteOverlay;
import com.baidu.mapapi.overlayutil.WalkingRouteOverlay;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.TransitRouteLine;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteLine;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.wenzhoujie.baidumap.BaiduMapManager;

/**
 * 百度地图基类
 * 
 * @author js02
 * 
 */
public class BaseBaiduMapActivity extends BaseActivity implements OnMapStatusChangeListener, OnMapClickListener, OnMarkerClickListener, BDLocationListener,
		OnGetRoutePlanResultListener, OnMapLoadedCallback
{

	public MapView mMapView = null;
	public BaiduMap mBaiduMap = null;
	public InfoWindow mInfoWindow = null;

	public LatLng mLlTopLeft = null;
	public LatLng mLlBottomRight = null;
	public LatLng mLlCurrent = null;

	/** 是否正在定位中 */
	private boolean mIsInLocation = false;
	/** 是否需要移动地图到当前位置 */
	private boolean mIsNeedAnimateMap = true;
	/** 是否需要一直定位 */
	private boolean mIsLocationAllTheTime = false;

	public boolean ismIsInLocation()
	{
		return mIsInLocation;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedSlideFinishLayout = false;
		super.onCreate(savedInstanceState);
		initBaseBaiduMap();
	}

	@Override
	public void setContentView(int resViewId)
	{
		View viewFinal = null;
		View resView = null;
		if (resViewId == 0)
		{

		} else
		{
			resView = getLayoutInflater().inflate(resViewId, null);
		}

		FrameLayout flLayout = new FrameLayout(getApplicationContext());
		flLayout.addView(mMapView);

		if (resView != null)
		{
			flLayout.addView(resView);
		}

		viewFinal = flLayout;

		super.setContentView(viewFinal);
	}

	private void initBaseBaiduMap()
	{
		initBaiduMap();
	}

	private void initBaiduMap()
	{
		mMapView = new MapView(this);
		mBaiduMap = mMapView.getMap();

		mBaiduMap.setOnMapLoadedCallback(this);
	}

	public void zoomMapTo(float zoom)
	{
		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(zoom);
		mBaiduMap.setMapStatus(msu);
	}

	public void showZoomView()
	{
		View zoom = getZoomControls();
		if (zoom != null)
		{
			zoom.setVisibility(View.VISIBLE);
		}
	}

	public void hideZoomView()
	{
		View zoom = getZoomControls();
		if (zoom != null)
		{
			zoom.setVisibility(View.GONE);
		}
	}

	public ZoomControls getZoomControls()
	{
		int childCount = mMapView.getChildCount();
		View zoom = null;
		for (int i = 0; i < childCount; i++)
		{
			zoom = mMapView.getChildAt(i);
			if (zoom instanceof ZoomControls)
			{
				return (ZoomControls) zoom;
			}
		}
		return null;
	}

	public BitmapDescriptor getMarkerFromAsset(String path)
	{
		BitmapDescriptor bmp = BitmapDescriptorFactory.fromAsset(path);
		return bmp;
	}

	public BitmapDescriptor getMarkerFromBitmap(Bitmap bitmap)
	{
		BitmapDescriptor bmp = BitmapDescriptorFactory.fromBitmap(bitmap);
		return bmp;
	}

	public BitmapDescriptor getMarkerFromFile(String path)
	{
		BitmapDescriptor bmp = BitmapDescriptorFactory.fromFile(path);
		return bmp;
	}

	public BitmapDescriptor getMarkerfromPath(String path)
	{
		BitmapDescriptor bmp = BitmapDescriptorFactory.fromPath(path);
		return bmp;
	}

	public BitmapDescriptor getMarkerFromResource(int resId)
	{
		BitmapDescriptor bmp = BitmapDescriptorFactory.fromResource(resId);
		return bmp;
	}

	public BitmapDescriptor getMarkerFromResource(View view)
	{
		BitmapDescriptor bmp = BitmapDescriptorFactory.fromView(view);
		return bmp;
	}

	public Overlay addMarkerToMap(LatLng ll, BitmapDescriptor bmp)
	{
		return addMarkerToMap(ll, bmp, null, 0);
	}

	public Overlay addMarkerToMap(LatLng ll, BitmapDescriptor bmp, Bundle data)
	{
		return addMarkerToMap(ll, bmp, data, 0);
	}

	public Overlay addMarkerToMap(LatLng ll, BitmapDescriptor bmp, Bundle data, int zIndex)
	{
		MarkerOptions markerOption = new MarkerOptions().position(ll).icon(bmp).zIndex(zIndex);
		Overlay marker = mBaiduMap.addOverlay(markerOption);
		if (data != null)
		{
			marker.setExtraInfo(data);
		}
		return marker;
	}

	public void openMyLocationOverlay()
	{
		mBaiduMap.setMyLocationEnabled(true);
	}

	public void closeMyLocationOverlay()
	{
		mBaiduMap.setMyLocationEnabled(false);
	}

	public void focusMapTo(LatLng ll, boolean isNeedAnimateMap)
	{
		if (ll != null)
		{
			MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
			mBaiduMap.animateMapStatus(u);
		}
	}

	public void addRouteOverlayBus(TransitRouteLine line)
	{
		if (line != null)
		{
			TransitRouteOverlay overlay = new TransitRouteOverlay(mBaiduMap)
			{

				@Override
				public boolean onRouteNodeClick(int index)
				{
					return onBusRouteNodeClick(index);
				}
			};
			mBaiduMap.setOnMarkerClickListener(overlay);
			overlay.setData(line);
			overlay.addToMap();
		}
	}

	public boolean onBusRouteNodeClick(int index)
	{
		return false;
	}

	public void addRouteOverlayDriving(DrivingRouteLine line)
	{
		if (line != null)
		{
			DrivingRouteOverlay overlay = new DrivingRouteOverlay(mBaiduMap)
			{

				@Override
				public boolean onRouteNodeClick(int index)
				{
					return onDrivingRouteNodeClick(index);
				}

			};
			mBaiduMap.setOnMarkerClickListener(overlay);
			overlay.setData(line);
			overlay.addToMap();
		}
	}

	public boolean onDrivingRouteNodeClick(int index)
	{
		return false;
	}

	public void addRouteOverlayWalking(WalkingRouteLine line)
	{
		if (line != null)
		{
			WalkingRouteOverlay overlay = new WalkingRouteOverlay(mBaiduMap)
			{

				@Override
				public boolean onRouteNodeClick(int index)
				{
					return onWalkingRouteNodeClick(index);
				}

			};
			mBaiduMap.setOnMarkerClickListener(overlay);
			overlay.setData(line);
			overlay.addToMap();
		}
	}

	public boolean onWalkingRouteNodeClick(int index)
	{
		return false;
	}

	/**
	 * 定位
	 * 
	 * @param isNeedAnimateMap
	 *            是否需要移动地图到当前位置
	 */
	public void startLocation(boolean isNeedAnimateMap)
	{
		startLocation(isNeedAnimateMap, mIsLocationAllTheTime);
	}

	public void startLocation(boolean isNeedAnimateMap, boolean isLocationAllTheTime)
	{
		if (mIsInLocation && !mIsLocationAllTheTime)
		{
			return;
		}
		SDToast.showToast("定位中...");
		mIsInLocation = true;
		this.mIsNeedAnimateMap = isNeedAnimateMap;
		this.mIsLocationAllTheTime = isLocationAllTheTime;
		BaiduMapManager.getInstance().startLocation(this); // 定位监听
	}

	public void stopLocation()
	{
		BaiduMapManager.getInstance().destoryLocation();
		mIsInLocation = false;
		mIsLocationAllTheTime = false;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		mMapView.onResume();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		mMapView.onPause();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		stopLocation();
		// 关闭定位图层
		closeMyLocationOverlay();
		mMapView.onDestroy();
		mMapView = null;
	}

	public void getScreenLatLng()
	{
		mLlTopLeft = mBaiduMap.getProjection().fromScreenLocation(new Point(0, 0));
		mLlBottomRight = mBaiduMap.getProjection().fromScreenLocation(new Point(SDViewUtil.getScreenWidth(), SDViewUtil.getScreenHeight()));
	}

	// =====================OnMapStatusChangeListener==========start================
	@Override
	public void onMapStatusChange(MapStatus mapStatus)
	{
	}

	@Override
	public void onMapStatusChangeFinish(MapStatus mapStatus)
	{
		getScreenLatLng();
	}

	@Override
	public void onMapStatusChangeStart(MapStatus mapStatus)
	{
	}

	// =====================OnMapStatusChangeListener==========end================

	// =====================OnMapClickListener========start=====================
	@Override
	public void onMapClick(LatLng ll)
	{

	}

	@Override
	public boolean onMapPoiClick(MapPoi mp)
	{
		return false;
	}

	// =====================OnMapClickListener========end=====================

	// =======================OnMarkerClickListener=========start=====================
	@Override
	public boolean onMarkerClick(Marker marker)
	{
		return false;
	}

	// =======================OnMarkerClickListener=========end=====================

	// ====================BDLocationListener========start
	@Override
	public void onReceiveLocation(BDLocation location)
	{
		// map view 销毁后不在处理新接收的位置
		if (location == null || mMapView == null)
		{
			stopLocation();
			return;
		}
		getScreenLatLng();
		// 此处设置开发者获取到的方向信息，顺时针0-360
		MyLocationData locData = new MyLocationData.Builder().accuracy(location.getRadius()).direction(100).latitude(location.getLatitude()).longitude(location.getLongitude())
				.build();
		mBaiduMap.setMyLocationData(locData);
		mLlCurrent = new LatLng(location.getLatitude(), location.getLongitude());
		if (mIsNeedAnimateMap)
		{
			mIsNeedAnimateMap = false;
			MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(mLlCurrent);
			mBaiduMap.animateMapStatus(u);
		}

		if (mIsLocationAllTheTime)
		{
			mIsInLocation = true;
		} else
		{
			stopLocation();
		}
	}

	@Override
	public void onReceivePoi(BDLocation location)
	{

	}

	// ====================BDLocationListener========end

	// =================OnGetRoutePlanResultListener===========start
	@Override
	public void onGetDrivingRouteResult(DrivingRouteResult result)
	{

	}

	@Override
	public void onGetTransitRouteResult(TransitRouteResult result)
	{

	}

	@Override
	public void onGetWalkingRouteResult(WalkingRouteResult result)
	{

	}

	// =================OnGetRoutePlanResultListener===========end

	// ==========================OnMapLoadedCallback=========start
	@Override
	public void onMapLoaded()
	{
		hideZoomView();
		zoomMapTo(15.0f);
		mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(LocationMode.NORMAL, true, null));
		openMyLocationOverlay();
		mBaiduMap.setOnMapStatusChangeListener(this); // 地图状态发生变化监听，如拖动地图
		mBaiduMap.setOnMapClickListener(this); // 地图被点击监听
		mBaiduMap.setOnMarkerClickListener(this); // 地图上面的marker标签被点击监听
	}
	// ==========================OnMapLoadedCallback=========end
}
