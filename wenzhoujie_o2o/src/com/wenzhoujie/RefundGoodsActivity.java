package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.adapter.OrderGoodsListAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.customview.SDStickyScrollView;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.customview.StickyScrollView;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.model.DealOrderItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.model.act.RefundOrdersActModel;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

/**
 * 商品申请退款
 * 
 * @author Administrator
 * 
 */
public class RefundGoodsActivity extends BaseActivity {

	public static final String EXTRA_ID = "extra_id";

	protected SDStickyScrollView mSsv_all;
	protected LinearLayout mLl_deals;
	protected LinearLayout mLl_coupon;
	protected EditText mEt_content;
	protected TextView mTv_submit;

	protected String mStrContent;

	protected List<DealOrderItemModel> mListModel = new ArrayList<DealOrderItemModel>();
	protected OrderGoodsListAdapter mAdapter;

	protected int mId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_refund_goods);
		init();
	}

	protected void init() {
		getIntentData();
		initTitle();
		findViews();
		bindDefaultData();
		initSDStickyScrollView();
		registerClick();
	}

	protected void initTitle() {
		mTitleSimple.setmListener(new SDTitleSimpleListener() {

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v) {
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v) {
				finish();
			}
		});
		mTitleSimple.setTitleTop("申请退款");
	}

	private void getIntentData() {
		mId = getIntent().getIntExtra(EXTRA_ID, 0);
		if (mId <= 0) {
			SDToast.showToast("id为空");
			finish();
		}
	}

	private void bindDefaultData() {
		mAdapter = new OrderGoodsListAdapter(mListModel, false,
				RefundGoodsActivity.this);
	}

	private void findViews() {
		mSsv_all = (SDStickyScrollView) findViewById(R.id.ssv_all);
		mLl_deals = (LinearLayout) findViewById(R.id.ll_deals);
		mLl_coupon = (LinearLayout) findViewById(R.id.ll_coupon);
		mEt_content = (EditText) findViewById(R.id.et_content);
		mTv_submit = (TextView) findViewById(R.id.tv_submit);
	}

	private void registerClick() {
		mTv_submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				clickSubmit();
			}
		});
	}

	protected boolean validateParams() {
		mStrContent = mEt_content.getText().toString();
		if (TextUtils.isEmpty(mStrContent)) {
			SDToast.showToast("请输入内容");
			return false;
		}
		return true;
	}

	protected void clickSubmit() {
		if (validateParams()) {
			requestSubmit();
		}

	}

	protected void requestSubmit() {
		RequestModel model = new RequestModel();
		model.putUser();
		model.putAct("order_do_refund");
		model.put("content", mStrContent);
		model.put("item_id", mId);

		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				BaseActModel actModel = JsonUtil.json2Object(
						responseInfo.result, BaseActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					int status = SDTypeParseUtil.getIntFromString(
							actModel.getStatus(), 0);
					if (status == 1) {
						SDEventManager.post(EnumEventTag.REFRESH_ORDER_LIST
								.ordinal());
						finish();
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	private void initSDStickyScrollView() {
		mSsv_all.setMode(Mode.PULL_FROM_START);
		mSsv_all.setOnRefreshListener(new OnRefreshListener2<StickyScrollView>() {

			@Override
			public void onPullDownToRefresh(
					PullToRefreshBase<StickyScrollView> refreshView) {
				requestData();
			}

			@Override
			public void onPullUpToRefresh(
					PullToRefreshBase<StickyScrollView> refreshView) {

			}
		});
		mSsv_all.setRefreshing();
	}

	protected void requestData() {
		RequestModel model = new RequestModel();
		model.putUser();
		model.putAct("order_refund");
		model.put("item_id", mId);

		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				RefundOrdersActModel actModel = JsonUtil.json2Object(
						responseInfo.result, RefundOrdersActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					mListModel = actModel.getListItem();
					mAdapter.setData(mListModel);
					bindData();
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
				mSsv_all.onRefreshComplete();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	protected void bindData() {
		if (mAdapter != null) {
			mLl_deals.removeAllViews();
			for (int i = 0; i < mAdapter.getCount(); i++) {
				View view = mAdapter.getView(i, null, null);
				mLl_deals.addView(view);
			}
		}
	}

}
