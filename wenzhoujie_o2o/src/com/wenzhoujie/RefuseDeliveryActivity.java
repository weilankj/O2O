package com.wenzhoujie;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.model.act.Uc_order_refundActModel;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

/**
 * 商品没收到货
 * 
 * @author Administrator
 * 
 */
public class RefuseDeliveryActivity extends RefundGoodsActivity
{

	@Override
	protected void init()
	{
		super.init();
		mEt_content.setHint("请输入详细原因");
	}

	@Override
	protected void requestData()
	{
		RequestModel model = new RequestModel();
		model.putUser();
		model.putAct("order_refuse_delivery");
		model.put("item_id", mId);

		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("请稍后...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				Uc_order_refundActModel actModel = JsonUtil.json2Object(
						responseInfo.result, Uc_order_refundActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					mListModel = actModel.getListItem();
					mAdapter.setData(mListModel);
					bindData();
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
				mSsv_all.onRefreshComplete();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
		
	}

	@Override
	protected void requestSubmit()
	{
		RequestModel model = new RequestModel();
		model.putUser();
		model.putAct("order_do_refuse_delivery");
		model.put("content", mStrContent);
		model.put("item_id", mId);

		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("正在删除...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				BaseActModel actModel = JsonUtil.json2Object(
						responseInfo.result, BaseActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					int status = SDTypeParseUtil.getIntFromString(
							actModel.getStatus(), 0);
					if (status == 1) {
						SDEventManager.post(EnumEventTag.REFRESH_ORDER_LIST.ordinal());
						finish();
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
				mSsv_all.onRefreshComplete();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	@Override
	protected void initTitle()
	{
		mTitleSimple.setTitleTop("没收到货");
	}

}
