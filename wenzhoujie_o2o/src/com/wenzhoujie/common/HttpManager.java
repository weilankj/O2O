package com.wenzhoujie.common;

import com.ta.SyncHttpClient;
import com.ta.util.http.AsyncHttpClient;

public class HttpManager
{
	private static final int TIME_OUT = 7 * 1000;
	private static boolean isInitAsyncHttpClient = false;
	private static boolean isInitSyncHttpClient = false;

	private static class Holder
	{
		private static AsyncHttpClient mAsyncHttpClient = new AsyncHttpClient();
		private static SyncHttpClient mSyncHttpClient = new SyncHttpClient();
	}

	private HttpManager()
	{
	}

	/**
	 * 得到全局异步请求对象
	 * 
	 * @return
	 */
	public static AsyncHttpClient getAsyncHttpClient()
	{
		if (!isInitAsyncHttpClient)
		{
			initAsyncHttpClient();
		}
		return Holder.mAsyncHttpClient;
	}

	/**
	 * 初始化异步请求对象
	 */
	private static void initAsyncHttpClient()
	{
		Holder.mAsyncHttpClient.setTimeout(TIME_OUT);
		isInitAsyncHttpClient = true;
	}

	/**
	 * 得到全局同步请求对象
	 * 
	 * @return
	 */
	public static AsyncHttpClient getSyncHttpClient()
	{
		if (!isInitSyncHttpClient)
		{
			initSyncHttpClient();
		}
		return Holder.mSyncHttpClient;
	}

	/**
	 * 初始化同步请求对象
	 */
	private static void initSyncHttpClient()
	{
		Holder.mSyncHttpClient.setTimeout(TIME_OUT);
		isInitSyncHttpClient = true;
	}

	/**
	 * 创建一个新的异步请求对象
	 * 
	 * @return
	 */
	public static AsyncHttpClient newAsyncHttpClient()
	{
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(TIME_OUT);
		return client;
	}

	/**
	 * 创建一个新的同步请求对象
	 * 
	 * @return
	 */
	public static SyncHttpClient newSyncHttpClient()
	{
		SyncHttpClient client = new SyncHttpClient();
		client.setTimeout(TIME_OUT);
		return client;
	}

}
