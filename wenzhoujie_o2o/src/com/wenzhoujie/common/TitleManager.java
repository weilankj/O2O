package com.wenzhoujie.common;

import com.wenzhoujie.library.title.SDTitleSimple;
import com.wenzhoujie.library.title.SDTitleTwoRightButton;
import com.wenzhoujie.app.App;

public class TitleManager
{

	public static SDTitleSimple newSDTitleSimple()
	{
		SDTitleSimple title = new SDTitleSimple(App.getApplication());
		return title;
	}

	public static SDTitleTwoRightButton newSDTitleTwoRightButton()
	{
		SDTitleTwoRightButton title = new SDTitleTwoRightButton(App.getApplication());
		return title;
	}

}
