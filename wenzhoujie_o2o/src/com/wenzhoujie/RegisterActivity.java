package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.RegisterActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class RegisterActivity extends BaseActivity implements OnClickListener
{

	public static final int RESULT_CODE_REGISTER_SUCCESS = 10;
	public static final String EXTRA_RESULT_USER_NAME = "extra_result_user_name";
	public static final String EXTRA_RESULT_PASSWORD = "extra_result_password";

	public static final String EXTRA_USER_NAME = "extra_user_name";
	public static final String EXTRA_NICK_NAME = "extra_nick_name";

	public static final String EXTRA_SINA_ID = "extra_sina_id";
	public static final String EXTRA_SINA_ACCESS_TOKEN = "extra_sina_access_token";

	public static final String EXTRA_QQ_ID = "extra_qq_id";
	public static final String EXTRA_QQ_ACCESS_TOKEN = "extra_qq_access_token";

	@ViewInject(id = R.id.act_register_et_email)
	private ClearEditText mEtEmail = null;

	@ViewInject(id = R.id.act_register_et_username)
	private ClearEditText mEtUsername = null;
	
	@ViewInject(id = R.id.act_register_et_nickname)
	private ClearEditText mEtNickname = null;

	@ViewInject(id = R.id.act_register_et_pwd)
	private ClearEditText mEtPwd = null;

	@ViewInject(id = R.id.act_register_et_pwd_confirm)
	private ClearEditText mEtPwdConfirm = null;

	@ViewInject(id = R.id.act_register_rg_gender)
	private RadioGroup mRgGender = null;

	@ViewInject(id = R.id.act_register_rb_boy)
	private RadioButton mRbBoy = null;

	@ViewInject(id = R.id.act_register_rb_girl)
	private RadioButton mRbGirl = null;

	@ViewInject(id = R.id.act_register_tv_register)
	private TextView mTvRegister = null;

	private String mStrEmail;
	private String mStrUsername;
	private String mStrNickname;
	private String mStrPwd;
	private String mStrPwdConfirm;
	private String mStrGender = "1";

	private String user_name = null;
	private String nick_name = null;

	private String sina_id = null;
	private String sina_access_token = null;

	private String openid = null;
	private String qq_access_token = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_register);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		initTitle();
		initRadioGroup();
		registeClick();

	}

	private void initIntentData()
	{
		Intent intent = this.getIntent();
		if (intent != null)
		{
			this.user_name = intent.getStringExtra(EXTRA_USER_NAME);
			if (!TextUtils.isEmpty(user_name))
			{
				mEtUsername.setText(user_name);
			}

			this.sina_id = intent.getStringExtra(EXTRA_SINA_ID);
			this.sina_access_token = intent.getStringExtra(EXTRA_SINA_ACCESS_TOKEN);

			this.openid = intent.getStringExtra(EXTRA_QQ_ID);
			this.qq_access_token = intent.getStringExtra(EXTRA_QQ_ACCESS_TOKEN);

		}
	}

	private void initRadioGroup()
	{
		mRgGender.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				switch (checkedId)
				{
				case 0:
					mStrGender = "1";
					break;
				case 1:
					mStrGender = "0";
					break;

				default:
					break;
				}
			}
		});
		mRgGender.check(R.id.act_register_rb_boy);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("注册");
	}

	private void registeClick()
	{
		mTvRegister.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_register_tv_register:
			clickRegister();
			break;

		default:
			break;
		}
	}

	private void clickRegister()
	{
		if (validateParam())
		{
			RequestModel model = new RequestModel();
			model.put("act", "register");
			model.put("email", mStrEmail);
			model.put("password", mStrPwd);
			model.put("user_name", mStrUsername);
			model.put("nick_name", mStrNickname);
			model.put("gender", mStrGender);
			if (openid != null)
			{
				model.put("openid", openid);
				model.put("access_token", qq_access_token);
			}
			if (sina_id != null)
			{
				model.put("sina_id", sina_id);
				model.put("access_token", sina_access_token);
			}
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					RegisterActModel actModel = JsonUtil.json2Object(responseInfo.result, RegisterActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							Intent data = new Intent();
							data.putExtra(EXTRA_RESULT_USER_NAME, actModel.getUser_name());
							data.putExtra(EXTRA_RESULT_PASSWORD, actModel.getUser_pwd());
							setResult(RESULT_CODE_REGISTER_SUCCESS, data);
							finish();
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}

	}

	private boolean validateParam()
	{
		mStrEmail = mEtEmail.getText().toString();
		if (TextUtils.isEmpty(mStrEmail))
		{
			SDToast.showToast("邮箱不能为空");
			return false;
		}
		mStrUsername = mEtUsername.getText().toString();
		if (TextUtils.isEmpty(mStrEmail))
		{
			SDToast.showToast("用户名不能为空");
			return false;
		}
		mStrNickname = mEtNickname.getText().toString();
		if (TextUtils.isEmpty(mStrEmail))
		{
			SDToast.showToast("昵称不能为空");
			return false;
		}
		mStrPwd = mEtPwd.getText().toString();
		if (TextUtils.isEmpty(mStrEmail))
		{
			SDToast.showToast("密码不能为空");
			return false;
		}
		mStrPwdConfirm = mEtPwdConfirm.getText().toString();
		if (TextUtils.isEmpty(mStrEmail))
		{
			SDToast.showToast("确认密码不能为空");
			return false;
		}

		if (!mStrPwd.equals(mStrPwdConfirm))
		{
			SDToast.showToast("两次密码不一致");
			return false;
		}

		return true;
	}

}