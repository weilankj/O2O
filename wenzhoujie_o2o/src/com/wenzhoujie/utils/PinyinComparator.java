package com.wenzhoujie.utils;

import java.util.Comparator;

import com.wenzhoujie.model.InitActCitylistModel;

public class PinyinComparator implements Comparator<InitActCitylistModel>
{

	public int compare(InitActCitylistModel o1, InitActCitylistModel o2)
	{
		if (o1.getSortLetters().equals("@") || o2.getSortLetters().equals("#"))
		{
			return -1;
		} else if (o1.getSortLetters().equals("#") || o2.getSortLetters().equals("@"))
		{
			return 1;
		} else
		{
			return o1.getSortLetters().compareTo(o2.getSortLetters());
		}
	}

}
