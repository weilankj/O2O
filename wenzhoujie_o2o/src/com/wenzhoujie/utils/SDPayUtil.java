package com.wenzhoujie.utils;

import android.text.TextUtils;

import com.wenzhoujie.constant.ApkConstant;

public class SDPayUtil
{

	/**
	 * 获得支付宝支付链接
	 * 
	 * @param order_id
	 * @param out_trade_no
	 * @return
	 */
	public static String getWalipayUrl(String order_id, String out_trade_no)
	{
		if (!TextUtils.isEmpty(order_id) && !TextUtils.isEmpty(out_trade_no))
		{
			String url = ApkConstant.SERVER_API_URL;
			return url.replace(ApkConstant.SERVER_API_URL_END, "alipay_web/alipayapi.php?order_id=" + order_id + "&out_trade_no=" + out_trade_no);
		} else
		{
			return null;
		}
	}

	/**
	 * 获得财付通支付链接
	 * 
	 * @param order_id
	 * @param out_trade_no
	 * @return
	 */
	public static String getWtenpayUrl(String order_id, String out_trade_no)
	{
		if (!TextUtils.isEmpty(order_id) && !TextUtils.isEmpty(out_trade_no))
		{
			String url = ApkConstant.SERVER_API_URL;
			return url.replace(ApkConstant.SERVER_API_URL_END, "wtenpay_web/wtenpayapi.php?order_id=" + order_id + "&out_trade_no=" + out_trade_no);
		} else
		{
			return null;
		}
	}

}
