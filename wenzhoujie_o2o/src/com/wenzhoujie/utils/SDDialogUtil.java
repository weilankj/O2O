package com.wenzhoujie.utils;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.wenzhoujie.library.common.SDActivityManager;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.customview.SDProgressDialog;

public class SDDialogUtil
{

	private static SDProgressDialog mProgressDialog = null;


	public static Dialog showLoading(String message)
	{
		return showLoading(message, false);
	}

	public static Dialog showLoading(String message, boolean cancelable)
	{
		return showLoading(message, cancelable, null);
	}

	public static Dialog showLoading(String message, boolean cancelable, OnDismissListener dismissListener)
	{
		dismissLoadingDialog();
		mProgressDialog = new SDProgressDialog(SDActivityManager.getInstance().getLastActivity(), R.style.dialogBase);
		TextView txt = mProgressDialog.getmTxtMsg();
		if (message != null && txt != null)
		{
			txt.setText(message);
		}
		mProgressDialog.setCancelable(cancelable);
		mProgressDialog.setOnDismissListener(dismissListener);
		mProgressDialog.show();
		return mProgressDialog;
	}

	public static void dismissLoadingDialog()
	{
		if (mProgressDialog != null && mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
	}

	// 弹出自定义的窗体
	public static Dialog showView(CharSequence title, View view, DialogInterface.OnClickListener confirmListener, DialogInterface.OnClickListener cancelListener)
	{
		AlertDialog.Builder builder = new Builder(App.getApplication());
		if (title != null)
		{
			builder.setTitle(title);
		}
		builder.setView(view);
		if (confirmListener != null)
		{
			builder.setPositiveButton("确定", confirmListener);
		}
		if (cancelListener != null)
		{
			builder.setNegativeButton("取消", cancelListener);
		}

		Dialog dialog = builder.create();
		dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		dialog.show();
		return dialog;
	}
}
