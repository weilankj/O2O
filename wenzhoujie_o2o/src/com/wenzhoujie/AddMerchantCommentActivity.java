package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDActivityUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 添加商家评论
 * 
 * @author js02
 * 
 */
public class AddMerchantCommentActivity extends BaseActivity implements OnClickListener
{
	/** 商家id */
	public static final String EXTRA_MERCHANT_ID = "extra_merchant_id";

	@ViewInject(id = R.id.act_tuan_add_deal_dp_rb_star)
	private RatingBar mRbStar = null;

	@ViewInject(id = R.id.act_tuan_add_deal_dp_tv_point)
	private TextView mTvPoint = null;

	@ViewInject(id = R.id.act_tuan_add_deal_dp_et_content)
	private EditText mEtContent = null;

	@ViewInject(id = R.id.act_tuan_add_deal_dp_btn_publish)
	private Button mBtnPublish = null;

	private float point = 5.0f;

	private String mStrMerchantId = null;

	private String mStrContent = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_add_merchant_comment);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{

		getIntentData();
		bindViewState();
		registeClick();
		initTitle();
	}

	private void bindViewState()
	{
		mRbStar.setRating(point);
		mTvPoint.setText(point + "分");
	}

	private void getIntentData()
	{
		Intent intent = getIntent();
		String strTuanId = intent.getStringExtra(EXTRA_MERCHANT_ID);
		if (strTuanId != null)
		{
			this.mStrMerchantId = strTuanId;
		}
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

		mTitleSimple.setTitleTop("我要评论");
	}

	private boolean validateParam()
	{
		mStrContent = mEtContent.getText().toString();
		if (TextUtils.isEmpty(mStrContent))
		{
			SDToast.showToast("评论内容不能为空");
			return false;
		}

		return true;
	}

	protected void requestComments()
	{
		if (!validateParam())
		{
			return;
		}
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "add_supplier_dp");
			model.put("content", mEtContent.getText().toString());
			model.put("point", point);
			model.put("id", mStrMerchantId);
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
						if (status == 1)
						{
							App.getApplication().mRuntimeConfig.setNeedRefreshMerchantDetail(true);
							Intent intent = new Intent(AddMerchantCommentActivity.this, MerchantCommentListActivity.class);
							intent.putExtra(MerchantCommentListActivity.EXTRA_MERCHANT_ID, mStrMerchantId);
							startActivity(intent);
							finish();
						} else if(status == 2)
						{
							SDToast.showToast("点评功能仅购买过的用户可以使用！");
						} else
						{
							SDToast.showToast("发表评论失败！");
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		} else
		{
			SDActivityUtil.startActivity(AddMerchantCommentActivity.this, LoginNewActivity.class);
		}

	}

	private void registeClick()
	{
		mBtnPublish.setOnClickListener(this);
		mRbStar.setOnRatingBarChangeListener(listener);
	}

	public RatingBar.OnRatingBarChangeListener listener = new OnRatingBarChangeListener()
	{

		@Override
		public void onRatingChanged(RatingBar view, float rating, boolean arg2)
		{
			if (rating <= 1)
			{
				rating = 1.0f;
			}
			point = rating;
			view.setRating(point);
			mTvPoint.setText(point + "分");
		}
	};

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_tuan_add_deal_dp_btn_publish:
			requestComments();
			break;

		default:
			break;
		}
	}

}