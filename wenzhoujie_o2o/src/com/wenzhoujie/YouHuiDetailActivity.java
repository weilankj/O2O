package com.wenzhoujie;

import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.event.EventTagJpush;
import com.wenzhoujie.fragment.BaseFragment;
import com.wenzhoujie.fragment.YouhuiDetailCommentFragment;
import com.wenzhoujie.fragment.YouhuiDetailDetailFragment;
import com.wenzhoujie.fragment.YouhuiDetailNoticeFragment;
import com.wenzhoujie.fragment.YouhuiDetailOtherMerchantFragment;
import com.wenzhoujie.fragment.YouhuiDetailTopFragment;
import com.wenzhoujie.fragment.YouhuiDetailTopFragment.YouhuiDetailTopFragmentListener;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.YouhuiitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;

public class YouHuiDetailActivity extends BaseActivity
{

	/** 优惠id */
	public static final String EXTRA_YOUHUI_ID = "extra_youhui_id";

	private YouhuiDetailTopFragment mFragTop = null;
	private YouhuiDetailOtherMerchantFragment mFragOtherMerchant = null;
	private YouhuiDetailDetailFragment mFragDetail = null;
	private YouhuiDetailNoticeFragment mFragNotice = null;
	private YouhuiDetailCommentFragment mFragComment = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();
	private String mStrYouHuiId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedSlideFinishLayout = false;
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_youhui_detail);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		initTitle();
		requestFavorableDetail("");
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("优惠券详情");
	}

	private void initIntentData()
	{
		Intent intent = getIntent();
		mStrYouHuiId = intent.getStringExtra(EXTRA_YOUHUI_ID);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		setIntent(intent);
		init();
		super.onNewIntent(intent);
	}

	/**
	 * 加载优惠明细
	 */
	private void requestFavorableDetail(String act2, final BaseFragment... fragments)
	{
		RequestModel model = new RequestModel();
		model.put("act", "youhuiitem");
		model.put("act_2", act2);
		model.put("email", mSearcher.getEmail());
		model.put("pwd", mSearcher.getPwd());
		model.put("id", mStrYouHuiId);
		model.put("m_latitude", mSearcher.getM_latitude());
		model.put("m_longitude", mSearcher.getM_longitude());
		//System.out.println("youhuiitem:" +"id:"+mStrYouHuiId);
		
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				YouhuiitemActModel model = JsonUtil.json2Object(responseInfo.result, YouhuiitemActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					if (model.getResponse_code() == 1)
					{
						addFragments(model, fragments);
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{

			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	/**
	 * 添加fragment
	 * 
	 * @param model
	 */
	protected void addFragments(YouhuiitemActModel model, BaseFragment... fragments)
	{
		if (model == null)
		{
			return;
		}

		List<BaseFragment> listFrag = null;
		if (fragments != null)
		{
			listFrag = Arrays.asList(fragments);
		}

		if (listFrag != null && listFrag.size() > 0)
		{
			if (listFrag.contains(mFragTop))
			{
				addTopFragment(model);
			}
		} else
		{
			addTopFragment(model);

			mFragOtherMerchant = new YouhuiDetailOtherMerchantFragment();
			mFragOtherMerchant.setmYouhuiitemActModel(model);
			addFragment(mFragOtherMerchant, R.id.fl_other_merchant);

			mFragDetail = new YouhuiDetailDetailFragment();
			mFragDetail.setmYouhuiitemActModel(model);
			addFragment(mFragDetail, R.id.fl_youhui_detail);

			mFragNotice = new YouhuiDetailNoticeFragment();
			mFragNotice.setmYouhuiitemActModel(model);
			addFragment(mFragNotice, R.id.fl_youhui_notice);

			mFragComment = new YouhuiDetailCommentFragment();
			mFragComment.setmYouhuiitemActModel(model);
			addFragment(mFragComment, R.id.fl_youhui_comments);
		}
	}

	private void addTopFragment(YouhuiitemActModel model)
	{
		mFragTop = new YouhuiDetailTopFragment();
		mFragTop.setmYouhuiitemActModel(model);
		mFragTop.setListener(new YouhuiDetailTopFragmentListener()
		{
			@Override
			public void onRefresh()
			{
				requestFavorableDetail("", mFragTop);
			}
		});
		addFragment(mFragTop, R.id.fl_top);
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		if (EventTagJpush.EVENT_FAVORABLEDETAIL_ID.equals(event.getEventTagString()))
		{
			finish();
		}
	}

}