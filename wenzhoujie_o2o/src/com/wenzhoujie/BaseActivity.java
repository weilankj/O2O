package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.RelativeLayout;

import cn.jpush.android.api.JPushInterface;

import com.wenzhoujie.library.common.SDActivityManager;
import com.wenzhoujie.library.title.SDTitleSimple;
import com.wenzhoujie.library.title.SDTitleTwoRightButton;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.sunday.eventbus.SDEventManager;
import com.sunday.eventbus.SDEventObserver;
import com.umeng.socialize.sso.UMSsoHandler;
import com.wenzhoujie.app.App;
import com.wenzhoujie.common.TitleManager;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.customview.SDSlidingFinishLayout;
import com.wenzhoujie.customview.SDSlidingFinishLayout.SDSlidingFinishLayoutListener;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.i.IBaseActivity;
import com.wenzhoujie.umeng.UmengSocialManager;
import com.wenzhoujie.utils.ActivityBlurer;

public class BaseActivity extends FragmentActivity implements IBaseActivity, SDEventObserver
{

	/** 是否是当作广告页面启动 (boolean) */
	public static final String EXTRA_IS_ADVS = "extra_is_advs";
	
	protected SDSlidingFinishLayout mSDFinishLayout = null;

	/** 是否需要侧滑关闭activity */
	protected boolean mIsNeedSlideFinishLayout = true;

	protected boolean mIsNeedAnimation = true;

	private TitleType mTitleType = TitleType.TITLE_NONE;
	protected SDTitleSimple mTitleSimple;
	protected SDTitleTwoRightButton mTitleTwoRightBtns;

	private ActivityBlurer mActivityBlurer;

	public TitleType getmTitleType()
	{
		return mTitleType;
	}

	public void setmTitleType(TitleType mTitleType)
	{
		this.mTitleType = mTitleType;
	}

	public SDSlidingFinishLayout getSDSlidingFinishLayout()
	{
		return mSDFinishLayout;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		if (mIsNeedAnimation)
		{
			overridePendingTransition(R.anim.anim_slide_in_from_right, R.anim.anim_slide_out_to_top);
		}
		super.onCreate(savedInstanceState);
		baseInit();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		if (outState != null)
		{
			outState.remove("android:support:fragments");
		}
	}

	@Override
	public void setContentView(View view)
	{
		View viewFinal = null;
		switch (getmTitleType())
		{
		case TITLE_SIMPLE:
			mTitleSimple = TitleManager.newSDTitleSimple();
			viewFinal = SDViewUtil.wrapperTitle(view, mTitleSimple);
			break;
		case TITLE_TWO_RIGHT_BUTTON:
			mTitleTwoRightBtns = TitleManager.newSDTitleTwoRightButton();
			viewFinal = SDViewUtil.wrapperTitle(view, mTitleTwoRightBtns);
			break;
		default:
			viewFinal = view;
			break;
		}

		if (mIsNeedSlideFinishLayout)
		{
			setSlidingFinishLayout(viewFinal);
		} else
		{
			super.setContentView(viewFinal);
		}
	}

	@Override
	public void setContentView(int resViewId)
	{
		View view = getLayoutInflater().inflate(resViewId, null);
		this.setContentView(view);
	}

	private void setSlidingFinishLayout(View view)
	{
		mSDFinishLayout = (SDSlidingFinishLayout) getLayoutInflater().inflate(R.layout.view_finish_wrapper, null);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
		mSDFinishLayout.addView(view, params);
		super.setContentView(mSDFinishLayout);
		mSDFinishLayout.setmListener(new SDSlidingFinishLayoutListener()
		{
			@Override
			public void onFinish()
			{
				finish();
			}

			@Override
			public void onScrolling()
			{
			}

			@Override
			public void onScrollToStart()
			{
			}
		});
	}

	@Override
	public void baseInit()
	{
		mActivityBlurer = new ActivityBlurer(this);
		baseInitIntentData();
		SDEventManager.register(this);
		SDActivityManager.getInstance().onCreate(this);
	}

	private void baseInitIntentData()
	{

	}

	@Override
	public void addFragment(Fragment fragment, int containerId)
	{
		getSupportFragmentManager().beginTransaction().add(containerId, fragment).commitAllowingStateLoss();
	}

	@Override
	public void replaceFragment(Fragment fragment, int containerId)
	{
		getSupportFragmentManager().beginTransaction().replace(containerId, fragment).commitAllowingStateLoss();
	}

	@Override
	public void showFragment(Fragment fragment)
	{
		getSupportFragmentManager().beginTransaction().show(fragment).commitAllowingStateLoss();
	}

	@Override
	public void hideFragment(Fragment fragment)
	{
		getSupportFragmentManager().beginTransaction().hide(fragment).commitAllowingStateLoss();
	}

	@Override
	protected void onDestroy()
	{
		SDEventManager.unregister(this);
		SDActivityManager.getInstance().onDestroy(this);
		super.onDestroy();
	}

	@Override
	protected void onPause()
	{
		mActivityBlurer.onPause();
		JPushInterface.onPause(this);
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		mActivityBlurer.onResume();
		SDActivityManager.getInstance().onResume(this);
		 JPushInterface.onResume(this);
		super.onResume();
	}

	@Override
	public void finish()
	{
		mActivityBlurer.finish();
		super.finish();
		if (mIsNeedAnimation)
		{
			overridePendingTransition(R.anim.anim_slide_in_from_right, R.anim.anim_slide_out_to_top);
		}
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case EXIT_APP:
			finish();
			break;
		case LOGOUT:
			if (!App.getApplication().mListClassNotFinishWhenLoginState0.contains(this.getClass()))
			{
				finish();
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onEvent(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventBackgroundThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventAsync(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		UMSsoHandler ssoHandler = UmengSocialManager.getUMLogin().getConfig().getSsoHandler(requestCode);
		if (ssoHandler != null)
		{
			ssoHandler.authorizeCallBack(requestCode, resultCode, data);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
