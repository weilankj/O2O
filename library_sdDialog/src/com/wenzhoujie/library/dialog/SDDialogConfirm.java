package com.wenzhoujie.library.dialog;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewUtil;

/**
 * 带标题，内容，确定按钮和取消按钮的窗口
 * 
 * @author js02
 * 
 */
public class SDDialogConfirm extends SDDialogCustom
{

	private TextView mTvContent;

	public SDDialogConfirm()
	{
		super();
	}

	public SDDialogConfirm(Context context)
	{
		super(context);
	}

	@Override
	protected void init()
	{
		super.init();
		addTextView();
	}

	public SDDialogConfirm setTextContent(String text)
	{
		if (TextUtils.isEmpty(text))
		{
			mTvContent.setVisibility(View.GONE);
		} else
		{
			mTvContent.setVisibility(View.VISIBLE);
			mTvContent.setText(text);
		}
		return this;
	}

	private void addTextView()
	{
		mTvContent = new TextView(getContext());
		mTvContent.setTextColor(Color.parseColor("#000000"));
		mTvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

		LinearLayout.LayoutParams params = SDViewUtil.getLayoutParamsLinearLayoutWW();
		int margin = SDViewUtil.dp2px(10);
		params.leftMargin = margin;
		params.rightMargin = margin;
		params.topMargin = margin;
		params.bottomMargin = margin;

		setCustomView(mTvContent, params);
	}

}
