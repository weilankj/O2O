package com.wenzhoujie.library.dialog;

import com.wenzhoujie.library.dialog.SDDialogProgress.SDDialogProgressListener;

public class SDDialogManager
{

	private static SDDialogProgress mProgress = null;

	public static SDDialogProgress showProgressDialog(String msg)
	{
		return showProgressDialog(msg, null);
	}

	public static SDDialogProgress showProgressDialog(SDDialogProgressListener listener)
	{
		return showProgressDialog(null, listener);
	}

	public static SDDialogProgress showProgressDialog(String msg, SDDialogProgressListener listener)
	{
		hideProgressDialog();
		mProgress = new SDDialogProgress();
		mProgress.setTextMsg(msg);
		mProgress.setmListener(listener);
		mProgress.show();
		return mProgress;
	}

	public static void hideProgressDialog()
	{
		if (mProgress != null)
		{
			mProgress.dismiss();
		}
	}

}
