package com.wenzhoujie.library.utils;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;

import com.wenzhoujie.library.SDLibrary;

/**
 * package包工具类
 * 
 * @author zhengjun
 * 
 */
public class SDPackageUtil
{

	public static void chmod(String permission, String path)
	{
		try
		{
			String command = "chmod " + permission + " " + path;
			Runtime runtime = Runtime.getRuntime();
			runtime.exec(command);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static Boolean isPackageExist(String packageName)
	{
		PackageManager manager = SDLibrary.getInstance().getApplication().getPackageManager();
		List<PackageInfo> pkgList = manager.getInstalledPackages(0);
		for (PackageInfo pi : pkgList)
		{
			if (pi.packageName.equalsIgnoreCase(packageName))
			{
				return true;
			}
		}
		return false;
	}

	public static PackageInfo getApkPackageInfo(String apkFilePath)
	{
		PackageManager pm = SDLibrary.getInstance().getApplication().getPackageManager();
		PackageInfo apkInfo = pm.getPackageArchiveInfo(apkFilePath, PackageManager.GET_META_DATA);
		return apkInfo;
	}

	public static PackageInfo getPackageInfo(String packageName)
	{
		PackageManager pm = SDLibrary.getInstance().getApplication().getPackageManager();
		PackageInfo apkInfo = null;
		try
		{
			apkInfo = pm.getPackageInfo(packageName, 0);
		} catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}
		return apkInfo;
	}

	public static PackageInfo getCurrentPackageInfo()
	{
		return getPackageInfo(SDLibrary.getInstance().getApplication().getPackageName());
	}

	public static Boolean installApkPackage(String apkFilePath)
	{
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setDataAndType(Uri.parse("file://" + apkFilePath), "application/vnd.android.package-archive");
		SDLibrary.getInstance().getApplication().startActivity(intent);
		return true;
	}

	public static Bundle getMetaData(Context context)
	{
		try
		{
			ApplicationInfo info = SDLibrary.getInstance().getApplication().getPackageManager()
					.getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
			return info.metaData;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static void startAPP(String appPackageName)
	{
		try
		{
			Intent intent = SDLibrary.getInstance().getApplication().getPackageManager().getLaunchIntentForPackage(appPackageName);
			SDLibrary.getInstance().getApplication().startActivity(intent);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void startCurrentApp()
	{
		startAPP(SDLibrary.getInstance().getApplication().getPackageName());
	}

}
