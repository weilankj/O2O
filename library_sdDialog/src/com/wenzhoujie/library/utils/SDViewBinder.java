package com.wenzhoujie.library.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageLoadingProgressListener;

public class SDViewBinder
{

	public static boolean mCanLoadImageFromUrl = true;

	public static void setBackgroundDrawable(View view, Drawable drawable)
	{
		view.setBackgroundDrawable(drawable);
	}

	public static void setRatingBar(RatingBar ratingBar, float rating)
	{
		ratingBar.setRating(rating);
	}

	public static void setTextView(TextView textView, String content, String emptyTip)
	{
		if (!TextUtils.isEmpty(content))
		{
			textView.setText(content);
		} else
		{
			textView.setText(emptyTip);
		}
	}

	public static void setTextView(TextView textView, String content)
	{
		setTextView(textView, content, "");
	}

	public static void setTextView(TextView textView, Spanned content)
	{
		if (!TextUtils.isEmpty(content))
		{
			textView.setText(content);
		} else
		{
			textView.setText("");
		}
	}

	public static void setTextViewsVisibility(TextView textView, String content)
	{
		if (TextUtils.isEmpty(content))
		{
			textView.setVisibility(View.GONE);
		} else
		{
			textView.setVisibility(View.VISIBLE);
			textView.setText(content);
		}
	}

	public static void setTextViewColorByColorId(TextView textView, int resId)
	{
		textView.setTextColor(SDResourcesUtil.getColor(resId));
	}

	public static boolean setViewsVisibility(View view, boolean visible)
	{
		int state = view.getVisibility();
		if (visible)
		{
			if (state != View.VISIBLE)
			{
				view.setVisibility(View.VISIBLE);
			}
			return true;
		} else
		{
			if (state != View.GONE)
			{
				view.setVisibility(View.GONE);
			}
			return false;
		}
	}

	public static boolean setViewsVisibility(View view, int visible)
	{
		if (visible == 0)
		{
			return setViewsVisibility(view, false);
		} else if (visible == 1)
		{
			return setViewsVisibility(view, true);
		}
		return false;
	}

	private static boolean canLoadImageFromUrl(String url)
	{
		if (!TextUtils.isEmpty(url))
		{
			if (mCanLoadImageFromUrl)// 可以从url加载图片
			{
				return true;
			} else
			{
				File cache = ImageLoader.getInstance().getDiscCache().get(url);
				if (cache != null && cache.exists() && cache.length() > 0)
				{
					return true;
				} else
				{
					return false;
				}
			}
		}
		return false;
	}

	public static void setImageView(ImageView imageView, String url)
	{
		setImageView(url, imageView, ImageLoader.getInstance().getConfiguration().getDisplayImageOptions(), null, null);
	}

	public static void setImageViewByImagesScale(final ImageView imageView, String url)
	{
		setImageViewByImagesScale(imageView, url, ImageLoader.getInstance().getConfiguration().getDisplayImageOptions());
	}

	public static void setImageViewByImagesScale(final ImageView imageView, String url, DisplayImageOptions options)
	{
		setImageView(url, imageView, options, new ImageLoadingListener()
		{
			@Override
			public void onLoadingStarted(String imageUri, View view)
			{
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason)
			{
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
			{
				if (loadedImage != null && imageView != null)
				{
					ViewGroup.LayoutParams params = imageView.getLayoutParams();
					if (params != null)
					{
						int scaleWidth = SDViewUtil.getViewWidth(imageView);
						int scaleHeight = SDViewUtil.getScaleHeight(loadedImage, scaleWidth);
						params.height = scaleHeight;
						imageView.setLayoutParams(params);
						imageView.setImageBitmap(loadedImage);
					}
				}
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view)
			{
			}
		});
	}

	public static void setImageFillScreenWidthByScale(final ImageView imageView, final View wrapView, String url)
	{
		setImageView(url, imageView, new ImageLoadingListener()
		{
			@Override
			public void onLoadingStarted(String imageUri, View view)
			{
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason)
			{
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
			{
				if (loadedImage != null && imageView != null && wrapView != null)
				{
					ViewGroup.LayoutParams params = wrapView.getLayoutParams();
					if (params != null)
					{
						int screenWidth = SDViewUtil.getScreenWidth();
						int scaleHeight = SDViewUtil.getScaleHeight(loadedImage, screenWidth);
						params.height = scaleHeight;
						wrapView.setLayoutParams(params);
						imageView.setImageBitmap(loadedImage);
					}
				}
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view)
			{
			}
		});
	}

	public static void setImageView(String uri, ImageView imageView, ImageLoadingListener listener)
	{
		setImageView(uri, imageView, listener, null);
	}

	public static void setImageView(String uri, ImageView imageView, ImageLoadingListener listener, ImageLoadingProgressListener progressListener)
	{
		setImageView(uri, imageView, ImageLoader.getInstance().getConfiguration().getDisplayImageOptions(), listener, progressListener);
	}

	public static void setImageView(String uri, ImageView imageView, DisplayImageOptions options, ImageLoadingListener listener)
	{
		setImageView(uri, imageView, options, listener, null);
	}

	public static void setImageView(String uri, ImageView imageView, DisplayImageOptions options, ImageLoadingListener listener,
			ImageLoadingProgressListener progressListener)
	{
		if (!canLoadImageFromUrl(uri))
		{
			return;
		}
		try
		{
			ImageLoader.getInstance().displayImage(uri, imageView, options, listener, progressListener);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
