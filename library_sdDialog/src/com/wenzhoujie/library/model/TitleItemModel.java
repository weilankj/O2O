package com.wenzhoujie.library.model;

import com.wenzhoujie.library.title.TitleItem.EnumShowType;
import com.wenzhoujie.library.utils.SDResourcesUtil;

public class TitleItemModel
{
	private int iconLeftResId;
	private int iconRightResId;
	private int textTopResId;
	private int textBotResId;
	private int backgroundTextResId;
	private String textTop;
	private String textBot;
	private EnumShowType showType = EnumShowType.TEXT;

	public int getBackgroundTextResId()
	{
		return backgroundTextResId;
	}

	public TitleItemModel setBackgroundTextResId(int backgroundTextResId)
	{
		this.backgroundTextResId = backgroundTextResId;
		return this;
	}

	public EnumShowType getShowType()
	{
		return showType;
	}

	public TitleItemModel setShowType(EnumShowType showType)
	{
		this.showType = showType;
		return this;
	}

	public int getIconLeftResId()
	{
		return iconLeftResId;
	}

	public TitleItemModel setIconLeftResId(int iconLeftResId)
	{
		this.iconLeftResId = iconLeftResId;
		return this;
	}

	public int getIconRightResId()
	{
		return iconRightResId;
	}

	public TitleItemModel setIconRightResId(int iconRightResId)
	{
		this.iconRightResId = iconRightResId;
		return this;
	}

	public int getTextTopResId()
	{
		return textTopResId;
	}

	public TitleItemModel setTextTopResId(int textTopResId)
	{
		this.textTopResId = textTopResId;
		setTextTop(SDResourcesUtil.getString(textTopResId));
		return this;
	}

	public int getTextBotResId()
	{
		return textBotResId;
	}

	public TitleItemModel setTextBotResId(int textBotResId)
	{
		this.textBotResId = textBotResId;
		setTextTop(SDResourcesUtil.getString(textBotResId));
		return this;
	}

	public String getTextTop()
	{
		return textTop;
	}

	public TitleItemModel setTextTop(String textTop)
	{
		this.textTop = textTop;
		return this;
	}

	public String getTextBot()
	{
		return textBot;
	}

	public TitleItemModel setTextBot(String textBot)
	{
		this.textBot = textBot;
		return this;
	}

}
