package com.wenzhoujie.library.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.customview.SDLvCategoryViewHelper.SDLvCategoryViewHelperAdapterInterface;
import com.wenzhoujie.library.customview.SDLvCategoryViewHelper.SDLvCategoryViewHelperListener;

public class SDLvCategoryView extends SDViewBase
{

	private SDLvCategoryViewHelper mHelper;

	public ImageView mIvLeft = null;
	public ImageView mIvRight = null;
	public TextView mTvTitle = null;

	private SDLvCategoryViewHelperListener mListener = null;

	// -----------------get set

	public SDLvCategoryViewHelperListener getmListener()
	{
		return mListener;
	}

	public void setmListener(SDLvCategoryViewHelperListener mListener)
	{
		this.mListener = mListener;
	}

	public void setTitle(String title)
	{
		if (title != null)
		{
			this.mTvTitle.setText(title);
		}
	}

	public SDLvCategoryView(Context context)
	{
		this(context, null);
	}

	public SDLvCategoryView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.view_category_tab, this, true);
		mTvTitle = (TextView) this.findViewById(R.id.view_category_tab_tv_title);
		mIvLeft = (ImageView) this.findViewById(R.id.view_category_tab_iv_left);
		mIvRight = (ImageView) this.findViewById(R.id.view_category_tab_iv_right);

		View popView = LayoutInflater.from(getContext()).inflate(R.layout.pop_category_single_lv, null);
		ListView listView = (ListView) popView.findViewById(R.id.pop_category_single_lv_lv);

		mHelper = new SDLvCategoryViewHelper(popView, listView);
		mHelper.setmListener(new SDLvCategoryViewHelperListener()
		{

			@Override
			public void onTitleChange(String title)
			{
				mTvTitle.setText(title);
				if (mListener != null)
				{
					mListener.onTitleChange(title);
				}
			}

			@Override
			public void onPopwindowDismiss(String title)
			{
				if (mListener != null)
				{
					mListener.onPopwindowDismiss(title);
				}
			}

			@Override
			public void onItemSelect(int index, Object model)
			{
				if (mListener != null)
				{
					mListener.onItemSelect(index, model);
				}
			}

			@Override
			public void onPopwindowShow(PopupWindow popwindow)
			{
				popwindow.showAsDropDown(SDLvCategoryView.this, 0, 0);
				if (mListener != null)
				{
					mListener.onPopwindowShow(popwindow);
				}
			}
		});
	}

	public void setAdapter(SDLvCategoryViewHelperAdapterInterface adapter)
	{
		mHelper.setAdapter(adapter);
	}

	@Override
	public void onNormal()
	{
		onNormalViewBackground(this);
		onNormalTextColor(mTvTitle);
		mHelper.dismissPopwindow();
		super.onNormal();
	}

	@Override
	public void onSelected()
	{
		onSelectedViewBackground(this);
		onSelectedTextColor(mTvTitle);
		mHelper.showPopwindow();
		super.onSelected();
	}

}
