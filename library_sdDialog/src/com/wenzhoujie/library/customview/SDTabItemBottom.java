package com.wenzhoujie.library.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.R;

/**
 * 底部菜单栏
 * 
 * @author js02
 * 
 */
public class SDTabItemBottom extends SDViewBase
{

	public ImageView mIvTitle = null;
	public TextView mTvTitle = null;
	public TextView mTvNumbr = null;

	// ----------------get set

	public void setTitleText(String title)
	{
		mTvTitle.setText(title);
	}

	public SDTabItemBottom(Context context)
	{
		super(context);
		init();
	}

	public SDTabItemBottom(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.view_tab_item_bottom, this, true);
		mIvTitle = (ImageView) findViewById(R.id.view_tab_item_iv_title);
		mTvTitle = (TextView) findViewById(R.id.view_tab_item_tv_title);
		mTvNumbr = (TextView) findViewById(R.id.view_tab_item_tv_number);
		mTvNumbr.setVisibility(View.GONE);
		onNormal();
	}

	public void setTitleNumber(String number)
	{
		if (number != null && number.length() > 0)
		{
			mTvNumbr.setVisibility(View.VISIBLE);
			mTvNumbr.setText(number);
		} else
		{
			mTvNumbr.setVisibility(View.GONE);
		}
	}

	public void setTextSizeTitleSp(int sizeSp)
	{
		setTextSizeSp(mTvTitle, sizeSp);
	}

	public void setTextSizeNumberSp(int sizeSp)
	{
		setTextSizeSp(mTvNumbr, sizeSp);
	}

	// ----------------------states

	@Override
	public void onNormal()
	{
		onNormalImageView(mIvTitle);
		onNormalTextColor(mTvTitle);
		super.onNormal();
	}

	@Override
	public void onSelected()
	{
		onSelectedImageView(mIvTitle);
		onSelectedTextColor(mTvTitle);
		super.onSelected();
	}

}
