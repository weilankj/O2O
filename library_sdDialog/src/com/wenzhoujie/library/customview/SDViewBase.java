package com.wenzhoujie.library.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class SDViewBase extends LinearLayout
{
	protected boolean mSelected;

	protected SDViewAttr mAttr;

	public SDViewBase(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		baseInit();
	}

	private void baseInit()
	{
		mAttr = new SDViewAttr();
	}

	public SDViewBase(Context context)
	{
		this(context, null);
	}

	public void onNormal()
	{
		setmSelected(false);
	}

	public void onSelected()
	{
		setmSelected(true);
	}

	public void onFocus()
	{

	}

	public void onPressed()
	{

	}

	// ------------------utils method

	public DisplayMetrics getDisplayMetrics()
	{
		return getContext().getResources().getDisplayMetrics();
	}

	public int getScreenWidth()
	{
		DisplayMetrics metrics = getDisplayMetrics();
		return metrics.widthPixels;
	}

	public int getScreenHeight()
	{
		DisplayMetrics metrics = getDisplayMetrics();
		return metrics.heightPixels;
	}

	public LinearLayout.LayoutParams getLayoutParamsMM()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
	}

	public LinearLayout.LayoutParams getLayoutParamsWW()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	}

	public LinearLayout.LayoutParams getLayoutParamsWM()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
	}

	public LinearLayout.LayoutParams getLayoutParamsMW()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	}

	// -----------------base method

	public void setTextSizeSp(TextView textView, float sizeSp)
	{
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeSp);
	}

	public void onTextColor(EnumViewState state, TextView... textViews)
	{
		if (textViews != null && textViews.length > 0)
		{
			for (TextView textView : textViews)
			{
				switch (state)
				{
				case NORMAL:
					setTextColorNormal(textView);
					break;
				case SELECTED:
					setTextColorSelected(textView);
					break;
				case PRESSED:
					setTextColorPressed(textView);
					break;

				default:
					break;
				}
			}
		}
	}

	public void onNormalTextColor(TextView... textViews)
	{
		onTextColor(EnumViewState.NORMAL, textViews);
	}

	public void onSelectedTextColor(TextView... textViews)
	{
		onTextColor(EnumViewState.SELECTED, textViews);
	}

	public void onPressedTextColor(TextView... textViews)
	{
		onTextColor(EnumViewState.PRESSED, textViews);
	}

	public void onViewBackground(EnumViewState state, View... views)
	{
		if (views != null && views.length > 0)
		{
			for (View view : views)
			{
				switch (state)
				{
				case NORMAL:
					setViewBackgroundNormal(view);
					break;
				case SELECTED:
					setViewBackgroundSelected(view);
					break;
				case PRESSED:
					setViewBackgroundPressed(view);
					break;

				default:
					break;
				}
			}
		}
	}

	public void onNormalViewBackground(View... views)
	{
		onViewBackground(EnumViewState.NORMAL, views);
	}

	public void onSelectedViewBackground(View... views)
	{
		onViewBackground(EnumViewState.SELECTED, views);
	}

	public void onPressedViewBackground(View... views)
	{
		onViewBackground(EnumViewState.PRESSED, views);
	}

	public void onImageView(EnumViewState state, ImageView... imageViews)
	{
		if (imageViews != null && imageViews.length > 0)
		{
			for (ImageView imageView : imageViews)
			{
				switch (state)
				{
				case NORMAL:
					setImageViewNormal(imageView);
					break;
				case SELECTED:
					setImageViewSelected(imageView);
					break;
				case PRESSED:
					setImageViewPressed(imageView);
					break;

				default:
					break;
				}
			}
		}
	}

	public void onNormalImageView(ImageView... imageViews)
	{
		onImageView(EnumViewState.NORMAL, imageViews);
	}

	public void onSelectedImageView(ImageView... imageViews)
	{
		onImageView(EnumViewState.SELECTED, imageViews);
	}

	public void onPressedImageView(ImageView... imageViews)
	{
		onImageView(EnumViewState.PRESSED, imageViews);
	}

	// -------------------------imageview
	public void setImageViewResId(ImageView iv, int resId)
	{
		if (resId != 0)
		{
			iv.setImageResource(resId);
		}
	}

	public void setImageViewNormal(ImageView iv)
	{
		setImageViewResId(iv, mAttr.getmImageNormalResId());
	}

	public void setImageViewSelected(ImageView iv)
	{
		setImageViewResId(iv, mAttr.getmImageSelectedResId());
	}

	public void setImageViewPressed(ImageView iv)
	{
		setImageViewResId(iv, mAttr.getmImagePressedResId());
	}

	// -------------------------textColor

	public void setTextColor(TextView tv, int color)
	{
		if (color != 0)
		{
			tv.setTextColor(color);
		}
	}

	public void setTextColorNormal(TextView tv)
	{
		setTextColor(tv, mAttr.getmTextColorNormal());
	}

	public void setTextColorSelected(TextView tv)
	{
		setTextColor(tv, mAttr.getmTextColorSelected());
	}

	public void setTextColorPressed(TextView tv)
	{
		setTextColor(tv, mAttr.getmTextColorPressed());
	}

	// ----------------------background

	@SuppressLint("NewApi")
	public void setViewBackground(View view, Drawable drawable, int resId)
	{
		if (resId != 0)
		{
			view.setBackgroundResource(resId);
		} else
		{
			if (drawable != null)
			{
				if (android.os.Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN)
				{
					view.setBackground(drawable);
				} else
				{
					view.setBackgroundDrawable(drawable);
				}
			}
		}
	}

	public void setViewBackgroundNormal(View view)
	{
		setViewBackground(view, mAttr.getmBackgroundDrawableNormal(), mAttr.getmBackgroundDrawableNormalResId());
	}

	public void setViewBackgroundSelected(View view)
	{
		setViewBackground(view, mAttr.getmBackgroundDrawableSelected(), mAttr.getmBackgroundDrawableSelectedResId());
	}

	public void setViewBackgroundPressed(View view)
	{
		setViewBackground(view, mAttr.getmBackgroundDrawablePressed(), mAttr.getmBackgroundDrawablePressedResId());
	}

	// ------------------setter getter

	public SDViewAttr getmAttr()
	{
		return mAttr;
	}

	public boolean ismSelected()
	{
		return mSelected;
	}

	public void setmSelected(boolean mSelected)
	{
		this.mSelected = mSelected;
	}

	public void setmAttr(SDViewAttr mAttr)
	{
		this.mAttr = mAttr;
	}

	// --------------------------enum
	public enum EnumViewState
	{
		NORMAL, SELECTED, PRESSED;
	}

	public enum EnumTabPosition
	{
		FIRST, MIDDLE, LAST, SINGLE;
	}

}
