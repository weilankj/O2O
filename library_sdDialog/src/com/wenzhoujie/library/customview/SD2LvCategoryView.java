package com.wenzhoujie.library.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.customview.SD2LvCategoryViewHelper.SD2LvCategoryViewHelperListener;

public class SD2LvCategoryView extends SDViewBase
{

	public ImageView mIvLeft = null;
	public ImageView mIvRight = null;
	public TextView mTvTitle = null;
	public View mPopView;
	public ListView mLvLeft;
	public ListView mLvRight;

	private SD2LvCategoryViewHelper mHelper;

	private SD2LvCategoryViewHelperListener mListener;

	// ----------------get set

	public void setmListener(SD2LvCategoryViewHelperListener mListener)
	{
		this.mListener = mListener;
	}

	public SD2LvCategoryView(Context context)
	{
		this(context, null);
	}

	public SD2LvCategoryView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.view_category_tab, this, true);
		mTvTitle = (TextView) this.findViewById(R.id.view_category_tab_tv_title);
		mIvLeft = (ImageView) this.findViewById(R.id.view_category_tab_iv_left);
		mIvRight = (ImageView) this.findViewById(R.id.view_category_tab_iv_right);

		mPopView = LayoutInflater.from(getContext()).inflate(R.layout.pop_category_two_lv, null);
		mLvLeft = (ListView) mPopView.findViewById(R.id.pop_category_two_lv_lv_left);
		mLvRight = (ListView) mPopView.findViewById(R.id.pop_category_two_lv_lv_right);
		setGravity(Gravity.CENTER_VERTICAL);
		mHelper = new SD2LvCategoryViewHelper(mPopView, mLvLeft, mLvRight);
		mHelper.setmListener(new SD2LvCategoryViewHelperListener()
		{

			@Override
			public void onTitleChange(String title)
			{
				mTvTitle.setText(title);
				if (mListener != null)
				{
					mListener.onTitleChange(title);
				}
			}

			@Override
			public void onRightItemSelect(int leftIndex, int rightIndex, Object leftModel, Object rightModel)
			{
				if (mListener != null)
				{
					mListener.onRightItemSelect(leftIndex, rightIndex, leftModel, rightModel);
				}
			}

			@Override
			public void onPopwindowShow(PopupWindow popwindow)
			{
				popwindow.showAsDropDown(SD2LvCategoryView.this, 0, 0);
				if (mListener != null)
				{
					mListener.onPopwindowShow(popwindow);
				}
			}

			@Override
			public void onPopwindowDismiss(String title)
			{
				if (mListener != null)
				{
					mListener.onPopwindowDismiss(title);
				}
			}

			@Override
			public void onLeftItemSelect(int leftIndex, Object leftModel, boolean isNotifyDirect)
			{
				if (mListener != null)
				{
					mListener.onLeftItemSelect(leftIndex, leftModel, isNotifyDirect);
				}
			}
		});
	}

	@Override
	public void onNormal()
	{
		onNormalViewBackground(this);
		onNormalTextColor(mTvTitle);
		mHelper.disMissPopWindow();
		super.onNormal();
	}

	@Override
	public void onSelected()
	{
		onSelectedViewBackground(this);
		onSelectedTextColor(mTvTitle);
		mHelper.showPopWindow();
		super.onSelected();
	}

}
