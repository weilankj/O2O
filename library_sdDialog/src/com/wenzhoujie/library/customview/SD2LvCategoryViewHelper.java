package com.wenzhoujie.library.customview;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;

import com.wenzhoujie.library.customview.SDLvCategoryViewHelper.SDLvCategoryViewHelperAdapterInterface;
import com.wenzhoujie.library.popupwindow.SDPWindowBase;

public class SD2LvCategoryViewHelper
{

	private static final int DEFAULT_INDEX = 0;

	private SD2LvCategoryViewHelperAdapterInterface mAdapterHelperLeft;
	private SD2LvCategoryViewHelperAdapterInterface mAdapterHelperRight;

	public ListView mLvLeft = null;
	public ListView mLvRight = null;

	public View mPopView = null;

	private int mCurrentIndexLeft = 0;
	private int mCurrentIndexRight = 0;

	public PopupWindow mPopCategory = null;

	private SD2LvCategoryViewHelperListener mListener = null;

	private int mOldLeftIndex = 0;

	/** 是否点击左边直接消失popwindow */
	private boolean mIsNeedNotifyDirect = true;

	/** 当子分类的数量小于等于这个值的时候点击左边直接消失popwindow */
	private int mNotifyDirectRightCount = 1;

	private String mTitle;

	// ----------------get set

	public String getmTitle()
	{
		return mTitle;
	}

	public void setmTitle(String mTitle)
	{
		this.mTitle = mTitle;
		if (mListener != null)
		{
			mListener.onTitleChange(mTitle);
		}
	}

	public void setmListener(SD2LvCategoryViewHelperListener mListener)
	{
		this.mListener = mListener;
	}

	public SD2LvCategoryViewHelper(View popView, ListView lvLeft, ListView lvRight)
	{
		this.mPopView = popView;
		this.mLvLeft = lvLeft;
		this.mLvRight = lvRight;
		init();
	}

	private void init()
	{
		initPopwindow();
	}

	private void initPopwindow()
	{
		mPopCategory = new SDPWindowBase(false);
		mPopCategory.setContentView(mPopView);
		mPopCategory.setOnDismissListener(new PopDismissListener());
	}

	public int getRightItemCount()
	{
		return mAdapterHelperRight.getAdapter().getCount();
	}

	public int getLeftItemCount()
	{
		return mAdapterHelperLeft.getAdapter().getCount();
	}

	private OnItemClickListener mLeftListViewItemClickListener = new OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			mAdapterHelperLeft.setPositionSelectState(mCurrentIndexLeft, false, false);
			mAdapterHelperLeft.setPositionSelectState((int) id, true, true);
			mCurrentIndexLeft = (int) id;

			Object rightListModel = mAdapterHelperLeft.getRightListModelFromPosition_left((int) id);
			mAdapterHelperRight.updateRightListModel_right(rightListModel);

			// --------------------------是否需要点击左边直接回调
			boolean isNotifyDirect = false;
			if (mIsNeedNotifyDirect)
			{
				int rightCount = getRightItemCount();
				if (rightCount <= mNotifyDirectRightCount) // 点击左边直接消失popWindow
				{
					isNotifyDirect = true;
					String title = mAdapterHelperLeft.getTitleNameFromPosition(mCurrentIndexLeft);
					// ===================让右边第一项直接选中(考虑到有的用户数据右边没有子分类的时候右边只有一项"全部")，如果不需要，删除这段代码==============
					if (getRightItemCount() > 0)
					{
						mAdapterHelperRight.setPositionSelectState(0, true, true);
					}
					// ================================================================================================
					removeOldRightSelect();
					setmTitle(title);
					disMissPopWindow();
				}
			}

			if (mListener != null)
			{
				Object leftModel = mAdapterHelperLeft.getSelectModelFromPosition((int) id);
				mListener.onLeftItemSelect(mCurrentIndexLeft, leftModel, isNotifyDirect);
			}
		}
	};

	private OnItemClickListener mRightListViewItemClickListener = new OnItemClickListener()
	{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			removeOldRightSelect();
			mAdapterHelperRight.setPositionSelectState((int) id, true, true);
			mCurrentIndexRight = (int) id;
			mOldLeftIndex = mCurrentIndexLeft;

			// ================================================================
			String title = null;
			if (mCurrentIndexRight <= (mNotifyDirectRightCount - 1))
			{
				if (mIsNeedNotifyDirect)
				{
					title = mAdapterHelperLeft.getTitleNameFromPosition(mCurrentIndexLeft);
				} else
				{
					title = mAdapterHelperRight.getTitleNameFromPosition(mCurrentIndexRight);
				}
			} else
			{
				title = mAdapterHelperRight.getTitleNameFromPosition(mCurrentIndexRight);
			}
			// ================================================================
			setmTitle(title);
			disMissPopWindow();

			if (mListener != null)
			{
				Object leftModel = mAdapterHelperLeft.getSelectModelFromPosition(mCurrentIndexLeft);
				Object rightModel = mAdapterHelperRight.getSelectModelFromPosition(mCurrentIndexRight);
				mListener.onRightItemSelect(mCurrentIndexLeft, mCurrentIndexRight, leftModel, rightModel);
			}
		}

	};

	private void removeOldRightSelect()
	{
		mAdapterHelperLeft.setPositionSelectState_left(mOldLeftIndex, mCurrentIndexRight, false);
		mAdapterHelperLeft.setPositionSelectState_left(mCurrentIndexLeft, mCurrentIndexRight, false);
	}

	public void setLeftAdapter(SD2LvCategoryViewHelperAdapterInterface adapter)
	{
		this.mAdapterHelperLeft = adapter;
		mLvLeft.setAdapter(mAdapterHelperLeft.getAdapter());
		mLvLeft.setOnItemClickListener(mLeftListViewItemClickListener);
	}

	public void setRightAdapter(SD2LvCategoryViewHelperAdapterInterface adapter)
	{
		this.mAdapterHelperRight = adapter;
		mLvRight.setAdapter(mAdapterHelperRight.getAdapter());
		mLvRight.setOnItemClickListener(mRightListViewItemClickListener);
	}

	public void setAdapterFinish()
	{
		initTitle();
	}

	private void initTitle()
	{
		int titleIndex = mAdapterHelperLeft.getTitleIndex();
		if (titleIndex >= 0)
		{
			mCurrentIndexLeft = titleIndex;
		} else
		{
			mCurrentIndexLeft = DEFAULT_INDEX;
		}

		setmTitle(mAdapterHelperLeft.getTitleNameFromPosition(mCurrentIndexLeft));
		mAdapterHelperLeft.setPositionSelectState(mCurrentIndexLeft, true, true);

		Object rightListModel = mAdapterHelperLeft.getRightListModelFromPosition_left(mCurrentIndexLeft);
		mAdapterHelperRight.updateRightListModel_right(rightListModel);

		if (getRightItemCount() > 0)
		{
			mAdapterHelperRight.setPositionSelectState(0, true, true);
		}
		mLvLeft.setSelection(mCurrentIndexLeft);
	}

	public void showPopWindow()
	{
		if (getLeftItemCount() > 0)
		{
			if (mListener != null)
			{
				mListener.onPopwindowShow(mPopCategory);
			}
		}
	}

	public void disMissPopWindow()
	{
		if (mPopCategory != null)
		{
			mPopCategory.dismiss();
		}
	}

	class PopDismissListener implements OnDismissListener
	{
		@Override
		public void onDismiss()
		{
			if (mListener != null)
			{
				mListener.onPopwindowDismiss(getmTitle());
			}
		}
	}

	public interface SD2LvCategoryViewHelperListener
	{
		public void onRightItemSelect(int leftIndex, int rightIndex, Object leftModel, Object rightModel);

		public void onLeftItemSelect(int leftIndex, Object leftModel, boolean isNotifyDirect);

		public void onPopwindowDismiss(String title);

		public void onTitleChange(String title);

		public void onPopwindowShow(PopupWindow popwindow);
	}

	public interface SD2LvCategoryViewHelperAdapterInterface extends SDLvCategoryViewHelperAdapterInterface
	{
		public Object getRightListModelFromPosition_left(int position);

		public void updateRightListModel_right(Object rightListModel);

		public void setPositionSelectState_left(int positionLeft, int positionRight, boolean select);
	}

}
