package com.wenzhoujie.library.title;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.R;

/**
 * title左边部分，一个imageview和一个textview
 * 
 * @author js02
 * 
 */
public class TitleLeft extends LinearLayout
{
	public View mView;
	public ImageView mIvLeft = null;
	public TextView mTvLeft = null;

	public TitleLeft(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public TitleLeft(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title_left, null);
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1));
		this.setBackgroundColor(getResources().getColor(android.R.color.transparent));

		mIvLeft = (ImageView) findViewById(R.id.iv_image);
		mTvLeft = (TextView) findViewById(R.id.tv_text);

		mIvLeft.setVisibility(View.GONE);
		mTvLeft.setVisibility(View.GONE);
	}

	// ------------------------------left

	public TitleLeft setText(String text)
	{
		if (TextUtils.isEmpty(text))
		{
			mTvLeft.setVisibility(View.GONE);
		} else
		{
			mTvLeft.setVisibility(View.VISIBLE);
			mTvLeft.setText(text);
		}
		return this;
	}

	public TitleLeft setTextBackground(int resId)
	{
		mTvLeft.setBackgroundResource(resId);
		return this;
	}

	public TitleLeft setImage(int resId)
	{
		if (resId == 0)
		{
			mIvLeft.setVisibility(View.GONE);
		} else
		{
			mIvLeft.setVisibility(View.VISIBLE);
			mIvLeft.setImageResource(resId);
		}
		return this;
	}

}
