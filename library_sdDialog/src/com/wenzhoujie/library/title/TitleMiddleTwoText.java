package com.wenzhoujie.library.title;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.R;

public class TitleMiddleTwoText extends LinearLayout
{
	public View mView;
	public TextView mTvTitleTop = null;
	public TextView mTvTitleBot = null;

	public TitleMiddleTwoText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public TitleMiddleTwoText(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title_middle_two_text, null);
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1));

		mTvTitleTop = (TextView) findViewById(R.id.tv_title_top);
		mTvTitleBot = (TextView) findViewById(R.id.tv_title_bot);

		mTvTitleTop.setVisibility(View.GONE);
		mTvTitleBot.setVisibility(View.GONE);
	}

	public TitleMiddleTwoText setTextTop(String text)
	{
		if (TextUtils.isEmpty(text))
		{
			mTvTitleTop.setVisibility(View.GONE);
		} else
		{
			mTvTitleTop.setVisibility(View.VISIBLE);
			mTvTitleTop.setText(text);
		}
		return this;
	}

	public TitleMiddleTwoText setTextBottom(String text)
	{
		if (TextUtils.isEmpty(text))
		{
			mTvTitleBot.setVisibility(View.GONE);
		} else
		{
			mTvTitleBot.setVisibility(View.VISIBLE);
			mTvTitleBot.setText(text);
		}
		return this;
	}

}
