package com.wenzhoujie.library.title;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.model.TitleItemModel;
import com.wenzhoujie.library.utils.SDViewBinder;

public class TitleItem extends LinearLayout
{
	public View mView;
	public ImageView mIvLeft;
	public ImageView mIvRight;
	public TextView mTvTop;
	public TextView mTvBot;

	public LinearLayout mLlText;
	private Drawable mBackgroundDrawable;
	private TitleItemModel mTitleItemModel;

	public TitleItemModel getmTitleItemModel()
	{
		return mTitleItemModel;
	}

	public void setmTitleItemModel(TitleItemModel mTitleItemModel)
	{
		this.mTitleItemModel = mTitleItemModel;
	}

	public TitleItem(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public TitleItem(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		this.setGravity(Gravity.CENTER);
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title_item, null);
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		this.setBackgroundColor(getResources().getColor(android.R.color.transparent));

		mIvLeft = (ImageView) findViewById(R.id.title_item_iv_left);
		mIvRight = (ImageView) findViewById(R.id.title_item_iv_right);

		mLlText = (LinearLayout) findViewById(R.id.title_item_ll_text);
		mTvTop = (TextView) findViewById(R.id.title_item_tv_top);
		mTvBot = (TextView) findViewById(R.id.title_item_tv_bot);

		setAllViewsVisibility(View.GONE);
	}

	public void setAllViewsVisibility(int visibility)
	{
		mIvLeft.setVisibility(visibility);
		mIvRight.setVisibility(visibility);

		mLlText.setVisibility(visibility);
		mTvTop.setVisibility(visibility);
		mTvBot.setVisibility(visibility);
		dealClickListener();
	}

	public TitleItem setTextTop(String text)
	{
		SDViewBinder.setTextViewsVisibility(mTvTop, text);
		dealClickListener();
		return this;
	}

	public TitleItem setTextBot(String text)
	{
		SDViewBinder.setTextViewsVisibility(mTvBot, text);
		dealClickListener();
		return this;
	}

	public TitleItem setBackgroundText(int resId)
	{
		mLlText.setBackgroundResource(resId);
		return this;
	}

	public TitleItem setImageLeft(int resId)
	{
		if (resId == 0)
		{
			mIvLeft.setVisibility(View.GONE);
		} else
		{
			mIvLeft.setVisibility(View.VISIBLE);
			mIvLeft.setImageResource(resId);
		}
		dealClickListener();
		return this;
	}

	public TitleItem setImageRight(int resId)
	{
		if (resId == 0)
		{
			mIvRight.setVisibility(View.GONE);
		} else
		{
			mIvRight.setVisibility(View.VISIBLE);
			mIvRight.setImageResource(resId);
		}
		dealClickListener();
		return this;
	}

	private void dealClickListener()
	{
		if (mIvLeft.getVisibility() == View.VISIBLE || mTvTop.getVisibility() == View.VISIBLE || mTvBot.getVisibility() == View.VISIBLE
				|| mIvRight.getVisibility() == View.VISIBLE)
		{
			if (mTvTop.getVisibility() == View.VISIBLE || mTvBot.getVisibility() == View.VISIBLE)
			{
				mLlText.setVisibility(View.VISIBLE);
			} else
			{
				mLlText.setVisibility(View.GONE);
			}

			SDViewBinder.setBackgroundDrawable(this, mBackgroundDrawable);
		} else
		{
			SDViewBinder.setBackgroundDrawable(this, null);
		}
	}

	public EnumShowType getmShowType()
	{
		return mTitleItemModel.getShowType();
	}
	
	public TitleItem showByType()
	{
		setmShowType(getmShowType());
		return this;
	}

	public TitleItem setmShowType(EnumShowType showType)
	{
		mTitleItemModel.setShowType(showType);
		switch (getmShowType())
		{
		case ICON:
			setImageLeft(mTitleItemModel.getIconLeftResId());
			break;
		case TEXT:
			setTextBot(mTitleItemModel.getTextBot());
			break;
		case ICON_TEXT:
			setImageLeft(mTitleItemModel.getIconLeftResId());
			setTextBot(mTitleItemModel.getTextBot());
			break;
		case TEXT_ICON:
			setImageRight(mTitleItemModel.getIconRightResId());
			setTextBot(mTitleItemModel.getTextBot());
			break;
		case TEXT_WITH_BACKGROUND:
			this.mBackgroundDrawable = null;
			setBackgroundDrawable(null);
			setTextBot(mTitleItemModel.getTextBot());
			setBackgroundText(mTitleItemModel.getBackgroundTextResId());
			break;

		default:
			break;
		}
		return this;
	}

	@Override
	@Deprecated
	public void setBackgroundDrawable(Drawable background)
	{
		if (background != null)
		{
			this.mBackgroundDrawable = background;
		}
		super.setBackgroundDrawable(background);
	}

	public enum EnumShowType
	{
		/** 默认以左边的ImageView显示 */
		ICON,
		/** 根据textBot来显示 */
		TEXT,
		/** 根据textBot来显示,背景根据 */
		TEXT_WITH_BACKGROUND,
		/** 根据textBot来显示,根据iconLeftResId来显示 */
		ICON_TEXT,
		/** 根据textBot来显示,根据iconRightResId来显示 */
		TEXT_ICON;
	}

}
