package com.wenzhoujie.library.title;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.SDLibrary;
import com.wenzhoujie.library.config.SDLibraryConfig;
import com.wenzhoujie.library.drawable.SDDrawable;

public class SDTitleTwoRightButton extends LinearLayout implements OnClickListener
{
	public View mView = null;

	public TitleLeft mTlLeft;
	public TitleMiddleTwoText mTlMiddle;

	public ImageView mIvRight1 = null;
	public ImageView mIvRight2 = null;

	private SDTitleTwoRightButtonListener mListener = null;

	public SDTitleTwoRightButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public SDTitleTwoRightButton(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title_two_right_button, null);
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		mTlLeft = (TitleLeft) mView.findViewById(R.id.tl_title_left);
		mTlMiddle = (TitleMiddleTwoText) mView.findViewById(R.id.tl_title_middle);

		mIvRight1 = (ImageView) mView.findViewById(R.id.iv_title_right1);
		mIvRight2 = (ImageView) mView.findViewById(R.id.iv_title_right2);

		mIvRight1.setVisibility(View.GONE);
		mIvRight2.setVisibility(View.GONE);

		registerClick();
		setDefaultConfig();
	}

	private void setDefaultConfig()
	{
		SDLibraryConfig config = SDLibrary.getInstance().getmConfig();
		setBackgroundColor(config.getmTitleColor());
		setLeftImage(config.getmIconArrowLeftResId());
		setHeightTitle(config.getmTitleHeight());

		SDDrawable none = new SDDrawable();
		none.color(config.getmTitleColor());

		SDDrawable pressed = new SDDrawable();
		pressed.color(config.getmTitleColorPressed());

		mTlLeft.setBackgroundDrawable(SDDrawable.getStateListDrawable(none, null, null, pressed));
		mIvRight1.setBackgroundDrawable(SDDrawable.getStateListDrawable(none, null, null, pressed));
		mIvRight2.setBackgroundDrawable(SDDrawable.getStateListDrawable(none, null, null, pressed));
	}

	public void setHeightTitle(int height)
	{
		this.removeAllViews();
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height));
	}

	public SDTitleTwoRightButtonListener getmListener()
	{
		return mListener;
	}

	public void setmListener(SDTitleTwoRightButtonListener mListener)
	{
		this.mListener = mListener;
	}

	private void registerClick()
	{
		mTlLeft.setOnClickListener(this);
		mIvRight1.setOnClickListener(this);
		mIvRight2.setOnClickListener(this);
	}

	public interface SDTitleTwoRightButtonListener
	{
		public void onLeftButtonCLick_SDTitleTwoRightButton(View v);

		public void onRightButton1CLick_SDTitleTwoRightButton(View v);

		public void onRightButton2CLick_SDTitleTwoRightButton(View v);

	}

	// ------------------------------middle

	public SDTitleTwoRightButton setTitleTop(String text)
	{
		mTlMiddle.setTextTop(text);
		return this;
	}

	public SDTitleTwoRightButton setTitleBottom(String text)
	{
		mTlMiddle.setTextBottom(text);
		return this;
	}

	// ------------------------------left

	public SDTitleTwoRightButton setLeftLayoutBackground(int resId)
	{
		mTlLeft.setBackgroundResource(resId);
		return this;
	}

	public SDTitleTwoRightButton setLeftText(String text)
	{
		mTlLeft.setText(text);
		return this;
	}

	public SDTitleTwoRightButton setLeftTextBackground(int resId)
	{
		mTlLeft.setTextBackground(resId);
		return this;
	}

	public SDTitleTwoRightButton setLeftImage(int resId)
	{
		mTlLeft.setImage(resId);
		return this;
	}

	public SDTitleTwoRightButton setLeftLayoutVisibility(int visibility)
	{
		mTlLeft.setVisibility(visibility);
		return this;
	}

	public SDTitleTwoRightButton setLeftLayoutClick(OnClickListener listener)
	{
		mTlLeft.setOnClickListener(listener);
		return this;
	}

	// -----------------------right

	public SDTitleTwoRightButton setRightImage1(int resId)
	{
		if (resId == 0)
		{
			mIvRight1.setVisibility(View.GONE);
		} else
		{
			mIvRight1.setVisibility(View.VISIBLE);
			mIvRight1.setImageResource(resId);
		}
		return this;
	}

	public SDTitleTwoRightButton setRightImage1Background(int resId)
	{
		mIvRight1.setBackgroundResource(resId);
		return this;
	}

	public SDTitleTwoRightButton setRightImage2(int resId)
	{
		if (resId == 0)
		{
			mIvRight2.setVisibility(View.GONE);
		} else
		{
			mIvRight2.setVisibility(View.VISIBLE);
			mIvRight2.setImageResource(resId);
		}
		return this;
	}

	public SDTitleTwoRightButton setRightImage2Background(int resId)
	{
		mIvRight2.setBackgroundResource(resId);
		return this;
	}

	public SDTitleTwoRightButton setRightImage1Click(OnClickListener listener)
	{
		mIvRight1.setOnClickListener(listener);
		return this;
	}

	public SDTitleTwoRightButton setRightImage2Click(OnClickListener listener)
	{
		mIvRight2.setOnClickListener(listener);
		return this;
	}

	@Override
	public void onClick(View v)
	{
		if (v == mTlLeft)
		{
			if (mListener != null)
			{
				mListener.onLeftButtonCLick_SDTitleTwoRightButton(v);
			}
		} else if (v == mIvRight1)
		{
			if (mListener != null)
			{
				mListener.onRightButton1CLick_SDTitleTwoRightButton(v);
			}
		} else if (v == mIvRight2)
		{
			if (mListener != null)
			{
				mListener.onRightButton2CLick_SDTitleTwoRightButton(v);
			}
		}

	}

}
