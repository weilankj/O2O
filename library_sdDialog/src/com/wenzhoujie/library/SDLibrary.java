package com.wenzhoujie.library;

import android.app.Application;

import com.wenzhoujie.library.config.SDLibraryConfig;

public class SDLibrary
{

	private static SDLibrary mInstance;
	private Application mApplication;

	private SDLibraryConfig mConfig;

	public SDLibraryConfig getmConfig()
	{
		return mConfig;
	}

	public void initConfig(SDLibraryConfig mConfig)
	{
		this.mConfig = mConfig;
	}

	private SDLibrary()
	{
	}

	public Application getApplication()
	{
		return mApplication;
	}

	public void init(Application application)
	{
		this.mApplication = application;
	}

	public static SDLibrary getInstance()
	{
		if (mInstance == null)
		{
			mInstance = new SDLibrary();
		}
		return mInstance;
	}

}
